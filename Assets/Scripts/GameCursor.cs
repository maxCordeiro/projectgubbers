﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using Rewired;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameCursor : MonoBehaviour {

    public Sprite[] cursorSprites;

    public float xPos;
    public float yPos;

    Image cursorImg;

    private void Start()
    {
        Canvas can = transform.parent.GetComponent<Canvas>();
        can.worldCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();

        cursorImg = GetComponent<Image>();
    }

    // Update is called once per frame
    void Update() {        
        if (!SaveSystem.settings_controllerEnabled)
        {
            UpdateMousePos();
        }
    }

    void UpdateMousePos()
    {
        bool ret = SaveSystem.settings_cursorType == 0;
        Cursor.visible = ret;
        cursorImg.enabled = !ret;
        
        Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePos.x + xPos, mousePos.y + yPos);

        cursorImg.sprite = cursorSprites[InterfaceSystem.rePlayer.GetButton("MousePrimary") ? 1 : 0];
    }
}
