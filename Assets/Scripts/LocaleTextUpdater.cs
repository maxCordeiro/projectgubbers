﻿using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocaleTextUpdater : MonoBehaviour
{
    public bool callOnAwake;
    public string translationKey;

    private void Awake()
    {
        if (callOnAwake) UpdateText();
    }

    private void Start()
    {
        if (!callOnAwake) UpdateText();
    }

    void UpdateText()
    {
        GetComponent<SuperTextMesh>().text = SystemVariables.trans[translationKey];
    }
}
