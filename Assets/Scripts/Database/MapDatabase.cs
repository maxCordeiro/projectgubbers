﻿using UnityEngine;
using UnityEditor;
using MonsterSystem;
using System.Collections.Generic;

namespace GameDatabase
{
    public static class MapDatabase
    {
        public static List<MapData> db;/* = new MapData[] {
            new MapData(Maps.VILLAGE),
            new MapData(Maps.PLAYERLOT),
            new MapData(Maps.PLAYERHOME),
            new MapData(Maps.DAYCARE, true),
            new MapData(Maps.GATEHOUSE, true),

            new MapData(Maps.WOODS, false, true, MapTier.C, MapTier.A),//.AvailableMonsters(Monsters.GUBBERS, Monsters.JILO, Monsters.FRESNOSE).BossMonsters(Monsters.WISPYRE).CraftingItems(new List<Items>{ Items.POTION, Items.HIGHPOTION }, new List<int>{ 5, 23 }),
            new MapData(Maps.WOODS, false, true, MapTier.C, MapTier.B),//.AvailableMonsters(Monsters.GUBBERS, Monsters.JILO, Monsters.FRESNOSE).BossMonsters(Monsters.WISPYRE),
            new MapData(Maps.WOODS, false, true, MapTier.C, MapTier.C),//.AvailableMonsters(Monsters.GUBBERS, Monsters.JILO, Monsters.FRESNOSE).BossMonsters(Monsters.WISPYRE),
            new MapData(Maps.WOODS, false, true, MapTier.C, MapTier.D),//.AvailableMonsters(Monsters.GUBBERS, Monsters.JILO, Monsters.FRESNOSE).BossMonsters(Monsters.WISPYRE),
            new MapData(Maps.WOODS, false, true, MapTier.C, MapTier.E),//.AvailableMonsters(Monsters.GUBBERS, Monsters.JILO, Monsters.FRESNOSE).BossMonsters(Monsters.WISPYRE),


            new MapData(Maps.GRASSLANDS, false, true, MapTier.E, MapTier.A)
            .AvailableMonsters(
                new MonsterSpawnInfo(Monsters.FRESNOSE, 0.1f))
            .BossMonsters(
                new MonsterSpawnInfo(Monsters.ARMORBEAR, 1)),

            new MapData(Maps.GRASSLANDS, false, true, MapTier.E, MapTier.B)
            .AvailableMonsters(
                new MonsterSpawnInfo(Monsters.FRESNOSE, 0.1f))
            .BossMonsters(
                new MonsterSpawnInfo(Monsters.ARMORBEAR, 1)),

            new MapData(Maps.GRASSLANDS, false, true, MapTier.E, MapTier.C)
            .AvailableMonsters(
                new MonsterSpawnInfo(Monsters.FRESNOSE, 0.1f))
            .BossMonsters(
                new MonsterSpawnInfo(Monsters.ARMORBEAR, 1)),

            new MapData(Maps.GRASSLANDS, false, true, MapTier.E, MapTier.D)
            .AvailableMonsters(
                new MonsterSpawnInfo(Monsters.FRESNOSE, 0.1f))
            .BossMonsters(
                new MonsterSpawnInfo(Monsters.ARMORBEAR, 1)),

            new MapData(Maps.GRASSLANDS, false, true, MapTier.E, MapTier.E)
            .AvailableMonsters(
                new MonsterSpawnInfo(Monsters.FRESNOSE, 0.2f),
                new MonsterSpawnInfo(Monsters.ELDARE, 0.2f),
                new MonsterSpawnInfo(Monsters.PIKWI, 0.2f),
                new MonsterSpawnInfo(Monsters.COSMIC, 0.2f),
                //new MonsterSpawnInfo(Monsters.PLANTMON, 0.2f),
                //new MonsterSpawnInfo(Monsters.NORMALMON, 0.2f),
                new MonsterSpawnInfo(Monsters.LEAFNINJA, 1f))
            .NightMonsters(
                new MonsterSpawnInfo(Monsters.FROGMONSTER, 0.2f))
            .MorningMons(
                new MonsterSpawnInfo(Monsters.WATERMUSHROOM, 0.2f))
            .BossMonsters(
                new MonsterSpawnInfo(Monsters.ARMORBEAR, 1),
                new MonsterSpawnInfo(Monsters.GUBBERS, 0.35f))
            .AvailableItems(
                new ItemSpawnInfo(Items.POTION, 1f),
                new ItemSpawnInfo(Items.HIGHPOTION, 0.5f))
        };*/
    }
}