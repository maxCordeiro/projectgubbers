﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonsterSystem;

namespace GameDatabase
{
    public static class FamilyDatabase
    {
        public static FamilyBreedingData[] db = new FamilyBreedingData[]
        {
            // AQUATIC
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.FAIRY, MonsterFamily.NATURE, MonsterFamily.FIELD }, new MonsterFamily[]{ MonsterFamily.MACHINE}),

            // AERIAL
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.MYSTERIOUS, MonsterFamily.FIELD }, new MonsterFamily[]{ MonsterFamily.UNDERGROUND, MonsterFamily.NATURE }),

            // FAIRY
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.AQUATIC, MonsterFamily.NATURE, MonsterFamily.FIELD }, new MonsterFamily[]{ MonsterFamily.UNDERGROUND, MonsterFamily.MACHINE, MonsterFamily.DARK }),

            // MYSTERIOUS
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.AERIAL, MonsterFamily.MACHINE, MonsterFamily.DARK }, new MonsterFamily[]{ MonsterFamily.FAIRY, MonsterFamily.FIELD }),

            // UNDERGROUND
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.NATURE, MonsterFamily.BEAST, MonsterFamily.MACHINE, MonsterFamily.DARK }, new MonsterFamily[]{ MonsterFamily.AERIAL, MonsterFamily.FAIRY, MonsterFamily.FIELD }),

            // NATURE
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.AQUATIC, MonsterFamily.FAIRY, MonsterFamily.UNDERGROUND, MonsterFamily.BEAST, MonsterFamily.FIELD }, new MonsterFamily[]{ MonsterFamily.AERIAL, MonsterFamily.MACHINE, MonsterFamily.DARK }),

            // BEAST
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.UNDERGROUND, MonsterFamily.NATURE, MonsterFamily.FIELD }, new MonsterFamily[]{}),

            // MACHINE
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.MYSTERIOUS, MonsterFamily.UNDERGROUND }, new MonsterFamily[]{ MonsterFamily.AQUATIC, MonsterFamily.FAIRY, MonsterFamily.NATURE }),

            // DARK
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.MYSTERIOUS, MonsterFamily.UNDERGROUND, MonsterFamily.DARK }, new MonsterFamily[]{ MonsterFamily.FAIRY, MonsterFamily.NATURE }),

            // FIELD
            new FamilyBreedingData(new MonsterFamily[]{ MonsterFamily.AQUATIC, MonsterFamily.FAIRY, MonsterFamily.NATURE, MonsterFamily.BEAST }, new MonsterFamily[]{ MonsterFamily.MYSTERIOUS, MonsterFamily.UNDERGROUND })
        };
    }
}