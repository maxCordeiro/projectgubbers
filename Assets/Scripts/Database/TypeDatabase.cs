﻿using UnityEngine;
using UnityEditor;
using MonsterSystem;

namespace GameDatabase
{
    public static class TypeDatabase
    {
        public static TypeData[] types = new TypeData[]
        {
            new TypeData(MonsterAttribute.WATER).Weak(MonsterAttribute.PLANT, MonsterAttribute.ELECTRIC, MonsterAttribute.SHADOW).Strong(MonsterAttribute.FIRE, MonsterAttribute.TECH, MonsterAttribute.EARTH).Resist(MonsterAttribute.WATER, MonsterAttribute.FIRE, MonsterAttribute.COSMIC, MonsterAttribute.TECH),
            new TypeData(MonsterAttribute.PLANT).Weak(MonsterAttribute.FIRE, MonsterAttribute.WIND).Strong(MonsterAttribute.WATER, MonsterAttribute.TECH, MonsterAttribute.EARTH).Resist(MonsterAttribute.PLANT),
            new TypeData(MonsterAttribute.FIRE).Weak(MonsterAttribute.WATER, MonsterAttribute.WIND, MonsterAttribute.EARTH).Strong(MonsterAttribute.PLANT, MonsterAttribute.TECH, MonsterAttribute.TECH).Resist(MonsterAttribute.PLANT, MonsterAttribute.FIRE, MonsterAttribute.SHADOW, MonsterAttribute.TECH),
            new TypeData(MonsterAttribute.COSMIC).Weak(MonsterAttribute.COSMIC, MonsterAttribute.ELECTRIC, MonsterAttribute.SHADOW, MonsterAttribute.TECH).Strong(MonsterAttribute.COSMIC, MonsterAttribute.WIND, MonsterAttribute.SHADOW).Resist(MonsterAttribute.EARTH).Immune(MonsterAttribute.WIND, MonsterAttribute.NORMAL),
            new TypeData(MonsterAttribute.ELECTRIC).Weak(MonsterAttribute.EARTH, MonsterAttribute.TECH).Strong(MonsterAttribute.WATER, MonsterAttribute.COSMIC, MonsterAttribute.WIND).Resist(MonsterAttribute.WATER, MonsterAttribute.PLANT, MonsterAttribute.ELECTRIC, MonsterAttribute.WIND),
            new TypeData(MonsterAttribute.WIND).Weak(MonsterAttribute.COSMIC, MonsterAttribute.ELECTRIC).Strong(MonsterAttribute.PLANT, MonsterAttribute.FIRE).Resist(MonsterAttribute.FIRE, MonsterAttribute.WIND),
            new TypeData(MonsterAttribute.SHADOW).Weak(MonsterAttribute.FIRE, MonsterAttribute.COSMIC).Strong(MonsterAttribute.WATER, MonsterAttribute.COSMIC).Immune(MonsterAttribute.SHADOW, MonsterAttribute.NORMAL),
            new TypeData(MonsterAttribute.TECH).Weak(MonsterAttribute.WATER, MonsterAttribute.PLANT, MonsterAttribute.FIRE, MonsterAttribute.EARTH).Strong(MonsterAttribute.COSMIC, MonsterAttribute.ELECTRIC).Resist(MonsterAttribute.COSMIC, MonsterAttribute.ELECTRIC, MonsterAttribute.SHADOW, MonsterAttribute.TECH, MonsterAttribute.NORMAL),
            new TypeData(MonsterAttribute.EARTH).Weak(MonsterAttribute.WATER, MonsterAttribute.PLANT).Strong(MonsterAttribute.FIRE, MonsterAttribute.WIND, MonsterAttribute.TECH).Resist(MonsterAttribute.FIRE, MonsterAttribute.COSMIC, MonsterAttribute.SHADOW, MonsterAttribute.TECH, MonsterAttribute.NORMAL).Immune(MonsterAttribute.ELECTRIC),
            new TypeData(MonsterAttribute.NORMAL).Immune(MonsterAttribute.SHADOW),
        };
    }
}