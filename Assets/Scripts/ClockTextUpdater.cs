﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using System;

public class ClockTextUpdater : MonoBehaviour {

    public Sprite[] sunMoon;

    public SuperTextMesh timeText;
    public SuperTextMesh dayText;
    public SuperTextMesh dateText;
    public Image sunMoonImage;

    TimeSpan curr;

    void Update ()
    {
        curr = TimeSpan.FromHours(SaveSystem.timeOfDay * 24);

        string date = string.Format("{0} {1}, Y{2}", SaveSystem.currentDayInMonth, ((Months)SaveSystem.currentMonth).ToString().Substring(0, 3), SaveSystem.currentYear);
        if (dateText.text != date) dateText.text = date;

        string day = SaveSystem.currentDay.ToString().Substring(0, 3);
        if (dayText.text != day) dayText.text = day;

        int hrs = curr.Hours;
        if (!SaveSystem.settings_twelvehrclock && hrs > 12)
        {
            hrs = curr.Hours - 12;
        }

        string time = string.Format("{0}:{1}", hrs.ToString("00"), (Mathf.Floor(curr.Minutes / 10) * 10).ToString("00"));
        if (timeText.text != time) timeText.text = time;

        switch (SaveSystem.dayNight)
        {
            case DayNight.MORNING:
            case DayNight.DAY:
                if (sunMoonImage.sprite != sunMoon[0]) sunMoonImage.sprite = sunMoon[0];
                break;
            case DayNight.EVENING:
            case DayNight.NIGHT:
                if (sunMoonImage.sprite != sunMoon[1]) sunMoonImage.sprite = sunMoon[1];
                break;
        }
    }
}
