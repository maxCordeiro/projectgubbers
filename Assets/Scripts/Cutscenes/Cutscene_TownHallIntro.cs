﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class Cutscene_TownHallIntro : MonoBehaviour
{
    public OverworldObject mayor;
    
    // Start is called before the first frame update
    void Start()
    {
        InterfaceSystem.isInteracting = true;
        StartCoroutine(TownHallCutscene());
    }

    public IEnumerator TownHallCutscene()
    {
        mayor.Start();
        yield return new WaitForSeconds(1);

        DialogueHandler msg = DialogueHandler.instance;
        ChoiceHandler choice = ChoiceHandler.instance;
        choice.SetPos(SystemVariables.choiceboxDialoguePos);

        msg.AddMessage(".<d=4>.<d=4>. <d=12>.<d=4>.<d=4>. <d=12>.<d=4>.<d=4>.");
        msg.Show();
        yield return new WaitForDialogueReading();

        mayor.gameObject.SetActive(true);
        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.DOWN, 2));

        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.LEFT, 6));

        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.DOWN, 2));

        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.RIGHT, 1));

        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.RIGHT, 2, true, OverworldDir.DOWN));

        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.RIGHT, 2));

        yield return StartCoroutine(mayor.MoveDirection(OverworldDir.DOWN, 2));

        msg.AddMessage("Hello?");
        msg.Show();
        yield return new WaitForDialogueReading();

        msg.AddMessage(string.Format("Ah, {0}! So nice of you to call.", SaveSystem.playerName));
        msg.AddMessage("I take it you're calling to let me know you're ready to move to Town?");
        msg.Show();
        yield return new WaitForDialogueReading();

        yield return new WaitForSeconds(0.5f);

        msg.AddMessage("Wonderful news!");
        msg.AddMessage("I think you're going to fit right in here as a Charmling Tamer.");
        msg.Show();
        yield return new WaitForDialogueReading();

        msg.AddMessage("Say, we're just finishing up on your house.");
        msg.AddMessage("Do you have a preference on your roof color?");
        msg.KeepOpen();
        msg.Show();
        yield return new WaitForDialogueReading();

        choice.AddChoice(0, "Red");
        choice.AddChoice(1, "Blue");
        choice.AddChoice(2, "Green");
        choice.AddChoice(3, "Yellow");
        choice.Show();
        yield return new WaitForChoice();
        yield return new WaitForDialogueReading();

        msg.AddMessage("Excellent choice. We will have the roof done by tomorrow.");
        msg.AddMessage("Have you put any more thought into your starting Charmling?");
        msg.Show();
        yield return new WaitForDialogueReading();

        yield return new WaitForSeconds(0.5f);

        msg.AddMessage(string.Format("Don't worry, {0}. I am sure you have come to a decision by the time you arrive.", SaveSystem.playerName));
        msg.AddMessage("We will be sending a [Sky Charmling] to your apartment complex tomorrow morning at 11 AM to pick you up.");
        msg.Show();
        yield return new WaitForDialogueReading();

        msg.AddMessage("...");
        msg.Show();
        yield return new WaitForDialogueReading();

        msg.AddMessage("Oh, that's right.");
        msg.AddMessage("[Sky Charmling] are one of the many Charmlings that roam [Region] and are commonly used for travel as they soar the skies with ease.");
        msg.Show();
        yield return new WaitForDialogueReading();
        yield break;
    }
}
