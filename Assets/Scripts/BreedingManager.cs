﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class BreedingManager : MonoBehaviour
{
    private static BreedingManager _instance;
    public static BreedingManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(BreedingManager)) as BreedingManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active BreedingManager on the scene");
            }

            return _instance;
        }
    }
    public void Start()
    {
        DayNightManager.instance.onHourPass += EggCheck;
        DayNightManager.instance.onHourPass += IncubatorCheck;
    }

    public void EggCheck()
    {
        for (int i = 0; i < SaveSystem.dcMons.Count; i++)
        {
            if ((SaveSystem.dcMons[i].male == null && SaveSystem.dcMons[i].female == null) || SaveSystem.dcMons[i].hasEgg)
            {
                continue;
            }

            float perc = Random.Range(0f, 1f);
            float compCheck = GameUtility.CompatibilityCheck(i);

            //Debug.Log("Egg Check: comp:" + compCheck + ", perc: " + perc);
            
            if (compCheck <= perc)
            {                
                GenerateEgg(i);
            }
        }
    }

    public void GenerateEgg(int dcIndex)
    {
        MonsterIdentifier mon = GameUtility.Choose(SaveSystem.dcMons[dcIndex].female.mon, SaveSystem.dcMons[dcIndex].male.mon);

        Monster gen = new Monster(mon);

        if (SaveSystem.dcMons[dcIndex].breedingItem.id == Items.FRESHINCENSE)
        {
            //gen.SetPerk(PerkSystem.GetRandomPerk(GameUtility.Choose(SaveSystem.dcMons[dcIndex].female.GetFamily(), SaveSystem.dcMons[dcIndex].male.GetFamily())));
        } else
        {
        }

        gen.SetPerk(GameUtility.Choose(SaveSystem.dcMons[dcIndex].female.perk, SaveSystem.dcMons[dcIndex].male.perk));
        if (gen.mon == SaveSystem.dcMons[dcIndex].female.mon)
        {
            if (Random.Range(0, 1) < 0.2f)
            {
                gen.monColor = SaveSystem.dcMons[dcIndex].male.monColor;
            } else
            {
                gen.monColor = SaveSystem.dcMons[dcIndex].female.monColor;
            }
        } else if (gen.mon == SaveSystem.dcMons[dcIndex].male.mon)
        {
            if (Random.Range(0, 1) < 0.2f)
            {
                gen.monColor = SaveSystem.dcMons[dcIndex].female.monColor;
            }
            else
            {
                gen.monColor = SaveSystem.dcMons[dcIndex].male.monColor;
            }
        }

        bool momExclusive = false, dadExclusive = false;
        Skills momExclusiveSkill = new Skills(), dadExclusiveSkill = new Skills();

        foreach (Skill s in SaveSystem.dcMons[dcIndex].female.monSkills)
        {
            if (SkillSystem.GetSkillDatabaseData(s.id).exclusiveMon == SaveSystem.dcMons[dcIndex].female.mon && Random.Range(0f, 1f) <= 0.4f)
            {
            }
        }

        for (int i = 0; i < 5; i++)
        {
            Skills tempMom, tempDad;

            tempMom = SaveSystem.dcMons[dcIndex].male.monSkills[i].id;
            tempDad = SaveSystem.dcMons[dcIndex].female.monSkills[i].id;

            if (SkillSystem.GetSkillDatabaseData(tempMom).exclusiveMon == SaveSystem.dcMons[dcIndex].female.mon)
            {
                momExclusive = true;
                momExclusiveSkill = tempMom;
            }
            if (SkillSystem.GetSkillDatabaseData(tempDad).exclusiveMon == SaveSystem.dcMons[dcIndex].male.mon)
            {
                dadExclusive = true;
                dadExclusiveSkill = tempDad;
            }
        }

        int momInd = -1, dadInd = -1;

        if (momExclusive && Random.Range(0f, 1f) <= 0.5f)
        {
            momInd = Random.Range(0, 5);
            gen.EquipSkill(momExclusiveSkill, momInd);
            gen.learnedExclusives.Add(momExclusiveSkill);
        }
        
        if (dadExclusive && Random.Range(0f, 1f) <= 0.5f)
        {
            do
            {
                dadInd = Random.Range(0, 5);
            } while (dadInd == momInd);

            gen.EquipSkill(dadExclusiveSkill, dadInd);
            gen.learnedExclusives.Add(dadExclusiveSkill);
        }

        SaveSystem.dcMons[dcIndex].eggsMade++;
        SaveSystem.dcMons[dcIndex].hasEgg = true;
        SaveSystem.dcMons[dcIndex].egg = gen;

        if (SaveSystem.dcMons[dcIndex].breedingItem.GetData().uses != -1)
        {
            SaveSystem.dcMons[dcIndex].breedingItem.currentUses -= 1;
            if (SaveSystem.dcMons[dcIndex].breedingItem.currentUses <= 0)
            {
                SaveSystem.dcMons[dcIndex].breedingItem = null;
            }
        }

        Debug.Log(string.Format("Egg generated! Index: [{0}] Species: [{1}], Perk: [{2}]", dcIndex, mon, gen.perk));
    }

    public void IncubatorCheck()
    {
        for (int i = 0; i <SaveSystem.dcIncubators.Count; i++)
        {
            if (GameUtility.HoursUntil(SaveSystem.dcIncubators[i].hatchDate) <= 0)
            {
                SaveSystem.dcIncubators[i].readyToHatch = true;
            }
        }
    }
}
