﻿using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoiceButtonHover : MonoBehaviour
{
    SuperTextMesh stm;

    private void Start()
    {
        stm = transform.GetChild(0).GetComponent<SuperTextMesh>();
        if (!SaveSystem.settings_controllerEnabled)
        {
            stm.color.a = 128;
            stm.Rebuild(false);
        }       
    }

    public void ChangeTextColor(bool hovering)
    {
        if (SaveSystem.settings_controllerEnabled) return;

        stm.color.a = (byte)(hovering? 255 : 128);
        stm.Rebuild(false);
    }
}
