using System;

namespace GameDatabase
{
	public enum Monsters {
		NONE = 0,
		GUBBERS = 1,
		CREMMY = 2,
		JILO = 3,
		AQUAMI = 4,
		AJ = 5,
		PINRAPTOR = 6,
		FRESNOSE = 7,
		WISPYRE = 8,
		ELDARE = 9,
		COTTONSHEEP = 10,
		BONEHEAD = 11,
		LEAFNINJA = 12,
		SHADOWGUY = 13,
		ROBOTANI = 14,
		ITO = 15,
		SKYMON = 16,
		FRITEBRITE = 17,
		SERVEON = 18,
		POCCO = 19,
		SOMBRUX = 20,
		ARMAGO = 21,
		WATERMUSHROOM = 22,
		SHADOWEEL = 23,
		PIKWI = 24,
		FROGMON = 25,
		COSMIC = 26,
		CHOMPCHEST = 27,
		BUZZBULB = 28,
		DIVAIN = 29,
		PLANTMON = 30,
		GOLEEM = 31,
		CLOWNMON = 32,
		MALAMANDER = 33,
		UCO = 34,
		PSYCHOSNAKE = 35,
		ROCKFIRE = 36,
		CAVEMON = 37,
	}
	public enum Items {
		NONE = 0,
		BOT_SPORTSHORTS = 10000,
		BOT_REDPANTS = 10001,
		BOT_GRAYPANTS = 10002,
		BOT_DENIMJEANS = 10003,
		BOT_CARGO = 10004,
		HAIR_MINIBUNS = 11000,
		HAIR_LONGSTRAIGHT = 11001,
		HAIR_BUZZ = 11002,
		HAIR_FRONTSPIKE = 11003,
		TOP_BROWNPONCHO = 12000,
		TOP_YELLOWBUTTONUP = 12001,
		TOP_YELLOWTANK = 12002,
		TOP_HEARTSHIRT = 12003,
		TOP_BEARSHIRT = 12004,
		TOP_REDWHITEHOODIE = 12005,
		HIGHSCOPE = 1,
		SPECIALDICE = 2,
		PRECISIONGOGGLES = 3,
		POTION = 4,
		POTIONHIGH = 5,
		POTIONULTRA = 6,
		REPEL = 7,
		LUREHEAVY = 8,
		LURELIGHT = 9,
		LUREMAGNETIC = 10,
		LURE = 11,
		XPERKINCENSE = 12,
		YPERKINCENSE = 13,
		FRESHINCENSE = 14,
		XHUEINCENSE = 15,
		YHUEINCENSE = 16,
		XTRAITINCENSE = 17,
		YTRAITINCENSE = 18,
		DAMAGEDSHIELD = 19,
		OVERCLOCKMODULE = 20,
		AMNESIAELIXIR = 21,
		WATERBOOST = 22,
		GLISTENINGPETAL = 23,
		FIREBOOST = 24,
		COSMICBOOST = 25,
		LOOSECABLE = 26,
		WINDBOOST = 27,
		MYSTERIOUSCLOAK = 28,
		POLISHEDGEAR = 29,
		SMOOTHSTONE = 30,
		NORMALBOOST = 31,
		COOLINGLEAF = 32,
		BINDCURE = 33,
		SEALCURE = 34,
		PARALYZECURE = 35,
		CURSECURE = 36,
		DROWSYCURE = 37,
		REVIVE = 38,
		EXPGOLD = 39,
		SKILLCAPSULE = 40,
		LOCKEDCHARM = 41,
		GOODDRINK = 42,
		NEUTRALCHARM = 43,
		SHARPTOOTH = 44,
		WATERCAPVOUCHER = 45,
		PLANTCAPVOUCHER = 46,
		FIRECAPVOUCHER = 47,
		COSMICCAPVOUCHER = 48,
		ELECTRICCAPVOUCHER = 49,
		WINDCAPVOUCHER = 50,
		SHADOWCAPVOUCHER = 51,
		TECHCAPVOUCHER = 52,
		EARTHCAPVOUCHER = 53,
		NORMALCAPVOUCHER = 54,
		COMMONCAPVOUCHER = 55,
		UNCOMMONCAPVOUCHER = 56,
		RARECAPVOUCHER = 57,
		ULTRARARECAPVOUCHER = 58,
		BATTERYPACK = 59,
		GRASSLANDSTIERAITEM = 60,
		GRASSLANDSTIERBITEM = 61,
		GRASSLANDSTIERCITEM = 62,
		GUARDIANSHARDS = 63,
		HOTSNOT = 64,
	}
	public enum Skills {
		NONE = 0,
		MULTIBLAST = 1,
		SPELLANCHOLY = 2,
		MUSICBOX = 3,
		MINDBEND = 4,
		COLLAPSE = 7,
		SKULLSMASH = 8,
		BELLYRUSH = 9,
		PEBBLEPUNCH = 10,
		STONEFLING = 11,
		ABYSS = 12,
		CHARGINGPUNCH = 13,
		BOUNDINGZAP = 14,
		ELECTRICLASSO = 15,
		JOLTSLAM = 16,
		SPICYSNEEZE = 17,
		BURNINGBARRIER = 18,
		OVENHOLD = 19,
		HEATPOD = 20,
		IGNITE = 21,
		SCORCHINGSKY = 22,
		WIGGLESLAP = 23,
		CONFETTICANNON = 24,
		SCISSORFLING = 25,
		SLAM = 26,
		TRIPLEFIST = 27,
		LIFEDRAIN = 28,
		DEFLECT = 29,
		SUPERSPIN = 30,
		SOOTHINGAROMA = 31,
		HEALINGHYMN = 32,
		DASH = 33,
		GREED = 34,
		VINESLASH = 35,
		PETALWHIRL = 36,
		BLOSSOMBLAST = 37,
		SNEAKATTACK = 38,
		BELLYBUFF = 42,
		MAGNETICSHOCK = 45,
		AQUACUT = 47,
		BUBBLEDROP = 48,
		CRASHINGWAVE = 49,
		BEAKJAB = 50,
		AVIANCURSE = 51,
		WINDBURN = 52,
		HYPERTRAIN = 53,
		MEDITATE = 54,
		GROUNDPOUND = 55,
		DAUNT = 56,
		PECK = 57,
		TANGLE = 58,
		REPLENISH = 59,
		FUNGOO = 60,
		MUSHPUFF = 61,
		MYSTICALTOUCH = 62,
		SUBMERGE = 63,
		PINCH = 64,
		DUST = 65,
		TICKLE = 66,
		MAG_DOWN_ONE = 68,
		RES_DOWN_ONE = 69,
		AGI_DOWN_ONE = 70,
		RECOVER = 71,
		ULTRAFIRE = 72,
		LUNARSTRIKE = 73,
		EONDRAIN = 74,
	}
	public enum Perks {
		NONE = 0,
		HYPERSCENT = 1,
		CASHCOW = 2,
		SCOUT = 3,
		SECONDCHANCE = 4,
		EMULATE = 5,
		MYTURN = 6,
		SOOTHINGHEAL = 8,
		MINENOW = 9,
		LUCKYCATCH = 10,
		ZOMBIE = 11,
		GOODEYE = 12,
		HYDRATION = 13,
		CONSUME = 14,
		ENCHANT = 15,
		SCOPE = 16,
		REDHOT = 20,
		FORAGE = 21,
		JUMPSTART = 22,
	}
	public enum Maps {
		NONE = 0,
		OVERGROWNGRASSLANDS = 1,
		SKYMOUNTAINS = 2,
		VILLAGE = 3,
		DAYCARE = 4,
		GATEHOUSE = 5,
		PLAYERLOT = 6,
		PLAYERHOME = 7,
	}
}
