﻿using GameDatabase;
using MonsterSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class TransitionHandler : MonoBehaviour {

    public Shader baseShader;
    Material transMat;

    void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (transMat != null)  Graphics.Blit(src, dst, transMat);
    }

    private void Awake()
    {
        transMat = new Material(baseShader);
        transMat.SetColor("_Color", Color.black);
        transMat.SetTexture("_TransitionTex", Resources.Load<Texture>("Transitions/" + TransitionSystem.tex));

        if (TransitionSystem.midSceneTrans) StartCoroutine(EndT());
    }

    public TransitionHandler SetSpeed(float _speed)
    {
        TransitionSystem.speed = _speed;
        return this;
    }

    public TransitionHandler SetDelay(float _delay)
    {
        TransitionSystem.delay = _delay;
        return this;
    }

    public TransitionHandler LoadScene(Maps _map)
    {
        if (SaveSystem.currentMap == _map)
        {
            TransitionSystem.sameMap = true;
            return this;
        }

        SaveSystem.currentMap = _map;

        TransitionSystem.sceneToLoad = _map.ToString().ToLower();
        return this;
    }

    public TransitionHandler SetMidTransAction(UnityAction _midTrans)
    {
        TransitionSystem.midTrans = _midTrans;
        return this;
    }

    public TransitionHandler SetEndTransAction(UnityAction _endTrans)
    {
        TransitionSystem.endTrans = _endTrans;
        return this;
    }

    public TransitionHandler SetTexture(string _texture)
    {
        TransitionSystem.tex = _texture;
        return this;
    }

    public TransitionHandler Fade()
    {
        TransitionSystem.tex = "fade";
        TransitionSystem.fade = true;
        return this;
    }

    public void ResetProperties()
    {
        transMat.SetFloat("_Fade", 1);
        transMat.SetFloat("_Cutoff", 0);

        TransitionSystem.speed = 0.075f;
        TransitionSystem.delay = 1f;

        TransitionSystem.sceneToLoad = "";
        TransitionSystem.tex = "";

        TransitionSystem.midTrans = null;
        TransitionSystem.endTrans = null;

        TransitionSystem.fade = false;

        TransitionSystem.midSceneTrans = true;
    }

    public void Transition()
    {
        StartCoroutine(T());
    }
        
    public IEnumerator T()
    {
        float incr = 0;

        InterfaceSystem.isFading = true;

        Texture tTex = Resources.Load<Texture>("Transitions/" + TransitionSystem.tex);

        if (tTex == null)
        {
            InterfaceSystem.isFading = false;
            throw new Exception("Transition Texture not found!");
        }

        transMat.SetTexture("_TransitionTex", Resources.Load<Texture>("Transitions/" + TransitionSystem.tex));
        
        string cutFade = TransitionSystem.fade ? "_Fade" : "_Cutoff";

        transMat.SetFloat(TransitionSystem.fade ? "_Cutoff" : "_Fade", 1);
        transMat.SetFloat(cutFade, 0);

        while (transMat.GetFloat(cutFade) < 1f)
        {
            incr += TransitionSystem.speed;
            transMat.SetFloat(cutFade, incr);
            yield return new WaitForEndOfFrame();
        }

        if (TransitionSystem.midTrans != null) TransitionSystem.midTrans.Invoke();

        if (TransitionSystem.sameMap)
        {
            Player p = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
            p.RelocatePlayer();

        } else if (!string.IsNullOrEmpty(TransitionSystem.sceneToLoad))
        {
            TransitionSystem.midSceneTrans = true;
            SceneManager.LoadScene(TransitionSystem.sceneToLoad);
        }

        if (string.IsNullOrEmpty(TransitionSystem.sceneToLoad)) yield return StartCoroutine(EndT());
    }

    public IEnumerator EndT()
    {
        transMat.SetFloat("_Cutoff", 1);
        transMat.SetFloat("_Fade", 1);

        yield return new WaitForSeconds(TransitionSystem.delay);

        float incr = 1;
        string cutFade = TransitionSystem.fade ? "_Fade" : "_Cutoff";

        while (transMat.GetFloat(cutFade) > 0f)
        {
            incr -= TransitionSystem.speed;
            transMat.SetFloat(cutFade, incr);
            yield return new WaitForEndOfFrame();
        }

        transMat.SetFloat("_Cutoff", 0);
        transMat.SetFloat("_Fade", 1);

        if (TransitionSystem.endTrans != null) TransitionSystem.endTrans.Invoke();

        InterfaceSystem.isFading = false;

        ResetProperties();

        yield break;
    }

    private static TransitionHandler _instance;
    public static TransitionHandler instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(TransitionHandler)) as TransitionHandler;
                if (!_instance)
                    Debug.Log("There need to be at least one active TransitionHandler on the scene");
            }

            return _instance;
        }
    }
}
