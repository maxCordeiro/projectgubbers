﻿using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkillEffects : MonoBehaviour
{
    [HideInInspector]
    public BattleNewHandler bnh;

    public SpriteRenderer GetMonImage()
    {
        return bnh.currAttacker == 1 ? bnh.enemyMonsterImage : bnh.playerMonsterImage;
    }

    public SpriteRenderer GetOpposingImage()
    {
        return bnh.currAttacker == 0 ? bnh.enemyMonsterImage : bnh.playerMonsterImage;
    }


    public IEnumerator HIT()
    {
        yield return StartCoroutine(bnh.ProcessDamage());

        yield return StartCoroutine(bnh.Anim());

        yield return StartCoroutine(bnh.HurtMonster());

        StartCoroutine(bnh.AnimFlicker());

        yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));

        yield return StartCoroutine(bnh.Anim_MotionReturn(bnh.currAttacker));

        yield return StartCoroutine(bnh.ContactCheck());

        yield break;
    }

    public IEnumerator HALFHP()
    {
        yield return StartCoroutine(bnh.ProcessDamage());

        yield return StartCoroutine(bnh.AnimMotion());

        yield return StartCoroutine(bnh.PlayAnim());

        bnh.prevHP = bnh.GetOpposing().GetCurrentHP();

        int healAmt = (int)((bnh.currDMG / 2) * (bnh.GetAttacker().perk == GameDatabase.Perks.SOOTHINGHEAL ? 1.2f : 1));

        string msg;

        // PERKCHECK: ZOMBIE
        if (bnh.GetOpposing().perk == GameDatabase.Perks.ZOMBIE)
        {
            bnh.GetAttacker().Hurt(healAmt);

            yield return StartCoroutine(bnh.PlayAnim(bnh.currAttacker));

            StartCoroutine(bnh.AnimFlicker(bnh.currAttacker));

            yield return StartCoroutine(bnh.UpdateHealth(HealthType.SELFHURT));

            msg = string.Format(SystemVariables.trans["battle_perk_zombie"], bnh.GetPrefix(true), bnh.GetOpposing().GetName(), PerkSystem.GetPerkName(bnh.GetOpposing().perk), bnh.GetPrefix(false), bnh.GetAttacker().GetName());

            yield return StartCoroutine(bnh.BattleMessage(msg));
        }
        else
        {
            yield return StartCoroutine(bnh.HurtMonster());

            StartCoroutine(bnh.AnimFlicker());

            yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));

            if (bnh.GetAttacker().Heal(healAmt))
            {
                yield return StartCoroutine(bnh.PlayAnim(bnh.currAttacker, false));

                yield return StartCoroutine(bnh.UpdateHealth(HealthType.HEAL));

                msg = string.Format(SystemVariables.trans["battle_skill_halfhp_drain"], bnh.GetPrefix(false), bnh.GetAttacker().GetName(), bnh.currDMG.ToString(), bnh.GetPrefix(true), bnh.GetOpposing().GetName());

                yield return StartCoroutine(bnh.BattleMessage(msg));
            }
        }

        yield return StartCoroutine(bnh.AnimMotionAfter());

        yield return StartCoroutine(bnh.ContactCheck());

        yield break;
    }

    public IEnumerator MUSICBOX()
    {
        Debug.Log("Music Box");
        bool sleep = Random.Range(0, 0.75f) <= 0.75f && bnh.GetOpposing().SetStatusEffect(StatusEffect.DROWSY);
        yield return StartCoroutine(bnh.BattleMessage(string.Format(SystemVariables.trans["battle_skill_halfhp_drain"], bnh.GetPrefix(false), bnh.GetAttacker().GetName()), 0));

        yield return StartCoroutine(bnh.PlayAnim());

        if (sleep)
        {
            yield return StartCoroutine(bnh.BattleMessage(string.Format(SystemVariables.trans["battle_status_drowsy_on"], bnh.GetPrefix(true), bnh.GetOpposing().GetName())));
        }
        else
        {
            yield return StartCoroutine(bnh.BattleMessage(SystemVariables.trans["battle_skillfail"]));
        }

        bnh.UpdateStatusEffects();
    }

    public IEnumerator BURN()
    {
        yield return StartCoroutine(bnh.ProcessDamage());

        yield return StartCoroutine(bnh.Anim());

        yield return StartCoroutine(bnh.HurtMonster());
        StartCoroutine(bnh.AnimFlicker());
        yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));

        //yield return StartCoroutine(bnh.AnimMotionAfter());

        yield return StartCoroutine(bnh.ContactCheck());

        bool canBurn = bnh.GetOpposing().SetStatusEffect(StatusEffect.BURN);

        if (canBurn)
        {
            yield return StartCoroutine(bnh.BattleMessage(string.Format(SystemVariables.trans["battle_status_burn_on"], bnh.GetPrefix(true), bnh.GetOpposing().GetName())));
        }

        bnh.UpdateStatusEffects();

        yield break;

        //bnh.AddLog(bnh.GetAttacker().GetSkillData(bnh.GetSkillUsed()).id.ToString().ToUpper());
    }

    public IEnumerator STRENGTH_DOWN_TWO()
    {
        bool canLower = bnh.LowerOpposingStat(2, (int)StatNames.STRENGTH);

        if (canLower)
        {
            yield return StartCoroutine(bnh.PlayAnim());

            yield return StartCoroutine(bnh.BattleMessage(string.Format(SystemVariables.trans["battle_stat_raise"], bnh.GetPrefix(true), bnh.GetOpposing().GetName(), SystemVariables.trans["sys_"] + StatNames.STRENGTH.ToString())));

            bnh.UpdateStatChanges();
        }
    }
    
    public IEnumerator ACC_DOWN_TWO()
    {
        bool canLower = bnh.LowerOpposingStat(2, 6);

        if (canLower)
        {
            yield return StartCoroutine(bnh.Anim());

            yield return StartCoroutine(bnh.BattleMessage(string.Format(SystemVariables.trans["battle_stat_lower"], bnh.GetPrefix(true), bnh.GetOpposing().GetName(), SystemVariables.trans["sys_"] + StatNames.STRENGTH.ToString())));

            bnh.UpdateStatChanges();
        }
    }

    public IEnumerator ACC_DOWN_TWO_HIT()
    {
        yield return StartCoroutine(HIT());

        bool canLower = bnh.LowerOpposingStat(2, 6);

        if (canLower)
        {
            yield return StartCoroutine(bnh.BattleMessage(string.Format(SystemVariables.trans["battle_stat_lower"], bnh.GetPrefix(true), bnh.GetOpposing().GetName(), SystemVariables.trans["sys_"] + StatNames.STRENGTH.ToString())));

            bnh.UpdateStatChanges();
        }
    }

    public IEnumerator TRIPLEFIST()
    {
        int dmgAmt = bnh.currDMG / 4;
        /*
        yield return StartCoroutine(bnh.PlayAnimation());

        yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));

        if (bnh.GetOpposing().GetCurrentHP() == 0)
        {
            yield return StartCoroutine(bnh.BattleMessage(string.Format("{0} punched {1} once!", bnh.GetAttacker().GetName(), bnh.GetOpposing().GetName())));
            yield break;
        }

        yield return StartCoroutine(bnh.PlayAnimation());

        bnh.GetOpposing().Hurt(dmgAmt);

        yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));

        if (bnh.GetOpposing().GetCurrentHP() == 0)
        {
            yield return StartCoroutine(bnh.BattleMessage(string.Format("{0} punched {1} two times!", bnh.GetAttacker().GetName(), bnh.GetOpposing().GetName())));
            yield break;
        }

        yield return StartCoroutine(bnh.PlayAnimation());

        bnh.GetOpposing().Hurt(dmgAmt);

        yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));
        
        yield return StartCoroutine(bnh.BattleMessage(string.Format("{0} punched {1} three times!", bnh.GetAttacker().GetName(), bnh.GetOpposing().GetName())));
        */

        yield break;
    }

    public IEnumerator MULTIHIT()
    {
        yield return StartCoroutine(bnh.ProcessDamage());

        int hitAmt = Random.Range(2, 5);
        int dmgAmt = Mathf.Clamp(bnh.currDMG / 3, 1, bnh.currDMG);

        yield return StartCoroutine(bnh.Anim());


        for (int i = 0; i < hitAmt; i++)
        {
            yield return StartCoroutine(bnh.HurtMonster());

            StartCoroutine(bnh.AnimFlicker());

            yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));

            if (bnh.GetOpposing().GetCurrentHP() == 0)
            {
                yield return StartCoroutine(bnh.Anim_MotionReturn(bnh.currAttacker));

                yield return StartCoroutine(bnh.BattleMessage(string.Format("{0} hit {1} {2} times!", bnh.GetAttacker().GetName(), bnh.GetOpposing().GetName(), i + 1)));

                yield return StartCoroutine(bnh.ContactCheck());

                yield break;
            }
        }

        yield return StartCoroutine(bnh.Anim_MotionReturn(bnh.currAttacker));

        yield return StartCoroutine(bnh.ContactCheck());

        yield break;
    }

    public IEnumerator RECOIL()
    {
        //yield return StartCoroutine(bnh.PlayAnimation());

        yield return StartCoroutine(bnh.UpdateHealth(HealthType.HURT));


        yield break;
    }


}
 