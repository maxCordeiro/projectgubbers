﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDatabase;

namespace MonsterSystem
{
    [System.Serializable]
    public class SettingsVariables
    {
        // ----- Gameplay
        public bool rumble;
        public Language lang = Language.EN_US;
        public bool controllerEnabled = true;

        // ----- Graphics
        // Video
        public int resolution;
        public int windowType;

        // UI
        public bool clockEnabled;
        public bool twelvehr;

        public int textSpeed;
        public int textDrawEffect;
        public float uiScale;
        public bool uiAnim = false;
        public int cursorType = 1;

        public bool blurEnabled = false;
        public int blurAmount;

        // ----- Audio
        public float bgm;
        public float sfx;
        public float monCries;
        public bool textSound;
    }
}