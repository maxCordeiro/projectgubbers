﻿using UnityEngine;
using System.Collections.Generic;

namespace MonsterSystem
{
    public static class SystemVariables
    {
        public const int maxPatterns = 4;
        public const int maxItemStack = 999;

        public const float CommonChance = 0.8f;
        public const float UncommonChance = 0.1f;
        public const float RareChance = 0.08f;
        public const float UltraRareChance = 0.02f;

        public const int maxParty = 4;

        public const int defaultDens = 2;
        public const int defaultIncubators = 4;

        public static readonly int[] monthLengths = new int[] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        public static Vector2 choiceboxDialoguePos = new Vector2(350, 96);

        public static string[] statNames = new string[] { "Strength", "Defense", "Magic", "Resistance", "Agility", "Health" };

        public static Dictionary<string, string> skillNames = new Dictionary<string, string>();
        public static Dictionary<string, string> monNames = new Dictionary<string, string>();

        public static Dictionary<string, DictTwoPair> itemText = new Dictionary<string, DictTwoPair>();
        public static Dictionary<string, DictTwoPair> perkText = new Dictionary<string, DictTwoPair>();

        public static Dictionary<string, string> mapNames = new Dictionary<string, string>();
        public static Dictionary<string, string> mapDesc = new Dictionary<string, string>();

        public static Dictionary<string, string> menuStrings = new Dictionary<string, string>();

        public static Translation.Translator trans;
        public static string[] transSelection;

        public static Vector3 tempPos = Vector3.zero;
        public static int tempDir = -1;
    }
}