﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDatabase;

namespace MonsterSystem
{
    [System.Serializable]
    public class PlayerVariables
    {
        public List<Monster> partyMonsters = new List<Monster>();
        public Monster[] inactiveMonsters = new Monster[384];

        public List<Item> inventoryGeneral = new List<Item>();
        public List<Item> inventoryCare = new List<Item>();
        public List<Item> inventoryBattle = new List<Item>();
        public List<Item> inventoryApparel = new List<Item>();
        public List<Item> inventorySkills = new List<Item>();
        public List<Item> inventoryCharms = new List<Item>();

        //public List<Notification> notifications = new List<Notification>();
        //public bool hasUnreadNotifs;

        public List<TierInfo> currentTiers = new List<TierInfo>();
        
        public bool wearingDress;
        public bool coverEars;

        public Items playerHair;
        public Items playerShirt;
        public Items playerPant;

        public string playerName = "Max";
        public string playerID; // Length = 16

        public int gold = 0;

        public Maps activeMap = Maps.VILLAGE;
        public MapTier activeMapTier = MapTier.E;

    }
}