﻿using UnityEngine;
using System.Collections.Generic;
using System;
using GameDatabase;

namespace MonsterSystem
{
    [System.Serializable]
    public class GameVariables
    {
        public string gameVersion = "0.1";

        #region Time
        public int _currentHour = 0;
        public int _currentMinute = 0;
        public int _currentMonth = 0; // Starts at 0 [Jan]
        public int _currentYear = 1; // Stats at 1
        public Days _currentDay; // Current day
        public int _currentDayIndex = 1; // Current int index of days in week
        public int _currentDayInMonth = 1; // Current int index of total days in month
        public DayNight _timeOfDay; // DayNight time of day

        public bool _tick;
        public TimeSpan _curr;
        public TimeSpan _prev;

        public float _currentTimeOfDay = 0.5f;

        #endregion

        #region Gym

        public List<Monster> trainingMons = new List<Monster>();
        public List<Date> trainingWait = new List<Date>();

        #endregion

        #region Daycare

        public List<DayCarePair> daycareMons = new List<DayCarePair>();
        public int denUnlocks;

        public List<Incubator> daycareIncubators = new List<Incubator>();
        public int incubatorUnlocks;

        #endregion

        #region Encyclopedia

        public List<EncUnlockMonster> unlockMonsters = new List<EncUnlockMonster>();
        public List<EncUnlockMap> unlockMaps = new List<EncUnlockMap>();

        #endregion

        public List<Items> shopAInv = new List<Items>();
        public List<Items> shopBInv = new List<Items>();
    }
}