﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using MonsterSystem;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Linq;
using System;

public class DialogueHandler : MonoBehaviour
{
    CanvasGroup cGroup;

    [Header("Components")]
    public SuperTextMesh dialogueMesh;
    public SuperTextMesh nameMesh;

    public Image windowSkin;
    public Image nameSkin;
    public Image windowOverlay;
    public GameObject pauseCursor;
    

    List<string> msgs = new List<string>();

    float waitTime = 0.15f; // Float used for (re)setting the start and end timers
    float startTimer, endTimer; // Timers adding a buffer of tweening and input at start and end of message-
    bool startTimerEnded, endTimerEnded;

    public Action whenCompleted = null; // Action to be invoked when the text is completed and the player presses Primary

    public bool closeAfter = true; // If Hide() is to be called after message is finished, true by default
    public bool isOpen, isFinishedReading, isFading;

    int msgsIndex;
    string leftoverText;

    private static DialogueHandler _instance;
    public static DialogueHandler instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(DialogueHandler)) as DialogueHandler;
                if (!_instance)
                    Debug.Log("There need to be at least one active DialogueHandler on the scene");
            }

            return _instance;
        }
    }

    public void Start()
    {
        cGroup = GetComponent<CanvasGroup>();
        Canvas can = transform.parent.GetComponent<Canvas>();
        can.worldCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();

        EnableAllObjects(false);

        dialogueMesh.speedReadScale = 4f;
    }

    public void EnableAllObjects(bool active)
    {
        foreach (Transform t in transform)
        {
            if (t.name != "PauseCursor") t.gameObject.SetActive(active);
        }
    }

    public DialogueHandler Name(string name)
    {
        nameMesh.text = name;
        return this;
    }

    public DialogueHandler AddMessage(string msg)
    {
        msgs.Add(msg);
        return this;
    }

    public DialogueHandler KeepOpen()
    {
        closeAfter = false;
        return this;
    }

    public DialogueHandler OnFinishedReading(Action onFinishedReading)
    {
        whenCompleted = onFinishedReading;
        return this;
    }

    public void Show()
    {
        isFinishedReading = false;

        EnableAllObjects(true);

        pauseCursor.SetActive(false);
                
        dialogueMesh.text = msgs[0];
        dialogueMesh.Rebuild(false);
        msgsIndex = 0;
        
        nameMesh.transform.parent.gameObject.SetActive(nameMesh.text != "");

        startTimer = waitTime;
        startTimerEnded = false;

        endTimer = waitTime;
        endTimerEnded = false;

        transform.SetAsLastSibling();

        if (!isOpen)
        {
            isOpen = true;

            isFading = true;

            if (SaveSystem.settings_UIAnim)
            {
                LeanTween.scale(gameObject, new Vector3(1, 1, 1), 0.1f);
            }

            LeanTween.alphaCanvas(cGroup, 1, SaveSystem.settings_UIAnim ? 0.1f : 0).setOnComplete(()=>{ isFading = false; }) ;
        }
    }

    public void Hide()
    {
        dialogueMesh.text = "";
        nameMesh.text = "";

        msgs = new List<string>();

        pauseCursor.SetActive(false);

        isFading = true;

        if (SaveSystem.settings_UIAnim)
        {
            LeanTween.scale(gameObject, new Vector3(0.9f, 0.9f, 1), 0.1f);
        }

        LeanTween.alphaCanvas(cGroup, 0, SaveSystem.settings_UIAnim? 0.1f : 0).setOnComplete(() => {
            isOpen = false;
            isFinishedReading = true;

            dialogueMesh.Rebuild(false);
            nameMesh.Rebuild(false);

            isFading = false;

            EnableAllObjects(false);
        });
    }

    public void Update()
    {
        if (!isOpen || isFading) return;

        if (!startTimerEnded)
        {
            startTimer -= Time.deltaTime;
            if (startTimer <= 0)
            {
                dialogueMesh.Read();
                startTimerEnded = true;
            }
            return;
        }

        if (dialogueMesh.reading)
        {
            if (InterfaceSystem.rePlayer.GetButton("A"))
            {
                dialogueMesh.SpeedRead();
            }
            else if (InterfaceSystem.rePlayer.GetButtonDown("B"))
            {
                dialogueMesh.RegularRead();
            }
            else if (InterfaceSystem.rePlayer.GetButtonUp("A"))
            {
                dialogueMesh.SkipToEnd();
            }


        }
        else
        {
            if (!endTimerEnded)
            {
                endTimer -= Time.deltaTime;
                if (endTimer <= 0)
                {
                    endTimerEnded = true;
                }
                return;
            }

            if (!pauseCursor.activeInHierarchy) pauseCursor.SetActive(true);

            if (InterfaceSystem.rePlayer.GetButtonDown("A") && !ChoiceHandler.instance.isOpen && !InterfaceSystem.inQuantity)
            {
                if (dialogueMesh.leftoverText != "")
                {
                    leftoverText = dialogueMesh.leftoverText.TrimStart();
                    dialogueMesh.text = leftoverText;
                    dialogueMesh.Rebuild();
                }
                else
                {
                    if (msgs.ElementAtOrDefault(msgsIndex + 1) != null)
                    {
                        msgsIndex++;
                        dialogueMesh.text = msgs[msgsIndex];
                        dialogueMesh.Rebuild(false);

                        startTimer = waitTime;
                        startTimerEnded = false;

                        endTimer = waitTime;
                        endTimerEnded = false;

                        return;
                    }

                    whenCompleted?.Invoke();

                    if (closeAfter)
                    {
                        Hide();
                    } else
                    {
                        closeAfter = true;
                        isFinishedReading = true;

                        msgs.Clear();
                    }
                }
            }
        }
    }
}