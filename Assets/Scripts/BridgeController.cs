﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeController : MonoBehaviour
{
    public GameObject objectWalls, objectUnderWalls;

    public SpriteRenderer[] horizontalPosts;
    public BoxCollider2D[] walls, underCollision;

    // Start is called before the first frame update
    public void Start()
    {
        walls = objectWalls.GetComponents<BoxCollider2D>();
        underCollision = objectUnderWalls.GetComponents<BoxCollider2D>();
    }
    
    public void OnBridge()
    {
        SortHPosts(true);
        ToggleCollision(walls, true);
        ToggleCollision(underCollision, false);
    }

    public void OffBridge()
    {
        SortHPosts(false);
        ToggleCollision(walls, false);
        ToggleCollision(underCollision, true);
    }

    public void ToggleCollision(BoxCollider2D[] _array, bool _enabled)
    {
        foreach (BoxCollider2D b in _array)
        {
            b.enabled = _enabled;
        }
    }

    public void SortHPosts(bool _enabled)
    {
        foreach (SpriteRenderer s in horizontalPosts)
        {
            s.sortingOrder = _enabled ? 21 : 0;
        }
    }
}
