﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour
{

    public Player target;
    [HideInInspector]
    public float verticalOffset;
    [HideInInspector]
    public float lookAheadDstX;
    public float lookSmoothTimeX;
    [HideInInspector]
    public float verticalSmoothTime;

    public Vector2 focusAreaSize;
    public Bounds levelBounds;

    FocusArea focusArea;

    float currentLookAheadX;
    float targetLookAheadX;
    public float lookAheadDirX;
    public float smoothLookVelocityX, smoothLookVelocityY;
    public float smoothVelocityY;

    float posX;
    float posY;

    float minX;
    float minY;
    float maxX;
    float maxY;

    bool lookAheadStopped;

    public Vector2 oldPos;
    public Vector2 newPos;
    public bool isFollowing = true;

    void Start()
    {
        if (target != null)
        {
            focusArea = new FocusArea(target.GetComponent<BoxCollider2D>().bounds, focusAreaSize);
            transform.position = new Vector3(target.transform.position.x, target.transform.position.y, transform.position.z);
        }

        verticalOffset = -0.5f;
        lookAheadDstX = 1.4f;
        verticalSmoothTime = 0.015f;
        //focusAreaSize = new Vector2(1.6f, 1.5f);

        float cameraHeight = Camera.main.orthographicSize * 2f;
        float cameraWidth = cameraHeight * Camera.main.aspect;

        minX = levelBounds.min.x + (cameraWidth / 2);
        minY = levelBounds.min.y + (cameraHeight / 2);
        maxX = levelBounds.max.x - (cameraWidth / 2);
        maxY = levelBounds.max.y - (cameraHeight / 2);

    }

    void LateUpdate()
    {
        if (target == null) return;

        if (isFollowing)
        {
            focusArea.Update(target.GetComponent<BoxCollider2D>().bounds);

            Vector2 focusPosition = focusArea.centre + Vector2.up * verticalOffset;

            /*if (focusArea.velocity.x != 0)
            {
                lookAheadDirX = Mathf.Sign(focusArea.velocity.x);
                if (Mathf.Sign(target.input.x) == Mathf.Sign(focusArea.velocity.x) && target.input.x != 0)
                {
                    lookAheadStopped = false;
                    targetLookAheadX = lookAheadDirX * lookAheadDstX;
                }
                else
                {
                    if (!lookAheadStopped)
                    {
                        lookAheadStopped = true;
                        targetLookAheadX = currentLookAheadX + (lookAheadDirX * lookAheadDstX - currentLookAheadX) / 4f;
                    }
                }
            }
            */

            //currentLookAheadX = Mathf.SmoothDamp(currentLookAheadX, targetLookAheadX, ref smoothLookVelocityX, lookSmoothTimeX);

            focusPosition.y = Mathf.SmoothDamp(transform.position.y, focusPosition.y, ref smoothLookVelocityX, lookSmoothTimeX);
            focusPosition.x = Mathf.SmoothDamp(transform.position.x, focusPosition.x, ref smoothLookVelocityY, lookSmoothTimeX);
            //focusPosition += Vector2.right * currentLookAheadX;

            posX = Mathf.Clamp(focusPosition.x, minX, maxX);
            posY = Mathf.Clamp(focusPosition.y, minY, maxY);

            oldPos = transform.position;
            transform.position = target.transform.position + new Vector3(0, 0, -10);// new Vector3(posX, posY, -10);
            newPos = transform.position;
        }

    }

    void OnDrawGizmos()
    {
        Gizmos.color = new Color(1, 0, 0, 0.2f);
        Gizmos.DrawCube(focusArea.centre, focusAreaSize);

        Vector3 point_topLeft = new Vector3(levelBounds.min.x, levelBounds.max.y);
        Vector3 point_topRight = new Vector3(levelBounds.max.x, levelBounds.max.y);

        Vector3 point_bottomLeft = new Vector3(levelBounds.min.x, levelBounds.min.y);
        Vector3 point_bottomRight = new Vector3(levelBounds.max.x, levelBounds.min.y);

        Gizmos.DrawLine(point_topLeft, point_topRight);
        Gizmos.DrawLine(point_topLeft, point_bottomLeft);
        Gizmos.DrawLine(point_bottomRight, point_topRight);
        Gizmos.DrawLine(point_bottomRight, point_bottomLeft);

    }

    struct FocusArea
    {
        public Vector2 centre;
        public Vector2 velocity;
        float left, right;
        float top, bottom;


        public FocusArea(Bounds targetBounds, Vector2 size)
        {
            left = targetBounds.center.x - size.x / 2;
            right = targetBounds.center.x + size.x / 2;
            bottom = targetBounds.min.y;
            top = targetBounds.min.y + size.y;

            velocity = Vector2.zero;
            centre = new Vector2((left + right) / 2, (top + bottom) / 2);
        }

        public void Update(Bounds targetBounds)
        {
            float shiftX = 0;
            if (targetBounds.min.x < left)
            {
                shiftX = targetBounds.min.x - left;
            }
            else if (targetBounds.max.x > right)
            {
                shiftX = targetBounds.max.x - right;
            }
            left += shiftX;
            right += shiftX;

            float shiftY = 0;
            if (targetBounds.min.y < bottom)
            {
                shiftY = targetBounds.min.y - bottom;
            }
            else if (targetBounds.max.y > top)
            {
                shiftY = targetBounds.max.y - top;
            }
            top += shiftY;
            bottom += shiftY;
            centre = new Vector2((left + right) / 2, (top + bottom) / 2);
            velocity = new Vector2(shiftX, shiftY);
        }
    }

}
