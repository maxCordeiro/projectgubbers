﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using UnityEngine.SceneManagement;
using System;

public class DayNightManager : MonoBehaviour {

    public float secondsInFullDay = 1200f;

    public float timeMultiplier;
    
    float dayTime = 0.5f;
    float nightTime = 0.8f;
    float morningTime = 0.2f;
    float morningEndTime = 0.25f;
    float eveningTime = 0.75f;
    
    float transitionTime = 0.0025f;
    bool updateLight;

    MapData currMap;

    private static DayNightManager _instance;
    public static DayNightManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(DayNightManager)) as DayNightManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active DayNightManager on the scene");
            }

            return _instance;
        }
    }

    public event Action onMinutePass;
    public event Action onHourPass;
    public event Action onDayPass;
    public event Action onWeekPass;
    public event Action onMonthPass;
    public event Action onYearPass;

    public void MinutePass() => onMinutePass?.Invoke();
    public void HourPass() => onHourPass?.Invoke();
    public void DayPass() => onDayPass?.Invoke();
    public void WeekPass() => onWeekPass?.Invoke();
    public void MonthPass() => onMonthPass?.Invoke();
    public void YearPass() => onYearPass?.Invoke();


    private void Start()
    {
        currMap = MapSystem.GetMapDatabaseData(SaveSystem.currentMap);
        updateLight = currMap.hideLighting;
    }   

    void Update() {
        if (InterfaceSystem.IsNotFree()) return;

        SaveSystem.prevTimeSpan = TimeSpan.FromHours(SaveSystem.timeOfDay * 24);

        SaveSystem.timeOfDay += (Time.deltaTime / secondsInFullDay) * timeMultiplier;

        SaveSystem.currTimeSpan = TimeSpan.FromHours(SaveSystem.timeOfDay * 24);

        SaveSystem.currentHour = SaveSystem.currTimeSpan.Hours;
        SaveSystem.currentMinute = Mathf.FloorToInt(SaveSystem.currTimeSpan.Minutes / 10);

        if (SaveSystem.currentHour != SaveSystem.prevTimeSpan.Hours)
        {
            HourPass();
        }

        if (SaveSystem.currentMinute != Mathf.FloorToInt(SaveSystem.prevTimeSpan.Minutes / 10))
        {
            MinutePass();
        }

        if (SaveSystem.timeOfDay >= 1)
        {
            SaveSystem.timeOfDay = 0;
            SaveSystem.currentDayIndex += 1;
            SaveSystem.currentDayInMonth += 1;
            SaveSystem.currentDay = (Days)SaveSystem.currentDayIndex;
            DayPass();
        }

        if (SaveSystem.currentDayIndex > 6)
        {
            SaveSystem.currentDayIndex = 0;
            SaveSystem.currentDay = 0;
            WeekPass();
        }

        if (SaveSystem.currentDayInMonth > SystemVariables.monthLengths[SaveSystem.currentMonth])
        {
            SaveSystem.currentDayInMonth = 1;
            MonthPass();

            if (SaveSystem.currentMonth > 11)
            {
                SaveSystem.currentYear += 1;
                SaveSystem.currentMonth = 0;
                YearPass();
            } else
            {
                SaveSystem.currentMonth += 1;
            }
        }

        if (SaveSystem.timeOfDay >= morningTime && SaveSystem.timeOfDay <= dayTime)
        {
            SaveSystem.dayNight = DayNight.MORNING;
        } else if (SaveSystem.timeOfDay >= eveningTime && SaveSystem.timeOfDay <= nightTime)
        {
            SaveSystem.dayNight = DayNight.EVENING;
        }
        else if (SaveSystem.timeOfDay > morningEndTime && SaveSystem.timeOfDay < eveningTime)
        {
            SaveSystem.dayNight = DayNight.DAY;
        }
        else if (SaveSystem.timeOfDay > nightTime && SaveSystem.timeOfDay < morningTime)
        {
            SaveSystem.dayNight = DayNight.NIGHT;
        }

        if (!currMap.hideLighting) UpdateLighting();
    }

    void UpdateLighting()
    {
        if ((SaveSystem.dayNight == DayNight.EVENING || SaveSystem.dayNight == DayNight.MORNING) && RenderSettings.ambientLight != new Color(0.9f, 0.65f, 0.35f))
        {
            RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, new Color(0.9f, 0.65f, 0.35f), transitionTime);
        } else if (SaveSystem.dayNight == DayNight.NIGHT && RenderSettings.ambientLight != new Color(0.5f, 0.6f, 0.9f))
        {
            RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, new Color(0.5f, 0.6f, 0.9f), transitionTime);
        } else if (SaveSystem.dayNight == DayNight.DAY && RenderSettings.ambientLight != new Color(1, 1, 1))
        {
            RenderSettings.ambientLight = Color.Lerp(RenderSettings.ambientLight, new Color(1, 1, 1), transitionTime);
        }
    }
}
