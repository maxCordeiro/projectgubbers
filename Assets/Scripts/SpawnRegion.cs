﻿using GameDatabase;
using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRegion : MonoBehaviour
{
    public GameObject monSpawnPrefab;

    Camera cam;
    public bool hasSpawned, canSpawn, timerSet, fromBattle, caveSpawn;
    public float waitTime, currentTime, battleDelayTime;

    public int currentSpawnCount, maxSpawnCount;
    public SpawnArea spawnArea;
    public float width, height;

    public EncounterType encounterType;

    public List<MonsterSpawn> spawnedMonsters;

    CompositeCollider2D collider;

    void Start()
    {
        cam = Camera.main;

        spawnArea = new SpawnArea(transform.position, width, height);

        collider = transform.parent.GetComponent<CompositeCollider2D>();

        spawnedMonsters = new List<MonsterSpawn>();
    }

    public void GenerateAllMons()
    {
        if (encounterType == EncounterType.NONE)
        {
            switch (SaveSystem.dayNight)
            {
                case DayNight.MORNING:
                    encounterType = EncounterType.MORNING;
                    break;
                case DayNight.DAY:
                case DayNight.EVENING:
                    encounterType = EncounterType.DAY;
                    break;
                case DayNight.NIGHT:
                    encounterType = EncounterType.NIGHT;
                    break;
                default:
                    break;
            }
        }

        foreach (GameObject ms in transform)
        {
            GameObject.Destroy(ms);
        }

        spawnedMonsters = new List<MonsterSpawn>();

        for (int i = 0; i < maxSpawnCount; i++)
        {
            currentSpawnCount++;
            MonsterSpawn m = Instantiate(monSpawnPrefab, PointInArea(), Quaternion.identity, transform).GetComponent<MonsterSpawn>();
            m.spawn = this;
            fromBattle = false;
            spawnedMonsters.Add(m);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (InterfaceSystem.IsNotFree()) return;
        
        /*if (currentSpawnCount < maxSpawnCount)
        {
            if (timerSet)
            {
                battleDelayTime -= Time.deltaTime;
                waitTime -= Time.deltaTime;

                if (fromBattle)
                {
                    if (battleDelayTime <= 0)
                    {
                        currentSpawnCount++;
                        MonsterSpawn m = Instantiate(monSpawnPrefab, PointInArea(), Quaternion.identity).GetComponent<MonsterSpawn>();
                        m.spawn = this;
                        fromBattle = false;
                        timerSet = false;
                    }
                }
                else
                {
                    if (waitTime <= 0)
                    {
                        currentSpawnCount++;
                        MonsterSpawn m = Instantiate(monSpawnPrefab, PointInArea(), Quaternion.identity).GetComponent<MonsterSpawn>();
                        m.spawn = this;
                        timerSet = false;
                    }
                }
            }
            else
            {
                battleDelayTime = Random.Range(14f, 20f);
                waitTime = 2f;// Random.Range(hasSpawned? 4f : 1f, hasSpawned? 14f : 4f);
                timerSet = true;
            }
        }
        else
        {
            timerSet = false;
            waitTime = 0;
            currentTime = 0;
        }*/
    }

    public Vector3 GetSpawnPosition()
    {
        float _x, _y, _z = 1;

        _x = Random.Range(spawnArea.maxWidth(), spawnArea.minWidth());
        _y = Random.Range(spawnArea.maxHeight(), spawnArea.minHeigth());

        return new Vector3(_x, _y, _z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = new Color(0.5f, 0.5f, 0.5f, 0.25f);
        Gizmos.DrawCube(transform.position, new Vector2(width, height));
    }

    public Vector3 PointInArea()
    {
        var bounds = collider.bounds;
        var center = bounds.center;

        float x = 0;
        float y = 0;
        int attempt = 0;

        do
        {
            x = Random.Range(bounds.min.x, bounds.max.x);
            y = Random.Range(bounds.min.y, bounds.max.y);
            attempt++;
        } while (!collider.OverlapPoint(new Vector2(x, y)) && attempt <= 100);

        //Debug.Log("Attemps: " + attempt);

        return new Vector3(x, y, 0);
    }

    public struct SpawnArea
    {
        public Vector2 center;
        public float tileWidth, tileHeight;

        public SpawnArea(Vector2 _center, float _width, float _height)
        {
            center = _center;
            tileWidth = _width;
            tileHeight = _height;
        }

        public float maxWidth()
        {
            return center.x + tileWidth / 2;
        }

        public float minWidth()
        {
            return center.x - tileWidth / 2;
        }

        public float maxHeight()
        {
            return center.y + tileHeight / 2;
        }

        public float minHeigth()
        {
            return center.y - tileHeight / 2;
        }
    }
}
