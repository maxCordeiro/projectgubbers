﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;
using System;
using System.Xml.Serialization;

[Serializable]
public class MonsterData {

    [XmlElement("basestats")]
    public BaseStats baseStats = new BaseStats();

    [XmlAttribute("id")]
    public Monsters id;
    [XmlElement("form")]
    public int form;

    [XmlElement("family")]
    public MonsterFamily family;

    [XmlElement("height")]
    public float baseHeight;
    [XmlElement("weight")]
    public int baseWeight;

    [XmlElement("firstAttr")]
    public MonsterAttribute firstType;
    [XmlElement("secAttr")]
    public MonsterAttribute secondType;

    [XmlElement("exp")]
    public ExpType exp;

    [XmlElement("commondrops")]
    public List<Items> commonDrops;
    [XmlElement("uncommondrops")]
    public List<Items> uncommonDrops;
    [XmlElement("raredrops")]
    public List<Items> rareDrops;

    [XmlElement("droprate")]
    public float dropRate;
    [XmlElement("tamerate")]
    public float tameRate;

    /*[XmlElement("hascommon")]
    public bool hasCommon;
    [XmlElement("hasuncommon")]
    public bool hasUncommon;
    [XmlElement("hasrare")]
    public bool hasRare;*/

    [XmlElement("perk")]
    public Perks specialPerk = Perks.NONE;

    public MonsterData(Monsters _id, int _form, BaseStats _baseStats, MonsterFamily _family, float _height, int _weight, ExpType _exp, MonsterAttribute _firstType, MonsterAttribute _secondType = MonsterAttribute.NONE)
    {
        id = _id;
        form = _form;

        baseStats = _baseStats;
        
        family = _family;
        baseHeight = _height;
        baseWeight = _weight;

        firstType = _firstType;
        secondType = _secondType;

        commonDrops = new List<Items>();
        uncommonDrops = new List<Items>();
        rareDrops = new List<Items>();

        /*hasCommon = false;
        hasUncommon = false;
        hasRare = false;*/

        exp = _exp;
    }

    private MonsterData()
    {

    }

    public MonsterData SetFormId(int _id)
    {
        form = _id;
        return this;
    }

    public MonsterData SetCommonDrops(params Items[] _drops)
    {
        commonDrops = new List<Items>(_drops);
        //hasCommon = true;
        return this;
    }

    public MonsterData SetUncommon(params Items[] _drops)
    {
        uncommonDrops = new List<Items>(_drops);
        //hasUncommon = true;
        return this;
    }

    public MonsterData SetRareDrops(params Items[] _drops)
    {
        rareDrops = new List<Items>(_drops);
        //hasRare = true;
        return this;
    }

    public MonsterData SetPerk(Perks _perk)
    {
        specialPerk = _perk;
        return this;
    }
}

[Serializable]
public struct MonsterIdentifier
{
    [XmlAttribute("mon")]
    public Monsters id;
    [XmlElement("form")]
    public int formId;
    [XmlElement("color")]
    public int monColor;

    public MonsterIdentifier(Monsters _id, int _formId, int _monColor = 0)
    {
        id = _id;
        formId = _formId;
        monColor = _monColor;
    }

    public static bool operator == (MonsterIdentifier a, MonsterIdentifier b)
    {
        return (a.id == b.id && a.formId == b.formId && a.monColor == b.monColor);
    }

    public static bool operator != (MonsterIdentifier a, MonsterIdentifier b)
    {
        return (a.id != b.id && a.formId != b.formId && a.monColor != b.monColor);
    }
}