﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;

public class MonsterFollower : MonoBehaviour {

    GameObject childObj;

    SpriteRenderer childSprite;
    SpriteRenderer shadowSprite;

    SpriteAnimation sprAnim;

    ParticleSystem partSys;

    int dir;
    int prDir;
    
    public int offset;

    public Player playerTrail;
    //private Animator _playerAnimator;
    private bool smoothBool;

    // Use this for initialization
    void Start ()
    {
        childObj = transform.GetChild(1).gameObject;

        playerTrail = FindObjectOfType<Player>();
        smoothBool = true;

        sprAnim = childObj.GetComponent<SpriteAnimation>();
        partSys = transform.GetChild(0).GetComponent<ParticleSystem>();

        childSprite = childObj.GetComponent<SpriteRenderer>();
        shadowSprite = childSprite.transform.GetChild(0).GetComponent<SpriteRenderer>();

        if (SaveSystem.playerParty.Count > 0)
        {
            UpdateSpriteAnimation();
        }
	}

    void Update()
    {/*
        if (Vector3.Distance(transform.position, target.position) > minDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
            Debug.DrawLine(transform.position, target.position, Color.red);
        }
        */
        if (Input.GetKeyDown(KeyCode.F))
        {
            UpdateSpriteAnimation();
        }/*

        Vector2 v = target.position - transform.position;
        float a = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;

        dir = (int)((Mathf.Round(a / 90f) + 4) % 4);

        if (prDir != dir && sprAnim.playing)
        {
            prDir = dir;
            sprAnim.PlayByIndex(dir);
        }*/

    }

    private void FixedUpdate()
    {
        if (playerTrail.playerTrail.Count < offset)
        {
            if (playerTrail.playerTrail.Count < offset/2)
            {
                return;
            }

            smoothBool = !smoothBool;
            if (!smoothBool) return;

            Vector3 direction = playerTrail.playerTrail.Peek() - transform.position;
            transform.position = Vector3.Lerp(transform.position, playerTrail.playerTrail.Peek(), 0.2f);

            playerTrail.playerTrail.Dequeue();
        } else
        {
            Vector3 direction = playerTrail.playerTrail.Peek() - transform.position;
            transform.position = Vector3.Lerp(transform.position, playerTrail.playerTrail.Peek(), 0.2f);

            playerTrail.playerTrail.Dequeue();
        }
    }

    void UpdateSpriteAnimation()
    {
        /*string monName = SaveSystem.playerParty[0].GetInternalName();
        Sprite[] monSprite = Resources.LoadAll<Sprite>("Monster_OW/ow_" + monName + "_" + SaveSystem.playerParty[0].monColor);
        
        if (monSprite.Length == 0)
        {
            monSprite = Resources.LoadAll<Sprite>("Monster_OW/ow_debug");
        }

        sprAnim.animations[3].frames = new Sprite[] { monSprite[0], monSprite[1], monSprite[2], monSprite[3] };
        sprAnim.animations[1].frames = new Sprite[] { monSprite[4], monSprite[5], monSprite[6], monSprite[7] };
        sprAnim.animations[2].frames = new Sprite[] { monSprite[8], monSprite[9], monSprite[10], monSprite[11] };
        sprAnim.animations[0].frames = new Sprite[] { monSprite[12], monSprite[13], monSprite[14], monSprite[15] };

        sprAnim.PlayByIndex(0);
        shadowSprite.enabled = true;
        partSys.Emit(4);
        LeanTween.scale(sprAnim.gameObject, new Vector3(0.7f, 1.3f, 1), 0.1f);
        LeanTween.scale(sprAnim.gameObject, new Vector3(1.3f, 0.7f, 1), 0.1f).setDelay(0.1f);
        LeanTween.scale(sprAnim.gameObject, new Vector3(1, 1, 1), 0.1f).setDelay(0.2f);*/
    }
}
