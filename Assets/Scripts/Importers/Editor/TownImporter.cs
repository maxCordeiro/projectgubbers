﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using SuperTiled2Unity;
using SuperTiled2Unity.Editor;
using UnityEngine;
using UnityEditor;
using GameDatabase;
using MonsterSystem;

public class TownImporter : CustomTmxImporter
{
    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
        // Just log the name of the map
        var map = args.ImportedSuperMap;

        var npcs = args.ImportedSuperMap.GetComponentsInChildren<SuperObject>().Where(o => o.m_Type == "NPC");
        var doors = args.ImportedSuperMap.GetComponentsInChildren<SuperObject>().Where(d => d.m_Type == "Door");
        var other = args.ImportedSuperMap.GetComponentsInChildren<SuperObject>().Where(o => o.m_Type == "OBJ");

        foreach (var npc in npcs)
        {
            var tempNPC = Resources.Load("NPCs/NPC " + npc.m_TiledName);
            Debug.Log("NPCs/NPC " + npc.m_TiledName);

            if (tempNPC != null)
            {
                GameObject npcObj = PrefabUtility.InstantiatePrefab(Resources.Load("NPCs/NPC " + npc.m_TiledName) as GameObject) as GameObject; //UnityEngine.Object.Instantiate(tempNPC, npc.transform) as GameObject;
                npcObj.transform.SetParent(npc.transform);
                npcObj.transform.position = npc.transform.position;
                Vector3 offset = new Vector3(0f, -1);
                npcObj.transform.position += offset;

                NPCFenceGate f = npcObj.GetComponent<NPCFenceGate>();

                if (f != null)
                {
                    SuperCustomProperties props = npc.GetComponent<SuperCustomProperties>();
                    
                    CustomProperty dir;
                    int dirint = 0;

                    if (props.TryGetCustomProperty("direction", out dir))
                    {
                        Debug.Log("dir");
                        dirint = dir.GetValueAsInt();
                        f.facingDir = (OverworldDir)dirint;
                    }
                }
            }
        }

        foreach (var door in doors)
        {
            Debug.Log("Instantiate - Door");
            GameObject tempDoor = PrefabUtility.InstantiatePrefab(Resources.Load("Door") as GameObject) as GameObject;

            tempDoor.transform.SetParent(door.transform);
            tempDoor.transform.position = door.transform.position;
            Vector3 offset = new Vector3(0, -1);
            tempDoor.transform.position += offset;

            Sprite[] doorSprites = Resources.LoadAll<Sprite>("DoorSprites/" + door.m_TiledName);

            if (doorSprites.Length > 0)
            {
                tempDoor.GetComponent<Door>().close = doorSprites[0];
                tempDoor.GetComponent<Door>().open = doorSprites[1];
            }
        }

        foreach (var obj in other)
        {
            Debug.Log(obj.m_TiledName);
            switch (obj.m_TiledName)
            {
                case "Exit":
                    GameObject sp = PrefabUtility.InstantiatePrefab(Resources.Load("TransferPoint") as GameObject) as GameObject;
                    if (!sp) break;

                    BoxCollider2D b = sp.GetComponent<BoxCollider2D>();
                    Transfer t = sp.GetComponent<Transfer>();

                    sp.transform.SetParent(obj.transform);
                    sp.transform.position = obj.transform.position;

                    SuperObject o = sp.GetComponentInParent<SuperObject>();

                    float w = o.m_Width;
                    float h = o.m_Height;

                    b.size = new Vector2(w/16f, h/16f);
                    b.offset = new Vector2(b.size.x / 2, -0.5f);

                    break;
                case "SpawnPoint":
                    GameObject spawn = PrefabUtility.InstantiatePrefab(Resources.Load("Spawn Point") as GameObject) as GameObject;
                    if (!spawn) break;

                    spawn.transform.SetParent(obj.transform);
                    spawn.transform.position = obj.transform.position;

                    break;
                case "SpawnRegion":
                    GameObject sr = PrefabUtility.InstantiatePrefab(Resources.Load("Spawn Region") as GameObject) as GameObject;
                    
                    if (!sr)
                    {
                        Debug.Log("No Spawn Region");
                        break;
                    }

                    SpawnRegion srComp = sr.GetComponent<SpawnRegion>();
                    SuperCustomProperties props = obj.GetComponent<SuperCustomProperties>();

                    sr.transform.SetParent(obj.transform);
                    Vector3 newPos = new Vector3(obj.m_Width / 16, -obj.m_Height / 16, 2)/2;
                    sr.transform.position = obj.transform.position + newPos;

                    CustomProperty maxSpawnCount;
                    int maxSpawnCountVal = 0;

                    CustomProperty encounterType;
                    EncounterType encTypeVal = EncounterType.NONE;

                    if (props.TryGetCustomProperty("maxSpawnCount", out maxSpawnCount))
                    {
                        maxSpawnCountVal = maxSpawnCount.GetValueAsInt();
                    }

                    if (props.TryGetCustomProperty("encounterType", out encounterType))
                    {
                        encTypeVal = (EncounterType)encounterType.GetValueAsInt();
                    }

                    srComp.encounterType = encTypeVal;

                    srComp.width = obj.m_Width/16;
                    srComp.height = obj.m_Height/16;
                    srComp.maxSpawnCount = maxSpawnCountVal;

                    break;
                case "BridgeV":
                    GameObject bridgeV = PrefabUtility.InstantiatePrefab(Resources.Load("Bridge_V") as GameObject) as GameObject;
                    if (!bridgeV) break;

                    BridgeController bComp = bridgeV.GetComponent<BridgeController>();

                    bridgeV.transform.SetParent(obj.transform);
                    bridgeV.transform.position = obj.transform.position;

                    BoxCollider2D[] vWallColliders = bridgeV.transform.Find("Walls").GetComponents<BoxCollider2D>();
                    BoxCollider2D[] vUnderColliders = bridgeV.transform.Find("UnderWalls").GetComponents<BoxCollider2D>();
                    BoxCollider2D[] vEnterTriggers =
                    {
                        bridgeV.transform.Find("EnterTrigger_U").GetComponent<BoxCollider2D>(),
                        bridgeV.transform.Find("EnterTrigger_D").GetComponent<BoxCollider2D>(),
                    };
                    /*BoxCollider2D[] vExitTriggers =
                    {
                        bridgeV.transform.Find("ExitTrigger_U").GetComponent<BoxCollider2D>(),
                        bridgeV.transform.Find("ExitTrigger_D").GetComponent<BoxCollider2D>(),
                    };*/

                    //PolygonCollider2D[] vExitTriggers = bridgeV.transform.Find("ExitTrigger").GetComponents<PolygonCollider2D>();

                    SuperCustomProperties bridgeProps = obj.GetComponent<SuperCustomProperties>();

                    CustomProperty bridgeHeight;
                    int bridgeHeightVal = 0;

                    if (bridgeProps.TryGetCustomProperty("height", out bridgeHeight))
                    {
                        bridgeHeightVal = bridgeHeight.GetValueAsInt();
                    }

                    bComp.GetComponent<SpriteRenderer>().size = new Vector2(2, bridgeHeightVal);

                    float halfHeight = bridgeHeightVal * 0.5f;

                    bComp.transform.GetChild(0).localPosition = new Vector3(0, halfHeight);
                    bComp.transform.GetChild(1).localPosition = new Vector3(0, halfHeight);
                    bComp.transform.GetChild(2).localPosition = new Vector3(0, halfHeight);
                    bComp.transform.GetChild(3).localPosition = new Vector3(0, halfHeight);

                    vWallColliders[0].offset = new Vector2(-1.5f, -halfHeight);
                    vWallColliders[0].size = new Vector2(1, bridgeHeightVal - 2);

                    vWallColliders[1].offset = new Vector2(1.5f, -halfHeight);
                    vWallColliders[1].size = new Vector2(1, bridgeHeightVal - 2);

                    vUnderColliders[1].offset = new Vector2(0f, -bridgeHeightVal + 0.5f);
                    vUnderColliders[1].size = new Vector2(2f, 1f);


                    vEnterTriggers[0].transform.localPosition = new Vector2(0, halfHeight - 0.5f);
                    vEnterTriggers[1].transform.localPosition = new Vector2(0, -halfHeight + 0.5f);

                    //vExitTriggers[0].transform.localPosition = new Vector2(0, halfHeight + 0.5f);
                    //vExitTriggers[1].transform.localPosition = new Vector2(0, -halfHeight - 0.5f);

                    break;
                case "BridgeH":
                    GameObject bridgeH = PrefabUtility.InstantiatePrefab(Resources.Load("Bridge_H") as GameObject) as GameObject;
                    if (!bridgeH) break;

                    BridgeController hBridgeComp = bridgeH.GetComponent<BridgeController>();

                    bridgeH.transform.SetParent(obj.transform);
                    bridgeH.transform.position = obj.transform.position;

                    BoxCollider2D[] hWallColliders = bridgeH.transform.Find("Walls").GetComponents<BoxCollider2D>();
                    BoxCollider2D[] hUnderColliders = bridgeH.transform.Find("UnderWalls").GetComponents<BoxCollider2D>();
                    /*BoxCollider2D[] hEnterTriggers = 
                    {
                        bridgeH.transform.Find("EnterTrigger_L").GetComponent<BoxCollider2D>(),
                        bridgeH.transform.Find("EnterTrigger_R").GetComponent<BoxCollider2D>(),
                    };*/
                    BoxCollider2D[] hExitTriggers =
                    {
                        bridgeH.transform.Find("ExitTrigger_L").GetComponent<BoxCollider2D>(),
                        bridgeH.transform.Find("ExitTrigger_R").GetComponent<BoxCollider2D>(),
                    };

                    SpriteRenderer[] hPosts = new SpriteRenderer[]
                    {
                        bridgeH.transform.Find("PostR").GetComponent<SpriteRenderer>(),
                        bridgeH.transform.Find("PostL").GetComponent<SpriteRenderer>(),
                    };

                    SuperCustomProperties hBridgeProps = obj.GetComponent<SuperCustomProperties>();

                    CustomProperty hBridgeWidth;
                    int hBridgeWidthVal = 0;

                    if (hBridgeProps.TryGetCustomProperty("width", out hBridgeWidth))
                    {
                        hBridgeWidthVal = hBridgeWidth.GetValueAsInt();
                    }

                    hBridgeComp.GetComponent<SpriteRenderer>().size = new Vector2(hBridgeWidthVal, 2);

                    float halfWidth = hBridgeWidthVal * 0.5f;

                    hWallColliders[0].offset = new Vector2(0, 1.5f);
                    hWallColliders[0].size = new Vector2(hBridgeWidthVal - 4, 1);

                    hWallColliders[1].offset = new Vector2(0, -1.5f);
                    hWallColliders[1].size = new Vector2(hBridgeWidthVal - 4, 1);

                    //-----

                    hUnderColliders[0].offset = new Vector2(-(halfWidth - 2.5f), 0);
                    hUnderColliders[1].offset = new Vector2(halfWidth - 1.5f, 0);


                    hPosts[0].transform.localPosition = new Vector3(halfWidth - 0.5f, -1);
                    hPosts[1].transform.localPosition = new Vector3(-halfWidth + 1.5f, -1);


                    //hEnterTriggers[0].transform.localPosition = new Vector2(0, -hBridgeWidthVal + 1.5f);
                    //hEnterTriggers[1].transform.localPosition = new Vector2(0, hBridgeWidthVal - 1.5f);

                    hExitTriggers[0].transform.localPosition = new Vector2(-halfWidth + 1.5f, 0);
                    hExitTriggers[1].transform.localPosition = new Vector2(halfWidth - 0.5f, 0);

                    break;
                case "Chest":
                    GameObject chest = PrefabUtility.InstantiatePrefab(Resources.Load("ChestMonster") as GameObject) as GameObject;
                    if (!chest) break;

                    ChestMonster chestMon = chest.GetComponent<ChestMonster>();
                    SuperCustomProperties chestProps = obj.GetComponent<SuperCustomProperties>();

                    CustomProperty spawnChance;
                    float spawnChanceVal = 0;

                    if (chestProps.TryGetCustomProperty("spawnChance", out spawnChance))
                    {
                        spawnChanceVal = spawnChance.GetValueAsFloat();
                    }

                    chestMon.spawnChance = spawnChanceVal;
                    chest.transform.SetParent(obj.transform);
                    chest.transform.position = obj.transform.position;
                    break;
                case "Boss":
                    GameObject bossObj = PrefabUtility.InstantiatePrefab(Resources.Load("Boss") as GameObject) as GameObject;
                    NPCBossMon bossMon = bossObj.GetComponent<NPCBossMon>();
                    SuperCustomProperties bossProps = obj.GetComponent<SuperCustomProperties>();
                                        
                    break;
            }
        }
    }
}

/*
// Use DisplayNameAttribute to control how class appears in the list
[DisplayName("My Other Importer")]
public class MyOtherTmxImporter : CustomTmxImporter
{
    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
        // Just log the number of layers in our tiled map
        var map = args.ImportedSuperMap;
        var layers = map.GetComponentsInChildren<SuperLayer>();
        Debug.LogFormat("Map '{0}' has {1} layers.", map.name, layers.Length);
    }
}

[AutoCustomTmxImporter()]
public class MyOrderedTmxImporter : CustomTmxImporter
{
    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
        Debug.Log("MyOrderedTmxImporter importer");
    }
}

[AutoCustomTmxImporter(1)]
public class MyOrderedTmxImporter1 : CustomTmxImporter
{
    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
        Debug.Log("MyOrderedTmxImporter1 importer");
    }
}

[AutoCustomTmxImporter(2)]
public class MyOrderedTmxImporter2 : CustomTmxImporter
{
    public override void TmxAssetImported(TmxAssetImportedArgs args)
    {
        Debug.Log("MyOrderedTmxImporter2 importer");
    }
}
*/