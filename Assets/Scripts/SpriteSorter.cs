﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteSorter : MonoBehaviour {

    public bool disableSorting;
    SpriteRenderer spriteRender;
    public int addition;

    private void Start()
    {
        spriteRender = GetComponent<SpriteRenderer>();

    }

    void LateUpdate()
    {
        if (!disableSorting)
        {
            spriteRender.sortingOrder = ((int)Camera.main.WorldToScreenPoint(spriteRender.bounds.min).y * -1) + addition;
        }

        switch (SaveSystem.dayNight)
        {
            case MonsterSystem.DayNight.MORNING:
                if (spriteRender.color != new Color(0.98f, 0.86f, 0.51f, spriteRender.color.a) && !LeanTween.isTweening(gameObject))
                {
                    LeanTween.color(gameObject, new Color(0.98f, 0.86f, 0.51f, spriteRender.color.a), 0.5f);
                }
                break;
            case MonsterSystem.DayNight.DAY:
                if (spriteRender.color != new Color(1,1,1, spriteRender.color.a) && !LeanTween.isTweening(gameObject))
                {
                    LeanTween.color(gameObject, new Color(1, 1, 1, spriteRender.color.a), 0.5f);
                }
                break;
            case MonsterSystem.DayNight.EVENING:
                if (spriteRender.color != new Color(0.98f, 0.86f, 0.51f, spriteRender.color.a) && !LeanTween.isTweening(gameObject))
                {
                    LeanTween.color(gameObject, new Color(0.98f, 0.86f, 0.51f, spriteRender.color.a), 0.5f);
                }
                break;
            case MonsterSystem.DayNight.NIGHT:
                if (spriteRender.color != new Color(0.65f, 0.75f, 0.97f, spriteRender.color.a) && !LeanTween.isTweening(gameObject))
                {
                    LeanTween.color(gameObject, new Color(0.65f, 0.75f, 0.97f, spriteRender.color.a), 0.5f);
                }
                break;
        }
    }
}