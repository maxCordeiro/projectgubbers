﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;
using UnityEngine.Events;
using System.Reflection;
using System;
using Random = UnityEngine.Random;

public enum BattleState
{
    BATTLESTARTWAIT,
    SELECTACTION,

    ACTIVETURN,
    ACTIVEANIM,
    ACTIVEHEALTHUPDATE,

    OPPONENTTURN,
    OPPONENTANIM,
    OPPONENTHEALTHUPDATE,

    AFTEREFFECT,

    ENDTURN,

    ENDBATTLE,
    NONE
}

public enum ActionSelected
{
    FIGHT,
    ITEMS,
    SWITCH,
    ESCAPE,
    NONE
}

public class BattleHandler : MonoBehaviour
{
    BattleLogic battleLogic;

    GameObject container;

    [Header("Monster Sprites")]
    public Image activeMonsterImage;
    public Image opponentImage;

    [Header("Health")]
    public Image activeMonHealthImage;
    public Image activeMonHealthBG;

    public Sprite greenHealth;
    public Sprite redHealth;

    public SuperTextMesh activeNameText;
    public SuperTextMesh activeLevelText;
    public SuperTextMesh activeHealthText;

    [Space(15)]
    public Image opponentHealthImage;
    public Image opponenntHealthBG;
    public SuperTextMesh opponentNameText;
    public SuperTextMesh opponentLevelText;
    public SuperTextMesh opponentHealthText;

    [Space(10)]
    public Image activeStatusEffect;
    public Image opponentStatusEffect;

    public Sprite[] statusEffectImages;

    [Header("Action Buttons")]
    public GameObject actionContainer;
    public Button[] actionButtons;
    public Button backButton;

    [Header("Skill Buttons")]
    public GameObject skillsContainer;
    public Button[] skillsButtons;
    public SuperTextMesh[] skillsNames;
    public SuperTextMesh[] skillsCooldown;

    [Header("Text")]
    public SuperTextMesh messageText;

    [Space(15)]
    public SuperTextMesh[] activeStatLevels;
    public SuperTextMesh[] opponenetStatLevels;

    [Header("Logic")]
    public BattleState battleState = BattleState.SELECTACTION;
    public ActionSelected selectedAction = ActionSelected.NONE;

    bool isActionSelected;
    bool processingTurn;
    bool processingAfterEffect;

    // Health Update Variables
    bool updateHealthInit;

    RectTransform hpRect;
    RectTransform hpBGRect;
    float currHP = -1;
    float maxHP = -1;
    float minWidth = -1;
    
    // Animation Variables;
    bool animationStart;

    bool waitingForMessage;
    bool messagePlayed;

    string prevString;

    [HideInInspector]
    public int coroutineWait = 2;

    // Start is called before the first frame update
    void Start()
    {
        battleState = BattleState.NONE;

        container = transform.GetChild(0).gameObject;
        container.SetActive(false);

        skillsContainer.SetActive(false);
        backButton.gameObject.SetActive(false);
        actionContainer.SetActive(false);

        battleLogic = new BattleLogic(Instance());
    }

    // Update is called once per frame
    void Update()
    {
        switch (battleState)
        {
            case BattleState.BATTLESTARTWAIT:
                if (!messagePlayed)
                {
                    StartCoroutine(BattleMessage(string.Format("{0} is ready to battle!", battleLogic.opponent.GetName()), 2, () => { UpdateState(BattleState.SELECTACTION); }));
                }
                return;
            case BattleState.SELECTACTION:
                if (!messagePlayed) StartCoroutine(BattleMessage(string.Format("What will you do?"), coroutineWait));

                if (isActionSelected)
                {
                    if (actionContainer.activeInHierarchy) actionContainer.SetActive(false);
                    switch (selectedAction)
                    {
                        case ActionSelected.FIGHT:
                            // Wait for attack to be selected
                            if (!skillsContainer.activeInHierarchy) skillsContainer.SetActive(true);
                            if (!backButton.gameObject.activeInHierarchy) backButton.gameObject.SetActive(true);
                            UpdateAction(4);
                            break;
                        case ActionSelected.ITEMS:
                            // Open bag
                            break;
                        case ActionSelected.SWITCH:
                            // Open party
                            break;
                        case ActionSelected.ESCAPE:
                            // Attempt to escape
                            break;
                        default:
                            break;
                    }
                } else
                {
                    if (!actionContainer.activeInHierarchy) actionContainer.SetActive(true);
                    if (skillsContainer.activeInHierarchy) skillsContainer.SetActive(false);
                    if (backButton.gameObject.activeInHierarchy) backButton.gameObject.SetActive(false);
                    return;
                }
                break;
            case BattleState.ACTIVEANIM:
                if (!animationStart) StartCoroutine(ActiveAnimation());
                break;
            case BattleState.ACTIVEHEALTHUPDATE:
            case BattleState.OPPONENTHEALTHUPDATE:
                UpdateHealthBar(0);
                break;
            case BattleState.ACTIVETURN:
            case BattleState.OPPONENTTURN:
                if (!processingTurn)
                {
                    battleLogic.PreAttack();
                    processingTurn = true;
                }
                break;
            case BattleState.OPPONENTANIM:
                if (!animationStart) StartCoroutine(OpponentAnimation());
                break;
            case BattleState.AFTEREFFECT:
                if (!processingAfterEffect)
                {
                    ProcessAfterEffect();
                    processingAfterEffect = true;
                }
                break;
            case BattleState.ENDBATTLE:
                break;
            case BattleState.ENDTURN:
                FaintCheck();
                battleLogic.EndTurn();
                break;
            default:
                break;
        }
    }

    public void BeginBattle()
    {
        InterfaceSystem.inBattle = true;
        Initialize();

        UpdateState(BattleState.BATTLESTARTWAIT);
    }

    public void UpdateState(int _state)
    {
        messagePlayed = false;
        battleState = (BattleState)_state;

        if (battleState == BattleState.SELECTACTION && isActionSelected)
        {
            isActionSelected = false;
        }
    }

    public void UpdateState(BattleState _state)
    {
        UpdateState((int)_state);
    }

    public void UpdateAction(int _state)
    {
        selectedAction = (ActionSelected)_state;
        isActionSelected = true;
    }

    public void OnSkillSelect(int _skillIndex)
    {
        backButton.gameObject.SetActive(false);
        skillsContainer.SetActive(false);
        battleLogic.TurnStart(_skillIndex);
    }

    // healthType is which bar should be updated and how
    // 0 = Opponent Hurt, 1 = Self Heal, 2 = Self Hurt
    public void UpdateHealthBar(int healthType = 0, bool afterTurnProcess = true) //Bool for if the health is going down as a part of the main attack
    {
        if (!updateHealthInit)
        {
            Debug.Log("Updating Health Bar");

            switch (healthType)
            {
                case 0:
                    hpRect = (battleLogic.attacker == 0 ? opponentHealthImage : activeMonHealthImage).GetComponent<RectTransform>();
                    hpBGRect = (battleLogic.attacker == 0 ? opponenntHealthBG : activeMonHealthBG).GetComponent<RectTransform>();

                    currHP = battleLogic.GetOpposing().GetCurrentHP();
                    maxHP = battleLogic.GetOpposing().GetHPStat();
                    break;
                case 1:
                case 2:
                    hpRect = (battleLogic.attacker == 1 ? opponentHealthImage : activeMonHealthImage).GetComponent<RectTransform>();
                    hpBGRect = (battleLogic.attacker == 1 ? opponenntHealthBG : activeMonHealthBG).GetComponent<RectTransform>();

                    currHP = battleLogic.GetAttacker().GetCurrentHP();
                    maxHP = battleLogic.GetAttacker().GetHPStat();
                    break;
                default:
                    break;
            }
            //tweenHP = battleLogic.attacker == 1 ? battleLogic.activePrevHP : battleLogic.opponentPrevHP;

            minWidth = Mathf.Floor(120 * (float)((float)currHP / (float)maxHP));
            int hpAmt = (int)Mathf.Abs(battleLogic.prevHP - currHP);

            if (healthType == 0 || healthType == 2)
            {
                hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, minWidth);

                LeanTween.moveY(hpRect.transform.parent.GetComponent<RectTransform>(), 80, 0.4f).setFrom(64).setEaseOutElastic().setDelay(0.2f);
            }

            LeanTween.size(healthType == 1? hpRect : hpBGRect, new Vector2(minWidth, 11), 0.2f).setDelay(healthType != 1? 0.5f : 0).setOnComplete(()=> {
                
                string hpTxt = currHP + "/" + maxHP;

                if (battleLogic.attacker == 0)
                {
                    if (healthType == 1 || healthType == 2)
                    {
                        activeHealthText.text = hpTxt;
                    }
                    else
                    {
                        opponentHealthText.text = hpTxt;
                    }
                }
                else
                {
                    if (healthType == 0)
                    if (healthType == 0)
                    {
                        activeHealthText.text = hpTxt;
                    }
                    else
                    {
                        opponentHealthText.text = hpTxt;
                    }
                }

                if (healthType == 1)
                {
                    hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, minWidth);
                }

                if (afterTurnProcess) AfterTurnHealthUpdate(hpAmt);
            });

            updateHealthInit = true;
        }
    }

    public void ProcessAfterEffect()
    {
        Debug.Log("Processing After Effect");
        battleLogic.bSkills.UseSkillEffect(battleLogic.skillEffectID);

        if (!messagePlayed)
        {
            StartCoroutine(BattleMessage(battleLogic.bSkills.msgArray, coroutineWait, () => {

                processingAfterEffect = false;
                NextPhase();
            }));
        }
    }

    public void AfterTurnHealthUpdate(int hpLost)
    {
        Debug.Log("Beginning After Turn Health Update");
        if (!messagePlayed)
        {
            StartCoroutine(BattleMessage(string.Format("{0} lost {1} HP!", battleLogic.GetOpposing().GetName(), hpLost.ToString()), coroutineWait, () => {
                updateHealthInit = false;

                if (battleLogic.usingSkillEffect)
                {
                    UpdateState(BattleState.AFTEREFFECT);
                } else
                {
                    NextPhase();
                }
            }));
        }
    }

    public void NextPhase()
    {
        updateHealthInit = false;
        Debug.Log("Beginning Next Phase");
        if (battleLogic.firstMonHurt)
        {
            battleLogic.secMonHurt = true;
        }
        else
        {
            battleLogic.firstMonHurt = true;
        }

        processingTurn = false;

        battleLogic.prevAttacker = battleLogic.attacker;
        battleLogic.attacker = battleLogic.attacker == 0 ? 1 : 0;

        if (battleLogic.firstMonHurt && battleLogic.secMonHurt)
        {
            UpdateState(BattleState.ENDTURN);
        }
        else
        {
            UpdateState(battleLogic.prevAttacker == 0 ? BattleState.OPPONENTTURN : BattleState.ACTIVETURN);
        }
    }

    public IEnumerator ActiveAnimation()
    {
        Debug.Log("Begin Player Animation");
        animationStart = true;

        LeanTween.color(activeMonsterImage.GetComponent<RectTransform>(), Color.red, 0.1f);
        LeanTween.color(activeMonsterImage.GetComponent<RectTransform>(), Color.white, 0.1f).setDelay(0.2f);

        LeanTween.scale(activeMonsterImage.GetComponent<RectTransform>(), new Vector3(-1.25f, 0.75f, 1), 0.1f);
        LeanTween.scale(activeMonsterImage.GetComponent<RectTransform>(), new Vector3(-0.75f, 1.25f, 1), 0.1f).setDelay(0.1f);
        LeanTween.scale(activeMonsterImage.GetComponent<RectTransform>(), new Vector3(-1, 1f, 1), 0.1f).setDelay(0.15f);

        yield return new WaitForSeconds(1);

        animationStart = false;
        UpdateState(BattleState.ACTIVEHEALTHUPDATE);

    }

    public IEnumerator OpponentAnimation()
    {
        Debug.Log("Begin Opponent Animation");
        animationStart = true;

        LeanTween.color(opponentImage.GetComponent<RectTransform>(), Color.red, 0.1f);
        LeanTween.color(opponentImage.GetComponent<RectTransform>(), Color.white, 0.1f).setDelay(0.2f);

        LeanTween.scale(opponentImage.GetComponent<RectTransform>(), new Vector3(1.25f, 0.75f, 1), 0.1f);
        LeanTween.scale(opponentImage.GetComponent<RectTransform>(), new Vector3(0.75f, 1.25f, 1), 0.1f).setDelay(0.1f);
        LeanTween.scale(opponentImage.GetComponent<RectTransform>(), new Vector3(1, 1f, 1), 0.1f).setDelay(0.15f);

        yield return new WaitForSeconds(1);
        
        animationStart = false;
        UpdateState(BattleState.OPPONENTHEALTHUPDATE);
    }

    public IEnumerator BattleMessage(string _msg, float pause = 1, UnityAction _action = null)
    {
        yield return BattleMessage(new string[] { _msg }, pause, _action);
    }

    public IEnumerator BattleMessage(string[] _msgs, float pause = 1, UnityAction _action = null)
    {
        messagePlayed = true;
        
        for (int i = 0; i < _msgs.Length; i++)
        {
            string msgtoAdd = "";

            if (string.IsNullOrEmpty(prevString))
            {
                msgtoAdd = _msgs[i];
            } else
            {
                msgtoAdd = "<c=ffffff73>" + prevString + "</c><br>" + _msgs[i];
            }

            messageText.text = msgtoAdd;
            messageText.Rebuild(true);
            prevString = _msgs[i];

            while (messageText.reading)
            {
                yield return new WaitForEndOfFrame();
            }

            yield return new WaitForSeconds(pause);
        }

        if (_action != null) _action.Invoke();
    }

    public void Initialize()
    {
        battleLogic.activeMon = SaveSystem.playerParty[0];

        for (int i = 0; i < skillsButtons.Length; i++)
        {
            Skills monSkill = battleLogic.activeMon.GetSkill(i);

            skillsNames[i].text = SystemVariables.skillNames[monSkill.ToString()];
            skillsCooldown[i].text = SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].cooldown.ToString();
            battleLogic.opponentCooldowns[i] = battleLogic.opponent.GetSkillData(i).cooldown;
        }

        for (int i = 0; i < 6; i++)
        {
            activeStatLevels[i].text = battleLogic.activeMon.GetStatValue((StatNames)i).ToString();
            opponenetStatLevels[i].text = battleLogic.opponent.GetStatValue((StatNames)i).ToString();
        }

        activeMonsterImage.sprite = battleLogic.activeMon.GetSprite();
        opponentImage.sprite = battleLogic.opponent.GetSprite();

        activeLevelText.text = string.Format("{0} Lv{1}", battleLogic.activeMon.GetGender(), battleLogic.activeMon.GetMonLevel());
        activeNameText.text = battleLogic.activeMon.GetName();
        activeHealthText.text = battleLogic.activeMon.GetCurrentHP() + "/" + battleLogic.activeMon.GetHPStat();

        opponentLevelText.text = string.Format("{0} Lv{1}", battleLogic.opponent.GetGender(), battleLogic.opponent.GetMonLevel());
        opponentNameText.text = battleLogic.opponent.GetName();
        opponentHealthText.text = battleLogic.opponent.GetCurrentHP() + "/" + battleLogic.opponent.GetHPStat();

        hpRect = activeMonHealthImage.GetComponent<RectTransform>();
        hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(120 * (float)((float)battleLogic.activeMon.GetCurrentHP() / (float)battleLogic.activeMon.GetHPStat())));

        hpBGRect = activeMonHealthBG.GetComponent<RectTransform>();
        hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, hpRect.rect.width);

        hpRect = opponentHealthImage.GetComponent<RectTransform>();
        hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(120 * (float)((float)battleLogic.opponent.GetCurrentHP() / (float)battleLogic.opponent.GetHPStat())));

        hpBGRect = opponenntHealthBG.GetComponent<RectTransform>();
        hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, hpRect.rect.width);

        hpBGRect = null;
        hpRect = null;

        UpdateStatusEffects();

        container.SetActive(true);
    }

    public void SetOpponent(Monster mon)
    {
        battleLogic.opponent = mon;
    }

    public void UpdateStatusEffects()
    {
        StatusEffect act = battleLogic.activeMon.GetStatusEffect();
        StatusEffect opp = battleLogic.opponent.GetStatusEffect();

        activeStatusEffect.enabled = act != StatusEffect.NONE;
        opponentStatusEffect.enabled = opp != StatusEffect.NONE;

        if (act != StatusEffect.NONE)
        {
            activeStatusEffect.sprite = statusEffectImages[(int)act - 1];
        }

        if (opp != StatusEffect.NONE)
        {
            opponentStatusEffect.sprite = statusEffectImages[(int)opp - 1];
        }
    }

    public void FaintCheck()
    {
        if (battleLogic.activeMon.GetCurrentHP() == 0)
        {
            activeMonsterImage.enabled = false;
            activeMonHealthBG.enabled = false;

            if (GameUtility.GetAliveMonsters() > 0)
            {
                // Open party to select new monster
            } else
            {
                // Lose battle
            }
        } else if (battleLogic.activeMon.GetCurrentHP() == 0)
        {
            opponentImage.enabled = false;
            opponentHealthImage.enabled = false;

            // End Battle
            InterfaceSystem.wonLastBattle = true;

            StartCoroutine(BattleMessage(string.Format("{0} was defeated!", battleLogic.opponent.GetName()), _action: () => {

            }));
        }
    }

    private static BattleHandler instance;
    public static BattleHandler Instance()
    {
        if (!instance)
        {
            instance = FindObjectOfType(typeof(BattleHandler)) as BattleHandler;
            if (!instance)
                Debug.Log("There need to be at least one active BattleHandler on the scene");
        }

        return instance;
    }
}

public class BattleLogic
{
    public Monster opponent;
    public Monster activeMon;

    public BattleSkills bSkills;

    public int[] activeCooldowns = new int[5];
    public int[] opponentCooldowns = new int[5];

    public int[] activeStatStages = new int[5];
    public int[] opponentStatStages = new int[5];
    
    public float activeAccuracy = 1;
    public float opponentAccuracy = 1;

    public int attacker = -1;
    public int prevAttacker = -1;

    public int activeSkillUsed = -1;
    public int opponentSkillUsed = -1;

    public bool firstMonHurt;
    public bool secMonHurt;

    public int activePrevHP = -1;
    public int opponentPrevHP = -1;

    public int currDmg = -1;
    public int prevHP = -1;

    public BattleHandler bHandle;

    public int turn = 0;

    public List<string> battleMsgs = new List<string>();

    public bool usingSkillEffect;
    public string skillEffectID;

    public int activeSleepTimer = 0;
    public int opponentSleepTimer = 0;

    public BattleLogic(BattleHandler _bHandle)
    {
        bHandle = _bHandle;
        bSkills = new BattleSkills(this);
    }

    public void TurnStart(int _skillUsed)
    {
        Debug.Log("Beginning Attack");
        activeSkillUsed = _skillUsed;

        OppAI_SkillPick();
        SpeedCheck();
    }

    public void SpeedCheck()
    {
        Debug.Log("Beginning Speed Check");

        int playerSpeed = activeMon.GetStatValue(StatNames.SPEED);
        int oppSpeed = opponent.GetStatValue(StatNames.SPEED);

        if (activeMon.GetSkillData(activeSkillUsed).priorityLevel > opponent.GetSkillData(opponentSkillUsed).priorityLevel)
        {
            attacker = 0;
        } else if (activeMon.GetSkillData(activeSkillUsed).priorityLevel < opponent.GetSkillData(opponentSkillUsed).priorityLevel)
        {
            attacker = 1;
        } else
        {
            if (playerSpeed == oppSpeed)
            {
                attacker = Random.Range(0, 2);
            }
            else
            {
                attacker = playerSpeed > oppSpeed ? 0 : 1;
            }
        }

        Debug.Log("Attacking index: " + attacker);

        PreAttack();
    }

    public void PreAttack()
    {
        Debug.Log("Beginning PreAttack");
        if (GetAttacker().GetStatusEffect() == StatusEffect.DROWSY && GetSleepCounter() > 0)
        {
            PassSleepCounter();
            if (GetSleepCounter() == 0)
            {
                bHandle.BattleMessage("{0} is no longer tired!", _action: () => { ProcessAttack(); });

            }
            return;
        }

        ProcessAttack();
    }

    public void ProcessAttack()
    {
        Debug.Log("Processing Attack");

        if (AccuracyCheck())
        {
            if (GetAttacker().GetSkillData(GetSkillUsed()).attack != 0)
            {
                ProcessDamage();
            }
            else
            {
                usingSkillEffect = GetAttacker().GetSkillData(GetSkillUsed()).effectID != "";
                skillEffectID = usingSkillEffect ? GetAttacker().GetSkillData(GetSkillUsed()).effectID : "";

                bSkills.UseSkillEffect(skillEffectID);
            }
        } else
        {
            battleMsgs.Add(string.Format("{0} tried to use {1} but missed!", GetAttacker().GetName(), SystemVariables.skillNames[GetAttacker().monSkills[GetSkillUsed()].id.ToString()]));

            bHandle.StartCoroutine(bHandle.BattleMessage(battleMsgs.ToArray(), bHandle.coroutineWait, () =>
            {
                bHandle.NextPhase();
            }));
        }
    }

    public void OppAI_SkillPick()
    {
        int counter = 0;

        while (opponentSkillUsed == -1)
        {
            counter++;
            int choice = Random.Range(0, 5);

            if (opponent.GetSkillData(choice).cooldown == opponentCooldowns[choice])
            {
                opponentSkillUsed = choice;
            }

            if (counter >= 10)
            {
                throw new System.Exception("OppAI_SkillPick exceeded while loop limit");
            }
        }

        Debug.Log("OppAi_SkillPick Skill: " + (Skills)opponent.GetSkillData(opponentSkillUsed).id);
    }

    public bool AccuracyCheck()
    {
        Debug.Log("Beginning Accuracy Check");
        float a = attacker == 0 ? activeAccuracy : opponentAccuracy;

        if (GetAttacker().statEffect == StatusEffect.DROWSY) a *= 0.8f;

        Debug.Log("Acc: " + a);

        return Random.Range(0f, 1f) <= a;
    }

    public void ProcessDamage()
    {
        battleMsgs.Clear();

        Debug.Log("Beginning Process Damage");

        currDmg = CalcDamage(GetAttacker().GetSkillData(GetSkillUsed()).attack);

        prevHP = GetOpposing().GetCurrentHP();

        GetOpposing().Hurt(currDmg);

        Debug.Log(GetOpposing().GetName() + " hurt: " + GetOpposing().GetCurrentHP() + "HP");

        battleMsgs.Add(string.Format("{0} used {1}!", GetAttacker().GetName(), SystemVariables.skillNames[GetAttacker().monSkills[attacker == 0 ? activeSkillUsed : opponentSkillUsed].id.ToString()]));

        usingSkillEffect = GetAttacker().GetSkillData(GetSkillUsed()).effectID != "";
        skillEffectID = usingSkillEffect ? GetAttacker().GetSkillData(GetSkillUsed()).effectID : "";

        bHandle.StartCoroutine(bHandle.BattleMessage(battleMsgs.ToArray(), bHandle.coroutineWait, () =>
        {
            bHandle.UpdateState(attacker == 0 ? BattleState.OPPONENTANIM : BattleState.ACTIVEANIM);
        }));
    }

    public int CalcDamage(int _pwr)
    {
        Debug.Log("Beginning Calc Damage");

        float atk = 1;
        float def = 1;
        float lvl = 1;
        float pwr = 1;
        float multi = 1;
        
        if (attacker == 1) // Opponent attacking Player
        {
            SkillData oS = opponent.GetSkillData(opponentSkillUsed);

            atk = opponent.GetStatValue(oS.skillType == SkillType.STRENGTH? StatNames.STRENGTH : StatNames.MAGIC);
            def = activeMon.GetStatValue(oS.skillType == SkillType.STRENGTH ? StatNames.DEFENSE : StatNames.RESISTANCE);
            lvl = opponent.GetMonLevel();
            pwr = oS.attack;

        } else if (attacker == 0) // Player attacking Opponent
        {
            SkillData s = activeMon.GetSkillData(activeSkillUsed);

            atk = activeMon.GetStatValue(s.skillType == SkillType.STRENGTH ? StatNames.STRENGTH : StatNames.MAGIC);
            def = opponent.GetStatValue(s.skillType == SkillType.STRENGTH ? StatNames.DEFENSE : StatNames.RESISTANCE);
            lvl = activeMon.GetMonLevel();
            pwr = s.attack;
        }

        Debug.Log(string.Format("Damage calc: {0} // Lv{1} * (A{2} / D{3}) * P{4} / 15 * M{5}", Mathf.RoundToInt(((lvl * (atk / def) * pwr) / 15) * multi), lvl, atk, def, pwr, multi));

        return Mathf.RoundToInt(((lvl * (atk / def) * pwr) / 15) * multi);
    }

    public void EndTurn()
    {

        firstMonHurt = secMonHurt = false;

        attacker = prevAttacker = activeSkillUsed = opponentSkillUsed = activePrevHP = opponentPrevHP = -1;
        
        bHandle.UpdateState(BattleState.SELECTACTION);
        turn++;
    }
    
    public Monster GetAttacker()
    {
        return attacker == 0 ? activeMon : opponent;
    }

    public Monster GetOpposing()
    {
        return attacker == 1 ? activeMon : opponent;
    }

    public int GetSkillUsed()
    {
        return attacker == 0 ? activeSkillUsed : opponentSkillUsed;
    }

    public int GetOpposingSkill()
    {
        return attacker == 0 ? opponentSkillUsed : activeSkillUsed;
    }

    public int GetSleepCounter()
    {
        return attacker == 0 ? activeSleepTimer : opponentSleepTimer;
    }

    public void PassSleepCounter(int turns = 1)
    {
        if (attacker == 0)
        {
            activeSleepTimer -= 1;
        } else
        {
            opponentSleepTimer -= 1;
        }
    }

    public void SetOppossingAccuracy(int stage)
    {
        float acc = attacker == 1 ? activeAccuracy : opponentAccuracy;

        acc -= 0.1f * stage;

        if (attacker == 1)
        {
            activeAccuracy = acc;
        } else
        {
            opponentAccuracy = acc;
        }
    }
}

