﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MonsterSystem;

public class BattleAnimations : MonoBehaviour
{
    public int playCount = 1;
    public BattleNewHandler bnh;
    public Animator animComp;

    public void Awake()
    {
        animComp = GetComponent<Animator>();
    }

    public virtual void Start()
    {
        ChangePosition(bnh.GetOpposingIndex());
    }

    public IEnumerator LoadAnim(BattleNewHandler _bnh)
    {
        bnh = _bnh;

        bnh.AddLog("Playing Anim: BattleAnims/Anim_" + bnh.GetAttacker().GetSkillData(bnh.GetSkillUsed()).id.ToString().ToUpper());

        yield break;
        /*animComp.SetTrigger("animStart");
        yield return new WaitForFixedUpdate();

        float clipLength = animComp.GetCurrentAnimatorStateInfo(0).length;

        //Debug.Log(clipLength);

        int t = playCount;

        while (t > 0)
        {
            //Debug.Log(t);
            --t;
            yield return new WaitForSeconds(clipLength);

            animComp.SetTrigger("animStart");
        }

        Destroy(gameObject);*/
    }

    public virtual IEnumerator AnimProcess()
    {
        yield break;
    }

    public IEnumerator PlayAnim()
    {
        animComp.SetTrigger("animStart");
        yield return new WaitForFixedUpdate();

        float clipLength = animComp.GetCurrentAnimatorStateInfo(0).length;

        //Debug.Log(clipLength);

        int t = playCount;

        while (t > 0)
        {
            //Debug.Log(t);
            --t;
            yield return new WaitForSeconds(clipLength);
        }
    }

    public void HurtEnemy()
    {
        //StartCoroutine(HurtCoroutine());
    }

    public void ShakeEnemy()
    {
        //StartCoroutine(ShakeCoroutine());        
    }

    public void ChangeOrientation(int attacker)
    {
        transform.localScale = new Vector3(-bnh.GetMonsterImage(attacker).transform.localScale.x, 1, 1);
    }

    public void ChangePosition(int attacker)
    {
        transform.position = bnh.GetMonsterImage(attacker).transform.position;
    }

    public void ShakeAttacker(int mon = -1)
    {
        if (mon == -1) mon = bnh.GetOpposingIndex();

        StartCoroutine(bnh.AnimShake(mon));
    }
}