﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;
using UnityEngine.Events;
using System.Reflection;
using System;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEditor;

public class BattleNewHandler : MonoBehaviour
{
    #region UI
    GameObject container;
    GameObject prefab;

    [Header("Monster Sprites")]
    public SpriteRenderer playerMonsterImage;
    public SpriteRenderer enemyMonsterImage;

    public SpriteRenderer playerCharm;
    public SpriteRenderer enemyCharm;

    public ParticleSystem[] playerCharmParticles;
    public ParticleSystem[] enemyCharmParticles;

    public Material playerCharmMat;
    public Material enemyCharmMat;

    [Header("Health")]
    public Image playerMonHealthImage;
    public Image playerMonHealthBG;
    public SpriteRenderer playerShadow;

    public SuperTextMesh playerNameText;
    public SuperTextMesh playerLevelText;
    public SuperTextMesh playerHealthText;

    public Image playerChargeBar;

    [Space(15)]
    public Image enemyHealthImage;
    public Image enemyHealthBG;
    public SpriteRenderer enemyShadow;
    public SuperTextMesh enemyNameText;
    public SuperTextMesh enemyLevelText;
    public SuperTextMesh enemyHealthText;

    [Space(10)]
    public Image playerStatusEffect;
    public Image enemyStatusEffect;

    public Sprite[] statusEffectImages;

    [Header("Action Buttons")]
    public GameObject actionContainer;
    public Button[] actionButtons;
    public Button backButton;

    [Header("Skill Buttons")]
    public GameObject skillsContainer;
    public Button[] skillsButtons;
    public SuperTextMesh[] skillsNames;
    public SuperTextMesh[] skillsPower;
    public SuperTextMesh[] skillsCooldown;
    public Image[] skillsTypes;

    public Sprite[] attrButtons;
    public Sprite[] attrButtonsHover;

    public Material[] attrMats;

    [Header("Text")]
    public SuperTextMesh messageText;
    public Image messageCursor;

    [Space(15)]
    public SuperTextMesh[] playerStatLevels;
    public SuperTextMesh[] enemyStatLevels;

    [Header("Results")]
    public GameObject resultsContainer;
    public Image[] resultsPartyIcons;
    public Image[] resultsXPBar;
    public Image[] resultsXPBG;
    public SuperTextMesh[] resultsPartyLevels;
    public SuperTextMesh[] resultsPartyLevelUp;
    public ParticleSystem[] resultsPartyParticles;

    public SuperTextMesh[] resultsPartyStatsBefore;
    public Image[] resultsPartyStatsArrow;
    public SuperTextMesh[] resultsPartyStatsAfter;

    public ItemTabUpdater[] resultsDrops;
    public SuperTextMesh resultsDropsPage;
    public Image[] resultsDropsArrows;

    public SuperTextMesh resultsGoldAmt;

    int resultsDropsPageIndex, resultsDropsTotalPages, resultsPartySummary;

    int dropItemPageIndex, dropItemTotalPages;

    public SuperTextMesh[] oldStatValues;
    public SuperTextMesh[] newStatValues;
    public GameObject levelUpPanel;

    #endregion

    #region Logic
    [Header("Logic")]
    public BattleState battleState = BattleState.SELECTACTION;
    public ActionSelected selectedAction = ActionSelected.NONE;

    public bool isBoss, isTamerBattle;

    bool isActionSelected;

    // Health Update Variables
    RectTransform hpRect;
    RectTransform hpBGRect;
    float currHP, maxHP, minWidth;

    bool messagePlayed;

    [HideInInspector]
    public int coroutineWait = 2;

    [Header("Monsters")]
    public Monster enemyMon;
    public Monster playerMon;

    [Header("Stats")]
    public int[] playerCooldowns = new int[5] { -1, -1, -1, -1, -1 };
    public int[] enemyCooldowns = new int[5] { -1, -1, -1, -1, -1 };

    public int[] playerStatChanges = new int[6] { 0, 0, 0, 0, 0, 0 };
    public int[] enemyStatChanges = new int[6] { 0, 0, 0, 0, 0, 0 };

    [Header("Accuracy")]
    public float playerAccuracy, enemyAccuracy;
    public int playerAccChanges, enemyAccChanges;

    [Header("Skills")]
    public int playerSkillUsed, enemySkillUsed;
    public int playerSpecialSkillCharge;
    public bool currSkillHasEffect;
    public string currSkillEffectID;

    [Header("Variables")]
    public int currAttacker, prevAttacker;
    public int playerSleepTimer, enemySleepTimer;
    public int currDMG, prevHP;
    public bool firstMonHurt, secMonHurt, isCurrentSkillCritical;

    public bool[] monsToGiveExp = new bool[] { true, false, false, false };
    int[,] beforeStats = new int[4, 6];

    public int turn;

    [Header("Items")]
    public bool playerAccessoryUsed, enemyAccessoryUsed;
    public Items playerAccessoryItem, enemyAccessoryItem;
    
    bool actionButtonsTweening;

    SkillEffects skillEffectsClass;

    [HideInInspector]
    public List<string> log = new List<string>();

    bool playerSecondChanceUsed, enemySecondChanceUsed;

    #endregion

    Sprite[] attrIcons;
    CameraFollow cam;

    public GameObject loadedAnim;

    Tamer battlingTamer;
    int tamerMonIndex;
    string debugString;
    bool displayDebug;

    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main.GetComponent<CameraFollow>();
        
        prefab = GameObject.FindGameObjectWithTag("BattlePrefab");

        playerMonsterImage = prefab.transform.GetChild(2).GetComponent<SpriteRenderer>();
        enemyMonsterImage = prefab.transform.GetChild(1).GetComponent<SpriteRenderer>();

        playerShadow = prefab.transform.GetChild(3).GetComponent<SpriteRenderer>();
        enemyShadow= prefab.transform.GetChild(4).GetComponent<SpriteRenderer>();

        playerCharm = prefab.transform.Find("PlayerCharm").GetComponent<SpriteRenderer>();
        playerCharmMat = playerCharm.material;
        playerCharmParticles = new ParticleSystem[] {
            playerCharm.transform.GetChild(0).GetComponent<ParticleSystem>(),
            playerCharm.transform.GetChild(1).GetComponent<ParticleSystem>()
        };

        skillEffectsClass = GetComponent<SkillEffects>();
        skillEffectsClass.bnh = this;
        battleState = BattleState.NONE;

        resultsContainer.SetActive(false);

        container = transform.GetChild(0).gameObject;
        container.SetActive(false);

        skillsContainer.SetActive(false);
        backButton.gameObject.SetActive(false);
        actionContainer.SetActive(false);
        levelUpPanel.SetActive(false);

        playerStatChanges = new int[6] { 0, 0, 0, 0, 0, 0 };
        enemyStatChanges = new int[6] { 0, 0, 0, 0, 0, 0 };

        playerCooldowns = new int[5] { -1, -1, -1, -1, -1 };
        enemyCooldowns = new int[5] { -1, -1, -1, -1, -1 };

        attrIcons = Resources.LoadAll<Sprite>("mon_Types");

        for (int i = 0; i < skillsButtons.Length; i++)
        {

            int ind = i;
            skillsButtons[i].onClick.AddListener(() => OnSkillSelect(ind));

            /*

            EventTrigger et = skillsButtons[i].gameObject.AddComponent<EventTrigger>();

            EventTrigger.Entry pEnter = new EventTrigger.Entry();
            pEnter.eventID = EventTriggerType.PointerEnter;
            pEnter.callback.AddListener((e) => SkillsEnter(ind));

            EventTrigger.Entry pExit = new EventTrigger.Entry();
            pExit.eventID = EventTriggerType.PointerExit;
            pExit.callback.AddListener((e) => SkillsExit(ind));

            et.triggers.Add(pEnter);
            et.triggers.Add(pExit);*/
        }

        enemyMon = null;
    }
    
    public void Update()
    {
        if (!InterfaceSystem.InDialogueChoices() && InterfaceInputManager.instance.es.currentSelectedGameObject != null && InterfaceInputManager.instance.controllerCursor.activeInHierarchy)
        {
            RectTransform sRect = InterfaceInputManager.instance.es.currentSelectedGameObject.GetComponent<RectTransform>();
            Vector3[] c = new Vector3[4];
            sRect.GetWorldCorners(c);

            InterfaceInputManager.instance.PositionCursor(new Vector3(c[1].x, c[1].y));
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            displayDebug = !displayDebug;
        }
    }

    private static BattleNewHandler _instance;
    public static BattleNewHandler instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(BattleNewHandler)) as BattleNewHandler;
                if (!_instance)
                    Debug.Log("There need to be at least one active BattleNewHandler on the scene");
            }

            return _instance;
        }
    }

    public void AddLog(string msg)
    {
        string m = "[" + DateTime.Now.ToLongTimeString() + "] ";
        m += msg;

        log.Add(m);
    }

    public void Initialize() // Reinit = sending a new mon out
    {
        ResetPlayerValues();
        ResetEnemyValues();
        
        playerMon = SaveSystem.playerParty[GameUtility.GetFirstAliveMon()];

        turn = 0;

        if (isTamerBattle)
        {
            enemyMon = battlingTamer.party[0];
            tamerMonIndex = 0;
        }

        currAttacker = prevAttacker = -1;

        UpdateDisplays();
    }

    public void ResetPlayerValues()
    {
        playerStatChanges = new int[6] { 0, 0, 0, 0, 0, 0 };
        playerCooldowns = new int[5] { -1, -1, -1, -1, -1 };

        playerSpecialSkillCharge = 0;
        playerChargeBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);

        playerSkillUsed = -1;
        playerSleepTimer = 0;
        playerAccuracy = 1;
        playerAccChanges = 0;
    }

    public void ResetEnemyValues()
    {
        enemyStatChanges = new int[6] { 0, 0, 0, 0, 0, 0 };
        enemyCooldowns = new int[5] { -1, -1, -1, -1, -1 };

        enemySkillUsed = -1;
        enemySleepTimer = 0;
        enemyAccuracy = 1;
        enemyAccChanges = 0;
    }

    public void UpdateDisplays()
    {
        for (int i = 0; i < skillsButtons.Length; i++)
        {
            Skills monSkill = playerMon.GetSkill(i);

            if (monSkill == Skills.NONE)
            {
                skillsButtons[i].image.sprite = null;
                skillsButtons[i].image.color = new Color(1, 1, 1, 0);
                skillsNames[i].text = "";
                skillsPower[i].text = "";
                skillsCooldown[i].text = "";
                skillsButtons[i].interactable = false;
                skillsTypes[i].sprite = null;
                skillsTypes[i].color = new Color(1, 1, 1, 0);
            }
            else
            {
                int attrIndex = (int)SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].attribute;
                int power = SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].attack;

                skillsNames[i].text = SystemVariables.trans[monSkill.ToString() + "_NAME"];
                skillsPower[i].text = power == 0 ? "--" : power.ToString();

                if (i == 5)
                {
                    //skillsCooldown[i].text = "0/" + SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].cooldown.ToString();
                    bool isChargedUp = playerSpecialSkillCharge >= SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].cooldown;
                    
                    skillsButtons[i].image.color = new Color(1, 1, 1, isChargedUp? 1 : 0.5f);
                    skillsButtons[i].interactable = isChargedUp;

                    //playerSpecialSkillCharge = 0;
                }
                else
                {
                    int cd = SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].cooldown;
                    bool isCool = playerCooldowns[i] == -1 || playerCooldowns[i] == cd;

                    skillsCooldown[i].text = isCool? cd.ToString() : playerCooldowns[i].ToString() + "/" + cd.ToString();
                    skillsButtons[i].interactable = isCool;
                    skillsButtons[i].image.color = new Color(1, 1, 1, isCool ? 1 : 0.5f);

                    //enemyCooldowns[i] = SkillDatabase.db[SkillSystem.GetSkillDatabaseIndex(monSkill)].cooldown;
                    //playerCooldowns[i] = -1;
                }

                skillsButtons[i].image.sprite = attrButtons[attrIndex];

                SpriteState temp = skillsButtons[i].spriteState;
                temp.highlightedSprite = attrButtonsHover[attrIndex];
                temp.pressedSprite = attrButtonsHover[attrIndex];
                temp.disabledSprite = attrButtons[attrIndex];

                skillsTypes[i].color = Color.white;
                skillsTypes[i].sprite = attrIcons[attrIndex];

                skillsNames[i].textMaterial = attrMats[attrIndex];
                skillsCooldown[i].textMaterial = attrMats[attrIndex];
                skillsPower[i].textMaterial = attrMats[attrIndex];

                skillsButtons[i].spriteState = temp;
            }
        }

        UpdateStatChanges();

        playerMonsterImage.enabled = true;
        playerMonHealthBG.enabled = true;

        playerMonsterImage.sprite = playerMon.GetSprite();
        enemyMonsterImage.sprite = enemyMon.GetSprite();

        enemyMonsterImage.transform.localScale = new Vector3(1, 1);

        playerLevelText.text = string.Format("{0} Lv{1}", playerMon.GetGender(), playerMon.GetMonLevel());
        playerNameText.text = playerMon.GetName();
        playerHealthText.text = playerMon.GetCurrentHP() + "/" + playerMon.GetHPStat();

        enemyLevelText.text = string.Format("{0} Lv{1}", enemyMon.GetGender(), enemyMon.GetMonLevel());
        enemyNameText.text = enemyMon.GetName();
        enemyHealthText.text = enemyMon.GetCurrentHP() + "/" + enemyMon.GetHPStat();

        hpRect = playerMonHealthImage.GetComponent<RectTransform>();
        hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(120 * (float)((float)playerMon.GetCurrentHP() / (float)playerMon.GetHPStat())));

        hpBGRect = playerMonHealthBG.GetComponent<RectTransform>();
        hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, hpRect.rect.width);

        hpRect = enemyHealthImage.GetComponent<RectTransform>();
        hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(120 * (float)((float)enemyMon.GetCurrentHP() / (float)enemyMon.GetHPStat())));

        hpBGRect = enemyHealthBG.GetComponent<RectTransform>();
        hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, hpRect.rect.width);

        hpBGRect = null;
        hpRect = null;

        enemyHealthImage.enabled = true;
        playerMonHealthImage.enabled = true;

        // PERKCHECK: SCOPE
        enemyHealthText.gameObject.SetActive(playerMon.perk == Perks.SCOPE);

        enemyMonsterImage.color = Color.white;
        enemyShadow.color = new Color(1, 1, 1, 0.3568628f);

        for (int i = 0; i < actionButtons.Length; i++)
        {
            LeanTween.moveLocalY(actionButtons[i].gameObject, -78, 0);
        }

        UpdateStatusEffects();

        container.SetActive(true);
        resultsContainer.SetActive(false);
    }

    public void BeginBattle()
    {
        InterfaceSystem.inBattle = true;
        Initialize();

        StartCoroutine(BattleBegin());
    }

    public void SetTamer(Tamer _tamer)
    {
        battlingTamer = _tamer;
        isTamerBattle = true;
    }

    public void UpdateStatusEffects()
    {
        StatusEffect act = playerMon.GetStatusEffect();
        StatusEffect opp = enemyMon.GetStatusEffect();

        playerStatusEffect.enabled = act != StatusEffect.NONE;
        enemyStatusEffect.enabled = opp != StatusEffect.NONE;

        if (act != StatusEffect.NONE)
        {
            playerStatusEffect.sprite = statusEffectImages[(int)act - 1];
        }

        if (opp != StatusEffect.NONE)
        {
            enemyStatusEffect.sprite = statusEffectImages[(int)opp - 1];
        }
    }

    public void UpdateStatChanges(bool init = false)
    {
        for (int i = 0; i < 7; i++)
        {
            int playerStat = i == 6 ? playerAccChanges : playerStatChanges[i];
            int enemyStat = i == 6 ? enemyAccChanges : enemyStatChanges[i];

            if (init)
            {
                if (i == 6)
                {
                    playerAccChanges = 0;
                    enemyAccChanges = 0;
                } else
                {
                    playerStatChanges[i] = 0;
                    enemyStatChanges[i] = 0;
                }
            }

            playerStatLevels[i].text = playerStat == 0 ? "--" : string.Format("x {0}", playerStat);
            enemyStatLevels[i].text = enemyStat == 0 ? "--" : string.Format("x {0}", enemyStat);
        }
    }

    public IEnumerator BattleBegin()
    {
        transform.parent.GetChild(0).gameObject.SetActive(false);
        
        Camera.main.GetComponent<CameraFollow>().isFollowing = false;
        Camera.main.transform.position = new Vector3(prefab.transform.position.x, prefab.transform.position.y, Camera.main.transform.position.z);
        Camera.main.GetComponent<PixelPerfectCamera>().enabled = false;
        float toSize = Camera.main.orthographicSize;
        Camera.main.orthographicSize *= 0.15f;
        LeanTween.value(Camera.main.orthographicSize, toSize, 0.4f).setEaseOutSine().setOnUpdate((float val) => { Camera.main.orthographicSize = val; }).setOnComplete(()=> {Camera.main.GetComponent<PixelPerfectCamera>().enabled = true;}).setDelay(0.95f);

        while (InterfaceSystem.isFading)
        {
            yield return new WaitForEndOfFrame();
        }

        string introMessage = "";

        if (isTamerBattle)
        {
            introMessage = string.Format(SystemVariables.trans["battle_ready_tamer"], battlingTamer.tamerClass.ToString(), battlingTamer.id.ToString());
        } else
        {
            introMessage = string.Format(SystemVariables.trans[isBoss ? "battle_ready_boss" : "battle_ready"], enemyMon.GetName());
        }

        yield return StartCoroutine(BattleMessage(introMessage));
        
        if (isBoss)
        {
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_boss_charge"])));

            for (int i = 0; i < 6; i++)
            {
                enemyStatChanges[i] = 1;
            }

            UpdateStatChanges();
        }
        
        // PERKCHECK: GOODEYE
        if (playerMon.perk == Perks.GOODEYE)
        {
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_perk_scope"], GetPrefix(false), playerMon.GetName(), PerkSystem.GetPerkName(playerMon.perk), GetPrefix(true), enemyMon.GetName(), PerkSystem.GetPerkName(enemyMon.perk))));
        }

        yield return new WaitForSeconds(0.25f);

        LeanTween.value(0.5f, -0.5f, 0.4f).setOnUpdate((float val) => { playerCharmMat.SetFloat("_Amount", val); });
        LeanTween.value(0f, 1f, 0.4f).setOnUpdate((float val) => { playerCharmMat.SetFloat("_ColorLerp", val); });
        yield return new WaitForSeconds(1f);

        LeanTween.scale(playerCharm.gameObject, Vector3.one, 0.3f).setFrom(new Vector3(1.2f, 0.8f));

        LeanTween.rotate(playerCharmParticles[0].gameObject, new Vector3(0, 0, 720), 1f);
        LeanTween.rotate(playerCharmParticles[1].gameObject, new Vector3(0, 0, 720), 1f);
        
        playerCharmParticles[0].Emit(15);
        playerCharmParticles[1].Emit(70);

        LeanTween.value(-0.5f, 0.5f, 0.2f).setOnUpdate((float val) => { playerCharmMat.SetFloat("_Amount", val); });
        LeanTween.value(1f, 0f, 0.2f).setOnUpdate((float val) => { playerCharmMat.SetFloat("_ColorLerp", val); });
        yield return new WaitForSeconds(0.1f);

        LeanTween.moveLocalY(playerMonsterImage.gameObject, -1.5f, 0.2f).setFrom(-0.5f).setEaseOutSine();
        LeanTween.scale(playerMonsterImage.gameObject, new Vector3(-1, 1, 1), 0.2f).setFrom(new Vector3(-0.66f, 0.66f, 1)).setEaseOutSine();

        LeanTween.color(playerMonsterImage.gameObject, Color.white, 0.2f);
        yield return new WaitForSeconds(0.2f);

        LeanTween.moveLocalY(playerMonsterImage.gameObject, -2.5f, 0.2f).setEaseInSine();
        LeanTween.color(playerShadow.gameObject, new Color(1, 1, 1, 0.31f), 0.2f);
        yield return new WaitForSeconds(0.2f);

        LeanTween.scale(playerMonsterImage.gameObject, new Vector3(-1.2f, 0.8f, 1), 0.1f);
        yield return new WaitForSeconds(0.1f);

        LeanTween.scale(playerMonsterImage.gameObject, new Vector3(-1f, 1f, 1), 0.1f);
        yield return new WaitForSeconds(0.3f);


        yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_what"], 0, false));
        yield return StartCoroutine(SelectAction());
    }

    public IEnumerator SelectAction()
    {
        InterfaceInputManager.instance.EnableCursor(false);

        isActionSelected = false;

        yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_what"], 0, false, false));

        backButton.gameObject.SetActive(false);
        skillsContainer.SetActive(false);

        actionButtonsTweening = true;
        actionContainer.SetActive(true);
        
        for (int i = 0; i < actionButtons.Length; i++)
        {
            LeanTween.moveLocalY(actionButtons[i].gameObject, 40, 0.3f).setEaseOutBack();
            yield return new WaitForSeconds(0.05f);
        }

        actionButtonsTweening = false;

        //inpMan.controllerCursor.transform.SetAsLastSibling();
        InterfaceInputManager.instance.es.SetSelectedGameObject(actionButtons[0].gameObject);

        while (!isActionSelected)
        {
            yield return new WaitForFixedUpdate();
        }

        if (actionContainer.activeInHierarchy) actionContainer.SetActive(false);
        
        switch (selectedAction)
        {
            case ActionSelected.FIGHT:
                for (int i = 0; i < skillsButtons.Length; i++)
                {
                    LeanTween.size(skillsButtons[i].GetComponent<RectTransform>(), new Vector2(220, 20), 0);
                }

                InterfaceInputManager.instance.controllerCursor.transform.SetAsLastSibling();
                InterfaceInputManager.instance.es.SetSelectedGameObject(skillsButtons[0].gameObject);
                InterfaceInputManager.instance.EnableCursor(true);
                
                if (!skillsContainer.activeInHierarchy) skillsContainer.SetActive(true);
                if (!backButton.gameObject.activeInHierarchy) backButton.gameObject.SetActive(true);
                break;
            case ActionSelected.ITEMS:
                StartCoroutine(SelectItem());
                break;
            case ActionSelected.SWITCH:
                StartCoroutine(ReplaceMon());
                break;
            case ActionSelected.ESCAPE:
                StartCoroutine(FleeAttempt());
                break;
            default:
                break;
        }

        yield break;
    }

    public void BackButton()
    {
        StartCoroutine(SelectAction());
    }

    public IEnumerator ReplaceMon(bool fainted = false)
    {
        bool selectedMonToSwap = false;

        PartyUI p = InterfaceSystem.OpenParty(null, false);
        
        p.SetRequest(new MonsterRequest()
        {
            request = MonsterRequestType.STATUS,
            status = StatusEffect.NONE
        });

        yield return new WaitForInMenu();

        yield return new WaitForMonsterSelect();
        
        /*while (!selectedMonToSwap && p != null)
        {
            yield return new WaitForMonsterSelect();

            ChoiceHandler ch = ChoiceHandler.Instance();
            ch.AddChoice(0, "Select");
            ch.AddChoice(1, "View");
            ch.AddChoice(2, "Cancel");
            ch.Show();

            int choiceIndex = InterfaceSystem.choiceIndex;

            switch (choiceIndex)
            {
                case 0:
                    selectedMonToSwap = true;
                    break;
                case 1:
                    InterfaceSystem.OpenSummary(p, InterfaceSystem.temporaryMonsterIndex, false);

                    yield return new WaitForUIPriority(p);

                    break;
                case 2:
                    InterfaceSystem.ResetTempMonsterPartyIndex();
                    break;
                default:
                    break;
            }
            yield return new WaitForEndOfFrame();
        }*/

        if (InterfaceSystem.temporaryMonsterIndex < 1 && !fainted)
        {
            yield return StartCoroutine(SelectAction());
        }
        else
        {
            monsToGiveExp[InterfaceSystem.temporaryMonsterIndex] = true;

            playerMon = SaveSystem.playerParty[InterfaceSystem.temporaryMonsterIndex];

            ResetPlayerValues();
            UpdateDisplays();

            if (fainted)
            {
                yield return StartCoroutine(SelectAction());
                yield break;
            }

            currAttacker = 1;
            firstMonHurt = true;

            yield return StartCoroutine(AI_SkillPick());
            yield return StartCoroutine(AttackProcess());
        }
    }

    public IEnumerator SelectItem()
    {
        InventoryUI iui = InterfaceSystem.OpenInventory(null, true, false);
        yield return new WaitForUIFade(iui);
        
        yield return new WaitForItemSelect();

        Debug.Log("waited");

        if (InterfaceSystem.temporaryPocketIndex == -1)
        {
            yield return StartCoroutine(SelectAction());
        } else
        {
            Item selectedItem = SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex];

            // Check Item
            switch (selectedItem.GetData().battleFunction)
            {
                case ItemEffects_Battle.NONE:
                    break;
                case ItemEffects_Battle.HEALING:
                    int prevHP = playerMon.GetCurrentHP();
                    playerMon.Heal(selectedItem.GetData().intParam);

                    int newHP = playerMon.GetCurrentHP();
                    yield return StartCoroutine(UpdateHealth(HealthType.HEAL));

                    yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["monster_heal"], playerMon.GetName(), newHP - prevHP)));
                    break;
                case ItemEffects_Battle.STATBOOST:
                    break;
                case ItemEffects_Battle.TAME:
                    yield return StartCoroutine(ThrowCharm());
                    yield break;
                default:
                    break;
            }

            currAttacker = 1;
            firstMonHurt = true;

            yield return StartCoroutine(AI_SkillPick());
            yield return StartCoroutine(AttackProcess());
        }

    }
    

    public IEnumerator ThrowCharm()
    {
        Item selectedItem = SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex];

        float tameRandom = Random.Range(0f, 1f);
        float hpPercent = (enemyMon.GetCurrentHP() / enemyMon.GetHPStat()) / 2;
        float tameRate = enemyMon.GetData().tameRate + hpPercent;

        if (tameRandom <= tameRate)
        {
            yield return StartCoroutine(BattleMessage("You tamed {0}!".With(enemyMon.GetName())));

            enemyMon.SetCharm(selectedItem);
            GameUtility.ObtainMonster(enemyMon);

            selectedItem.currentQuantity--;

            if (selectedItem.currentQuantity <= 0)
            {
                SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex).RemoveAt(InterfaceSystem.temporaryItemIndex);
            }

            yield return StartCoroutine(EndBattle());
        } else
        {
            yield return StartCoroutine(BattleMessage("You failed to tame {0}!".With(enemyMon.GetName())));

            currAttacker = 1;
            firstMonHurt = true;

            yield return StartCoroutine(AI_SkillPick());
            yield return StartCoroutine(AttackProcess());
        }

        yield break;
    }

    public IEnumerator FleeAttempt()
    {
        float chance = Random.Range(0f, 1f);
        float perc = enemyMon.GetMonLevel() > playerMon.GetMonLevel() ? 0.45f : 0.8f;

        if (chance <= perc)
        {
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_escape_player"], SaveSystem.playerName), waitForInput: true));
            yield return StartCoroutine(EndBattle());
        } else
        {
            yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_escape_fail"]));

            currAttacker = 1;
            firstMonHurt = true;

            yield return StartCoroutine(AI_SkillPick());
            yield return StartCoroutine(AttackProcess());
        }
    }

    public IEnumerator AttackProcess()
    {
        AddLog("Beginning AttackProcess()");

        // Pre Attack
        if (GetAttacker().GetStatusEffect() == StatusEffect.DROWSY && GetSleepCounter() > 0)
        {
            PassSleepCounter();
            if (GetSleepCounter() == 0)
            {
                GetAttacker().SetStatusEffect(StatusEffect.NONE);
                UpdateStatusEffects();
                yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_status_drowsy_off"], GetPrefix(false), GetAttacker().GetName())));
            }
        }

        yield return StartCoroutine(ProcessAttack());

        yield return StartCoroutine(AfterAttack());
    }

    public IEnumerator BeginAttackTurn()
    {
        AddLog("================================================");
        AddLog("Beginning BeginAttackTurn()" + "/ " + turn);
        yield return StartCoroutine(AI_SkillPick());
        yield return StartCoroutine(SpeedCheck());

        yield return StartCoroutine(AttackProcess());
    }

    public IEnumerator AI_SkillPick()
    {
        int counter = 0;

        while (enemySkillUsed == -1)
        {
            counter++;
            int choice = Random.Range(0, 5);

            if (enemyMon.GetSkillData(choice).cooldown == enemyCooldowns[choice] || enemyCooldowns[choice] == -1)
            {
                enemySkillUsed = choice;
            }

            if (counter >= 10)
            {
                throw new Exception("AI_SkillPick exceeded loop limit");
            }
        }

        yield return null;
    }

    public IEnumerator SpeedCheck()
    {
        int playerSpeed = playerMon.GetStatValue(StatNames.SPEED);
        int enemySpeed = enemyMon.GetStatValue(StatNames.SPEED);

        if (playerMon.GetSkillData(playerSkillUsed).priorityLevel > enemyMon.GetSkillData(enemySkillUsed).priorityLevel)
        {
            currAttacker = 0;
        }
        else if (playerMon.GetSkillData(playerSkillUsed).priorityLevel < enemyMon.GetSkillData(enemySkillUsed).priorityLevel)
        {
            currAttacker = 1;
        }
        else
        {
            // PERKCHECK: MYTURN
            if (playerSpeed == enemySpeed || playerMon.perk == Perks.MYTURN && enemyMon.perk == Perks.MYTURN)
            {
                currAttacker = Random.Range(0, 2);
            }
            else
            {
                if (playerMon.perk == Perks.MYTURN)
                {
                    currAttacker = 0;
                    yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_perk_myturn"], GetPrefix(false), GetAttacker().GetName(), PerkSystem.GetPerkName(GetAttacker().perk))));
                } else if (enemyMon.perk == Perks.MYTURN)
                {
                    currAttacker = 1;

                    yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_perk_myturn"], GetPrefix(false), GetAttacker().GetName(), PerkSystem.GetPerkName(GetAttacker().perk))));
                } else
                {
                    currAttacker = playerSpeed > enemySpeed ? 0 : 1;
                }
            }
        }

        yield break;
    }

    public IEnumerator ProcessAttack()
    {
        AddLog("Beginning ProcessAttack()");
        if (AccuracyCheck())
        {

            currSkillEffectID = GetAttacker().GetSkillData(GetSkillUsed()).effectID;

            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_use"], GetPrefix(false), GetAttacker().GetName(), SkillSystem.GetSkillName(currAttacker == 0 ? playerSkillUsed < 5 ? playerMon.monSkills[playerSkillUsed].id : playerMon.specialSkill.id : enemySkillUsed < 5 ? enemyMon.monSkills[enemySkillUsed].id : enemyMon.specialSkill.id))));

            yield return skillEffectsClass.StartCoroutine(currSkillEffectID.ToUpper());
        }
        else
        {
            //bHandle.NextPhase();

            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_miss"], GetPrefix(false), GetAttacker().GetName(), SkillSystem.GetSkillName(currAttacker == 0 ? playerSkillUsed < 5 ? playerMon.monSkills[playerSkillUsed].id : playerMon.specialSkill.id : enemySkillUsed < 5 ? enemyMon.monSkills[enemySkillUsed].id : enemyMon.specialSkill.id))));
        }
    }

    public IEnumerator UpdateHealth(HealthType _health)
    {
        AddLog("Beginning UpdateHealth()");
        switch (_health)
        {
            case HealthType.HURT:
                hpRect = (currAttacker == 0 ? enemyHealthImage : playerMonHealthImage).GetComponent<RectTransform>();
                hpBGRect = (currAttacker == 0 ? enemyHealthBG : playerMonHealthBG).GetComponent<RectTransform>();

                currHP = GetOpposing().GetCurrentHP();
                maxHP = GetOpposing().GetHPStat();
                break;
            case HealthType.HEAL:
            case HealthType.SELFHURT:
                hpRect = (currAttacker == 1 ? enemyHealthImage : playerMonHealthImage).GetComponent<RectTransform>();
                hpBGRect = (currAttacker == 1 ? enemyHealthBG : playerMonHealthBG).GetComponent<RectTransform>();

                currHP = GetAttacker().GetCurrentHP();
                maxHP = GetAttacker().GetHPStat();
                break;
            default:
                break;
        }

        minWidth = Mathf.Floor(120 * (float)((float)currHP / (float)maxHP));
        int hpAmt = (int)Mathf.Abs(prevHP - currHP);

        if (_health != HealthType.HEAL)
        {
            hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, minWidth);

            float percentLoss = 1 - (currHP / (prevHP + 0.01f));

            float time = 0.4f;
            int setFrom = 52;

            if (percentLoss == 1)
            {
                time = 0.7f;
                setFrom = 32;
            } else if (percentLoss >= 0.5f && percentLoss < 1)
            {
                time = 0.6f;
                setFrom = 44;
            } else if (percentLoss < 0.5f && percentLoss >= 0.2f)
            {
                time = 0.4f;
                setFrom = 52;
            } else if (percentLoss < 0.2f)
            {
                time = 0.3f;
                setFrom = 70;
            }

            AddLog("Tweening HP Bar: " + setFrom);

            LeanTween.moveY(hpRect.transform.parent.GetComponent<RectTransform>(), 90, time).setFrom(setFrom).setEaseOutElastic();

            yield return new WaitForSeconds(time);
        }

        LeanTween.size(_health == HealthType.HEAL ? hpRect : hpBGRect, new Vector2(minWidth, 11), 0.2f);

        yield return new WaitForSeconds(0.2f);

        string hpTxt = currHP + "/" + maxHP;

        if (currAttacker == 0)
        {
            if (_health != HealthType.HURT)
            {
                playerHealthText.text = hpTxt;
            }
            else
            {
                enemyHealthText.text = hpTxt;

                if (enemyAccessoryUsed)
                {
                    yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_accessory_weaken"].With(GetPrefix(false), enemyMon.GetName(), ItemSystem.GetItemName(enemyAccessoryItem))));

                    enemyMon.accessory = null;
                }
            }
        } else
        {
            if (_health == HealthType.HURT)
            {
                playerHealthText.text = hpTxt;

                if (playerAccessoryUsed)
                {
                    yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_accessory_weaken"].With(playerMon.GetName(), ItemSystem.GetItemName(playerAccessoryItem))));

                    playerMon.accessory = null;
                }
            }
            else
            {
                enemyHealthText.text = hpTxt;
            }
        }

        if (_health == HealthType.HEAL)
        {
            hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, minWidth);
        }

        UpdateStatChanges();
        
        yield return new WaitForSeconds(0.25f);
    }

    public IEnumerator AfterAttack()
    {
        AddLog("Beginning AfterAttack()");
        //string msg = string.Format("{0} lost {1} HP!", GetOpposing().GetName(), hpLost.ToString());

        /*if (currSkillEffect)
        {
            yield return sEffects.StartCoroutine(skillEffectID.ToUpper());
        }
        */

        if (isCurrentSkillCritical) yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_crit"])));

        // PERKCHECK: SECONDCHANCE
        if (GetAttacker().perk == Perks.SECONDCHANCE && GetAttacker().GetCurrentHP() <= GetAttacker().GetHPStat() * 0.3f && !(currAttacker == 0 ? playerSecondChanceUsed : enemySecondChanceUsed))
        {
            GetAttacker().Heal(GetAttacker().GetHPStat() * 0.4f);

            yield return StartCoroutine(UpdateHealth(HealthType.HEAL));
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_perk_secondchance"], GetPrefix(false), GetAttacker().GetName(), PerkSystem.GetPerkName(GetAttacker().perk)), waitForInput: true));
        }

        if (GetAttacker().GetStatusEffect() == StatusEffect.BURN)
        {
            prevHP = GetAttacker().GetCurrentHP();

            LeanTween.color(GetAttackerImage().gameObject, new Color(0.882f, 0.392f, 0.082f), 0.15f);
            yield return new WaitForSeconds(0.2f);
            LeanTween.color(GetAttackerImage().gameObject, Color.white, 0.15f);
            yield return new WaitForSeconds(0.15f);

            GetAttacker().Hurt(enemyMon.GetHPStat() / 16);

            yield return StartCoroutine(UpdateHealth(HealthType.SELFHURT));
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_status_burn_hurt"], GetPrefix(false), GetAttacker().GetName())));

        } else if (GetAttacker().GetStatusEffect() == StatusEffect.CURSE && GetAttacker().GetSkillData(GetSkillUsed()).skillType == SkillType.MAGIC)
        {
            prevHP = GetAttacker().GetCurrentHP();

            LeanTween.color(GetAttackerImage().gameObject, new Color(0.792f, 0.306f, 0.878f), 0.15f);
            yield return new WaitForSeconds(0.2f);
            LeanTween.color(GetAttackerImage().gameObject, Color.white, 0.15f);
            yield return new WaitForSeconds(0.15f);

            GetAttacker().Hurt(enemyMon.GetHPStat() / 14);

            yield return StartCoroutine(UpdateHealth(HealthType.SELFHURT));
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_status_curse_hurt"], GetPrefix(false), GetAttacker().GetName())));
        } else if (GetAttacker().GetStatusEffect() == StatusEffect.TRAP && GetAttacker().GetSkillData(GetSkillUsed()).skillType == SkillType.STRENGTH)
        {
            prevHP = GetAttacker().GetCurrentHP();

            LeanTween.color(GetAttackerImage().gameObject, Color.gray, 0.15f);
            yield return new WaitForSeconds(0.2f);
            LeanTween.color(GetAttackerImage().gameObject, Color.white, 0.15f);
            yield return new WaitForSeconds(0.15f);

            GetAttacker().Hurt(enemyMon.GetHPStat() / 14);

            yield return StartCoroutine(UpdateHealth(HealthType.SELFHURT));
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_status_trap_hurt"], GetPrefix(false), GetAttacker().GetName())));
        }

        if (currAttacker == 0)
        {
            if (playerSkillUsed != -1 && playerSkillUsed != 5) playerCooldowns[playerSkillUsed] = 0;

            if (playerMon.GetSkill(5) != Skills.NONE)
            {
                playerSpecialSkillCharge = Mathf.Clamp(playerSpecialSkillCharge + 5, 0, 100);

                playerChargeBar.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(48 * (float)((float)playerSpecialSkillCharge / (float)playerMon.GetSpecialSkillData().cooldown)));

                if (playerSpecialSkillCharge != playerMon.GetSpecialSkillData().cooldown)
                {
                    skillsButtons[5].image.color = new Color(1, 1, 1, 0.5f);
                }
                else
                {
                    skillsButtons[5].image.color = new Color(1, 1, 1, 1f);
                }
            }

            PassCooldowns(currAttacker);
        }

        if (playerMon.GetCurrentHP() == 0 || enemyMon.GetCurrentHP() == 0)
        {
            yield return StartCoroutine(FaintCheck());
        } else
        {
            yield return StartCoroutine(NextPhase());
        }
    }

    public IEnumerator NextPhase()
    {
        AddLog("Beginning NextPhase()");

        if (firstMonHurt)
        {
            secMonHurt = true;
        }
        else
        {
            firstMonHurt = true;
        }

        prevAttacker = currAttacker;
        currAttacker = currAttacker == 0 ? 1 : 0;

        if (firstMonHurt && secMonHurt)
        {
            firstMonHurt = secMonHurt = false;

            currAttacker = prevAttacker = playerSkillUsed = enemySkillUsed = -1;
            
            turn++;

            yield return StartCoroutine(SelectAction());
        }
        else
        {
            yield return StartCoroutine(AttackProcess());
            //UpdateState(battleLogic.prevAttacker == 0 ? BattleState.OPPONENTTURN : BattleState.ACTIVETURN);
        }

    }

    public void PassCooldowns(int attacker)
    {
        // NEEDS SUPPORT FOR ENEMIES

        AddLog("Passing Cooldowns: " + attacker.ToString());

        for (int i = 0; i < 5; i++)
        {
            int maxSkillCooldown = GetMonster(attacker).GetSkillData(i).cooldown;

            if (playerCooldowns[i] != -1 && i != playerSkillUsed)
            {
                playerCooldowns[i]++;
            }

            if (playerCooldowns[i] == maxSkillCooldown)
            {
                playerCooldowns[i] = -1;
            }

            if (playerCooldowns[i] != -1)
            {
                skillsButtons[i].image.color = new Color(1, 1, 1, 0.5f);
                skillsCooldown[i].text = (playerCooldowns[i] == maxSkillCooldown ? "" : playerCooldowns[i] + "/") + maxSkillCooldown;
            }
            else
            {
                skillsButtons[i].image.color = new Color(1, 1, 1, 1f);
                skillsCooldown[i].text = maxSkillCooldown.ToString();
            }
        }
    }

    public IEnumerator FaintCheck()
    {
        AddLog("Beginning FaintCheck()");
        if (playerMon.GetCurrentHP() == 0)
        {
            playerMon.SetStatusEffect(StatusEffect.DEAD);
            StopCoroutine(NextPhase());

            playerMonsterImage.enabled = false;
            playerMonHealthBG.enabled = false;

            if (GameUtility.GetAliveMonsters() > 0)
            {
                // Open party to select new monster
                yield return StartCoroutine(ReplaceMon(true));
            }
            else
            {
                // Lose battle
                TransitionHandler tHand = TransitionHandler.instance;
                tHand.Fade();
                tHand.LoadScene(Maps.PLAYERLOT);
                tHand.Transition();
                yield break;
            }
        }
        else if (enemyMon.GetCurrentHP() == 0)
        {
            bool battleOver = false;

            if (isTamerBattle && battlingTamer.party[tamerMonIndex + 1] != null)
            {
                tamerMonIndex++;
                enemyMon = battlingTamer.party[tamerMonIndex];

                ResetEnemyValues();
                UpdateDisplays();

            } else
            {
                battleOver = true;
            }


            if (battleOver)
            {
                StopCoroutine(NextPhase());
                yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_tired"], GetPrefix(true), enemyMon.GetName())));

                // End Battle
                InterfaceSystem.wonLastBattle = true;

                yield return StartCoroutine(ResultsDisplay());

                yield return StartCoroutine(EndBattle());

            } else
            {
                firstMonHurt = true;
                secMonHurt = true;

                StartCoroutine(NextPhase());
            }

            
                /*float o = 0.3568628f;

                while (o > 0)
                {
                    o -= 0.025f;
                    enemyMonsterImage.color = new Color(enemyMonsterImage.color.r, enemyMonsterImage.color.g, enemyMonsterImage.color.b, o);
                    enemyShadow.color = new Color(enemyMonsterImage.color.r, enemyMonsterImage.color.g, enemyMonsterImage.color.b, o);
                    yield return new WaitForEndOfFrame();
                }*/

                /*enemyMonsterImage.enabled = false;
                enemyShadow.enabled = false;
                enemyHealthImage.enabled = false;*/


            /*yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_tame_wait"]));

            float tameChance = Random.Range(0f, 1f);
            int choiceIndex = -1;

            if (tameChance <= 0.7f && SaveSystem.invCharms.Count > 0)
            {
                yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_impressed"], GetPrefix(true), enemyMon.GetName()), waitForInput: true));
                yield return StartCoroutine(BattleMessage(SystemVariables.trans["battle_tame"]));

                ChoiceHandler ch = ChoiceHandler.Instance();

                ch.AddChoice(0, SystemVariables.trans["sys_yes"]);
                ch.AddChoice(1, SystemVariables.trans["sys_no"]);
                ch.SetPos(new Vector2(350, 96));
                ch.Show();

                yield return new WaitForChoice();

                choiceIndex = InterfaceSystem.choiceIndex;
            }

            if (choiceIndex == 0)
            {
                InterfaceSystem.OpenInventory(true, false, false);

                yield return new WaitForItemSelect();

                GameUtility.DisableBackground();

                Item selectedCharm = SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex];

                selectedCharm.currentQuantity--;

                if (selectedCharm.currentQuantity <= 0)
                {
                    SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex).RemoveAt(InterfaceSystem.temporaryItemIndex);
                }

                yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_tame_success"], SaveSystem.playerName, GetPrefix(true), enemyMon.GetName())));

                enemyMon.HealFull();
                enemyMon.tamedCharm = selectedCharm;
                GameUtility.ObtainMonster(enemyMon);
            } else
            {
                yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_tame_fail"], GetPrefix(true), enemyMon.GetName())));

                LeanTween.moveLocalX(enemyMonsterImage.gameObject, 9, 0.5f).setEaseInExpo();
                LeanTween.moveLocalX(enemyShadow.gameObject, 9, 0.5f).setEaseInExpo();
                enemyMonsterImage.transform.localScale = new Vector3(-1, 1);
                LeanTween.color(enemyMonsterImage.gameObject, new Color(1, 1, 1, 0), 0.4f);
                LeanTween.color(enemyShadow.gameObject, new Color(1, 1, 1, 0), 0.4f);


                float o = 0.3568628f;

                while (o > 0)
                {
                    o -= 0.025f;
                    enemyMonsterImage.color = new Color(enemyMonsterImage.color.r, enemyMonsterImage.color.g, enemyMonsterImage.color.b, o);
                    enemyShadow.color = new Color(enemyMonsterImage.color.r, enemyMonsterImage.color.g, enemyMonsterImage.color.b, o);
                    yield return new WaitForEndOfFrame();
                }
            }*/

            //yield return StartCoroutine(UpdateEXP());            
        }
    }

    public IEnumerator EndBattle()
    {        
        isBoss = false;
        TransitionHandler tHandle = TransitionHandler.instance;
        tHandle.Fade();
        tHandle.SetMidTransAction(() =>
        {
            Camera.main.GetComponent<CameraFollow>().isFollowing = true;
            container.SetActive(false);
            InterfaceSystem.inBattle = false;
            transform.parent.GetChild(0).gameObject.SetActive(false);
        });
        tHandle.Transition();

        yield break;
    }

    public IEnumerator UpdateEXP()
    {
        if (playerMon.GetMonLevel() == 100) yield break;
        float exp;// = enemyMon.GetStatTotal() * enemyMon.level * 1 / 9;

        exp = ((enemyMon.GetStatTotal() * 0.15f) * enemyMon.level) / 7;

        playerMon.GainEXP(exp, false);
        yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_exp"], playerMon.GetName(), exp.ToString())));

        // PERKCHECK: ENCHANT
        if (playerMon.perk == Perks.ENCHANT)
        {
            float extra = Mathf.Round(exp * 0.25f);
            playerMon.GainEXP(extra, false);
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_perk_enchant"], playerMon.GetName(), PerkSystem.GetPerkName(playerMon.perk), extra.ToString())));
        }
        
        while (playerMon.currentEXP >= playerMon.nextEXP)
        {
            LeanTween.size(playerChargeBar.rectTransform, new Vector2(60, 6), 0.4f).setEaseInOutQuart();

            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_level"], playerMon.GetName())));

            for (int i = 0; i < 6; i++)
            {
                oldStatValues[i].text = playerMon.GetStatValue((StatNames)i).ToString();
            }

            playerMon.LevelUp();

            for (int i = 0; i < 6; i++)
            {
                newStatValues[i].text = playerMon.GetStatValue((StatNames)i).ToString();
            }

            playerLevelText.text = string.Format("{0} Lv{1}", playerMon.GetGender(), playerMon.GetMonLevel());
            playerHealthText.text = playerMon.GetCurrentHP() + "/" + playerMon.GetHPStat();

            hpRect = playerMonHealthImage.GetComponent<RectTransform>();
            hpBGRect = playerMonHealthBG.GetComponent<RectTransform>();
            hpRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(120 * (float)((float)playerMon.GetCurrentHP() / (float)playerMon.GetHPStat())));
            hpBGRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(120 * (float)((float)playerMon.GetCurrentHP() / (float)playerMon.GetHPStat())));

            levelUpPanel.SetActive(true);

            yield return new WaitForSeconds(0.5f);

            while (!InterfaceSystem.rePlayer.GetButtonDown("A"))
            {
                yield return new WaitForEndOfFrame();
            }
            
            levelUpPanel.SetActive(false);

            yield return new WaitForSeconds(0.5f);

            LeanTween.size(playerChargeBar.rectTransform, new Vector2(0, 6), 0);
        }

        float width = Mathf.Floor(60 * (float)((float)playerMon.currentEXP / (float)playerMon.nextEXP));

        LeanTween.size(playerChargeBar.rectTransform, new Vector2(width, 6), 0.4f).setEaseInOutQuart();

        yield return new WaitForSeconds(1);
    }

    public IEnumerator ResultsDisplay()
    {
        monsToGiveExp[1] = true;

        AddLog("Drop check!");
        bool hasMon = false;

        float[] expTo = new float[4];


        // Initialize Party Images
        for (int i = 0; i < 4; i++)
        {
            hasMon = SaveSystem.playerParty.ElementAtOrDefault(i) != null;
            
            resultsPartyIcons[i].sprite = hasMon ? SaveSystem.playerParty[i].GetIcon() : null;
            resultsPartyIcons[i].color = hasMon ? Color.white : Color.clear;

            resultsXPBG[i].color = hasMon ? Color.black : Color.clear;

            resultsPartyLevels[i].text = hasMon ? "Lv {0}".With(SaveSystem.playerParty[i].GetMonLevel()) : "";

            resultsXPBar[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(38 * (float)((float)playerMon.GetCurrentEXP() / (float)playerMon.GetNextEXP())));
        }

        // Calculate Drops

        List<Item> itemDrops = new List<Item>();

        if (enemyMon.HasDrops())
        {
            float dropChance = Random.Range(0f, 1f);
            float dropQuantityRate = 1f;
            float dropRate = enemyMon.GetData().dropRate;

            if (dropChance <= enemyMon.GetData().dropRate)
            {
                float dropQuantityChance = Random.Range(0f, 1f);

                while (dropQuantityChance <= dropQuantityRate)
                {
                    bool addedItem = false;

                    while (!addedItem)
                    {
                        float chance = Random.Range(0f, 1f);

                        if (chance <= 0.5f && enemyMon.HasCommonDrops())
                        {
                            int commIndex = Random.Range(0, enemyMon.GetCommonDrops().Count);
                            itemDrops.Add(new Item(enemyMon.GetCommonDrops()[commIndex]));
                            addedItem = true;

                        }
                        else if (chance > 0.5 && chance <= 0.8f && enemyMon.HasUncommonDrops())
                        {
                            int uncommIndex = Random.Range(0, enemyMon.GetUncommonDrops().Count);
                            itemDrops.Add(new Item(enemyMon.GetUncommonDrops()[uncommIndex]));
                            addedItem = true;

                        }
                        else if (chance > 0.8f && enemyMon.HasRareDrops())
                        {
                            int rareIndex = Random.Range(0, enemyMon.GetRareDrops().Count);
                            itemDrops.Add(new Item(enemyMon.GetRareDrops()[rareIndex]));
                            addedItem = true;
                        }
                    }

                    dropQuantityRate *= 0.25f;
                }
            }

            itemDrops = itemDrops.GroupBy(i => i.id).Select(g => new Item(g.First().id) { currentQuantity = g.Sum(i => i.currentQuantity) }).ToList();

            for (int i = 0; i < itemDrops.Count; i++)
            {
                ItemSystem.ObtainItem(itemDrops[i], itemDrops[i].currentQuantity, displayMessage: false);
            }

        }

        for (int i = 0; i < resultsDrops.Length; i++)
        {
            if (i < itemDrops.Count)
            {
                resultsDrops[i].AssignItem(itemDrops[i]);
            }

            resultsDrops[i].UpdateDisplay();
        }

        resultsDropsTotalPages = (int)Mathf.Ceil(itemDrops.Count / 5);
        resultsDropsPageIndex = 0;
        resultsDropsPage.text = "{0}/{1}".With(resultsDropsPageIndex + 1, resultsDropsTotalPages + 1);

        if (resultsDropsPageIndex == resultsDropsTotalPages)
        {
            resultsDropsArrows[0].enabled = false;
            resultsDropsArrows[1].enabled = false;
        }

        // Initialize stat text
        StartCoroutine(ResultsUpdateStats(0, false));
        

        for (int i = 0; i < monsToGiveExp.Length; i++)
        {
            hasMon = SaveSystem.playerParty.ElementAtOrDefault(i) != null;

            for (int s = 0; s < 6; s++)
            {
                if (!hasMon) continue;
                beforeStats[i, s] = SaveSystem.playerParty[i].GetStatValue((StatNames)s);
            }

            if (!hasMon) continue;

            if (SaveSystem.playerParty[i].GetMonLevel() == 100 || !monsToGiveExp[i]) continue;

            float exp = enemyMon.GetStatTotal() * enemyMon.GetMonLevel() * 1 / 9;

            if (i != 0) exp *= 0.5f;

            SaveSystem.playerParty[i].GainEXP(exp, false);

            // PERKCHECK: ENCHANT
            if (SaveSystem.playerParty[i].perk == Perks.ENCHANT)
            {
                float extra = Mathf.Round(exp * 0.25f);
                SaveSystem.playerParty[i].GainEXP(extra, false);
            }
        }

        int gold = (int)(enemyMon.GetMonLevel() / 2 * Random.Range(9, 14) * (GetAttacker().perk == Perks.CASHCOW ? 1.65f : 1));
        SaveSystem.playerGold += gold;

        resultsGoldAmt.text = GameUtility.GetGold(gold);

        resultsContainer.SetActive(true);

        CanvasGroup cg = resultsContainer.GetComponent<CanvasGroup>();

        LeanTween.alphaCanvas(cg, 1, 0.1f);
        LeanTween.scale(resultsContainer, Vector3.one, 0.1f);

        yield return new WaitForSeconds(1);

        for (int i = 0; i < 4; i++)
        {
            hasMon = SaveSystem.playerParty.ElementAtOrDefault(i) != null;

            if (!hasMon) continue;

            while (SaveSystem.playerParty[i].GetCurrentEXP() >= playerMon.GetNextEXP())
            {
                LeanTween.size(resultsXPBar[i].rectTransform, new Vector2(38, 2), 0.3f).setEaseInOutQuart();

                yield return new WaitForSeconds(0.3f);

                RectTransform lup = resultsPartyLevelUp[i].GetComponent<RectTransform>();

                resultsPartyLevelUp[i].gameObject.SetActive(true);

                LeanTween.moveY(lup, 46f, 0.2f);

                SaveSystem.playerParty[i].LevelUp();

                resultsPartyLevels[i].text = "Lv {0}".With(SaveSystem.playerParty[i].GetMonLevel());

                resultsPartyParticles[i].Emit(10);

                LeanTween.color(resultsXPBG[i].rectTransform, Color.white, 0.1f).setRecursive(false);
                LeanTween.scale(resultsPartyIcons[i].rectTransform, new Vector3(1.2f, 1.2f, 1), 0.1f);
                yield return new WaitForSeconds(0.2f);

                LeanTween.size(resultsXPBar[i].rectTransform, new Vector2(0, 2), 0f);
                LeanTween.scale(resultsPartyIcons[i].rectTransform, Vector3.one, 0.1f);
                LeanTween.color(resultsXPBG[i].rectTransform, Color.black, 0.1f).setRecursive(false);
                yield return new WaitForSeconds(0.3f);
                
                if (i == 0) StartCoroutine(ResultsUpdateStats(0, true));

                yield return new WaitForSeconds(0.15f);
            }

            LeanTween.size(resultsXPBar[i].rectTransform, new Vector2(Mathf.Floor(38 * (float)((float)playerMon.GetCurrentEXP() / (float)playerMon.GetNextEXP())), 2), 0.3f).setEaseInOutQuart();
        }


        while (!InterfaceSystem.rePlayer.GetButtonDown("A"))
        {
            yield return new WaitForEndOfFrame();
        }

        yield return null;
    }

    public IEnumerator ResultsUpdateStats(int index, bool anim)
    {
        for (int i = 0; i < 6; i++)
        {
            int after = SaveSystem.playerParty[0].GetStatValue((StatNames)i);

            resultsPartyStatsBefore[i].text = anim? beforeStats[index, i].ToString() : after.ToString();

            if (after <= beforeStats[index, i] || !anim)
            {
                resultsPartyStatsArrow[i].enabled = false;
                resultsPartyStatsAfter[i].enabled = false;
            }
            else if (anim)
            {
                resultsPartyStatsAfter[i].text = after.ToString();
                resultsPartyStatsArrow[i].enabled = true;

                float xx = resultsPartyStatsArrow[i].rectTransform.anchoredPosition.x;

                LeanTween.moveX(resultsPartyStatsArrow[i].rectTransform, xx + 5, 0.1f).setEaseOutQuart().setLoopPingPong(1).setDelay(0.02f * i);
            }
        }

        yield break;
    }

    public IEnumerator ProcessDamage()
    {
        AddLog("Beginning ProcessDamage()");
        isCurrentSkillCritical = false;

        currDMG = CalcDamage(GetAttacker().GetSkillData(GetSkillUsed()).attack);
        
        // SKILLCHECK: MINDBEND
        if (GetAttacker().GetSkill(GetSkillUsed()) == Skills.MINDBEND)
        {
            currDMG = CalcDamage(GetAttacker().GetSkillData(GetSkillUsed()).attack, GetOpposing().GetStatValue(StatNames.STRENGTH));
        }

        bool hasAccessory = GetOpposing().accessory != null ? GetOpposing().accessory.id != Items.NONE : false;

        if (hasAccessory && GetOpposing().accessory.GetData().holdFunction == ItemEffects_Hold.DAMAGECALC)
        {
            switch (GetOpposing().accessory.id)
            {
                // ITEMCHECK: DAMAGEDSHIELD
                case Items.DAMAGEDSHIELD:
                    currDMG = Mathf.RoundToInt(currDMG * 0.5f);
                    break;
                default:
                    break;
            }

            if (currAttacker == 0)
            {
                enemyAccessoryUsed = true;
                enemyAccessoryItem = GetOpposing().accessory.id;
            } else
            {
                playerAccessoryUsed = true;
                playerAccessoryItem = GetOpposing().accessory.id;
            }
        }

        // Critical Check
        if (Random.Range(0f, 1f) <= 0.05f)
        {
            isCurrentSkillCritical = true;
            currDMG = Mathf.RoundToInt((float)currDMG * 1.5f);
        }

        if (currDMG < 1) currDMG = 1;
        
        yield break;

        //yield return StartCoroutine(BattleMessage(string.Format("{0} used {1}!", GetAttacker().GetName(), SystemVariables.skillNames[GetAttacker().monSkills[currAttacker == 0 ? playerSkillUsed : enemySkillUsed].id.ToString()])));
    }

    public IEnumerator HurtMonster()
    {
        prevHP = GetOpposing().GetCurrentHP();

        GetOpposing().Hurt(currDMG);

        AddLog(GetOpposing().GetName() + " hurt: " + GetOpposing().GetCurrentHP() + "/" + GetOpposing().GetHPStat() + "HP");

        yield break;
    }

    #region Animation

    public IEnumerator Anim(string animID = "")
    {
        GameObject loadedAnim = Resources.Load<GameObject>("BattleAnims/" + (animID.Length != 0? animID : "Anim_" + GetAttacker().GetSkillData(GetSkillUsed()).id.ToString().ToUpper()));

        if (loadedAnim == null) loadedAnim = Resources.Load<GameObject>("BattleAnims/DEBUG");

        GameObject anim = Instantiate(loadedAnim, prefab.transform.position, Quaternion.identity);
        BattleAnimations animComp = anim.GetComponent<BattleAnimations>();

        yield return StartCoroutine(animComp.LoadAnim(this));

        yield return StartCoroutine(animComp.AnimProcess());

        yield break;
    }

    public IEnumerator PlayAnim(int attacker = -1, bool shakeOnImpact = true, string animSuffix = "", float pauseLength = 0.1f)
    {
        if (attacker == -1) attacker = GetOpposingIndex();

        GameObject loadedAnim = Resources.Load<GameObject>("BattleAnims/Anim_" + GetAttacker().GetSkillData(GetSkillUsed()).id.ToString().ToUpper() + (animSuffix.Length != 0 ? animSuffix : ""));

        if (loadedAnim == null) loadedAnim = Resources.Load<GameObject>("BattleAnims/DEBUG");

        GameObject anim = Instantiate(loadedAnim, GetMonsterImage(attacker).transform.position, Quaternion.identity);
        anim.transform.localScale = new Vector3(-GetMonsterImage(attacker).transform.localScale.x, 1, 1);
        BattleAnimations animComp = anim.GetComponent<BattleAnimations>();
        /*animComp.playHurtAnim = shakeOnImpact;
        animComp.monIndex = attacker;
        animComp.isRanged = GetAttacker().GetSkillData(GetSkillUsed()).ranged;
        */
        yield return StartCoroutine(animComp.LoadAnim(this));

        yield return new WaitForSeconds(pauseLength);
    }

    public IEnumerator AnimShake(int attacker, float waitTime = 0.025f)
    {
        GameObject monS = GetMonsterImage(attacker).gameObject;
        int mult = attacker == 1 ? 1 : -1;
        //mult *= isRanged ? -1 : 1;

        LeanTween.moveLocalX(monS, 7 * mult + (0.5f * mult), waitTime);
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveLocalX(monS, 7 * mult, waitTime);
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveLocalX(monS, 7 * mult - (0.5f * mult), waitTime);
        yield return new WaitForSeconds(waitTime);

        LeanTween.moveLocalX(monS, 7 * mult, waitTime);
        yield return new WaitForSeconds(waitTime);
    }

    public IEnumerator AnimDarkenBG(Color _to, float _time)
    {
        GameObject bg = prefab.transform.GetChild(0).gameObject;
        
        LeanTween.color(bg, _to, _time);
        yield return new WaitForSeconds(_time);
    }

    // To be called as StartCoroutine, do not yield
    public IEnumerator AnimFlicker(int attacker = -1)
    {
        if (attacker == -1) attacker = GetOpposingIndex();

        SpriteRenderer monS = GetMonsterImage(attacker);

        for (int i = 0; i < 3; i++)
        {
            GameUtility.ChangeSpriteAlpha(monS, 0);

            yield return new WaitForSeconds(0.1f);

            GameUtility.ChangeSpriteAlpha(monS, 1);

            yield return new WaitForSeconds(0.15f);
        }
    }

    public IEnumerator AnimMotion()
    {
        int goTo = currAttacker == 0 ? 4 : -4;
        int goToRanged = currAttacker == 0 ? -4 : 4;

        int returnTo = currAttacker == 0 ? -7 : 7;

        if (!GetAttacker().GetSkillData(GetSkillUsed()).ranged)
        {
            LeanTween.moveLocalX(GetAttackerImage().gameObject, goTo, 0.5f).setEaseOutExpo();
            LeanTween.moveLocalX(GetAttackerShadow().gameObject, goTo, 0.5f).setEaseOutExpo();
            yield return new WaitForSeconds(0.5f);
        } else
        {
            LeanTween.moveLocalX(GetAttackerImage().gameObject, goToRanged, 0.25f).setEaseOutExpo();
            LeanTween.moveLocalX(GetAttackerShadow().gameObject, goToRanged, 0.25f).setEaseOutExpo();
            yield return new WaitForSeconds(0.25f);

            LeanTween.moveLocalX(GetAttackerImage().gameObject, returnTo, 0.1f).setEaseInExpo();
            LeanTween.moveLocalX(GetAttackerShadow().gameObject, returnTo, 0.1f).setEaseInExpo();
            yield return new WaitForSeconds(0.1f);
        }
    }

    public IEnumerator Anim_MotionClose(int attacker = -1)
    {
        int tempAttacker = attacker == -1 ? currAttacker : attacker;
        int goTo = tempAttacker == 0 ? 4 : -4;

        LeanTween.moveLocalX(GetMonsterImage(tempAttacker).gameObject, goTo, 0.5f).setEaseOutExpo();
        LeanTween.moveLocalX(GetMonsterShadow(tempAttacker).gameObject, goTo, 0.5f).setEaseOutExpo();
        yield return new WaitForSeconds(0.5f);
    }

    public IEnumerator Anim_MotionToss(int attacker = -1)
    {
        int tempAttacker = attacker == -1 ? currAttacker : attacker;
        int goTo = tempAttacker == 0 ? -4 : 4;
        int returnTo = tempAttacker == 0 ? -7 : 7;

        LeanTween.moveLocalX(GetMonsterImage(tempAttacker).gameObject, goTo, 0.25f).setEaseOutExpo();
        LeanTween.moveLocalX(GetMonsterShadow(tempAttacker).gameObject, goTo, 0.25f).setEaseOutExpo();
        yield return new WaitForSeconds(0.25f);

        LeanTween.moveLocalX(GetMonsterImage(tempAttacker).gameObject, returnTo, 0.1f).setEaseInExpo();
        LeanTween.moveLocalX(GetMonsterShadow(tempAttacker).gameObject, returnTo, 0.1f).setEaseInExpo();
        yield return new WaitForSeconds(0.1f);
    }

    public IEnumerator Anim_MotionReturn(int attacker = -1)
    {
        int tempAttacker = attacker == -1 ? currAttacker : attacker;
        int returnTo = tempAttacker == 0 ? -7 : 7;

        LeanTween.moveLocalX(GetMonsterImage(tempAttacker).gameObject, returnTo, 0.5f).setEaseOutSine();
        LeanTween.moveLocalX(GetMonsterShadow(tempAttacker).gameObject, returnTo, 0.5f).setEaseOutSine();
        yield return new WaitForSeconds(0.5f);
    }

    public IEnumerator Anim_Jump(int attacker, int jumpHeight = 1)
    {
        float startY = GetMonsterImage(attacker).transform.localPosition.y;
        float yy = startY + jumpHeight;
        float transitionTime = jumpHeight * 0.15f;

        LeanTween.moveLocalY(GetMonsterImage(attacker).gameObject, yy, transitionTime).setEaseOutExpo();
        yield return new WaitForSeconds(transitionTime);

        LeanTween.moveLocalY(GetMonsterImage(attacker).gameObject, startY, transitionTime).setEaseInExpo();
        yield return new WaitForSeconds(transitionTime);
    }

    public IEnumerator Anim_Squash(int attacker, float squashAmt)
    {
        float multi = Mathf.Sign(GetMonsterImage(attacker).transform.localScale.x);

        LeanTween.scale(GetMonsterImage(attacker).gameObject, new Vector3((1 + squashAmt) * multi, 1 - squashAmt, 1), 0.25f);
        yield return new WaitForSeconds(0.25f);

        LeanTween.scale(GetMonsterImage(attacker).gameObject, new Vector3(1 * multi, 1, 1), 0.25f);
        yield return new WaitForSeconds(0.25f);
    }

    public IEnumerator Anim_Stretch(int attacker, float stretchAmt)
    {
        float multi = Mathf.Sign(GetMonsterImage(attacker).transform.localScale.x);

        LeanTween.scale(GetMonsterImage(attacker).gameObject, new Vector3((1 - stretchAmt) * multi, 1 + stretchAmt, 1), 0.25f);
        yield return new WaitForSeconds(0.25f);

        LeanTween.scale(GetMonsterImage(attacker).gameObject, new Vector3(1 * multi, 1, 1), 0.25f);
        yield return new WaitForSeconds(0.25f);
    }

    public IEnumerator AnimMotionAfter()
    {
        int multiplier = (int)(Mathf.Sign(currAttacker) * -1);

        int returnTo = currAttacker == 0 ? -7 : 7;

        if (!GetAttacker().GetSkillData(GetSkillUsed()).ranged)
        {
            LeanTween.moveLocalX(GetAttackerImage().gameObject, returnTo, 0.5f).setEaseOutSine();
            LeanTween.moveLocalX(GetAttackerShadow().gameObject, returnTo, 0.5f).setEaseOutSine();
            yield return new WaitForSeconds(0.5f);
        }
    }

    #endregion

    public IEnumerator ContactCheck()
    {
        // PERKCHECK: REDHOT
        if (GetOpposing().perk == Perks.REDHOT && GetAttacker().SetStatusEffect(StatusEffect.BURN))
        {
            yield return StartCoroutine(BattleMessage(string.Format(SystemVariables.trans["battle_perk_redhot"], GetPrefix(false), GetAttacker().GetName(), GetPrefix(true), GetOpposing().GetName(), PerkSystem.GetPerkName(GetAttacker().perk))));
            UpdateStatusEffects();
        }
    }

    public int CalcDamage(int _pwr, int _str = -1)
    {
        AddLog("Beginning Calc Damage");

        float str = 1;
        float def = 1;
        float lvl = 1;
        float pwr = 1;
        float multi = 1;

        if (currAttacker == 1) // Opponent attacking Player
        {
            SkillData oS = enemyMon.GetSkillData(enemySkillUsed);

            str = enemyMon.GetStatValue(oS.skillType == SkillType.STRENGTH ? StatNames.STRENGTH : StatNames.MAGIC);
            def = playerMon.GetStatValue(oS.skillType == SkillType.STRENGTH ? StatNames.DEFENSE : StatNames.RESISTANCE);
            lvl = enemyMon.GetMonLevel();
            pwr = oS.attack;

        }
        else if (currAttacker == 0) // Player attacking Opponent
        {
            SkillData s = playerMon.GetSkillData(playerSkillUsed);

            str = playerMon.GetStatValue(s.skillType == SkillType.STRENGTH ? StatNames.STRENGTH : StatNames.MAGIC);
            def = enemyMon.GetStatValue(s.skillType == SkillType.STRENGTH ? StatNames.DEFENSE : StatNames.RESISTANCE);
            lvl = playerMon.GetMonLevel();
            pwr = s.attack;

            if (TypeSystem.IsResistedBy(playerMon.GetSkillData(playerSkillUsed).attribute, enemyMon.GetType()))
            {
                multi *= 0.5f;
            }
            if (enemyMon.GetType(1) != MonsterAttribute.NONE)
            {
                if (TypeSystem.IsResistedBy(playerMon.GetSkillData(playerSkillUsed).attribute, enemyMon.GetType(1)))
                {
                    multi *= 0.5f;
                }
            }

            if (TypeSystem.IsStrongAgainst(playerMon.GetSkillData(playerSkillUsed).attribute, enemyMon.GetType()))
            {
                multi *= 2;
            }
            if (enemyMon.GetType(1) != MonsterAttribute.NONE)
            {
                if (TypeSystem.IsStrongAgainst(playerMon.GetSkillData(playerSkillUsed).attribute, enemyMon.GetType(1)))
                {
                    multi *= 2;
                }
            }
        }

        if (_str != -1)
        {
            str = _str;
        }

        if (Random.Range(0f, 1f) <= 0.05f)
        {
            multi *= 1.15f;
        }
        
        int dmg = (int)Mathf.Ceil(((((2 * lvl)/10 * (str / def) * pwr)/ 50) + 2) * multi);

        AddLog(string.Format("DMG: {0} [2 + Lv{1}/5 * (STR{2} / DEF{3}) * P{4} * 2 / 50 * M{5}]", dmg, lvl, str, def, pwr, multi));

        //(40/5 * 43/27 * 40) * 2/50
        return dmg;
    }

    public bool AccuracyCheck()
    {
        AddLog("Beginning Accuracy Check");
        float a = GetAccuracy(currAttacker);

        if (GetAttacker().statEffect == StatusEffect.DROWSY) a *= 0.7f;

        AddLog("Acc: " + a);

        return Random.Range(0f, 1f) <= a;
    }

    public void OnSkillSelect(int _skillIndex)
    {
        if (!isActionSelected) return;

        if (_skillIndex == 5)
        {
            if (playerSpecialSkillCharge != playerMon.GetSpecialSkillData().cooldown)
            {
                LeanTween.moveX(skillsButtons[_skillIndex].gameObject, 110, 0.2f).setFrom(90).setEaseOutElastic();
                return;
            }
        } else
        {
            if (playerCooldowns[_skillIndex] != -1)
            {
                LeanTween.moveX(skillsButtons[_skillIndex].gameObject, 110, 0.2f).setFrom(90).setEaseOutElastic();
                return;
            }
        }

        backButton.gameObject.SetActive(false);
        skillsContainer.SetActive(false);

        playerSkillUsed = _skillIndex;

        //inpMan.controllerCursor.transform.SetAsLastSibling();
        InterfaceInputManager.instance.EnableCursor(false);
        
        StartCoroutine(BeginAttackTurn());
    }

    public void UpdateAction(int _state)
    {
        if (actionButtonsTweening) return;

        selectedAction = (ActionSelected)_state;
        isActionSelected = true;
    }

    public IEnumerator BattleMessage(string _msg, float pause = 0.5f, bool waitForInput = false, bool readMessage = true, UnityAction _action = null)
    {
        messagePlayed = true;
        
        messageText.text = _msg;
        messageText.Rebuild();

        if (!readMessage) messageText.SkipToEnd();

        while (messageText.reading)
        {
            yield return new WaitForEndOfFrame();
        }

        messageCursor.enabled = true;

        float waitTime = 0f;
        
        if (waitForInput)
        {
            while (!InterfaceSystem.rePlayer.GetButtonDown("A"))
            {
                yield return new WaitForEndOfFrame();
            }
        } else
        {
            while (waitTime < 2f)
            {
                if (InterfaceSystem.rePlayer.GetButtonDown("A")) waitTime = 2;

                waitTime += 2 * Time.deltaTime;

                yield return new WaitForEndOfFrame();
            }
        }

        messageCursor.enabled = false;

        if (_action != null) _action.Invoke();
    }

    public void SetOpponent(Monster mon)
    {
        enemyMon = mon;
    }

    public Monster GetAttacker() => currAttacker == 0 ? playerMon : enemyMon;
    public Monster GetOpposing() => currAttacker == 1 ? playerMon : enemyMon;
    public Monster GetMonster(int attacker) => attacker == 0 ? playerMon : enemyMon;
    public int GetSkillUsed() => currAttacker == 0 ? playerSkillUsed : enemySkillUsed;
    public int GetOpposingSkill() => currAttacker == 0 ? enemySkillUsed : playerSkillUsed;
    public int GetSleepCounter() => currAttacker == 0 ? playerSleepTimer : enemySleepTimer;
    public SpriteRenderer GetAttackerImage() => currAttacker == 0 ? playerMonsterImage : enemyMonsterImage;
    public SpriteRenderer GetOpposingImage() => currAttacker == 0 ? enemyMonsterImage : playerMonsterImage;
    public SpriteRenderer GetMonsterImage(int attacker) => attacker == 1 ? enemyMonsterImage : playerMonsterImage;
    public SpriteRenderer GetMonsterShadow(int attacker) => attacker == 1 ? enemyShadow : playerShadow;
    public SpriteRenderer GetAttackerShadow() => currAttacker == 0 ? playerShadow : enemyShadow;
    public SpriteRenderer GetOpposingShadow() => currAttacker == 1 ? playerShadow : enemyShadow;

    public string GetPrefix(bool opposing)
    {
        bool returnVal = currAttacker == 0 ? opposing ? true : false : opposing ? false : true;

        return returnVal ? SystemVariables.trans[isTamerBattle ? "battle_prefix_enemy" : "battle_prefix_wild"] : "";
    }

    public int GetOpposingIndex() => currAttacker == 0 ? 1 : 0;

    public bool LowerOpposingStat(int stageCount, int stat)
    {
        return (currAttacker == 0 ? LowerEnemyStat(stageCount, stat) : LowerPlayerStat(stageCount, stat));
    }

    public bool LowerEnemyStat(int stageCount, int stat)
    {
        if (stat == 6)
        {
            enemyAccChanges = Mathf.Clamp(enemyAccChanges - stageCount, -10, 0);
            return true;
        } else {
            if (enemyStatChanges[stat] < 10)
            {
                enemyStatChanges[stat] = Mathf.Clamp(enemyStatChanges[stat] - stageCount, -10, 10);
                return true;
            }
        }
        return false;
    }

    public bool LowerPlayerStat(int stageCount, int stat)
    {
        if (stat == 6)
        {
            playerAccChanges = Mathf.Clamp(playerAccChanges - stageCount, -10, 0);
        } else {
            if (playerStatChanges[stat] < 10)
            {
                playerStatChanges[stat] = Mathf.Clamp(playerStatChanges[stat] - stageCount, -10, 10);
                return true;
            }
        }

        return false;
    }

    public void PassSleepCounter(int turns = 1)
    {
        if (currAttacker == 0)
        {
            playerSleepTimer -= 1;
        }
        else
        {
            enemySleepTimer -= 1;
        }
    }

    /*public void SetOppossingAccuracy(int stage)
    {
        float acc = currAttacker == 1 ? playerAccuracy : enemyAccuracy;

        acc -= 0.1f * stage;

        if (currAttacker == 1)
        {
            playerAccuracy = acc;
        }
        else
        {
            enemyAccuracy = acc;
        }
    }*/

    public int GetAttackerStat(int stat) => Mathf.FloorToInt(GetAttacker().GetStatValue((StatNames)stat) * ((currAttacker == 0 ? playerStatChanges : enemyStatChanges)[stat] * 1.25f));
    public int GetOpposingStat(int stat) => Mathf.FloorToInt(GetOpposing().GetStatValue((StatNames)stat) * ((currAttacker == 0 ? playerStatChanges : enemyStatChanges)[stat] * 1.25f));
    public float GetAccuracy(int _attacker) => Mathf.Clamp(_attacker == 0 ? playerAccuracy + (playerAccChanges* 0.075f) : enemyAccuracy + (enemyAccChanges* 0.075f), 0f, 1f);

    public void SkillsEnter(int ind)
    {
        LeanTween.size(skillsButtons[ind].GetComponent<RectTransform>(), new Vector2(240, 20), 0.1f);
    }

    public void SkillsExit(int ind)
    {
        LeanTween.size(skillsButtons[ind].GetComponent<RectTransform>(), new Vector2(220, 20), 0.1f);
    }

    #region Animations

    public void CloseAttack()
    {
        playerMonsterImage.color = Color.red;
    }

    #endregion

    public void OnGUI()
    {
        if (InterfaceSystem.inBattle && EditorApplication.isPlaying && displayDebug)
        {
            //scrollPos = GUI.BeginScrollView(new Rect(10, 10, 200, 400), scrollPos, new Rect(0, 0, 200, 600));

            debugString = GUI.TextArea(new Rect(10, 150, 200, 450), playerMon.ToDebugString());
            debugString = GUI.TextArea(new Rect(1070, 150, 200, 450), enemyMon.ToDebugString());
            
            //GUI.EndScrollView();
        }
    }

}


public class BattleSkills
{
    BattleLogic bLogic;
    public string[] msgArray;

    public BattleSkills(BattleLogic _logic)
    {
        bLogic = _logic;
    }

    public void UseSkillEffect(string _effect)
    {
        //AddLog("Using Skill Effect");
        Type thisType = this.GetType();
        MethodInfo theMethod = thisType.GetMethod("Skill_" + _effect.ToUpper());

        theMethod.Invoke(this, null);
    }

    public void Skill_HALFHP()
    {
        bLogic.prevHP = bLogic.GetOpposing().GetCurrentHP();

        int healAmt = bLogic.currDmg / 2;
        bLogic.GetAttacker().Heal(healAmt);

        msgArray = new string[] { string.Format("{0} drained {1} HP from {2}!", bLogic.GetAttacker().GetName(), healAmt.ToString(), bLogic.GetOpposing().GetName()) };
        bLogic.bHandle.UpdateHealthBar(1, false);
    }

    public void Skill_RECOIL()
    {
        bLogic.prevHP = bLogic.GetOpposing().GetCurrentHP();

        bLogic.GetAttacker().Hurt(bLogic.currDmg * 0.1f);

        msgArray = new string[] { string.Format("{0} hurt itself!", bLogic.GetAttacker().GetName()) };
        bLogic.bHandle.UpdateHealthBar(2, false);
    }

    public void Skill_DEFLECT()
    {
        if (bLogic.GetOpposing().GetSkillData(bLogic.GetOpposingSkill()).attack > 0)
        {
            if (Random.Range(0, 0.7f) <= 0.7f)
            {
                msgArray = new string[] { string.Format("{0} deflected {1}'s attack!", bLogic.GetAttacker().GetName(), bLogic.GetOpposing().GetName()) };
                bLogic.bHandle.StartCoroutine(bLogic.bHandle.BattleMessage(msgArray, 2, () => {
                    bLogic.firstMonHurt = true;
                    bLogic.bHandle.NextPhase();
                }));
            }
        }
        else
        {
            msgArray = new string[] { string.Format("It failed!") };
        }
    }

    public void Skill_ACCURACY()
    {
        bLogic.SetOppossingAccuracy(1);

        msgArray = new string[] { string.Format("{0}'s accuracy was lowered!", bLogic.GetOpposing().GetName()) };

        bLogic.bHandle.StartCoroutine(bLogic.bHandle.BattleMessage(msgArray, 2, () => {
            bLogic.bHandle.NextPhase();
        }));

        //AddLog(bLogic.GetOpposing().GetName() + ", " + bLogic.opponentAccuracy);
    }
}

public enum HealthType
{
    HURT,
    HEAL,
    SELFHURT
}