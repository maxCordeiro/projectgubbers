﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MonsterSystem;

public class BattleAnim_SPICYSNEEZE : BattleAnimations
{
    public override void Start()
    {
        ChangeOrientation(bnh.currAttacker);
        ChangePosition(bnh.currAttacker);
    }

    public override IEnumerator AnimProcess()
    {
        StartCoroutine(bnh.Anim_MotionToss(bnh.currAttacker));

        yield return StartCoroutine(PlayAnim());
    }
}