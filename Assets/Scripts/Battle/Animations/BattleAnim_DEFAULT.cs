﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MonsterSystem;

public class BattleAnim_DEFAULT : BattleAnimations
{
    public override IEnumerator AnimProcess()
    {
        yield return StartCoroutine(bnh.Anim_MotionClose(bnh.currAttacker));

        yield return StartCoroutine(PlayAnim());

        yield return StartCoroutine(bnh.Anim_MotionReturn(bnh.currAttacker));
    }
}
