﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;
using UnityEngine.Events;
using System.Reflection;
using System;
using Random = UnityEngine.Random;
using UnityEngine.EventSystems;
using System.Linq;
using UnityEditor;

public class BattleUnit
{
    public Monster mon;

    public int[] skillCooldowns;
    public int[] statMultipliers;

    public float accuracy;
    public int accuracyMultiplier;

    public Image monImage;
    public Image monShadowImage;

    //public BattleUnitHUD hud;

    public bool turnPassed;
    public bool isActive;

    public int skillUsed, lastSkillUsed;
    public int sleepTimer;

    public int specialSkillCharge; //Only to be used by Player Unit

    public BattleUnit AssignMonster(Monster _unitMon)
    {
        mon = _unitMon;
        return this;
    }

    public BattleUnit ResetUnit()
    {
        skillCooldowns = new int[] { -1, -1, -1, -1, -1 };
        statMultipliers = new int[] { 0, 0, 0, 0, 0, 0 };

        lastSkillUsed = -1;
        sleepTimer = -1;

        turnPassed = false;
        isActive = false;

        accuracy = 1f;
        accuracyMultiplier = 0;
        return this;
    }
}

public class BattleUnitHUD
{
    public GameObject hpContainer;

    public Image hpBG, hpBar;
    public SuperTextMesh monName, monLevel, monGender;

    public Image specialBG, specialBar;
}