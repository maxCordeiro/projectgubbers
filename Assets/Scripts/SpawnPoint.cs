﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class SpawnPoint : MonoBehaviour
{
    public GameObject monSpawnPrefab;

    Camera cam;
    public bool hasSpawned, canSpawn, timerSet, fromBattle;
    public float waitTime, currentTime, battleDelayTime;
    
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        if (hasSpawned || InterfaceSystem.IsNotFree()) return;

        canSpawn = GameUtility.InView(gameObject, cam);

        if (canSpawn)
        {
            if (timerSet)
            {
                battleDelayTime -= Time.deltaTime;
                waitTime -= Time.deltaTime;

                if (fromBattle)
                {
                    if (battleDelayTime <= 0)
                    {
                        hasSpawned = true;
                        MonsterSpawn m = Instantiate(monSpawnPrefab, transform.position, Quaternion.identity).GetComponent<MonsterSpawn>();
                        //m.spawn = this;
                        fromBattle = false;
                    }
                } else
                {
                    if (waitTime <= 0)
                    {
                        hasSpawned = true;
                        MonsterSpawn m = Instantiate(monSpawnPrefab, transform.position, Quaternion.identity).GetComponent<MonsterSpawn>();
                        //m.spawn = this;
                    }
                }
            } else
            {
                battleDelayTime = Random.Range(7, 12);
                waitTime = Random.Range(2f, 8f);
                timerSet = true;
            }
        } else
        {
            timerSet = false;
            waitTime = 0;
            currentTime = 0;
        }
    }
}
