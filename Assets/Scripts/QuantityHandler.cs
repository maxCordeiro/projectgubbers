﻿using MonsterSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class QuantityHandler : MonoBehaviour
{
    public Button upButton, downButton;
    
    InterfaceInputManager inpMan;
    RectTransform rectT;

    public int min, max, current;

    bool beginShow, beginHide, doneShow, doneHide, closeMessage;

    SuperTextMesh quantityText;
    
    public float xx, yy;

    bool rememberCursorPos;
    Vector2 prevCursorPos;

    float currentTime, timeMax;
    bool timer;

    private void Start()
    {
        inpMan = FindObjectOfType<InterfaceInputManager>();
        transform.localScale = new Vector3(0, 0, 0);
        rectT = GetComponent<RectTransform>();
        quantityText = transform.GetChild(0).GetComponent<SuperTextMesh>();
        timeMax = 0.15f;
    }

    public QuantityHandler SetRange(int _min = 0, int _max = 1)
    {
        min = _min;
        max = _max;
        return this;
    }

    public void CloseMessage()
    {
        closeMessage = true;
    }

    public void SetPos(Vector2 pos)
    {
        GetComponent<RectTransform>().anchoredPosition = pos;
    }

    public void Show()
    {
        StartCoroutine(ShowCoroutine());
    }
    
    public void Hide()
    {
        doneHide = false;
        beginHide = true;
        
        //inpMan.EnableCursor(keepCursor);
        inpMan.es.SetSelectedGameObject(null);

        if (closeMessage)
        {
            DialogueHandler d = DialogueHandler.instance;
            d.Hide();
        }

        quantityText.gameObject.SetActive(false);
        upButton.gameObject.SetActive(false);
        downButton.gameObject.SetActive(false);
        GetComponent<Image>().enabled = false;

        InterfaceSystem.temporaryQuantity = current;
        InterfaceSystem.quantitySelected = true;
        
        doneHide = true;
        beginHide = false;
        
        InterfaceSystem.quantitySelected = false;
        InterfaceSystem.inQuantity = false;
        //InterfaceSystem.isInteracting = false;
    }

    public void Update()
    {
        if (!InterfaceSystem.inQuantity) return;
        if (SaveSystem.settings_controllerEnabled && inpMan.es.currentSelectedGameObject == null) return;

        if (SaveSystem.settings_controllerEnabled)
        {
            inpMan.controllerCursor.transform.localPosition = new Vector2(transform.localPosition.x + inpMan.es.currentSelectedGameObject.transform.localPosition.x - 54, transform.localPosition.y + inpMan.es.currentSelectedGameObject.transform.localPosition.y);
        }

        if (InterfaceSystem.rePlayer.GetAxisRaw("Vertical") != 0 || InterfaceSystem.rePlayer.GetAxis("Horizontal") != 0)
        {
            timer = true;
        } else
        {
            timer = false;
        }

        if (timer)
        {
            currentTime += Time.deltaTime;
            if (currentTime >= timeMax)
            {
                if (InterfaceSystem.rePlayer.GetAxisRaw("Vertical") < 0)
                {
                    current = Mathf.Clamp(current - 1, min, max);
                    quantityText.text = current.ToString();
                }
                else if (InterfaceSystem.rePlayer.GetAxisRaw("Vertical") > 0)
                {
                    current = Mathf.Clamp(current + 1, min, max);
                } else if (InterfaceSystem.rePlayer.GetAxis("Horizontal") < 0)
                {
                    current = Mathf.Clamp(current - 10, min, max);
                }
                else if (InterfaceSystem.rePlayer.GetAxis("Horizontal") > 0)
                {
                    current = Mathf.Clamp(current + 10, min, max);
                }

                quantityText.text = current.ToString();
                currentTime = 0;
            }
        }

        if (InterfaceSystem.rePlayer.GetButtonDown("A"))
        {
            Hide();
        }
    }

    IEnumerator ShowCoroutine()
    {
        InterfaceSystem.inQuantity = true; // Begin quantity

        //InterfaceSystem.isInteracting = true;
        InterfaceSystem.temporaryQuantity = -1;

        while (!doneHide && beginHide)
        {
            yield return null;
        }

        quantityText.text = min.ToString();

        yield return new WaitForFixedUpdate();

        Vector2 pos = inpMan.controllerCursor.transform.localPosition;

        quantityText.gameObject.SetActive(true);
        upButton.gameObject.SetActive(true);
        downButton.gameObject.SetActive(true);
        GetComponent<Image>().enabled = true;

        if (!SaveSystem.settings_controllerEnabled)
        {
            //rectT.anchorMin = new Vector2(0, 1);
            //rectT.anchorMax = new Vector2(0, 1);
            //rectT.pivot = new Vector2(0, 1);

            Vector2 curPos = new Vector2(pos.x + xx, pos.y + yy);

            transform.localPosition = rememberCursorPos ? prevCursorPos : curPos;
            prevCursorPos = curPos;

        }
        else
        {
            //rectT.anchorMin = new Vector2(0, 0);
            //rectT.anchorMax = new Vector2(0, 0);
            //rectT.pivot = new Vector2(0, 0);
        }

        transform.localScale = new Vector3(1, 1, 1);
        transform.SetAsLastSibling();

        //inpMan.es.SetSelectedGameObject(transform.GetChild(0).gameObject);

        //inpMan.EnableCursor(true);
        //inpMan.controllerCursor.transform.localPosition = new Vector2(32, inpMan.es.currentSelectedGameObject.transform.localPosition.y - 82);
        //inpMan.controllerCursor.transform.SetAsLastSibling();

        yield break;
    }

    private static QuantityHandler _instance;
    public static QuantityHandler instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(QuantityHandler)) as QuantityHandler;
                if (!_instance)
                    Debug.Log("There need to be at least one active QuantityHandler on the scene");
            }

            return _instance;
        }
    }

}
