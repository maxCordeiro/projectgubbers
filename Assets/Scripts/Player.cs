﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using Prime31;
using Rewired;
using GameDatabase;
using UnityEditor;
using System.IO;
using System;

public class Player : OverworldObject {

    CharacterController2D charContr;

    public SpriteRenderer spriteRenderer, shadowSpriteR;
    
    [Header("Movement")]
    public Vector2 moveInput;
    public Vector2 velocity;
    public float moveSpeed;
    //public float baseMaxSpeed;
    //public float runMaxSpeed;
    //public float acceleration;
    //public bool isRunning;
    //float maxSpeed;
    
    public SuperTextMesh stm;

    //SpriteAnimation spAnim;
    PlayerAnimState animState;
    PlayerAnimState prevState;

    Material playerMat;

    bool canMove;

    public Queue<Vector3> playerTrail;
    public Vector3 lastMovement;
    BoxCollider2D box;
    ContactFilter2D filter;

    [Range(0, 11)]
    public int layerMaskTemp;

    public bool bridgeOn;
    float prevSpeed;

    private void Awake()
    {
        charContr = GetComponent<CharacterController2D>();
        box = GetComponent<BoxCollider2D>();

    }

    public override void Start () {

        base.Start();
        playerTrail = new Queue<Vector3>();
        
        spriteRenderer = transform.GetChild(0).GetComponent<SpriteRenderer>();
        shadowSpriteR = transform.GetChild(0).GetChild(0).GetComponent<SpriteRenderer>();
        playerMat = spriteRenderer.material;

        RelocatePlayer();

        SaveSystem.shopB = new List<Items> {
            Items.POTION,
            Items.POTIONHIGH,
            Items.POTIONULTRA,
            Items.REVIVE,
            Items.COOLINGLEAF,
            Items.BINDCURE,
            Items.SEALCURE,
            Items.PARALYZECURE,
            Items.CURSECURE,
            Items.DROWSYCURE
        };
        SaveSystem.shopA = new List<Items> {
            Items.DAMAGEDSHIELD,
            Items.REPEL,
            Items.NEUTRALCHARM,
            Items.NORMALBOOST,
            Items.NORMALCAPVOUCHER,
            Items.COMMONCAPVOUCHER
        };

        SaveSystem.playerTiers.Add(new TierInfo(Maps.OVERGROWNGRASSLANDS, 5, MapTier.D));

        LayerMask layer = LayerMask.GetMask("Interactable");
        filter = new ContactFilter2D();
        filter.SetLayerMask(layer);

        UpdateSpriteAnimation();
    }

    void Update()
    {
        if ((InterfaceSystem.rePlayer.GetButtonDown("Start")) &&
            !InterfaceSystem.inMenu && !InterfaceSystem.isInteracting &&
            !ChoiceHandler.instance.isOpen && !InterfaceSystem.inBattle &&
            !DialogueHandler.instance.isOpen && !ChoiceHandler.instance.isOpen &&
            ((QFSW.QC.QuantumConsole.Instance != null && !QFSW.QC.QuantumConsole.Instance.IsActive) || QFSW.QC.QuantumConsole.Instance == null))
        {
            InterfaceSystem.Pause();
        }

        ProcessInputs();

        if (Input.GetKeyDown(KeyCode.F9))
        {
            charContr.debugCollision = !charContr.debugCollision;
            //spriteRenderer.color = new Color(1, 1, 1, !charContr.debugCollision ? 1 : 0.5f);
        }

        if (!InterfaceSystem.IsNotFree())
        {
            canMove = true;
        } else
        {
            canMove = false;
        }
        
        if (!beingMoved) UpdateAnimation(velocity);

        InteractCheck();
    }

    void ProcessInputs()
    {
        moveInput = new Vector2(InterfaceSystem.rePlayer.GetAxisRaw("Horizontal"), InterfaceSystem.rePlayer.GetAxisRaw("Vertical"));
        moveInput.Normalize();// = Vector2.ClampMagnitude(moveDir, 1);

        velocity = moveInput * moveSpeed;// * speedModifier;
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            charContr.move(velocity * Time.deltaTime);
        }

        if (transform.position == lastMovement) return;

        lastMovement = transform.position;
        playerTrail.Enqueue(transform.position);

    }



    public void InteractCheck()
    {
        Vector3 startPos = box.bounds.center;
        Vector2 vecDir = Vector2.zero;

        switch (owDir)
        {
            case OverworldDir.DOWN:
                startPos.y -= box.bounds.extents.y;
                vecDir = Vector2.down;
                break;
            case OverworldDir.RIGHT:
                startPos.x += box.bounds.extents.x;
                vecDir = Vector2.right;
                break;
            case OverworldDir.UP:
                startPos.y += box.bounds.extents.y;
                vecDir = Vector2.up;
                break;
            case OverworldDir.LEFT:
                startPos.x -= box.bounds.extents.x;
                vecDir = Vector2.left;
                break;
        }

        RaycastHit2D hit = Physics2D.Raycast(startPos, vecDir, 0.2f, LayerMask.GetMask("Interactable"));

        Debug.DrawLine(startPos, startPos + (Vector3)vecDir * 0.2f, Color.magenta);

        if (hit.collider != null)
        {
            GameObject hitObj = hit.collider.gameObject;

            if (!InterfaceSystem.IsNotFree())
            {
                if (InterfaceSystem.rePlayer.GetButtonDown("A"))
                {
                    if (hitObj.GetComponent<NPC>())
                    {
                        StartCoroutine(hitObj.GetComponent<NPC>().InteractEvent());
                    }
                    else if (hitObj.GetComponent<Door>())
                    {
                        hitObj.GetComponent<Door>().Transfer();
                    }
                    else if (hitObj.GetComponent<ChestMonster>())
                    {
                        StartCoroutine(hitObj.GetComponent<ChestMonster>().DropItem());
                    }
                }
                /*else
                {
                    if (hitObj.GetComponent<Transfer>())
                    {
                        hitObj.GetComponent<Transfer>().Transfer();
                    }
                    else if (hitObj.GetComponent<MonsterSpawn>())
                    {
                        StartCoroutine(hitObj.GetComponent<MonsterSpawn>().BattleEnum());
                    } else if (hitObj.GetComponent<BridgeOnTrigger>())
                    {
                        if (!bridgeOn)
                        {
                            bridgeOn = true;
                            hitObj.GetComponentInParent<Bridge>().OnBridge();
                            spriteRenderer.sortingOrder = 20;
                            shadowSpriteR.enabled = false;
                            //shadowSpriteR.transform.localPosition = new Vector3(0, -2.125f);
                        }
                    } else if (hitObj.GetComponent<BridgeOffTrigger>())
                    {
                        if (bridgeOn)
                        {
                            bridgeOn = false;
                            hitObj.GetComponentInParent<Bridge>().OffBridge();
                            spriteRenderer.sortingOrder = 0;
                            shadowSpriteR.enabled = true;
                            //shadowSpriteR.transform.localPosition = new Vector3(0, -0.125f);
                        }
                    }
                }*/
            }
        }
    }
    
    public void UpdatePlayerSprite()
    {
        playerMat.SetFloat("_EnableEars", SaveSystem.playerEars? 0 : 1);
        
        playerMat.SetTexture("_Bottom", Resources.Load<Texture>("Apparel/" + ((int)SaveSystem.playerBottom == -1 ? "none" : "Bottoms/" + ((Items)SaveSystem.playerBottom).ToString())));
        playerMat.SetTexture("_Top", Resources.Load<Texture>("Apparel/" + ((int)SaveSystem.playerTop == -1 ? "none" : "Tops/" + ((Items)SaveSystem.playerTop).ToString())));
    }

    public void RelocatePlayer()
    {
        if (SystemVariables.tempPos != Vector3.zero)
        {
            transform.position = SystemVariables.tempPos;
            SystemVariables.tempPos = Vector3.zero;
        }

        if (SystemVariables.tempDir != -1)
        {
            owDir = (OverworldDir)SystemVariables.tempDir;
            SystemVariables.tempDir = -1;
            
            if (prevDir != owDir)
            {
                prevDir = owDir;
            }
        }
    }
}
