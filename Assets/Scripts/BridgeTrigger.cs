﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BridgeTrigger : TriggerSystem
{
    public BridgeAngle bridgeAngle;

    BridgeController b;

    // Start is called before the first frame update
    public void Start()
    {
        b = GetComponentInParent<BridgeController>();

        onPlayerTriggerStayEvent += PlayerBridgeStayTrigger;
        onPlayerTriggerExitEvent += PlayerBridgeExitTrigger;
    }

    public void PlayerBridgeExitTrigger(Collider2D coll)
    {
        Player p = coll.gameObject.GetComponent<Player>();

        if (!p) return;

        switch (bridgeAngle)
        {
            case BridgeAngle.TOP:
                if (p.velocity.y >= 0)
                {
                    p.bridgeOn = false;
                    b.OffBridge();
                }
                break;
            case BridgeAngle.DOWN:
                if (p.velocity.y <= 0)
                {
                    p.bridgeOn = false;
                    b.OffBridge();
                }
                break;
            case BridgeAngle.LEFT:
                if (p.velocity.x <= 0)
                {
                    p.bridgeOn = false;
                    b.OffBridge();
                }
                break;
            case BridgeAngle.RIGHT:
                if (p.velocity.x >= 0)
                {
                    p.bridgeOn = false;
                    b.OffBridge();
                }
                break;
            default:
                break;
        }

        p.spriteRenderer.sortingOrder = p.bridgeOn ? 20 : 0;
        p.shadowSpriteR.enabled = !p.bridgeOn;
    }

    public void PlayerBridgeStayTrigger(Collider2D coll)
    {
        Player p = coll.gameObject.GetComponent<Player>();

        if (!p) return;

        switch (bridgeAngle)
        {
            case BridgeAngle.TOP:
                if (p.velocity.y < 0)
                {
                    p.bridgeOn = true;
                    b.OnBridge();
                }
                break;
            case BridgeAngle.DOWN:
                if (p.velocity.y > 0)
                {
                    p.bridgeOn = true;
                    b.OnBridge();
                }
                break;
            case BridgeAngle.LEFT:
                if (p.velocity.x > 0)
                {
                    p.bridgeOn = true;
                    b.OnBridge();
                }
                break;
            case BridgeAngle.RIGHT:
                if (p.velocity.x < 0)
                {
                    p.bridgeOn = true;
                    b.OnBridge();
                }
                break;
            default:
                break;
        }

        p.spriteRenderer.sortingOrder = p.bridgeOn? 20 : 0;
        p.shadowSpriteR.enabled = !p.bridgeOn;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public enum BridgeAngle
{
    TOP,
    DOWN,
    LEFT,
    RIGHT
}