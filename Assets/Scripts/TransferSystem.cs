﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;
using UnityEngine.SceneManagement;
using SuperTiled2Unity;
using System;

public class TransferSystem : TriggerSystem
{
    public Maps mapID;
    public int dir;
    public Vector3 pos;
    public bool transferring;
    public bool sameMap;

    public virtual void Start()
    {
        SuperCustomProperties props = transform.parent.GetComponent<SuperCustomProperties>();
        CustomProperty xProp, yProp, zProp, dirProp, mapProp, sameMapProp;

        float x = 0;
        float y = 0;
        float z = 1;
        int _dir = 0;
        Maps map = Maps.NONE;

        if (props.TryGetCustomProperty("x", out xProp))
        {
            x = xProp.GetValueAsFloat();
        }

        if (props.TryGetCustomProperty("y", out yProp))
        {
            y = yProp.GetValueAsFloat();
        }

        if (props.TryGetCustomProperty("z", out zProp))
        {
            z = zProp.GetValueAsFloat();
        }

        if (props.TryGetCustomProperty("dir", out dirProp))
        {
            _dir = dirProp.GetValueAsInt();
        }

        if (props.TryGetCustomProperty("map", out mapProp))
        {
            map = (Maps)Enum.Parse(typeof(Maps), mapProp.GetValueAsString());
        }

        if (props.TryGetCustomProperty("sameMap", out sameMapProp))
        {
            sameMap = sameMapProp.GetValueAsBool();
        }

        pos = new Vector3(x, y, z);
        dir = _dir;
        mapID = map;

        onPlayerTriggerEnterEvent += Transfer;
    }

    public void Transfer(Collider2D coll = null)
    {
        if (transferring) return;

        transferring = true;
        SystemVariables.tempPos = pos;
        SystemVariables.tempDir = dir;

        TransitionHandler tHand = TransitionHandler.instance;
        tHand.Fade();
        if (!sameMap)
        {
            tHand.LoadScene(mapID);
        } else
        {
            tHand.SetMidTransAction(() =>
            {
                Player p = FindObjectOfType<Player>();
                p.RelocatePlayer();
            });

        }
        tHand.Transition();
        transferring = false;
    }
}
