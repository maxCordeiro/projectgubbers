﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class NotificationViewerUI : UISystem
{
    public GameObject[] notifButtons;
    public SuperTextMesh[] notifText;
    public SuperTextMesh[] notifDate;

    int curPage;

    public override void OnBecomePriority()
    {
        throw new System.NotImplementedException();
    }

    public override void OnFadeOut()
    {
        throw new System.NotImplementedException();
    }

    public override void OnNotPriority()
    {
        throw new System.NotImplementedException();
    }

    public override void OnFadeIn()
    {
        throw new System.NotImplementedException();
    }
}
