﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBarUI : MonoBehaviour
{
    public List<ControlGraphic> controller_xbone;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[Serializable]
public struct ControlGraphic
{
    public ControllerIcons control;
    public Sprite controlGraphic;
}