﻿using MonsterSystem;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public abstract class UISystem : MonoBehaviour
{
    [Header("UI System")]
    public CanvasGroup cGroup;
    public bool isFading;
    public Canvas can;
    public bool btnsActive;

    [Header("Testz")]
    bool test;
    [HideInInspector]
    public bool subMenu;

    Image controlBar;

    bool _isPriority;
    public bool IsPriority
    {
        get { return _isPriority; }
        set
        {
            if (_isPriority != value)
            {
                _isPriority = value;
                if (value == true)
                {
                    OnBecomePriority();
                    if (isHidden) FadeIn();
                } else
                {
                    OnNotPriority();
                    if (isHidden) FadeOut();
                }
            }
        }
    }
    public bool isHidden;
    public UISystem referrer;

    public abstract void OnFadeIn();
    public abstract void OnFadeOut();
    public abstract void OnBecomePriority();
    public abstract void OnNotPriority();

    public virtual void Awake()
    {
        cGroup = GetComponent<CanvasGroup>();
        can = transform.parent.GetComponent<Canvas>();
        if (can != null) can.worldCamera = Camera.main;
        cGroup.alpha = 0;
        transform.localScale = new Vector3(0.9f, 0.9f, 1);

        isFading = true;
        controlBar = GameObject.FindGameObjectWithTag("ControlBar").GetComponent<Image>();
    }

    public void Start()
    {
        SetPriority(true);

        if (GetType() == typeof(PauseUI))
        {
            InterfaceSystem.isPaused = true;
            InterfaceSystem.inMenu = false;
        } else
        {
            InterfaceSystem.inMenu = true;
        }
    }


    public void FadeIn(Action afterFade = null, bool hideFade = false)
    {
        isFading = true;

        LeanTween.scale(gameObject, new Vector3(1, 1, 1), SaveSystem.settings_UIAnim && !hideFade ? 0.1f : 0);

        LeanTween.alphaCanvas(cGroup, 1, SaveSystem.settings_UIAnim && !hideFade ? 0.1f : 0).setOnComplete(() => {
            //OnFadeIn();

            afterFade?.Invoke();

            isFading = false;
        });
    }

    public void FadeOut(bool _destroyAfter = false, bool callOnFadeOut = true, bool hideFade = false)
    {
        isFading = true;

        if ((!referrer || referrer && referrer.GetType() == typeof(PauseUI)) && _destroyAfter)
        {
            if (GetType() == typeof(PauseUI))
            {
                InterfaceSystem.isPaused = false;
            }
            else
            {
                InterfaceSystem.inMenu = false;
            }
        }

        if (SaveSystem.settings_UIAnim && !hideFade)
        {
            LeanTween.scale(gameObject, new Vector3(0.9f, 0.9f, 1), 0.1f);
        }

        LeanTween.alphaCanvas(cGroup, 0, SaveSystem.settings_UIAnim && !hideFade ? 0.1f : 0).setOnComplete(() => {
            if (callOnFadeOut) OnFadeOut();
            
            isFading = false;

            if (_destroyAfter) Destroy(gameObject);
        });
    }

    /*public void FadeOut(bool destroyAfter = true, bool scale = true, UnityAction onComplete = null)
    {
        if (scale && SaveSystem.settings_UIAnim) LeanTween.scale(gameObject, new Vector3(0.9f, 0.9f, 1), 0.1f);
        LeanTween.alphaCanvas(cGroup, 0, SaveSystem.settings_UIAnim ? 0.1f : 0).setOnComplete(() => {
            if (onComplete != null) onComplete.Invoke();
            isFading = false;
            if (destroyAfter) Destroy(gameObject);
        });
    }*/
    
    public IEnumerator FadeOutCoroutine(bool destroyAfter = true, bool scale = true)
    {
        isFading = true;
        if (scale && SaveSystem.settings_UIAnim) LeanTween.scale(gameObject, new Vector3(0.9f, 0.9f, 1), 0.1f);
        LeanTween.alphaCanvas(cGroup, 0, SaveSystem.settings_UIAnim? 0.1f : 0);
        yield return new WaitForSeconds(SaveSystem.settings_UIAnim? 0.1f : 0);
        isFading = false;
        if (destroyAfter) Destroy(gameObject);
    }

    public void ButtonsActive(bool active, Transform obj, bool isObject = true)
    {
        foreach (Transform butt in obj)
        {
            if (!isObject)
            {
                butt.GetComponent<Button>().enabled = active;
            } else
            {
                butt.gameObject.SetActive(active);
            }
        }
        
        btnsActive = active;
    }
    
    public void ReposCursor()
    {
        RectTransform sRect = InterfaceInputManager.instance.es.currentSelectedGameObject.GetComponent<RectTransform>();
        Vector3[] c = new Vector3[4];
        sRect.GetWorldCorners(c);
         
        InterfaceInputManager.instance.PositionCursor(new Vector3(c[1].x, c[1].y));
    }

    public void ButtonsActive(bool active)
    {
        cGroup.interactable = active;
    }

    public void SetPriority(bool priority, bool hidden = false)
    {
        //Debug.Log(this.name + ", " + priority);
        IsPriority = priority;
        isHidden = hidden;
    }

    public void AddControlGraphic(string transKey, int iconIndex)
    {
        GameObject control = Instantiate<GameObject>(Resources.Load<GameObject>("UI/ControlGraphic"), controlBar.transform);

        control.transform.GetChild(2).GetComponent<LocaleTextUpdater>().translationKey = transKey;
        control.transform.GetChild(2).GetComponent<SuperTextMesh>().Rebuild(false);
        //control.transform.GetChild(1).GetComponent<Image>().sprite
    }

    public void ShowControlBar()
    {
        controlBar.transform.SetAsLastSibling();
        controlBar.enabled = true;
    }

    public void HideControlbar(bool clear)
    {
        controlBar.enabled = false;

        foreach (Transform child in controlBar.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
    }
}

public enum ControllerIcons
{
    L_STICK = 0,
    L_STICK_L = 1,
    L_STICK_R = 2,
    L_STICK_U = 3,
    L_STICK_D = 4,
    L_STICK_CLICK = 5,

    R_STICK = 6,
    R_STICK_L = 7,
    R_STICK_R = 8,
    R_STICK_U = 9,
    R_STICK_D = 10,
    R_STICK_CLICK = 11,

    LT = 12,
    LB = 13,

    RT = 14,
    RB = 15,

    START = 16,
    SELECT = 17,

    A = 18,
    B = 19,
    X = 20,
    Y = 21,

    DPAD = 22,
    DPAD_L = 23,
    DPAD_R = 24,
    DPAD_U = 25,
    DPAD_D = 26
}