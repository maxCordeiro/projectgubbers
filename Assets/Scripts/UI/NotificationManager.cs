﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationManager : MonoBehaviour
{
    public GameObject[] notifButtons;
    SuperTextMesh[] notifText;
    CanvasGroup[] notifCGroup;

    float[] timeAmt, currentTime;
    bool[] isShowing;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i <notifButtons.Length; i++)
        {
            notifText[i] = notifButtons[i].transform.GetChild(1).GetComponent<SuperTextMesh>();
            notifCGroup[i] = notifButtons[i].GetComponent<CanvasGroup>();
        }
    }

    // Update is called once per frame
    void Update()
    {
//        if (!SaveSystem.playerUnreadNotifs) return;

        UpdateNotifications();
    }

    public void UpdateNotifications()
    {
        for (int i = 0; i < notifButtons.Length; i++)
        {

        }
    }
}