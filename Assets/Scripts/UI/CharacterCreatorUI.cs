﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using UnityEngine.UI;
using GameDatabase;

public class CharacterCreatorUI : UISystem
{
    public Button[] skinColorButtons;
    public Button hairButton;
    public Button topButton;
    public Button bottomButton;
    public Button[] itemPreviews;
    public Image[] itemPreviewSprites;
    public GameObject panelObj;

    public Sprite[] hairPreviewSprites;
    public Sprite[] topPreviewSprites;
    public Sprite[] botPreviewSprites;

    public SuperTextMesh hairText;
    public SuperTextMesh topText;
    public SuperTextMesh bottomText;

    public Text nameInput;

    public SpriteRenderer playerIcon;
    public SpriteRenderer playerOverworld;

    public Material playerIconMat;
    public Material playerOverworldMat;

    public Button doneButton;

    int skinIndex;
    int hairIndex;
    int topIndex;
    int bottomIndex;

    int panel; // 0 = hair, 1 = top, 2 = bottom
    bool panelOpen;

    Items[] hairs = new Items[] {
        Items.HAIR_BUZZ,
        Items.HAIR_LONGSTRAIGHT,
    };

    Items[] tops = new Items[] {
        Items.TOP_HEARTSHIRT,
        Items.TOP_REDWHITEHOODIE,
        Items.TOP_YELLOWBUTTONUP,
        Items.TOP_YELLOWTANK
    };

    Items[] bottoms = new Items[] {
        Items.BOT_GRAYPANTS,
        Items.BOT_REDPANTS,
        Items.BOT_SPORTSHORTS
    };

    new void Start()
    {
        base.Start();

        playerIconMat = playerIcon.material;
        playerOverworldMat = playerOverworld.material;

        //SaveSystem.playerHair = hairs[hairIndex];
        SaveSystem.playerTop = tops[topIndex];
        Debug.Log(tops[topIndex].ToString());
        SaveSystem.playerBottom = bottoms[bottomIndex];

        itemPreviews[0].onClick.AddListener(()=> { UpdateChar(0); });
        itemPreviews[1].onClick.AddListener(()=> { UpdateChar(1); });
        itemPreviews[2].onClick.AddListener(()=> { UpdateChar(2); });
        itemPreviews[3].onClick.AddListener(()=> { UpdateChar(3); });
        itemPreviews[4].onClick.AddListener(()=> { UpdateChar(4); });
        itemPreviews[5].onClick.AddListener(()=> { UpdateChar(5); });
        itemPreviews[6].onClick.AddListener(()=> { UpdateChar(6); });
        itemPreviews[7].onClick.AddListener(()=> { UpdateChar(7); });
        
        hairButton.onClick.AddListener(() => { HairButton(); });
        topButton.onClick.AddListener(() => { TopButton(); });
        bottomButton.onClick.AddListener(() => { HairButton(); });

        UpdateDisplay();

        FadeIn();
    }

    public void UpdateDisplay()
    {
        Item tHair = new Item(hairs[hairIndex]);
        Item tTop = new Item(tops[topIndex]);
        Item tBott = new Item(bottoms[bottomIndex]);
        
        hairText.text = ItemSystem.GetItemName(tHair);
        topText.text = ItemSystem.GetItemName(tTop);
        bottomText.text = ItemSystem.GetItemName(tBott);

        playerOverworldMat.SetTexture("_Top", ItemSystem.GetClothingSprite(tTop).texture);
        playerOverworldMat.SetTexture("_Bottom", ItemSystem.GetClothingSprite(tBott).texture);

        UpdatePlayerPreview(tTop);
        UpdatePlayerPreview(tBott);

    }

    public void UpdatePlayerPreview(Item _item)
    {
        playerOverworldMat.SetFloat("_EnableEars", SaveSystem.playerEars ? 0 : 1);

        if (_item.GetData().menuFunction == ItemEffects_Menu.WEARTOP)
        {
            playerOverworldMat.SetTexture("_Top", ItemSystem.GetClothingSprite(_item).texture);
            playerOverworldMat.SetTexture("_Bottom", Resources.Load<Texture>("Apparel/" + ((int)SaveSystem.playerBottom == -1 ? "none" : "Bottoms/" + SaveSystem.playerBottom.ToString())));

        }
        else
        {
            playerOverworldMat.SetTexture("_Bottom", ItemSystem.GetClothingSprite(_item).texture);
            playerOverworldMat.SetTexture("_Top", Resources.Load<Texture>("Apparel/" + ((int)SaveSystem.playerTop == -1 ? "none" : "Tops/" + SaveSystem.playerTop.ToString())));
        }
    }

    public void HairButton()
    {
        if (panelOpen && panel == 0)
        {
            panelObj.SetActive(false);
            panelOpen = false;
        } else
        {
            panelObj.SetActive(true);
            panelOpen = true;
            UpdatePanel(0);
        }
    }

    public void TopButton()
    {
        if (panelOpen && panel == 1)
        {
            panelObj.SetActive(false);
            panelOpen = false;
        }
        else
        {
            panelObj.SetActive(true);
            panelOpen = true;
            UpdatePanel(1);
        }
    }
    
    public void BottomButton()
    {
        if (panelOpen && panel == 2)
        {
            panelObj.SetActive(false);
            panelOpen = false;
        }
        else
        {
            panelObj.SetActive(true);
            panelOpen = true;
            UpdatePanel(2);
        }
    }

    public void UpdateChar(int index)
    {
        switch (panel)
        {
            case 0:
                hairIndex = index;
                break;
            case 1:
                topIndex = index;
                break;
            case 2:
                bottomIndex = index;
                break;
        }

        UpdateDisplay();
    }

    public void UpdatePanel(int index)
    {
        panel = index;

        switch (panel)
        {
            case 0:
                for (int i = 0; i < hairPreviewSprites.Length; i++)
                {
                    itemPreviewSprites[i].sprite = hairPreviewSprites[i];
                }
                break;
            case 1:
                for (int i = 0; i < topPreviewSprites.Length; i++)
                {
                    itemPreviewSprites[i].sprite = topPreviewSprites[i];
                }
                break;
            case 2:
                for (int i = 0; i < botPreviewSprites.Length; i++)
                {
                    itemPreviewSprites[i].sprite = botPreviewSprites[i];
                }
                break;
            default:
                break;
        }
    }

    public void Done()
    {
        hairButton.interactable = false;
        topButton.interactable = false;
        bottomButton.interactable = false;

        Item tHair = new Item(hairs[hairIndex]);
        Item tTop = new Item(tops[topIndex]);
        Item tBott = new Item(bottoms[bottomIndex]);

        SaveSystem.playerHair = hairs[hairIndex];
        SaveSystem.playerTop = tops[topIndex];
        SaveSystem.playerBottom = bottoms[bottomIndex];

        FindObjectOfType<Player>().UpdatePlayerSprite();

        TransitionHandler tH = TransitionHandler.instance;

        tH.Fade();
        tH.SetMidTransAction(() => { Destroy(gameObject); });
        tH.Transition();
    }

    public override void OnFadeIn()
    {
        throw new System.NotImplementedException();
    }

    public override void OnFadeOut()
    {
        throw new System.NotImplementedException();
    }

    public override void OnBecomePriority()
    {
        throw new System.NotImplementedException();
    }

    public override void OnNotPriority()
    {
        throw new System.NotImplementedException();
    }
}
