﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameDatabase;
using MonsterSystem;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.EventSystems;

public class GatehouseUI : UISystem
{
    [Header("Tiers")]
    public Button[] tierButtons;
    public SuperTextMesh[] tierText;
    public Image[] tierCrafting;
    public GameObject[] tierElixirBG;
    public Image[] tierElixirBGIMG;
    public Image[] tierElixir;
    public SuperTextMesh[] tierElixirUses;
    public Sprite[] tierElixirBGSprites;

    [Header("MapList")]
    public GameObject mapListPanel;
    public Button[] mapListButtons;
    public SuperTextMesh[] mapListNames;
    public SuperTextMesh mapListPageCount;
    public Sprite[] mapListSprites;
    public Button[] mapListPageIndicators;
    int pageIndex;

    int selectedMapID;

    bool viewingMapInfo;
    bool tierOptionsShowing;

    [Header("MapInfo")]
    public GameObject mapInfoPanel;
    public SuperTextMesh selectedMapName;
    public SuperTextMesh selectedMapDesc;
    public Image[] availMons;
    public Image[] bossMons;
    Sprite unknownSprite;

    public Image mapPreview;
    public Image mapOutline;

    public Image[] craftingIcon;
    public SuperTextMesh[] craftingText;
    public SuperTextMesh[] craftingQuantities;
    public GameObject craftingPanel;

    float timerCount;
    bool timerStarted, timerUp, toMap;

    public float cursorX = 0, cursorY = 0;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();

        unknownSprite = Resources.Load<Sprite>("Unknown");

        pageIndex = 0;

        for (int i = 0; i < mapListButtons.Length; i++)
        {
            int ind = i;

            EventTrigger et = mapListButtons[i].gameObject.AddComponent<EventTrigger>();

            EventTrigger.Entry pSelect = new EventTrigger.Entry();
            pSelect.eventID = EventTriggerType.Select;
            pSelect.callback.AddListener((e) => MapListPointerEnter(ind));
            
            et.triggers.Add(pSelect);
        }

        for (int i = 0; i < tierButtons.Length; i++)
        {
            int ind = i;

            EventTrigger etr = tierButtons[i].gameObject.AddComponent<EventTrigger>();

            EventTrigger.Entry pSelect = new EventTrigger.Entry();
            pSelect.eventID = EventTriggerType.Select;
            pSelect.callback.AddListener((e) => TierPointerEnter(ind));

            etr.triggers.Add(pSelect);
        }

        Image[] img = new Image[5];

        for (int i = 0; i < tierElixirBG.Length; i++)
        {
            img[i] = tierElixirBG[i].GetComponent<Image>();
        }

        tierElixirBGIMG = img;

        StartCoroutine(StartingRoutine());
    }

    IEnumerator StartingRoutine()
    {
        GameUtility.EnableBackground();
        UpdateMapList();
        UpdateMapPreview(0);
        yield return new WaitForEndOfFrame();

        FadeIn();
        InterfaceInputManager.instance.EnableCursor(true);

        yield break;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !InterfaceSystem.InDialogueChoices())
        {
            FadeOut(true);
        }

        if (!InterfaceSystem.InDialogueChoices() && InterfaceInputManager.instance.es.currentSelectedGameObject != null)
        {
            ReposCursor();
        }

        if (timerStarted)
        {
            timerCount += Time.deltaTime;

            if (timerCount > 0.15f)
            {
                if (viewingMapInfo)
                {
                    ClearCrafting();
                } else
                {
                    ClearMapPreview();
                }

                timerStarted = false;
            }
        } else
        {
            timerCount = 0;
        }
    }

    public void UpdateMapList()
    {
        Back();

        int currCount = SaveSystem.encMaps.Count;
        int totalPages = Mathf.CeilToInt(currCount / 10);

        craftingPanel.SetActive(false);
        mapPreview.enabled = true;

        for (int i = 0; i < tierButtons.Length; i++)
        {
            tierButtons[i].interactable = false;
        }

        for (int i = 0; i < 10; i++)
        {
            int pInd = i + (10 * pageIndex);
            bool isMap = SaveSystem.encMaps.ElementAtOrDefault(pInd) != null;

            if (isMap)
            {
                mapListButtons[i].interactable = true;
                mapListNames[i].text = MapSystem.GetMapName(SaveSystem.encMaps[pInd].map);
            }
            else
            {
                mapListButtons[i].interactable = false;
                mapListNames[i].text = "";
            }
        }

        mapListPageIndicators[0].gameObject.SetActive(pageIndex > 0);
        mapListPageIndicators[1].gameObject.SetActive(pageIndex < totalPages);

        mapListPageCount.text = string.Format("{0}/{1}", pageIndex + 1, totalPages + 1);

        if (SaveSystem.settings_controllerEnabled && InterfaceInputManager.instance.es.currentSelectedGameObject == null)
        {
            InterfaceInputManager.instance.es.SetSelectedGameObject(mapListButtons[0].gameObject);
        }
    }

    public void DisplayMapInfo(int index)
    {
        int pInd = index + (10 * pageIndex);

        mapListPanel.SetActive(false);
        mapInfoPanel.SetActive(true);

        selectedMapName.text = MapSystem.GetMapName(SaveSystem.encMaps[pInd].map);
        selectedMapDesc.text = MapSystem.GetMapDesc(SaveSystem.encMaps[pInd].map);

        for (int i = 0; i < tierButtons.Length; i++)
        {
            tierButtons[i].interactable = true;
            tierElixirBGIMG[i].sprite = tierElixirBGSprites[1];
        }

        MapData md = MapSystem.GetMapDatabaseData(SaveSystem.encMaps[pInd].map);

        for (int i = 0; i < availMons.Length; i++)
        {

            if (i < md.availableMons.Count)
            {
                availMons[i].sprite = GameUtility.GetSpeciesIcon(md.availableMons[i].mon);
                availMons[i].color = EncyclopediaSystem.HasSeen(md.availableMons[i].mon) ? new Color(1, 1, 1, 1) : new Color(0, 0, 0, 1);
            }
            else
            {
                availMons[i].sprite = null;
                availMons[i].color = new Color(1, 1, 1, 0);
            }
        }

        for (int i = 0; i < bossMons.Length; i++)
        {
            if (i < md.bossMons.Count)
            {
                bossMons[i].sprite = GameUtility.GetSpeciesIcon(md.bossMons[i].mon);
                bossMons[i].color = EncyclopediaSystem.HasSeen(md.bossMons[i].mon) ? new Color(1, 1, 1, 1) : new Color(0, 0, 0, 1);
            }
            else
            {
                bossMons[i].sprite = null;
                bossMons[i].color = new Color(1, 1, 1, 0);
            }
        }

        InterfaceInputManager.instance.es.SetSelectedGameObject(tierButtons[0].gameObject);
        viewingMapInfo = true;
    }

    public void Back()
    {
        mapListPanel.SetActive(true);
        mapInfoPanel.SetActive(false);

        ClearCrafting();

        viewingMapInfo = false;

        for (int i = 0; i < tierButtons.Length; i++)
        {
            tierButtons[i].interactable = false;
            tierElixirBGIMG[i].sprite = tierElixirBGSprites[2];
        }
    }

    public void UpdateMapPreview(int index)
    {
        if (craftingPanel.activeInHierarchy) craftingPanel.SetActive(false);

        selectedMapID = index + (10 * pageIndex);
        bool isMap = SaveSystem.encMaps.ElementAtOrDefault(selectedMapID) != null;

        if (!isMap) return;

        mapPreview.sprite = Resources.Load<Sprite>("MapPreviews/" + SaveSystem.encMaps[selectedMapID].map.ToString());
        //Debug.Log(SaveSystem.encMaps[selectedMapID].map.ToString());
        mapPreview.color = Color.white;

        for (int i = 0; i < 5; i++)
        {
            int tierInd = MapSystem.GetTierInfoIndex(SaveSystem.encMaps[selectedMapID].map, (MapTier)i);
            MapData md = MapSystem.GetMapDatabaseData(SaveSystem.encMaps[selectedMapID].map);

            if (md.defaultTier == (MapTier)i)
            {
                tierElixirBG[i].SetActive(false);
                tierCrafting[i].gameObject.SetActive(false);
                continue;
            }

            if (tierInd != -1)
            {
                tierCrafting[i].gameObject.SetActive(false);
                tierElixirBG[i].SetActive(true);
                tierElixirBGIMG[i].sprite = tierElixirBGSprites[2];
                tierElixirUses[i].text = SaveSystem.playerTiers[tierInd].uses.ToString();
            }
            else
            {
                tierCrafting[i].gameObject.SetActive(true);
                tierElixirBG[i].SetActive(false);
            }
        }
    }

    public void ClearMapPreview()
    {
        mapPreview.color = new Color(1, 1, 1, 0);

        for (int i = 0; i < 5; i++)
        {
            tierCrafting[i].gameObject.SetActive(false);
            tierElixirBG[i].SetActive(false);
        }
    }

    public void TierButtonClick(int tier)
    {
        Debug.Log("Clicked");

        if (!viewingMapInfo || tierOptionsShowing) return;
        
        StartCoroutine(SelectTier(tier));
    }

    public void CraftingDisplay(int tier)
    {
        MapData md = MapSystem.GetMapDatabaseData(SaveSystem.encMaps[selectedMapID].map, (MapTier)tier);

        if (!viewingMapInfo || tierOptionsShowing || (int)md.defaultTier == tier) return;

        craftingPanel.SetActive(true);
        mapPreview.gameObject.SetActive(false);

        int tierInd = MapSystem.GetTierInfoIndex(SaveSystem.encMaps[selectedMapID].map, (MapTier)tier);
        List<TierCraftingItems> crafting = md.GetCraftingList((MapTier)tier);

        for (int i = 0; i < 5; i++)
        {
            if (i >= crafting.Count)
            {
                craftingText[i].text = "";
                craftingQuantities[i].text = "";
                craftingIcon[i].enabled = false;
                continue;
            }

            craftingIcon[i].enabled = true;
            craftingText[i].text = ItemSystem.GetItemName(crafting[i].item);
            craftingQuantities[i].text = "x" + crafting[i].quantity;
            craftingIcon[i].sprite = ItemSystem.GetItemSprite(crafting[i].item);
        }
    }

    public void ClearCrafting()
    {
        craftingPanel.SetActive(false);
        mapPreview.gameObject.SetActive(true);
    }

    public void MapListPointerEnter(int mapIndex)
    {
        timerStarted = false;
        UpdateMapPreview(mapIndex);
    }

    public void MapListPointerExit()
    {
        timerStarted = true;
    }

    public void TierPointerEnter(int tierIndex)
    {
        timerStarted = false;
        if (!tierOptionsShowing) tierElixirBGIMG[tierIndex].sprite = tierElixirBGSprites[0];

        CraftingDisplay(tierIndex);
    }

    public void TierPointerExit()
    {
        timerStarted = true;

        int bgInd = tierOptionsShowing ? 2 : 1;

        for (int i = 0; i < tierButtons.Length; i++)
        {
            tierElixirBGIMG[i].sprite = tierElixirBGSprites[bgInd];
        }
    }

    public IEnumerator SelectTier(int tierButton)
    {
        tierOptionsShowing = true;
        int tierInd = MapSystem.GetTierInfoIndex(SaveSystem.encMaps[selectedMapID].map, (MapTier)tierButton);
        MapData md = MapSystem.GetMapDatabaseData(SaveSystem.encMaps[selectedMapID].map);

        ChoiceHandler ch = ChoiceHandler.instance;
        
        for (int i = 0; i < tierButtons.Length; i++)
        {
            if (i == tierButton) continue;

            tierButtons[i].interactable = false;
            tierElixirBGIMG[i].sprite = tierElixirBGSprites[2];
        }

        if (tierInd == -1 && tierButton != (int)md.defaultTier)
        {
            ch.AddChoice(0, "Craft");
        } else
        {
            ch.AddChoice(1, "Embark");
        }

        ch.AddChoice(2, "Cancel");
        ch.SetPos(SystemVariables.choiceboxDialoguePos);
        ch.Show();


        yield return new WaitForChoice();

        int chInd = InterfaceSystem.choiceIndex;

        switch (chInd)
        {
            case 0:
                StartCoroutine(CraftingCheck(tierButton));
                break;
            case 1:
                if (tierButton != (int)md.defaultTier)
                {
                    Debug.Log(SaveSystem.playerTiers[tierInd].uses);
                    SaveSystem.playerTiers[tierInd].uses -= 1;

                    Debug.Log(SaveSystem.playerTiers[tierInd].uses);
                    if (SaveSystem.playerTiers[tierInd].uses == 0)
                    {
                        SaveSystem.playerTiers.RemoveAt(tierInd);
                    }
                }
                toMap = true;

                FadeOut(false);

                while (isFading) 
                {
                    yield return new WaitForEndOfFrame();
                }

                yield return new WaitForSeconds(0.1f);

                TransitionHandler t = TransitionHandler.instance;
                t.Fade();
                t.LoadScene(md.id);
                t.SetMidTransAction(() =>
                {
                    SaveSystem.currentMap = md.id;
                    SaveSystem.currentTier = (MapTier)tierButton;
                });
                t.SetEndTransAction(() =>
                {
                    CommandSystem.reloadMap();
                });


                t.Transition();
                break;
            case 2:
                InterfaceInputManager.instance.es.SetSelectedGameObject(tierButtons[tierButton].gameObject);
                break;
            default:
                break;
        }

        for (int i = 0; i < tierButtons.Length; i++)
        {
            tierButtons[i].interactable = true;
        }
        
        InterfaceInputManager.instance.EnableCursor(true);
        tierOptionsShowing = false;
    }

    public IEnumerator CraftingCheck(int tierButton)
    {
        int tierInd = MapSystem.GetTierInfoIndex(SaveSystem.encMaps[selectedMapID].map, (MapTier)tierButton);
        MapData md = MapSystem.GetMapDatabaseData(SaveSystem.encMaps[selectedMapID].map);

        DialogueHandler msg = DialogueHandler.instance;
        msg.Name(DialogueSystem.LoadName("npc_daycare", "dialogue_npc"));

        List<TierCraftingItems> crafting = md.GetCraftingList((MapTier)(tierButton));

        if (ItemSystem.HasItems(crafting))
        {
            msg.AddMessage("Crafted!");
            msg.Show();

            for (int i = 0; i < crafting.Count; i++)
            {
                ItemSystem.TossItem(crafting[i].item, crafting[i].quantity);
            }

            TierInfo t = new TierInfo(SaveSystem.encMaps[selectedMapID].map, 1, (MapTier)tierButton);
            SaveSystem.playerTiers.Add(t);

            UpdateMapPreview(selectedMapID - (10 * pageIndex));
        } else
        {
            msg.AddMessage("Not enough!");
            msg.Show();
        }

        yield return new WaitForDialogueClose();
        InterfaceInputManager.instance.es.SetSelectedGameObject(tierButtons[tierButton].gameObject);
    }

    public override void OnFadeIn()
    {

    }

    public override void OnFadeOut()
    {
        if (toMap) InterfaceSystem.inMenu = false;

        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {

    }

    public override void OnNotPriority()
    {

    }
}
