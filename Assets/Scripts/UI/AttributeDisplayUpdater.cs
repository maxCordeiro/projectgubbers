﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;

public class AttributeDisplayUpdater : MonoBehaviour
{
    public Sprite[] icons, bgs;
    public Material[] mats;
    
    public Image iconComp, bgComp;
    Material mat;
    public SuperTextMesh attrText;
        
    public void UpdateDisplay(MonsterAttribute _attr)
    {
        int attrind = (int)_attr;
        
        bgComp.sprite = bgs[attrind];
        iconComp.sprite = icons[attrind];
        
        if (_attr == MonsterAttribute.NONE)
        {
            attrText.text = "";
            return;
        }
        
        mat = mats[attrind];
        attrText.text = SystemVariables.trans["attr_" + _attr.ToString()];
        attrText.textMaterial = mat;
        attrText.Rebuild(false);
    }
}
