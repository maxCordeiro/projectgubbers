﻿using GameDatabase;
using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Translation;
using System;

public class InventoryUI : UISystem {

    public Item requestedItem;
    public ItemTypes requestedType;
    public bool requestingItem;

    public int activeTab;
    public SuperTextMesh pocketText;
    public SuperTextMesh descText;

    public GameObject slotContainer;
    public List<GameObject> slots = new List<GameObject>();

    [HideInInspector]
    public Sprite[] itemSprites;

    public GameObject pocketContainer;
    List<Button> pocketButtons = new List<Button>();

    public bool isSelecting;
    
    public Sprite[] pocketLarge;
    public Sprite[] pocketReg;
    public Sprite[] pocketDisabled;

    public Sprite pocketButtonBG;
    public Sprite pocketButtonActive;
    
    public SuperTextMesh goldAmt;

    public SuperTextMesh headingText;
    
    public GameObject clothesPreview;
        
    public SpriteAnimation playerPreview;
    public Material playerMat;
    bool previewPlaying = true;
    OverworldDir previewDirection;
    PlayerAnimState animState;
    PlayerAnimState prevState;
    
    public int cursorX;
    public int cursorY;
    
    Player _player;

    int currentPage;
    int totalPages;

    public GameObject leftArrow;
    public GameObject rightArrow;
    public SuperTextMesh pageNumber;

    public int currSelIndex;
    int prevSelIndex;

    float slotWaitAmount = 0.25f;
    float slotWaitTimer = 0.25f;

    float pageWaitAmount = 0.25f;
    float pageWaitTimer = 0.25f;
    bool canMoveToNextPage;

    string[] pocketNames;

    public bool isShop;
    public List<Items> shopInv;


    public float timerCount;
    public bool timerStarted, timerUp;
    public bool selling;
    
    private new void Start()
    {
        base.Start();
        clothesPreview.SetActive(false);
        GameUtility.EnableBackground();

        _player = FindObjectOfType<Player>();
        playerMat = playerPreview.GetComponent<SpriteRenderer>().material;

        for (int i = 0; i < slotContainer.transform.childCount; i++)
        {
            slots.Add(slotContainer.transform.GetChild(i).gameObject);
        }

        for (int i = 0; i < pocketContainer.transform.childCount; i++)
        {
            pocketButtons.Add(pocketContainer.transform.GetChild(i).GetComponent<Button>());
        }

        InterfaceInputManager.instance.controllerCursor.transform.SetAsLastSibling();
        InterfaceInputManager.instance.es.SetSelectedGameObject(null);
        
        pocketNames = new string[] {
            SystemVariables.trans["inv_general"],
            SystemVariables.trans["inv_care"],
            SystemVariables.trans["inv_battle"],
            SystemVariables.trans["inv_apparel"],
            SystemVariables.trans["inv_capsules"],
            SystemVariables.trans["inv_charms"],
        };

        if (isShop)
        {
            goldAmt.text = SaveSystem.playerGold.ToString("##,#");
        }

        SwitchTab(0);
        FadeIn();
    }

    public void UpdateInventoryDisplay()
    {
        int enabledButtons = 0;
        int currCount = 0;
        int slotCount = 14;

        currCount = isShop ? shopInv.Count : SaveSystem.GetInventoryList(activeTab).Count;
        
        totalPages = (int)Mathf.Ceil(currCount / slotCount);

        for (int i = 0; i < slotCount; i++)
        {
            int ind = i;
            bool isItem = false;

            if (SaveSystem.GetInventoryList(activeTab).ElementAtOrDefault(i + (slotCount * currentPage)) != null)
            {
                //Debug.Log(SaveSystem.GetInventoryList(activeTab).ElementAtOrDefault(i + (slotCount * currentPage)));
                isItem = true;
            } else if (i < shopInv.Count)
            {
                isItem = true;
            }

            InventorySlotUpdater slotComp = slots[i].GetComponent<InventorySlotUpdater>();
            slotComp.invManager = this;

            int width = 222;
            bool quantity = true;
            
            slots[i].GetComponent<Button>().interactable = true;

            if (isItem && !isShop)
            {
                if (!SaveSystem.GetInventoryList(activeTab)[i + (slotCount * currentPage)].GetData().stackable)
                {
                    width = 220;
                    quantity = false;
                }
            }

            slots[i].transform.GetChild(0).GetChild(2).gameObject.SetActive(quantity);
            if (!isShop) slots[i].transform.GetChild(0).GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, width);
            slotComp.listIndex = i;

            if (!isItem)
            {
                slotComp.item = null;
                slotComp.UpdateSlotInfo( true);
                slots[i].GetComponent<Button>().interactable = false;
                continue;
            }
            if (isShop)
            {
                slotComp.shopItem = shopInv[i + (slotCount * currentPage)];
            }
            else
            {
                slotComp.item = SaveSystem.GetInventoryList(activeTab)[i + (slotCount * currentPage)];
            }
            slotComp.UpdateSlotInfo();
            slots[i].name = isShop? slotComp.shopItem.ToString() : slotComp.item.id.ToString() + " (" + slotComp.item.currentQuantity + ")";

        }

        foreach (GameObject button in slots)
        {
            if (button.GetComponent<Button>().interactable) enabledButtons++;
        }

        if (InterfaceInputManager.instance.es.currentSelectedGameObject == null && enabledButtons > 0)
        {
            InterfaceInputManager.instance.es.SetSelectedGameObject(slots[0]);
            InterfaceInputManager.instance.EnableCursor(true);
            ReposCursor();
        }

        if (currCount == 0) 
        {
            descText.text = "";
            currSelIndex = -1;
        }
        pageNumber.text = string.Format("{0} / {1}", currentPage + 1, totalPages + 1);
    }

    void Update()
    {
        if (isFading || !IsPriority) return;

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !isSelecting && !InterfaceSystem.InDialogueChoices())
        {
            //if (isShop || selling || InterfaceSystem.inBattle) GameUtility.DisableBackground();
            FadeOut(_destroyAfter: true);
        }

        if (!InterfaceSystem.InDialogueChoices())
        {
            if (InterfaceInputManager.instance.es.currentSelectedGameObject != null)
            {
                ReposCursor();
            }
            else
            {
                InterfaceInputManager.instance.EnableCursor(false);
            }
        }

        if (timerStarted)
        {
            timerCount += Time.deltaTime;

            if (timerCount > 0.15f)
            {
                descText.text = "";
                timerStarted = false;
            }
        }
        else
        {
            timerCount = 0;
        }

        if (!isShop)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                SwitchTab(0);
            } else if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                SwitchTab(1);                
            } else if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                SwitchTab(2);                
            } else if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                SwitchTab(3);                
            } else if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                SwitchTab(4);                
            } else if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                SwitchTab(5);
            }
            
            if (InterfaceSystem.rePlayer.GetButtonDown("LShoulder"))
            {
                if (activeTab == 0)
                {
                    SwitchTab(5);
                }
                else
                {
                    SwitchTab(activeTab - 1);
                }
            }
            else if (InterfaceSystem.rePlayer.GetButtonDown("RShoulder"))
            {
                if (activeTab == 5)
                {
                    SwitchTab(0);
                }
                else
                {
                    SwitchTab(activeTab + 1);
                }
            }
        }

        if (prevSelIndex != currSelIndex)
        {
            slotWaitTimer -= Time.deltaTime;
            if (slotWaitTimer <= 0)
            {
                prevSelIndex = currSelIndex;
                slotWaitTimer = slotWaitAmount;
            }
        }

        if (!canMoveToNextPage)
        {
            pageWaitTimer -= Time.deltaTime;
            if (pageWaitTimer <= 0)
            {
                pageWaitTimer = pageWaitAmount;
                canMoveToNextPage = true;
            }
        }

        if (currSelIndex != -1 && prevSelIndex == currSelIndex && canMoveToNextPage && !isSelecting)
        {
            if (InterfaceSystem.rePlayer.GetAxisRaw("Horizontal") == 1)
            {
                //Debug.Log("Less than. " + currSelIndex + ", " + (currentPage - 1 >= 0));
                if (currSelIndex % 2 == 0 && (currentPage - 1 >= 0))
                {
                    currentPage--;
                    UpdateInventoryDisplay();
                    canMoveToNextPage = false;
                }
            } else if (InterfaceSystem.rePlayer.GetAxisRaw("Horizontal") == -1)
            {
                //Debug.Log("Greater than. " + currSelIndex + ", " + (currentPage + 1 <= totalPages));
                if (currSelIndex % 2 == 1 && (currentPage + 1 <= totalPages))
                {
                    currentPage++;
                    UpdateInventoryDisplay();
                    canMoveToNextPage = false;
                }
            }
        }

        if (activeTab == 3)
        {
            UpdateAnimation();

            if (InterfaceSystem.rePlayer.GetButtonDown("X"))
            {
                previewPlaying = !previewPlaying;
            }

            Vector2 previewVector = new Vector2(InterfaceSystem.rePlayer.GetAxisRaw("HorizontalSecondary"), InterfaceSystem.rePlayer.GetAxisRaw("VerticalSecondary"));
            previewDirection = GameUtility.CalculateDirection(previewVector);
        }
    }

    public void SwitchTab(int id)
    {
        activeTab = id;

        for (int i = 0; i < pocketButtons.Count; i++)
        {
            pocketButtons[i].GetComponent<Image>().sprite = i == id ? pocketButtonActive : pocketButtonBG;
            pocketButtons[i].GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, i == id ? 26 : 24);
            pocketButtons[i].transform.GetChild(0).GetComponent<Image>().sprite = i == id? pocketLarge[i] : pocketDisabled[i];
            pocketButtons[i].transform.GetChild(0).GetComponent<Image>().color = i == id ? Color.white : new Color(1, 1, 1, 0.5f);
        }

        clothesPreview.SetActive(activeTab == 3 ? true : false);

        pocketText.text = "<m=default><q=gold></m>" + SaveSystem.playerGold.ToString("##,#");
        if (!isShop) headingText.text = pocketNames[id];
        UpdateInventoryDisplay();
    }

    public IEnumerator SelectItem(int listIndex)
    {
        isSelecting = true;
        slots[listIndex].GetComponent<InventorySlotUpdater>().isSelected = true;

        bool cancel = false;

        int slotCount = isShop ? 7 : 14;
        int itemPageIndex = listIndex + (slotCount * currentPage);

        foreach (GameObject obj in slots)
        {
            obj.GetComponent<Button>().enabled = false;
        }

        foreach (Button b in pocketButtons)
        {
            b.enabled = false;
        }

        Transform BG = transform.parent.Find("BG");

        ChoiceHandler choices = ChoiceHandler.instance;
        choices.KeepCursor();
        choices.SetPos(new Vector2(410, 38));

        if (isShop)
        {
            choices.AddChoice(1, "Buy");
        } else if (selling)
        {
            choices.AddChoice(4, "Sell");
        } else 
        {
            if (requestingItem)
            {
                if (InterfaceSystem.inBattle)
                {
                    if (InterfaceSystem.requestingBattleEffect != ItemEffects_Battle.NONE && SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().battleFunction == InterfaceSystem.requestingBattleEffect)
                    {
                            choices.AddChoice(2, "Use");
                    } else {
                        switch (SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().battleFunction)
                        {
                            case ItemEffects_Battle.NONE:
                                break;
                            case ItemEffects_Battle.HEALING:
                                choices.AddChoice(2, "Use");
                                break;
                            case ItemEffects_Battle.STATBOOST:
                                choices.AddChoice(2, "Use");
                                break;
                            case ItemEffects_Battle.TAME:
                                choices.AddChoice(2, "Use");
                                break;
                            default:
                                break;
                        }
                    }
                }
                else
                {
                    if (InterfaceSystem.requestingMenuEffect != ItemEffects_Menu.NONE && SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().menuFunction == InterfaceSystem.requestingMenuEffect)
                    {
                        choices.AddChoice(2, "Select");
                    } else {
                        choices.AddChoice(2, "Select");
                    }
                }
            } else
            {
                switch (SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().menuFunction)
                {
                    case ItemEffects_Menu.NONE:
                        break;
                    case ItemEffects_Menu.HEALING:
                        if (SaveSystem.playerParty.Count > 0) choices.AddChoice(5, "Use");
                        break;
                    case ItemEffects_Menu.EQUIP:
                        Item it = SaveSystem.GetInventoryList(activeTab)[itemPageIndex];

                        bool canLearn = false;

                        for (int i = 0; i < SaveSystem.playerParty.Count; i++)
                        {
                            if (SaveSystem.playerParty[i].CanLearn((Skills)it.uniqueParam))
                            {
                                canLearn = true;
                                break;
                            }
                        }

                        choices.AddChoice(7, "View");

                        if (canLearn)
                        {
                            choices.AddChoice(3, "Equip");
                        }

                        break;
                    case ItemEffects_Menu.WEARTOP:
                    case ItemEffects_Menu.WEARBOTTOM:
                        choices.AddChoice(6, "Wear");
                        break;
                    default:
                        break;
                }
            }
        }

        choices.AddChoice(0, "Cancel");
        choices.Show();

        yield return new WaitForChoice();

        int selIndex = InterfaceSystem.choiceIndex;

        switch (selIndex)
        {
            case 0:
                cancel = true;
                break;
            case 1: // Buy
                int p = ItemSystem.GetItemDatabaseData(shopInv[listIndex]).price;
                if (SaveSystem.playerGold >= p)
                {
                    SaveSystem.playerGold -= p;
                }

                pocketText.text = "<m=default><q=gold></m>" + SaveSystem.playerGold.ToString("##,#");
                ItemSystem.ObtainItem(shopInv[listIndex], displayMessage: false);
                cancel = true;
                break;
            case 2: // Select
                InterfaceInputManager.instance.EnableCursor(false);
                InterfaceInputManager.instance.es.SetSelectedGameObject(null);

                InterfaceSystem.temporaryItemIndex = listIndex;
                InterfaceSystem.temporaryPocketIndex = activeTab;

                FadeOut(_destroyAfter: true);
                break;
            case 3: // Equip

                /*FadeOut(destroyAfter: false, onComplete: ()=> {
                    foreach (GameObject obj in slots)
                    {
                        InventorySlotUpdater s = obj.GetComponent<InventorySlotUpdater>();
                        obj.GetComponent<Button>().enabled = false;
                        s.nameBG.enabled = false;
                        s.quantityBG.enabled = false;
                        s.itemSprite.enabled = false;
                    }

                    foreach (Button b in pocketButtons)
                    {
                        b.gameObject.SetActive(false);
                    }
                });*/

                InterfaceSystem.temporarySkillIndex = (int)(Skills)SaveSystem.GetInventoryList(activeTab)[itemPageIndex].uniqueParam;//(int)SkillSystem.GetSkillDatabaseData(.itemData.intParam).id;
                InterfaceSystem.temporaryItemIndex = itemPageIndex;
                InterfaceSystem.OpenParty(this, true).SetRequest(new MonsterRequest()
                {
                    request = MonsterRequestType.USINGITEM
                });

                while (!IsPriority)
                {
                    yield return null;
                }

                foreach (GameObject obj in slots)
                {
                    InventorySlotUpdater s = obj.GetComponent<InventorySlotUpdater>();
                    s.nameBG.enabled = true;
                    s.quantityBG.enabled = true;
                    s.itemSprite.enabled = true;
                }

                foreach (Button b in pocketButtons)
                {
                    b.gameObject.SetActive(true);
                    b.enabled = true;
                }

                UpdateInventoryDisplay();

                FadeIn();
                InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex]);

                break;
            case 4:
                Item item = SaveSystem.GetInventoryList(activeTab)[itemPageIndex];

                QuantityHandler q = QuantityHandler.instance;
                DialogueHandler d = DialogueHandler.instance;

                if (item.GetData().price == -1)
                {
                    d.AddMessage("I can't buy that.");
                    d.Show();
                    yield return new WaitForDialogueClose();

                    cancel = true;
                    break;
                }

                d.AddMessage("How many?");
                d.KeepOpen();
                d.Show();
                yield return new WaitForDialogueReading();

                q.SetRange(0, item.currentQuantity);
                q.SetPos(SystemVariables.choiceboxDialoguePos);
                q.CloseMessage();
                q.Show();

                yield return new WaitForQuantity();

                int qInt = InterfaceSystem.temporaryQuantity;

                if (qInt > 0)
                {
                    int goldamt = qInt * Mathf.RoundToInt(item.GetData().price / 2);
                    SaveSystem.playerGold += goldamt;

                    item.currentQuantity -= qInt;

                    if (item.currentQuantity <= 0)
                    {
                        SaveSystem.GetInventoryList(activeTab).RemoveAt(listIndex);
                    }

                    UpdateInventoryDisplay();

                    if (SaveSystem.settings_controllerEnabled) InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex - 1]);
                    yield return new WaitForDialogueClose();

                    d.AddMessage(string.Format("{0} sold for <m=default><quad=gold></m>{1}.", item.GetData().id, goldamt.ToString("##,#")));
                    d.Show();
                    yield return new WaitForDialogueReading();

                }
                cancel = true;

                break;
            case 5:
                /*FadeOut(destroyAfter: false, onComplete: () => {
                    foreach (GameObject obj in slots)
                    {
                        InventorySlotUpdater s = obj.GetComponent<InventorySlotUpdater>();
                        obj.GetComponent<Button>().enabled = false;
                        s.nameBG.enabled = false;
                        s.quantityBG.enabled = false;
                        s.itemSprite.enabled = false;
                    }

                    foreach (Button b in pocketButtons)
                    {
                        b.gameObject.SetActive(false);
                    }
                });*/

                InterfaceSystem.temporaryItemIndex = itemPageIndex;
                InterfaceSystem.temporaryPocketIndex = activeTab;
                
                BG.SetAsLastSibling();

                InterfaceSystem.OpenParty(this, false).SetRequest(new MonsterRequest()
                {
                    request = MonsterRequestType.USINGITEM
                });

                while (!IsPriority)
                {
                    yield return null;
                }

                BG.SetSiblingIndex(transform.GetSiblingIndex() - 1);

                UpdateInventoryDisplay();

                foreach (GameObject obj in slots)
                {
                    InventorySlotUpdater s = obj.GetComponent<InventorySlotUpdater>();
                    s.nameBG.enabled = true;
                    s.quantityBG.enabled = true;
                    s.itemSprite.enabled = true;
                }

                foreach (Button b in pocketButtons)
                {
                    b.gameObject.SetActive(true);
                    b.enabled = true;
                }


                FadeIn();
                InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex]);
                break;
            case 6:
                StartCoroutine(UseItem(listIndex));
                break;
            case 7:
                BG.SetAsLastSibling();

                InterfaceSystem.DisplaySkill(this, (Skills)SaveSystem.GetInventoryList(activeTab)[itemPageIndex].uniqueParam, transform.parent);

                yield return new WaitForUIPriority(this);

                InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex]);
                InterfaceInputManager.instance.EnableCursor(true);
                BG.SetSiblingIndex(2);
                break;
            default:
                break;
        }

        foreach (GameObject obj in slots)
        {
            obj.GetComponent<InventorySlotUpdater>().EnableButtons();
            obj.GetComponent<Button>().enabled = true;
        }

        foreach (Button b in pocketButtons)
        {
            b.gameObject.SetActive(true);
            b.enabled = true;
        }

        if (cancel)
        {
            InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex]);
        }


        InterfaceSystem.requestingBattleEffect = ItemEffects_Battle.NONE;
        InterfaceSystem.requestingMenuEffect = ItemEffects_Menu.NONE;
        
        isSelecting = false;
        slots[listIndex].GetComponent<InventorySlotUpdater>().isSelected = false;
    }

    public IEnumerator UseItem(int listIndex)
    {
        switch (SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().menuFunction)
        {
            case ItemEffects_Menu.NONE:
                break;
            case ItemEffects_Menu.HEALING:
                break;
            case ItemEffects_Menu.EQUIP:
                break;
            case ItemEffects_Menu.WEARTOP:
                SaveSystem.playerTop = SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().id;
                _player.UpdatePlayerSprite();

                ButtonsActive(true, slotContainer.transform);
                ButtonsActive(true, pocketContainer.transform);
                InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex]);
                isSelecting = false;
                break;
            case ItemEffects_Menu.WEARBOTTOM:
                SaveSystem.playerBottom = SaveSystem.GetInventoryList(activeTab)[listIndex].GetData().id;

                _player.UpdatePlayerSprite();

                ButtonsActive(true, slotContainer.transform);
                ButtonsActive(true, pocketContainer.transform);
                InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex]);
                isSelecting = false;
                break;
            default:
                break;
        }
        yield break;
    }
    
    public IEnumerator TossItem(int listIndex)
    {
        DialogueHandler msg = DialogueHandler.instance;
        msg.AddMessage("Are you sure?");
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();

        ChoiceHandler choices = ChoiceHandler.instance;
        choices.AddChoice(0, "Yes");
        choices.AddChoice(1, "No");
        choices.CloseMessage();
        choices.KeepCursor();
        choices.Show();
        
        yield return new WaitForChoice();
        
        int choiceIndex = InterfaceSystem.choiceIndex;

        if (choiceIndex == 0)
        {
            ButtonsActive(true, slotContainer.transform);
            ButtonsActive(true, pocketContainer.transform);

            SaveSystem.GetInventoryList(activeTab).RemoveAt(listIndex);
            UpdateInventoryDisplay();
            InterfaceInputManager.instance.es.SetSelectedGameObject(slots[listIndex - 1]);
        }
        else
        {
            StartCoroutine(SelectItem(listIndex));
        }
    }

    public void UpdatePlayerPreview(Item _item)
    {
        playerMat.SetFloat("_EnableEars", SaveSystem.playerEars ? 0 : 1);

        if (_item.GetData().menuFunction == ItemEffects_Menu.WEARTOP)
        {
            playerMat.SetTexture("_Top", ItemSystem.GetClothingSprite(_item).texture);
            playerMat.SetTexture("_Bottom", Resources.Load<Texture>("Apparel/" + ((int)SaveSystem.playerBottom == -1 ? "none" : "Bottoms/" + SaveSystem.playerBottom.ToString())));

        }
        else
        {
            playerMat.SetTexture("_Bottom", ItemSystem.GetClothingSprite(_item).texture);
            playerMat.SetTexture("_Top", Resources.Load<Texture>("Apparel/" + ((int)SaveSystem.playerBottom == -1? "none" : "Tops/" + SaveSystem.playerBottom.ToString())));

        }
    }

    public void UpdateAnimation()
    {
        /*switch (previewDirection)
        {
            case OverworldDir.DOWN:
                animState = PlayerAnimState.walkDown;
                break;
            case OverworldDir.RIGHT:
                animState = PlayerAnimState.walkRight;
                break;
            case OverworldDir.UP:
                animState = PlayerAnimState.walkUp;
                break;
            case OverworldDir.LEFT:
                animState = PlayerAnimState.walkLeft;
                break;
            default:
                break;
        }

        if (previewPlaying)
        {
            if (prevState != animState)
            {
                playerPreview.Play(animState.ToString());
                prevState = animState;
            }
        }
        else
        {
            if (playerPreview.playing)
            {
                playerPreview.Stop(true);
                
                prevState = PlayerAnimState.NONE;
            }
        }*/
    }

    public void SlotEnter(int ind)
    {

    }

    public void SlotExit(int ind)
    {

    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            InterfaceInputManager.instance.es.SetSelectedGameObject(null);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
        ButtonsActive(true);
    }

    public override void OnNotPriority()
    {
        ButtonsActive(false);
    }
}
