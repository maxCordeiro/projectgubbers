﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;
using UnityEngine.EventSystems;

public class InventorySlotUpdater : MonoBehaviour, IDeselectHandler, ISelectHandler{

    public Item item;
    public Items shopItem;

    public Image itemSprite;
    SuperTextMesh itemName;
    SuperTextMesh itemQuantity;

    public Image spriteBG;
    public Image nameBG;
    public Image quantityBG;
    
    Button slotButton;

    public int listIndex;
    public bool isClothingSlot;
    public bool isSelected;

    public Sprite selectedButton, hoverButton, disableButton;

    public InventoryUI invManager;
    bool isDimmed;

    private void Awake()
    {
        itemSprite = nameBG.transform.GetChild(1).GetComponent<Image>();
        itemName = nameBG.transform.GetChild(0).GetComponent<SuperTextMesh>();
        itemQuantity = nameBG.transform.GetChild(2).GetComponent<SuperTextMesh>();

        slotButton = GetComponent<Button>();
        slotButton.onClick.AddListener(() => { SelectItem(); });
    }

    private void Update()
    {
        if (!slotButton.interactable && !isSelected && !isDimmed)
        {
            Dim(true);
            nameBG.sprite = disableButton;
            //quantityBG.sprite = disableButton;
        }
    }

    public void UpdateSlotInfo(bool clear = false)
    {
        bool isShop = invManager.isShop;

        //nameBG.sprite = clear ? invManager.inactiveSlotBG : invManager.activeSlotBG;
        //quantityBG.sprite = clear ? invManager.inactiveSlotBG : invManager.activeSlotBG;

        if (clear)
        {
            itemSprite.sprite = null;
        } else
        {
            Sprite spr;
            
            if (isShop)
            {
                if (ItemSystem.IsCapsule(shopItem))
                {
                    int attrIndex = (int)SkillSystem.GetSkillDatabaseData((Skills)ItemSystem.GetItemDatabaseData(shopItem).intParam).attribute;
                    spr = Resources.LoadAll<Sprite>("skillcapsules")[attrIndex];
                } else {
                    ItemData tempShopItem = ItemSystem.GetItemDatabaseData(shopItem);
                    spr = Resources.Load<Sprite>("Items/" + (ItemSystem.IsClothing(shopItem) ? tempShopItem.menuFunction == ItemEffects_Menu.WEARTOP ? "top" : "bottom" : shopItem.ToString()));
                }
            } else
            {
                if (ItemSystem.IsCapsule(item))
                {
                    int attrIndex = (int)SkillSystem.GetSkillDatabaseData((Skills)item.uniqueParam).attribute;
                    spr = Resources.LoadAll<Sprite>("skillcapsules")[attrIndex];
                } else
                {
                    spr = Resources.Load<Sprite>("Items/" + (ItemSystem.IsClothing(item) ? item.GetData().menuFunction == ItemEffects_Menu.WEARTOP ? "top" : "bottom" : item.id.ToString()));
                }
            }

            itemSprite.sprite = spr;
        }

        itemSprite.color = new Color(1, 1, 1, clear ? 0 : 1);
        itemName.text = clear? "" : isShop? ItemSystem.GetItemName(shopItem) : ItemSystem.GetItemName(item);
        itemQuantity.text = isShop? clear? "" : "<m=default><quad=gold></m> " + ItemSystem.GetItemDatabaseData(shopItem).price.ToString("##,#") : clear? "" : item.currentQuantity.ToString();
    }

    public void UpdateDescription()
    {
        invManager.timerStarted = false;

        if (item == null && !invManager.isShop)
        {
            invManager.descText.text = "";
            return;
        }
        
        invManager.currSelIndex = listIndex;

        if (invManager.isShop)
        {
            if (listIndex >= invManager.shopInv.Count)
            {
                invManager.descText.text = "";
                return;
            }
        }
        else
        {
            if (ItemSystem.IsClothing(item))
            {
                invManager.UpdatePlayerPreview(item);
            }
        }

        Dim(false);

        nameBG.sprite = hoverButton;
        //quantityBG.sprite = hoverButton;
        invManager.descText.text = SystemVariables.trans[item.id.ToString() + "_DESC"];//DialogueSystem.LoadText(invManager.isShop? shopItem.ToString() : item.id.ToString(), 2, "items");
    }   
    
    public void SelectItem()
    {
        if ((item == null && !invManager.isShop) || (invManager.isShop && listIndex >= invManager.shopInv.Count) || invManager.isSelecting) return;
        isSelected = true;

        Dim(false);

        nameBG.sprite = selectedButton;
        //quantityBG.sprite = selectedButton;
        StartCoroutine(invManager.SelectItem(listIndex));
    }

    public void OnDeselect(BaseEventData eventData)
    {
        invManager.timerStarted = true;

        if (invManager.isShop)
        {
            if (listIndex >= invManager.shopInv.Count) return;
        }
        else
        {
            if (item == null) return;
        }

        nameBG.sprite = selectedButton;
    }

    public void EnableButtons()
    {
        if (!gameObject.activeInHierarchy) return;

        if (invManager.isShop)
        {
            if (listIndex >= invManager.shopInv.Count) return;
        } else
        {
            if (item == null) return;
        }
        
        nameBG.enabled = true;
        itemSprite.enabled = true;
        Dim(false);
        nameBG.sprite = selectedButton;
        slotButton.interactable = true;
    }

    public void OnSelect(BaseEventData eventData)
    {
        UpdateDescription();
    }

    public void Dim(bool _dim)
    {
        if (invManager.isShop)
        {
            if (listIndex >= invManager.shopInv.Count) return;
        }
        else
        {
            if (item == null) return;
        }

        float a = _dim ? 0.5f : 1;
        byte ab = (byte)(_dim ? 128 : 255);
        isDimmed = _dim;
        itemSprite.color = new Color(1, 1, 1, a);

        itemName.color = new Color32(itemName.color.r, itemName.color.g, itemName.color.b, ab);
        itemName.Rebuild(false);
        itemQuantity.color = new Color32(itemQuantity.color.r, itemQuantity.color.g, itemQuantity.color.b, ab);
        itemQuantity.Rebuild(false);
    }
}
