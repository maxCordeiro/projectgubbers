﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;

public class AttackaponUI : UISystem
{
    public SuperTextMesh[] capsuleNameText;
    public SuperTextMesh[] priceText;
    public SuperTextMesh voucherButtonText, voucherText, goldAmt;
    public Image voucherIcon, voucherBG;

    public Button[] sellButtons;

    public GameObject capsuleBG, capsulePanel;
    public ItemTabUpdater[] capsuleItems;
    public Button voucherButton, actualVoucher;

    public Rarity specRarity;
    Rarity vRarity;
    public MonsterAttribute specAttr;
    MonsterAttribute vAttr;

    public AttackaponSpecial special = AttackaponSpecial.NONE;
    AttackaponSpecial vSpecial = AttackaponSpecial.NONE;

    float discount;

    Item voucher;

    public int[] quantity = new int[3], price = new int[3];

    List<Item> capsules = new List<Item>(10);

    bool panelOpen, usingVoucher, buying, updatingCapsules;
    
    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
       
        quantity[0] = 1;
        quantity[1] = 10;
        quantity[2] = 5;

        sellButtons[0].onClick.AddListener(() => {StartCoroutine(BuyingCapsules(0, false)); });
        sellButtons[1].onClick.AddListener(() => { StartCoroutine(BuyingCapsules(1, false)); });
        sellButtons[2].onClick.AddListener(() => { StartCoroutine(BuyingCapsules(2, true)); });
        voucherButton.onClick.AddListener(() => { UseVoucher(); });

        capsuleNameText[0].text = quantity[0] + " Skill Capsules";
        capsuleNameText[1].text = quantity[1] + " Skill Capsules";
        capsuleNameText[2].text = quantity[2] + " Skill Capsules";

        price[0] = Mathf.RoundToInt(500 * quantity[0]);
        price[1] = Mathf.RoundToInt(500 * quantity[1]);
        price[2] = Mathf.RoundToInt(500 * (quantity[2] * 2) * (special == AttackaponSpecial.DISCOUNT? 0.9f : 1));

        priceText[0].text = string.Format(priceText[0].text, price[0].ToString("#,##"));
        priceText[1].text = string.Format(priceText[1].text, price[1].ToString("#,##"));
        priceText[2].text = string.Format(priceText[2].text, price[2].ToString("#,##"));
        
        for (int i = 0; i < capsuleItems.Length; i++)
        {
            int ind = i;
            capsuleItems[ind].GetComponent<Button>().onClick.AddListener(() => {
                InterfaceInputManager.instance.EnableCursor(false);
                InterfaceSystem.DisplaySkill(this, (Skills)capsules[ind].uniqueParam, capsuleItems[ind].transform.parent.parent);
            });
        }

        goldAmt.text = InterfaceSystem.GetPlayerGold();

        GameUtility.EnableBackground();
        FadeIn(OnFadeIn);
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !(buying || usingVoucher) && InterfaceSystem.InDialogueChoices())
        {
            FadeOut(true);
        }

        if (!InterfaceSystem.InDialogueChoices() && InterfaceInputManager.instance.es.currentSelectedGameObject != null)
        {
            ReposCursor();
        }

        /*if (!InterfaceSystem.inSubMenu && !InterfaceInputManager.instance.controllerCursor.activeInHierarchy && !InterfaceSystem.inPanel)
        {
            InterfaceInputManager.instance.EnableCursor(true);
        }
        
        if (InterfaceSystem.inPanel && InterfaceInputManager.instance.controllerCursor.activeInHierarchy)
        {
            InterfaceInputManager.instance.EnableCursor(false);
        } else if (!InterfaceSystem.inPanel && !InterfaceInputManager.instance.controllerCursor.activeInHierarchy)
        {
            InterfaceInputManager.instance.EnableCursor(true);
        }*/

        if ((InterfaceSystem.rePlayer.GetButtonDown("A") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !InterfaceSystem.inPanel && buying && !updatingCapsules && !ChoiceHandler.instance.isOpen)
        {
            buying = false;
            capsuleBG.SetActive(false);
            capsulePanel.SetActive(false);

            Buttons(true);

            InterfaceInputManager.instance.es.SetSelectedGameObject(sellButtons[0].gameObject);
        }

    }

    public void UseVoucher()
    {
        if (usingVoucher) return;

        StartCoroutine(SelectVoucher());
    }

    public IEnumerator SelectVoucher()
    {
        usingVoucher = true;

        Buttons(false);

        InterfaceSystem.OpenInventory(this, true, false);

        yield return new WaitForUIPriority(this);

        if (InterfaceSystem.temporaryItemIndex != -1)
        {
            Item selectedItem = SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex];

            if (selectedItem.GetData().menuFunction != ItemEffects_Menu.VOUCHER)
            {
                usingVoucher = false;
                yield break;
            }
            
            voucherText.text = selectedItem.GetData().id.ToString();
        
            if (selectedItem.GetData().param.GetType() == typeof(Rarity))
            {
                voucher = selectedItem;
                vRarity = (Rarity)selectedItem.GetData().param;
                vSpecial = AttackaponSpecial.RARITY;
            } else if (selectedItem.GetData().param.GetType() == typeof(MonsterAttribute))
            {
                voucher = selectedItem;
                vAttr = (MonsterAttribute)selectedItem.GetData().param;
                vSpecial = AttackaponSpecial.ATTRIBUTE;
            }
        }
        
        Buttons(true);

        InterfaceInputManager.instance.es.SetSelectedGameObject(voucherButton.gameObject);

        usingVoucher = false;
        yield break;
    }

    public IEnumerator BuyingCapsules(int ind, bool _special)
    {
        if (buying) yield break;

        Buttons(false);

        buying = true;

        DialogueHandler m = DialogueHandler.instance;
        ChoiceHandler c = ChoiceHandler.instance;

        if (SaveSystem.playerGold < price[ind])
        {
            m.AddMessage("BROKE!");
            m.Show();
            yield return new WaitForDialogueClose();
            yield break;
        }
        
        m.AddMessage(string.Format("Would you like to purchase<br>{0} Skill Capsules for <m=default><q=gold></m>{1}?", quantity[ind], price[ind].ToString("#,##")));
        m.KeepOpen();
        m.Show();

        yield return new WaitForDialogueReading();

        c.AddChoice(0, "Yes");
        c.AddChoice(1, "No");
        c.CloseMessage();
        c.SetPos(SystemVariables.choiceboxDialoguePos);
        c.Show();

        yield return new WaitForChoice();

        int chind = InterfaceSystem.choiceIndex;

        if (chind == 0)
        {
            for (int i = 0; i < quantity[ind]; i++)
            {
                Rarity randRarity = Rarity.NONE;
                MonsterAttribute randAttr = MonsterAttribute.NONE;

                if (_special)
                {
                    if (special == AttackaponSpecial.RARITY)
                    {
                        randRarity = specRarity;
                    } else if (special == AttackaponSpecial.ATTRIBUTE)
                    {
                        randAttr = specAttr;
                    }
                } else
                {
                    if (vSpecial == AttackaponSpecial.RARITY)
                    {
                        randRarity = vRarity;
                    } else if (vSpecial == AttackaponSpecial.ATTRIBUTE)
                    {
                        randAttr = vAttr;
                    }
                }

                Skills s = SkillSystem.GetRandomSkill(randRarity, randAttr);
                Item cap = new Item(Items.SKILLCAPSULE, s);

                capsules.Add(cap);
            }

            for (int i = 0; i < capsules.Count; i++)
            {
                capsuleItems[i].AssignItem(capsules[i]);
            }

            InterfaceInputManager.instance.EnableCursor(false);

            capsuleBG.SetActive(true);
            capsulePanel.SetActive(true);


            updatingCapsules = true;

            yield return new WaitForEndOfFrame();

            for (int i = 0; i < capsules.Count; i++)
            {
                ItemSystem.ObtainItem(capsules[i], displayMessage: false);
                
                capsuleItems[i].UpdateDisplay();

                if (i == 0)
                {
                    yield return new WaitForSeconds(0.75f);
                } else
                {
                    yield return new WaitForSeconds(0.15f);
                }

            }

            InterfaceInputManager.instance.EnableCursor(true);
            InterfaceInputManager.instance.es.SetSelectedGameObject(capsuleItems[0].gameObject);

            SaveSystem.playerGold -= price[ind];
            goldAmt.text = InterfaceSystem.GetPlayerGold();
            updatingCapsules = false;

        } else
        {
            buying = false;

            Buttons(true);

            InterfaceInputManager.instance.es.SetSelectedGameObject(voucherButton.gameObject);
        }

        yield break;
    }

    public void DisplayPanel()
    {
        
    }

    public void Buttons(bool active)
    {
        ButtonsActive(active, transform.GetChild(0).GetChild(3), false);
        voucherButton.enabled = active;
        actualVoucher.enabled = active;
    }

    public override void OnFadeIn()
    {

    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
        InterfaceInputManager.instance.controllerCursor.transform.SetAsLastSibling();

        InterfaceInputManager.instance.es.SetSelectedGameObject(sellButtons[0].gameObject);
        InterfaceInputManager.instance.EnableCursor(true);
    }

    public override void OnNotPriority()
    {
    }
}

public enum AttackaponSpecial
{
    NONE,
    ATTRIBUTE,
    RARITY,
    DISCOUNT,
}