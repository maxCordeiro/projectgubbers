﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using GameDatabase;
using System.Linq;
using System;
using Random = UnityEngine.Random;

public class DaycareUI : UISystem
{
    [Space]
    [Header("Dens")]
    public DaycareDenUpdater[] denUpdater;
    public Button[] denUpdaterButtons;
    
    public SuperTextMesh denInfo, momHeader, momName, dadHeader, dadName, itemHeader, itemName, eggHeader, statsText, pageText;
    public Image momIcon, dadIcon, itemIcon, eggIcon, leftPage, rightPage;

    int currentPage = 0, totalPages;
    bool buttonClicked;

    int buttonIndex;
    bool textDisplaying;

    BreedingManager bm;
    Sprite[] eggs;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();

        eggs = Resources.LoadAll<Sprite>("eggs");
        bm = GameObject.FindObjectOfType<BreedingManager>();
        
        for (int i = 0; i < 6; i++)
        {
            int _i = i;
            denUpdater[i].dmv = this;
            
            denUpdater[i].button.onClick.AddListener(()=>{MonsterButtonSelect(denUpdater[_i].index);});
        }
        
        UpdateInfo();

        GameUtility.EnableBackground();
        FadeIn();
    }
    
    public void UpdatePanelInfo(int _index)
    {
        bool clear = SaveSystem.dcMons.ElementAtOrDefault(_index) == null;
        
        if (!clear)
        {
            DayCarePair dcp = SaveSystem.dcMons[_index];
            
            if (dcp.female != null)
            {
                momName.text = dcp.female.GetName() + "<br>LV" + dcp.female.GetMonLevel() + " " + dcp.female.GetGender();
                momIcon.sprite = dcp.female.GetIcon();
                momIcon.enabled = true;
            } else {
                momIcon.enabled = false;
                momName.text = "--";
            }
            
            if (dcp.male != null)
            {
                dadName.text = dcp.male.GetName() + "<br>LV" + dcp.male.GetMonLevel() + " " + dcp.male.GetGender();
                dadIcon.sprite = dcp.male.GetIcon();
                dadIcon.enabled = true;
            } else {
                dadIcon.enabled = false;
                dadName.text = "--";
            }
            
            if (dcp.breedingItem != null)
            {
                itemIcon.sprite = ItemSystem.GetItemSprite(dcp.breedingItem);
                itemIcon.enabled = true;
                itemName.text = ItemSystem.GetItemName(dcp.breedingItem);
            } else {
                itemIcon.enabled = false;
                itemName.text = "--";
            }
            
            if (dcp.hasEgg)
            {
                eggHeader.enabled = true;
                eggIcon.sprite = eggs[(int)dcp.egg.GetFamily()];
                eggIcon.enabled = true;
            } else {
                eggHeader.enabled = false;
                eggIcon.enabled = false;
            }
            
            statsText.text = string.Format("{0} eggs made<br>Started on {1}", dcp.eggsMade, dcp.startTime.ToString());
        } else {
            momName.text = "--";
            momIcon.enabled = false;
            dadName.text = "--";
            dadIcon.enabled = false;
            itemName.text = "--";
            itemIcon.enabled = false;
            eggIcon.enabled = false;
            eggHeader.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !ChoiceHandler.instance.isOpen && !buttonClicked)
        {
            GameUtility.DisableBackground();
            
            FadeOut(true, true);
        }
        
        if (!InterfaceSystem.InDialogueChoices() && InterfaceInputManager.instance.es.currentSelectedGameObject != null)
        {
            ReposCursor();
        }
    }

    public void UpdateInfo()
    {
        totalPages = (int)Mathf.Ceil(SaveSystem.dcDenUnlocks / 6);
        pageText.text = string.Format("{0}/{1}", currentPage + 1, totalPages + 1);  
        
        
        for (int i = 0; i < 6; i++)
        {
            int p = i + 6 * currentPage;

            denUpdater[i].index = p;

            if (p >= SystemVariables.defaultDens + SaveSystem.dcDenUnlocks)
            {
                denUpdater[i].ClearDisplay(true);
                continue;
            }
            
            if (SaveSystem.dcMons.ElementAtOrDefault(p) == null)
            {
                denUpdater[i].ClearDisplay();
                continue;
            }
            
            denUpdater[p].UpdateDisplay();
        }

        InterfaceInputManager.instance.es.SetSelectedGameObject(denUpdater[0].gameObject);
        ReposCursor();
        InterfaceInputManager.instance.EnableCursor(true);
    }

    public void MonsterButtonSelect(int _index)
    {
        if (buttonClicked || !IsPriority) return;

        buttonIndex = _index;

        StartCoroutine(MonsterSelected());
    }

    public IEnumerator MonsterSelected()
    {
        buttonClicked = true;

        int p = buttonIndex + 4 * currentPage;

        ChoiceHandler choices = ChoiceHandler.instance;
        choices.SetPos(new Vector2(350, 96));
        DialogueHandler msgH = DialogueHandler.instance;

        if (SaveSystem.playerParty.ElementAtOrDefault(1) != null && ((SaveSystem.dcMons.ElementAtOrDefault(p) == null) || (SaveSystem.dcMons.ElementAtOrDefault(p) != null && SaveSystem.dcMons[p].male == null || SaveSystem.dcMons[p].female == null)))
        {
            choices.AddChoice(0, "Add Charmling");
        }
            
        if (SaveSystem.dcMons.ElementAtOrDefault(p) != null)
        {
            DayCarePair dcp = SaveSystem.dcMons.ElementAtOrDefault(p);

            if (dcp.breedingItem == null) choices.AddChoice(1, "Add Item");

            if (SaveSystem.dcMons[p].male != null && SaveSystem.dcMons[p].female != null)
            {
                choices.AddChoice(2, "Check Status");
            }

            choices.AddChoice(3, "Take Charmling");

            if (dcp.breedingItem != null)
            {
                choices.AddChoice(4, "Take Item");
            }

            if (dcp.hasEgg)
            {
                choices.AddChoice(5, "Transfer Egg");
            }
        }

        choices.AddChoice(6, "Cancel");

        choices.Show();

        yield return new WaitForChoice();

        int c = InterfaceSystem.choiceIndex;

        switch (c)
        {
            case 0: // Add Charmling
                yield return StartCoroutine(AddingMonster());
                break;
            case 1: // Add Item
                yield return StartCoroutine(AddingItem());
                break;
            case 2: // Check Status

                float chance = GameUtility.CompatibilityCheck(p);

                string msg = "";

                if (chance <= 0.1f)
                {
                    msg = "The two don't seem to get along very well.";
                } else if (chance < 0.6f)
                {
                    msg = "The two seem to get along fine.";
                } else
                {
                    msg = "The two are getting along great!";
                }

                msgH.AddMessage(msg);
                msgH.Show();
                yield return new WaitForDialogueClose();

                break;
            case 3: // Take Charmling
                yield return StartCoroutine(TakingMonster());
                break;
            case 4: // Take Item
                ItemSystem.ObtainItem(SaveSystem.dcMons[p].breedingItem, displayMessage: false);
                SaveSystem.dcMons[buttonIndex + 4 * currentPage].breedingItem = null;
                UpdateInfo();
                break;
            case 5: // Transfer Egg
                SaveSystem.dcIncubators.Add(new Incubator(SaveSystem.dcMons[p].egg, SaveSystem.dcMons[p].female, SaveSystem.dcMons[p].male, new Date(SaveSystem.dcMons[p].egg.daysToHatch), new Date(), null, SaveSystem.dcMons[p].breedingItem));
                SaveSystem.dcMons[p].egg = null;
                SaveSystem.dcMons[p].hasEgg = false;
                UpdateInfo();
                break;
            default:
                break;
        }
        buttonClicked = false;
        InterfaceInputManager.instance.es.SetSelectedGameObject(denUpdater[buttonIndex].gameObject);
        InterfaceInputManager.instance.EnableCursor(true);
    }

    public IEnumerator AddingItem()
    {
        yield return StartCoroutine(FadeOutCoroutine(false));
        
        for (int i = 0; i < 6; i++)
        {
            denUpdater[i].button.enabled = false;
        }
        
        InterfaceSystem.OpenInventory(this, true);

        yield return new WaitForUIPriority(this);

        SaveSystem.dcMons[buttonIndex + 4 * currentPage].breedingItem = SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex];

        denUpdater[buttonIndex].UpdateDisplay();
        UpdateInfo();

        buttonClicked = false;
        
        for (int i = 0; i < 6; i++)
        {
            denUpdater[i].button.enabled = true;
        }
        
        FadeIn();
    }

    public IEnumerator AddingMonster()
    {

        FadeOut(false, false);

        while (isFading)
        {
            yield return new WaitForEndOfFrame();
        }

        cGroup.interactable = false;

        //InterfaceSystem.OpenParty(0, true);
        InterfaceSystem.OpenCharmBag(this).SetRequest(new MonsterRequest()
        {
            request = MonsterRequestType.ANY
        });

        yield return new WaitForUIPriority(this);

        FadeIn();

        int p = buttonIndex + 4 * currentPage;
        
        cGroup.interactable = true;
        
        if (SaveSystem.dcMons.ElementAtOrDefault(p) != null && SaveSystem.dcMons[p].hasEgg || InterfaceSystem.temporaryMonsterIndex == -1)
        {
            yield return new WaitForSeconds(0.1f);

            buttonClicked = false;

            yield break;
        }

        bool inBag = InterfaceSystem.temporaryInBag;
        int monIndex = InterfaceSystem.temporaryMonsterIndex;

        Monster tempMon = inBag? SaveSystem.playerRanch[monIndex] : SaveSystem.playerParty[monIndex];
        DialogueHandler msg = DialogueHandler.instance;

        bool addedMon = false;

        if (tempMon.GetGenderInt() == 0) // Female
        {

            if (SaveSystem.dcMons.ElementAtOrDefault(p) == null)
            {
                DayCarePair dcp = new DayCarePair();
                dcp.female = tempMon;

                SaveSystem.dcMons.Insert(p, dcp);
                GameUtility.RemoveMonster(!inBag, monIndex);
                addedMon = true;
            }
            else
            {
                if (SaveSystem.dcMons[p].female != null)
                {
                    ChoiceHandler choices = ChoiceHandler.instance;

                    choices.AddChoice(0, "Replace " + SaveSystem.dcMons[p].female.GetName());
                    choices.AddChoice(1, "Cancel");
                    choices.RememberCursorPos();
                    choices.Show();

                    yield return new WaitForChoice();

                    int choiceIndex = InterfaceSystem.choiceIndex;

                    if (choiceIndex == 0)
                    {
                        Debug.Log("TempIndex: " + InterfaceSystem.temporaryMonsterIndex);
                        GameUtility.RemoveMonster(!inBag, monIndex);
                        GameUtility.ObtainMonster(SaveSystem.dcMons[p].female);
                        SaveSystem.dcMons[buttonIndex + 4 * currentPage].female = tempMon;
                        addedMon = true;
                    }
                } else
                {
                    SaveSystem.dcMons[p].female = tempMon;
                    GameUtility.RemoveMonster(!inBag, monIndex);
                    addedMon = true;
                }
            }
        } else if (tempMon.GetGenderInt() == 1) // Male
        {
            if (SaveSystem.dcMons.ElementAtOrDefault(p) == null)
            {
                DayCarePair dcp = new DayCarePair();
                dcp.male = tempMon;

                SaveSystem.dcMons.Insert(p, dcp);
                GameUtility.RemoveMonster(!inBag, monIndex);
                addedMon = true;
            }
            else
            {
                if (SaveSystem.dcMons[p].male != null)
                {
                    ChoiceHandler choices = ChoiceHandler.instance;

                    /*choices.AddChoice("Replace " + SaveSystem.dcMons[p].male.GetName(), () => {
                        Debug.Log("TempIndex: " + InterfaceSystem.temporaryMonsterIndex);

                        GameUtility.RemoveMonster(!inBag, monIndex);
                        GameUtility.ObtainMonster(SaveSystem.dcMons[p].male);
                        SaveSystem.dcMons[p].male = tempMon;
                        addedMon = true;
                    });

                    choices.AddChoice("Cancel", () => {
                    });*/
                    choices.RememberCursorPos();
                    choices.Show();

                    yield return new WaitForChoice();
                } else
                {
                    SaveSystem.dcMons[p].male = tempMon;
                    GameUtility.RemoveMonster(!inBag, monIndex);
                    addedMon = true;
                }
            }
        }
        else
        {
            msg.Name(DialogueSystem.LoadName("npc_daycare", "dialogue_npc"));

            msg.AddMessage(DialogueSystem.LoadMsg("npc_daycare", "greeting", "dialogue_npc"));
            msg.Show();

            yield return new WaitForDialogueReading();

            buttonClicked = false;
        }

        UpdateInfo();
        denUpdater[buttonIndex].UpdateDisplay();
        
        if (addedMon && !SaveSystem.dcMons[p].hasEgg)
        {
            SaveSystem.dcMons[p].startTime = new Date().AddHours(Random.Range(4, 15));
        }

        yield return new WaitForSeconds(0.1f);

        buttonClicked = false;
    }

    public IEnumerator TakingMonster()
    {
        ChoiceHandler choices = ChoiceHandler.instance;

        if (SaveSystem.dcMons[buttonIndex + 4 * currentPage].male != null)
        {
            choices.AddChoice(0, SaveSystem.dcMons[buttonIndex + 4 * currentPage].male.GetName());
        }

        if (SaveSystem.dcMons[buttonIndex + 4 * currentPage].female != null)
        {
            choices.AddChoice(1, SaveSystem.dcMons[buttonIndex + 4 * currentPage].female.GetName());
        }

        choices.AddChoice(2, "Cancel");
        choices.RememberCursorPos();
        choices.Show();

        yield return new WaitForChoice();

        int choiceIndex = InterfaceSystem.choiceIndex;

        switch (choiceIndex)
        {
            case 0:
                GameUtility.ObtainMonster(SaveSystem.dcMons[buttonIndex + 4 * currentPage].male);
                SaveSystem.dcMons[buttonIndex + 4 * currentPage].male = null;
                break;
            case 1:
                GameUtility.ObtainMonster(SaveSystem.dcMons[buttonIndex + 4 * currentPage].female);
                SaveSystem.dcMons[buttonIndex + 4 * currentPage].female = null;
                break;
            default:
                break;
        }

        UpdateInfo();
        buttonClicked = false;
    }

    [ContextMenu("Update References")]
    public void UpdateRef()
    {
        /*
        bg = new Image[4];

        femaleSprites = new Image[4];
        maleSprites = new Image[4];
        eggSprites = new Image[4];
        itemSprites = new Image[4];
        itemBGs = new Image[4];

        femaleNames = new SuperTextMesh[4];
        maleNames = new SuperTextMesh[4];
        itemNames = new SuperTextMesh[4];

        for (int i = 0; i < 4; i++)
        {
            Transform tempObj = transform.GetChild(0).GetChild(i);

            bg[i] = tempObj.GetComponent<Image>();

            femaleSprites[i] = tempObj.GetChild(0).GetComponent<Image>();
            maleSprites[i] = tempObj.GetChild(1).GetComponent<Image>();
            eggSprites[i] = tempObj.GetChild(2).GetComponent<Image>();
            itemBGs[i] = tempObj.GetChild(3).GetComponent<Image>();
            itemSprites[i] = itemBGs[i].transform.GetChild(0).GetComponent<Image>();

            femaleNames[i] = femaleSprites[i].transform.GetChild(0).GetComponent<SuperTextMesh>();
            maleNames[i] = maleSprites[i].transform.GetChild(0).GetComponent<SuperTextMesh>();
            itemNames[i] = itemBGs[i].transform.GetChild(1).GetComponent<SuperTextMesh>();
        }*/
    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
    }

    public override void OnNotPriority()
    {
    }
}
