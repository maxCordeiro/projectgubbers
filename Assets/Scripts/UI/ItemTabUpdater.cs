﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDatabase;
using MonsterSystem;

public class ItemTabUpdater : MonoBehaviour
{
    public Item tabItem;
    public Image itemSprite;

    public SuperTextMesh itemName, itemQuantity;
    public Image nameBG;

    public Button tabButton;

    //public Sprite selectedButton, hoverButton, disableButton;

    //bool isDimmed;

    // Start is called before the first frame update
    void Start()
    {
        tabButton.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AssignItem(Item _item)
    {
        tabItem = _item;
    }

    public void UpdateDisplay()
    {
        if (tabItem == null || tabItem.id == Items.NONE)
        {
            nameBG.gameObject.SetActive(false);

            return;
        }

        Sprite spr;

        if (ItemSystem.IsCapsule(tabItem))
        {
            int attrIndex = (int)SkillSystem.GetSkillDatabaseData((Skills)tabItem.uniqueParam).attribute;
            spr = Resources.LoadAll<Sprite>("skillcapsules")[attrIndex];
        } else
        {
            spr = Resources.Load<Sprite>("Items/" + (ItemSystem.IsClothing(tabItem) ? tabItem.GetData().menuFunction == ItemEffects_Menu.WEARTOP ? "top" : "bottom" : tabItem.id.ToString()));
        }

        itemSprite.sprite = spr;
        itemName.text = ItemSystem.GetItemName(tabItem);
        itemQuantity.text = tabItem.currentQuantity.ToString();

        tabButton.enabled = true;
    }
}
