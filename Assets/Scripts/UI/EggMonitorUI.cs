﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;
using UnityEngine.UI;
using System.Linq;

public class EggMonitorUI : UISystem
{
    [Header("Incubators")]
    public Image[] eggSprites;
    public Image[] eggTerrains;
    public Image[] incLight;
    public Image[] incubator;
    public Image[] incBG;
    public Image[] incItem;
    public Image[] incShadows;
    public Sprite[] incBGSprites;

    public Button[] nextButtons;
    public Button[] incButtons;

    public SuperTextMesh[] incTimers;
    public Image[] incClocks;

    [Header("Info")]
    public SuperTextMesh currItem;
    public SuperTextMesh usedItem;
    public SuperTextMesh date;

    public SuperTextMesh incPages;

    public Image currItemImg;
    public Image usedItemImg;

    public Image momImg;
    public Image dadImg;

    int currentPage;

    Sprite[] eggs;
    Sprite[] terrains;

    bool inCoroutine;

    int selectedInd; // 1 of 4 incubators
    int selectedListInd; // Index in incubator list

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();

        eggs = Resources.LoadAll<Sprite>("eggs");
        terrains = Resources.LoadAll<Sprite>("incTerrains");

        ClearInfo();
        UpdateIncubators();

        FadeIn();
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if (InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B"))
        {
            GameUtility.DisableBackground();

            FadeOut(true, true);
        }

        if (!InterfaceSystem.InDialogueChoices() && InterfaceInputManager.instance.es.currentSelectedGameObject != null)
        {
            ReposCursor();
        }
    }

    public void UpdateIncubators()
    {
        for (int i = 0; i < 4; i++)
        {
            int b = i + currentPage * 4;

            if (b >= SystemVariables.defaultIncubators + SaveSystem.dcIncubatorUnlocks || SaveSystem.dcIncubators.ElementAtOrDefault(b) == null)
            {
                incButtons[i].interactable = false;
                incBG[i].sprite = incBGSprites[2];

                incLight[i].enabled = false;

                eggSprites[i].enabled = false;
                eggTerrains[i].enabled = false;

                incItem[i].enabled = false;

                incClocks[i].enabled = false;
                incTimers[i].enabled = false;

                if (b >= SystemVariables.defaultIncubators + SaveSystem.dcIncubatorUnlocks)
                {
                    incubator[i].enabled = false;
                }
                else if (SaveSystem.dcIncubators.ElementAtOrDefault(b) == null)
                {

                }

                continue;
            }

            incBG[i].sprite = incBGSprites[0];

            int famIndex = (int)SaveSystem.dcIncubators[b].egg.GetFamily();

            incButtons[i].interactable = true;

            incLight[i].enabled = true;

            eggSprites[i].enabled = true;
            eggTerrains[i].enabled = true;

            incItem[i].enabled = true;

            incClocks[i].enabled = true;
            incTimers[i].enabled = true;

            eggSprites[i].sprite = eggs[famIndex];
            eggTerrains[i].sprite = terrains[famIndex];

            incItem[i].enabled = SaveSystem.dcIncubators[b].hatchingItem != null;
    
            if (!SaveSystem.dcIncubators[b].readyToHatch)
            {
                incTimers[i].text = GameUtility.HoursUntil(SaveSystem.dcIncubators[b].hatchDate).ToString() + "h";
            }
            else
            {
                incTimers[i].text = "Ready!";
            }

        }
    }

    public void UpdateInfo(int ind)
    {
        if (ChoiceHandler.instance.isOpen) return;

        int b = ind + currentPage * 4;

        selectedInd = ind;
        selectedListInd = b;

        if (SaveSystem.dcIncubators[b].readyToHatch)
        {
            StartCoroutine(Choices());
            return;
        }

        incBG[ind].sprite = incBGSprites[1];

        momImg.sprite = SaveSystem.dcIncubators[b].mom.GetIcon();
        dadImg.sprite = SaveSystem.dcIncubators[b].dad.GetIcon();

        momImg.enabled = true;
        dadImg.enabled = true;

        if (SaveSystem.dcIncubators[b].hatchingItem != null)
        {
            currItem.text = ItemSystem.GetItemName(SaveSystem.dcIncubators[b].hatchingItem);
            currItemImg.enabled = true;
            currItemImg.sprite = ItemSystem.GetItemSprite(SaveSystem.dcIncubators[b].hatchingItem);
        }
        else
        {
            currItem.text = "--";
            currItemImg.enabled = false;
            currItemImg.sprite = null;
        }

        if (SaveSystem.dcIncubators[b].bredItem != null)
        {
            usedItem.text = ItemSystem.GetItemName(SaveSystem.dcIncubators[b].bredItem);
            usedItemImg.enabled = true;
            usedItemImg.sprite = ItemSystem.GetItemSprite(SaveSystem.dcIncubators[b].bredItem);
        }
        else
        {
            usedItem.text = "--";
            usedItemImg.enabled = false;
            usedItemImg.sprite = null;
        }

        date.text = SaveSystem.dcIncubators[b].createDate.ToString();
    }

    public IEnumerator Choices()
    {
        ChoiceHandler ch = ChoiceHandler.instance;

        ch.AddChoice(0, "Take Egg");
        ch.AddChoice(1, "Cancel");
        ch.Show();

        yield return new WaitForChoice();

        int choiceInd = InterfaceSystem.choiceIndex;

        if (choiceInd == 0)
        {
            StartCoroutine(TakeEgg());
        }

        yield break;
    }

    public IEnumerator TakeEgg()
    {
        GameUtility.ObtainMonster(SaveSystem.dcIncubators[selectedListInd].egg);
        ItemSystem.ObtainItem(SaveSystem.dcIncubators[selectedListInd].bredItem, displayMessage: false);

        yield return null;
    }

    public void ClearInfo()
    {
        momImg.sprite = null;
        momImg.enabled = false;

        dadImg.sprite = null;
        dadImg.enabled = false;

        usedItem.text = "--";
        usedItemImg.enabled = false;
        usedItemImg.sprite = null;

        currItem.text = "--";
        currItemImg.enabled = false;
        currItemImg.sprite = null;

        date.text = "--";

        for (int i = 0; i < 4; i++)
        {
            int b = i + currentPage * 4;

            if (b >= SystemVariables.defaultIncubators + SaveSystem.dcIncubatorUnlocks || SaveSystem.dcIncubators.ElementAtOrDefault(b) == null)
            {
                incButtons[i].interactable = false;
                incBG[i].sprite = incBGSprites[2];

                continue;
            }

            incButtons[i].interactable = true;
            incBG[i].sprite = incBGSprites[0];
        }
    }

    public void UpdateTime()
    {
        for (int i = 0; i < 4; i++)
        {
            int b = i + currentPage * 4;

            if (b >= SystemVariables.defaultIncubators + SaveSystem.dcIncubatorUnlocks || SaveSystem.dcIncubators.ElementAtOrDefault(b) == null)
            {
                continue;
            }

            int hr = GameUtility.HoursUntil(SaveSystem.dcIncubators[b].hatchDate);

            if (!SaveSystem.dcIncubators[b].readyToHatch)
            {
                incTimers[i].text = hr + "h";
            }
            else
            {
                incTimers[i].text = "Ready!";
            }
        }
    }


    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
    }

    public override void OnNotPriority()
    {
    }
}