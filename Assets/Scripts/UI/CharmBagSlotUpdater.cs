﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Linq;

public class CharmBagSlotUpdater : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public CharmBagUI cbm;
    public int pageIndex, slotIndex;
    public bool partySlot;

    public Button button;
    public Image icon, bg;

    private void Start()
    {
        cbm.onPageChanged += UpdateSummary;
        button.onClick.AddListener(()=> { cbm.SelectMonster(partySlot, slotIndex, pageIndex); });
    }

    public void OnSelect(BaseEventData baseEvent)
    {
        UpdateSummary();
        //bg.color = Color.blue;
    }

    public void UpdateSummary()
    {
        if (InterfaceInputManager.instance.es.currentSelectedGameObject != gameObject) return;

        cbm.UpdateSummaryDisplay(partySlot, slotIndex, pageIndex);
    }

    public void OnDeselect(BaseEventData baseEvent)
    {
        //bg.color = Color.red;
    }

    public void UpdateDisplay()
    {
        StartCoroutine(UpdateDisplayRoutine());
    }

    IEnumerator UpdateDisplayRoutine()
    {
        LeanTween.color(icon.rectTransform, new Color(1, 1, 1, 0), 0.075f);
        yield return new WaitForSeconds(0.075f);

        if (partySlot)
        {
            if (SaveSystem.playerParty.ElementAtOrDefault(pageIndex) != null)
            {
                icon.sprite = SaveSystem.playerParty[pageIndex].GetIcon();
                icon.enabled = true;
            }
            else
            {
                icon.enabled = false;
            }
        }
        else
        {
            if (SaveSystem.playerRanch.ElementAtOrDefault(pageIndex) != null)
            {
                icon.sprite = SaveSystem.playerRanch[pageIndex].GetIcon();
                icon.enabled = true;
            }
            else
            {
                icon.enabled = false;
            }
        }

        LeanTween.color(icon.rectTransform, Color.white, 0.075f);
        yield return new WaitForSeconds(0.075f);
    }
}
