﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;
using UnityEngine.EventSystems;
using Rewired;
using Rewired.Integration.UnityUI;
using System;

public class PauseUI : UISystem {

    public Button[] pauseButtons;
    public Transform buttonsContainer;
    Vector3[] buttonPos;

    string[] buttonNames;

    public SuperTextMesh menuText;
    
    bool prevInMenu;

    int lastSelectedButton = -1;
    
    private new void Start()
    {
        base.Start();

        cGroup = GetComponent<CanvasGroup>();

        //Party,Inventory,Ranch Permit,Settings,Exit To Desktop
        
        buttonNames = new string[] {
            SystemVariables.trans["pause_party"],
            SystemVariables.trans["pause_inventory"],
            SystemVariables.trans["pause_charmbag"],
            SystemVariables.trans["pause_permit"],
            SystemVariables.trans["pause_encyclopedia"],
            SystemVariables.trans["pause_settings"],
            SystemVariables.trans["pause_exit"],
        };
        
        //SystemVariables.menuStrings["pauseOptions"].Split(',');

        pauseButtons = new Button[5];
        buttonPos = new Vector3[5];

        for (int i = 0; i < 5; i++)
        {
            pauseButtons[i] = transform.GetChild(0).GetChild(i).GetComponent<Button>();
            buttonPos[i] = pauseButtons[i].transform.position;
        }

        menuText = transform.GetChild(1).GetComponent<SuperTextMesh>();

        if (SaveSystem.playerParty.Count > 0)
        {
            InterfaceInputManager.instance.es.SetSelectedGameObject(pauseButtons[0].gameObject);
        }
        else
        {
            pauseButtons[0].interactable = false;
            InterfaceInputManager.instance.es.SetSelectedGameObject(pauseButtons[1].gameObject);
        }

        buttonsContainer = transform.GetChild(0);

        GameUtility.EnableBackground();
        FadeIn();
    }

    private void Update()
    {
        if (isFading || !IsPriority) return;

        if (InterfaceInputManager.instance.es.currentSelectedGameObject == null)
        {
            if (lastSelectedButton > -1)
            {
                pauseButtons[lastSelectedButton].Select();
                menuText.text = buttonNames[lastSelectedButton];
            }
            else
            {
                pauseButtons[SaveSystem.playerParty.Count > 0 ? 0 : 1].Select();
            }
        }
        else
        {
            for (int i = 0; i < 5; i++)
            {
                if (InterfaceInputManager.instance.es.currentSelectedGameObject.GetComponent<Button>() == pauseButtons[i])
                {
                    menuText.text = "<w=pause>" + buttonNames[i];
                }
            }
        }
    }

    public void ChangeDisplayText(Button obj)
    {
        InterfaceInputManager.instance.es.SetSelectedGameObject(obj.gameObject);
        obj.Select();
    }

    public void OnMouseDown(int id)
    {
        if (isFading) return;

        FadeOut(false, false);

        switch (id)
        {
            case 0:
                InterfaceSystem.OpenParty(this);
                break;
            case 1:
                InterfaceSystem.OpenInventory(this);
                break;
            case 2:
                InterfaceSystem.OpenCharmBag(this);
                break;
            case 3:
                InterfaceSystem.OpenPermit();
                break;
            case 4:
                break;
            case 5:
                InterfaceSystem.OpenSettings();
                break;
            case 6:
                InterfaceSystem.ExitGame();
                break;
            default:
                return;
        }

        lastSelectedButton = id;
    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        ButtonsActive(false);
        InterfaceInputManager.instance.EnableCursor(false);
        GameUtility.DisableBackground();

        InterfaceSystem.isPaused = false;
    }

    public override void OnBecomePriority()
    {
        InterfaceInputManager.instance.EnableCursor(false);
        ButtonsActive(true);
    }

    public override void OnNotPriority()
    {
        FadeOut(false, false);
        ButtonsActive(false);
    }
}
