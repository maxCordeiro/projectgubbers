﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;
using UnityEngine.UI;

public class SkillPanelUI : UISystem
{
    public SuperTextMesh skillName, rarity, type, range, power, desc, monAttr;

    public Image attrBG, attrIcon;
    public Skills skill;

    public void DisplaySkill()
    {
        InterfaceSystem.inPanel = true;
        SkillData d = SkillSystem.GetSkillDatabaseData(skill);

        skillName.text = SystemVariables.trans[d.id.ToString() + "_NAME"];
        rarity.text = SystemVariables.trans["sys_" + d.skillRarity.ToString()];
        type.text = SystemVariables.trans["attr_" + d.skillType];
        range.text = SystemVariables.trans["sys_" + (d.ranged ? "ranged" : "direct")];

        desc.text = SystemVariables.trans[d.id.ToString() + "_DESC"];

        attrBG.sprite = Resources.LoadAll<Sprite>("typeBGs")[(int)d.attribute];
        attrIcon.sprite = Resources.LoadAll<Sprite>("mon_Types")[(int)d.attribute];
        monAttr.text = d.attribute.ToString();

        power.text = d.attack.ToString() + " PWR";

        FadeIn();
    }

    private void Update()
    {
        if (isFading || !IsPriority) return;

        if (InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B"))
        {
            FadeOut(_destroyAfter: true);
        }
    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {

    }

    public override void OnNotPriority()
    {

    }
}
