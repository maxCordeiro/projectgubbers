﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using MonsterSystem;
using System.Linq;
using UnityEditor;
using GameDatabase;

public class SummaryUI : UISystem {

    public Sprite[] buttonSkins;

    [Header("Internal Info")]
    public SuperTextMesh intNameText;
    public SuperTextMesh famText;
    public SuperTextMesh weightText;
    public SuperTextMesh heightText;

    [Header("Monster Info")]
    public SuperTextMesh nameText;
    public SuperTextMesh levelText;
    public SuperTextMesh genderText;
    public SuperTextMesh hpAmountText;
    public Image hpFillImage;

    public Image monsterSprite;

    [Header("Types")]
    public AttributeDisplayUpdater attr1;
    public AttributeDisplayUpdater attr2;

    [Header("Item")]
    public SuperTextMesh itemText;

    [Header("Info Window")]
    public SuperTextMesh infoHeaderText;
    public SuperTextMesh infoText;

    [Header("Perk Window")]
    public SuperTextMesh perkHeaderText;
    public SuperTextMesh perkText;
    public SuperTextMesh perkDescText;

    [Header("Stats Window")]
    public SuperTextMesh statsHeaderText;
    public SuperTextMesh[] statNameText;
    public SuperTextMesh[] statAmountText;
    public SuperTextMesh statPointsRemaining;

    [Header("Stat Points Panel")]
    public GameObject statPointsPanel;
    public Button[] statPointButtons;
    public SuperTextMesh[] statPointNames;
    public SuperTextMesh[] statPointAmounts;
    public Button applyButton;
    public Button backButton;
    public SuperTextMesh statPointApply;
    public SuperTextMesh statPointUsingAmount;
    public SuperTextMesh statPointsPanelRemaining;
    public GameObject applyPanel;

    [Header("Skills")]
    public SuperTextMesh[] skillNames;
    public SuperTextMesh[] skillCooldowns;
    public Image[] skillButtonsImages;
    public Image[] skillAttrs;
    public Button[] skillButtons;

    [Header("Misc")]
    public Image[] sideMonImage;
    public Image[] sideShoulderImage;
    public Image[] sideMonBGImage;

    public GameObject infoWindow;
    public GameObject statsWindow;

    public Material[] typeMats;

    [Header("Variables")]
    public bool requestingMonster;
    public bool itemOnMonster;
    bool isSelecting, isNicknaming;
    
    bool inSPPanel;
    bool teachingSkill;

    public Toggle[] infoSkillsButton;

    Sprite[] monTypeIcons;
    Sprite[] monTypeBGs;
    public Sprite[] skillBGs;

    int curMonIndex;
    int tempStatIndex;
    int tempStatValue; // temp value when applying stat
    int tempPointsUsing;
    int tempTotalPoints;
    bool pointSelected;

    public bool goBackToParty, inBag;

    public int startingMonID; // ID to initially update info with

    public override void Awake()
    {
        base.Awake();
        
        monTypeIcons = Resources.LoadAll<Sprite>("mon_Types");
        monTypeBGs = Resources.LoadAll<Sprite>("typeBGs");
    }

    public new void Start()
    {
        base.Start();

        InterfaceInputManager.instance.controllerCursor.transform.SetAsLastSibling();
        InterfaceInputManager.instance.es.SetSelectedGameObject(infoSkillsButton[0].gameObject);
        InterfaceInputManager.instance.EnableCursor(true);

        for (int i = 0; i < skillButtons.Length; i++)
        {
            int c = i;
            skillButtons[c].onClick.AddListener(() => { SelectSkill(c); });
        }

        SwitchPanel(0);
        applyPanel.SetActive(false);

        UpdateInfo(startingMonID);

        FadeIn();
    }

    private void Update()
    {
        if (isFading || !IsPriority) return;

        if (!inSPPanel)
        {
            if (InterfaceSystem.rePlayer.GetButtonDown("LShoulder") && (!inBag? SaveSystem.playerParty.ElementAtOrDefault(curMonIndex - 1) != null : SaveSystem.playerRanch[curMonIndex - 1] != null))
            {
                UpdateInfo(curMonIndex - 1);
            }
            else if (InterfaceSystem.rePlayer.GetButtonDown("RShoulder") && (!inBag ? SaveSystem.playerParty.ElementAtOrDefault(curMonIndex - 1) != null : SaveSystem.playerRanch[curMonIndex - 1] != null))
            {
                UpdateInfo(curMonIndex + 1);
            }
        }

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !isSelecting && !InterfaceSystem.InDialogueChoices())
        {
            if (inSPPanel)
            {
                statPointsPanel.SetActive(false);
                UpdateInfo(curMonIndex);
            } else
            {
                isSelecting = false;
                FadeOut(true, true);
            }
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            DisplayStatPoints();
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            isNicknaming = true;

            InterfaceSystem.OpenNicknamePanel(this);
        }

        if (!ChoiceHandler.instance.isOpen && !InterfaceSystem.isInteracting && InterfaceInputManager.instance.es.currentSelectedGameObject != null && InterfaceInputManager.instance.controllerCursor.activeInHierarchy)
        {
            ReposCursor();
        }

    }

    public void UpdateInfo(int index)
    {
        if (!IsPriority) return;
        //if (index > 3) throw new UnityException("Party Monsters only go up to 4. Call 3 or less");

        curMonIndex = index;
        Monster tempMonster = null;

        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[index];
        } else
        {
            tempMonster = SaveSystem.playerParty[index];
        }

        intNameText.text = tempMonster.GetInternalName() + " #" + (tempMonster.GetID() + 1).ToString("D3");
        nameText.text = tempMonster.GetName();
        levelText.text = "<size=7><f=pinch>Lv</f></size>" + tempMonster.GetMonLevel();
        famText.text = SystemVariables.trans["fam_" + tempMonster.GetFamily().ToString()] + " " + SystemVariables.trans["sys_fam"];
        weightText.text = SystemVariables.trans["sys_weight"] + ": " + tempMonster.weight.ToString() + SystemVariables.trans["sys_lb"];
        heightText.text = SystemVariables.trans["sys_height"] + ": " + tempMonster.GetHeight();

        monsterSprite.sprite = tempMonster.GetSprite();

        attr1.UpdateDisplay(tempMonster.GetType());
        attr2.UpdateDisplay(tempMonster.GetType(1));

        infoText.text = string.Format(SystemVariables.trans["summary_tamed"], tempMonster.partner, tempMonster.dateObtained.ToString()) +
        "<br>" + string.Format(SystemVariables.trans["summary_met"], tempMonster.mapObtainedID.ToString(), tempMonster.metLevel.ToString());// + "<br>" + SystemVariables.trans["summary_retire"];

        hpAmountText.text = string.Format("{0}/{1}", tempMonster.stats.currentHP, tempMonster.stats.statArray[5]);
        hpFillImage.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(118 * (float)((float)tempMonster.stats.currentHP / (float)tempMonster.stats.statArray[5])));

        if (tempMonster.perk != Perks.NONE)
        {
            perkText.text = SystemVariables.trans[tempMonster.perk.ToString() + "_NAME"] + " - " + SystemVariables.trans["sys_" + PerkSystem.GetPerkDatabaseData(tempMonster.perk).rarity.ToString()];
            perkDescText.text = SystemVariables.trans[tempMonster.perk.ToString() + "_DESC"];
        }

        itemText.text = tempMonster.accessory == null ? "--" : SystemVariables.trans[tempMonster.accessory.id.ToString() + "_NAME"];

        for (int i = 0; i < 6; i++)
        {
            statAmountText[i].text = tempMonster.GetStatValue((StatNames)i).ToString();
            statNameText[i].text = SystemVariables.trans["sys_" + ((StatNames)i).ToString()];

            if (tempMonster.upStatA == i)
            {
                statNameText[i].text += " +" + (tempMonster.upStatExtra? "+" : "");
            } else if (tempMonster.upStatB == i)
            {
                statNameText[i].text += " +";
            } else if (tempMonster.downStat == i)
            {
                statNameText[i].text += " -" + (tempMonster.downStatExtra? "-" : "");
            }
        }

        statPointsRemaining.text = "Stat Points: " + tempMonster.statPointsLeft;

        for (int i = 0; i < 5; i++)
        {
            int typeInd = (int)tempMonster.GetSkillData(i).attribute;
            skillButtonsImages[i].sprite = skillBGs[typeInd];
            skillNames[i].text = SystemVariables.trans[tempMonster.GetSkill(i).ToString() + "_NAME"];
            skillNames[i].textMaterial = typeMats[typeInd];
            skillAttrs[i].sprite = monTypeIcons[typeInd];
            skillCooldowns[i].text = tempMonster.GetSkillData(i).cooldown.ToString();   
            skillNames[i].Rebuild(false);
        }

        int nextIndex = index + 1;
        int prevIndex = index - 1;

        if ((!inBag ? SaveSystem.playerParty.ElementAtOrDefault(nextIndex) != null : SaveSystem.playerRanch[nextIndex] != null))
        {
            sideShoulderImage[1].color = Color.white;

            sideMonImage[1].color = Color.white;
            
            if (inBag)
            {
                sideMonImage[1].sprite = SaveSystem.playerRanch[nextIndex].GetIcon();
            } else
            {
                sideMonImage[1].sprite = SaveSystem.playerParty[nextIndex].GetIcon();
            }

            sideMonBGImage[1].color = Color.white;
        } else
        {
            sideShoulderImage[1].color = new Color(1, 1, 1, 0);
            sideMonImage[1].color = new Color(1, 1, 1, 0);
            sideMonBGImage[1].color = new Color(1, 1, 1, 0);
        }

        if ((!inBag ? SaveSystem.playerParty.ElementAtOrDefault(prevIndex) != null : SaveSystem.playerRanch[prevIndex] != null))
        {
            sideShoulderImage[0].color = Color.white;

            sideMonImage[0].color = Color.white;

            if (inBag)
            {
                sideMonImage[0].sprite = SaveSystem.playerRanch[prevIndex].GetIcon();
            }
            else
            {
                sideMonImage[0].sprite = SaveSystem.playerParty[prevIndex].GetIcon();
            }

            sideMonBGImage[0].color = Color.white;
        } else
        {
            sideShoulderImage[0].color = new Color(1, 1, 1, 0);
            sideMonImage[0].color = new Color(1, 1, 1, 0);
            sideMonBGImage[0].color = new Color(1, 1, 1, 0);
        }
    }

    private void OnDestroy()
    {
        /*
        if (goBackToParty)
        {
            if (inBag)
            {
                CharmBagUI cbm = FindObjectOfType<CharmBagUI>();
                if (cbm)
                {
                    cbm.inSummary = false;
                    cbm.cGroup.interactable = true;
                    cbm.FadeIn();
                }
            } else
            {
                PartyUI p = FindObjectOfType<PartyUI>();
                if (p)
                {
                    p.inSummary = false;
                    p.cGroup.interactable = true;
                    p.FadeIn();
                }
            }
        } else {
            InterfaceSystem.inSubMenu = false;
        }*/
    }

    public void SwitchPanel(int dir)
    {
        infoWindow.SetActive(dir == 0 ? true : false);
        statsWindow.SetActive(dir == 0 ? false : true);
    }

    public void DisplayStatPoints()
    {
        inSPPanel = true;

        Monster tempMonster = null;

        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[curMonIndex];
        }
        else
        {
            tempMonster = SaveSystem.playerParty[curMonIndex];
        }

        for (int i = 0; i < 6; i++)
        {
            statPointAmounts[i].text = tempMonster.stats.statArray[i].ToString();

            statPointNames[i].text = SystemVariables.statNames[i].ToString() + (tempMonster.upStatA == i ? " +" : "");
            statPointNames[i].text = SystemVariables.statNames[i].ToString() + (tempMonster.upStatB == i ? " +" : "");
            statPointNames[i].text = SystemVariables.statNames[i].ToString() + (tempMonster.downStat == i ? " -" : "");
        }

        statPointsPanelRemaining.text = "Stat Points: " + tempMonster.statPointsLeft;



        statPointsPanel.SetActive(true);
        
    }

    public void SelectStatPoint(int stat)
    {
        pointSelected = true;
        Monster tempMonster = null;

        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[curMonIndex];
        } else
        {
            tempMonster = SaveSystem.playerParty[curMonIndex];
        }

        tempStatIndex = stat;

        int currStat = tempMonster.stats.statArray[tempStatIndex];

        for (int i = 0; i < statPointButtons.Length; i++)
        {
            statPointButtons[i].interactable = false;

            if (i == stat) continue;

            statPointNames[i].color = new Color(0.09803922f, 0.3490196f, 0.2039216f, 0.4f);
            statPointNames[i].Rebuild(false);
            statPointAmounts[i].color = new Color(0.09803922f, 0.3490196f, 0.2039216f, 0.4f);
            statPointAmounts[i].Rebuild(false);
        }

        tempTotalPoints = tempMonster.statPointsLeft;
        statPointsPanelRemaining.text = "Stat Points: " + tempMonster.statPointsLeft;

        statPointUsingAmount.text = tempPointsUsing.ToString();
        statPointApply.text = currStat.ToString() + " > " + currStat.ToString();

        applyPanel.SetActive(true);
    }

    public void AddPoint()
    {
        Monster tempMonster = null;
        
        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[curMonIndex];
        } else
        {
            tempMonster = SaveSystem.playerParty[curMonIndex];
        }

        MonsterData tempMonData = GameUtility.GetMonsterDatabaseData(tempMonster.mon);

        int currStat = tempMonster.stats.statArray[tempStatIndex];

        if (tempPointsUsing < tempTotalPoints)
        {
            tempPointsUsing++;
        }

        tempStatValue = Mathf.FloorToInt(((2.82f * (tempMonData.baseStats.baseArray[tempStatIndex] * tempMonster.level * (1 + (tempPointsUsing * 0.0062f))) + 5.5f) / 100 + 2.5f) * 1);

        statPointUsingAmount.text = tempPointsUsing.ToString() + "/" + tempTotalPoints;
        statPointApply.text = currStat.ToString() + " > <c=tak>" + tempStatValue.ToString() + "</c>";
    }

    public void MinusPoint()
    {
        Monster tempMonster = null;

        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[curMonIndex];
        }
        else
        {
            tempMonster = SaveSystem.playerParty[curMonIndex];
        }

        MonsterData tempMonData = GameUtility.GetMonsterDatabaseData(tempMonster.mon);

        int currStat = tempMonster.stats.statArray[tempStatIndex];

        if (tempPointsUsing > 0)
        {
            tempPointsUsing--;
        }

        tempStatValue = Mathf.FloorToInt(((2.82f * (tempMonData.baseStats.baseArray[tempStatIndex] * tempMonster.level * (1 + (tempPointsUsing * 0.0062f))) + 5.5f) / 100 + 2.5f) * 1);

        statPointUsingAmount.text = tempPointsUsing.ToString() + "/" + tempTotalPoints;
        statPointApply.text = currStat.ToString() + " > <c=tak>" + tempStatValue.ToString() + "</c>";
    }

    public void ApplyPoint()
    {
        Monster tempMonster = null;

        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[curMonIndex];
        }
        else
        {
            tempMonster = SaveSystem.playerParty[curMonIndex];
        }

        tempMonster.stats.statPoints[tempStatIndex] = tempPointsUsing;
        tempMonster.statPointsLeft -= tempPointsUsing;
        tempMonster.CalculateAllStats(false);

        tempPointsUsing = 0;
        tempTotalPoints = 0;
        tempStatValue = 0;
        
        if (inBag)
        {
            SaveSystem.playerRanch[curMonIndex] = tempMonster;
        } else
        {
            SaveSystem.playerParty[curMonIndex] = tempMonster;
        }


        pointSelected = false;

        applyPanel.SetActive(false);

        DisplayStatPoints();

        for (int i = 0; i < statPointButtons.Length; i++)
        {
            statPointButtons[i].interactable = true;

            statPointNames[i].color = new Color(0.09803922f, 0.3490196f, 0.2039216f);
            statPointNames[i].Rebuild(false);
            statPointAmounts[i].color = new Color(0.09803922f, 0.3490196f, 0.2039216f);
            statPointAmounts[i].Rebuild(false);
        }
    }

    public void BackPoint()
    {
        tempPointsUsing = 0;
        tempTotalPoints = 0;
        tempStatValue = 0;

        applyPanel.SetActive(false);
        pointSelected = false;

        for (int i = 0; i < statPointButtons.Length; i++)
        {
            statPointButtons[i].interactable = true;

            statPointNames[i].color = new Color(0.09803922f, 0.3490196f, 0.2039216f);
            statPointNames[i].Rebuild(false);
        }
    }

    public void StatPointsEnter(int _ind)
    {
        if (pointSelected) return;

        statPointNames[_ind].color = new Color(1f, 0.4196078f, 0.6f);
        statPointAmounts[_ind].color = new Color(1f, 0.4196078f, 0.6f);

        statPointNames[_ind].Rebuild(false);
        statPointAmounts[_ind].Rebuild(false);
    }

    public void StatPointsExit(int _ind)
    {
        if (pointSelected) return;

        statPointNames[_ind].color = new Color(0.09803922f, 0.3490196f, 0.2039216f);
        statPointAmounts[_ind].color = new Color(0.09803922f, 0.3490196f, 0.2039216f);

        statPointNames[_ind].Rebuild(false);
        statPointAmounts[_ind].Rebuild(false);
    }

    public void SelectSkill(int index)
    {
        StartCoroutine(SelectSkillCo(index));
    }

    public IEnumerator SelectSkillCo(int _skillIndex)
    {
        if (ChoiceHandler.instance.isOpen)
        {
            yield break;
        }

        Monster tempMonster;

        if (inBag)
        {
            tempMonster = SaveSystem.playerRanch[curMonIndex];
        }
        else
        {
            tempMonster = SaveSystem.playerParty[curMonIndex];
        }

        ChoiceHandler ch = ChoiceHandler.instance;
        DialogueHandler d = DialogueHandler.instance;

        if (InterfaceSystem.temporarySkillIndex != -1)
        {
            ch.AddChoice(0, SystemVariables.trans["sys_equip"]);
        }
        ch.AddChoice(1, SystemVariables.trans["sys_view"]);
        ch.AddChoice(2, SystemVariables.trans["sys_cancel"]);

        ch.Show();

        yield return new WaitForChoice();

        int tempChoice = InterfaceSystem.choiceIndex;

        switch (tempChoice)
        {
            case 0:
                int replaceChoice = -1;

                d.AddMessage(string.Format("Equip {0} with {1} by replacing {2}?", tempMonster.GetName(), SystemVariables.skillNames[((Skills)InterfaceSystem.temporarySkillIndex).ToString()], SystemVariables.skillNames[((Skills)tempMonster.GetSkill(_skillIndex)).ToString()]));
                //d.KeepOpen();
                d.Show();

                yield return new WaitForDialogueReading();

                ch.AddChoice(0, SystemVariables.trans["sys_yes"]);
                ch.AddChoice(1, SystemVariables.trans["sys_no"]);

                ch.Show();

                yield return new WaitForChoice();

                replaceChoice = InterfaceSystem.choiceIndex;

                if (replaceChoice == 0)
                {
                    tempMonster.EquipSkill((Skills)InterfaceSystem.temporarySkillIndex, _skillIndex);
                    if (inBag)
                    {
                        SaveSystem.playerRanch[curMonIndex] = tempMonster;
                    }
                    else
                    {
                        SaveSystem.playerParty[curMonIndex] = tempMonster;
                    }
                    SaveSystem.invSkills[InterfaceSystem.temporaryItemIndex].currentQuantity -= 1;
                    
                    if (SaveSystem.invSkills[InterfaceSystem.temporaryItemIndex].currentQuantity == 0)
                    {
                        SaveSystem.invSkills.RemoveAt(InterfaceSystem.temporaryItemIndex);
                    }

                    InterfaceSystem.temporarySkillIndex = -1;

                    UpdateInfo(curMonIndex);

                    yield return new WaitForSeconds(1);

                    FadeOut(_destroyAfter: true);
                }

                yield break;
            case 1:
                Transform BG = transform.parent.Find("BG");
                    
                BG.SetAsLastSibling();
                    
                InterfaceSystem.DisplaySkill(this, tempMonster.GetSkill(_skillIndex), transform.parent);

                yield return new WaitForUIPriority(this);
                    
                InterfaceInputManager.instance.es.SetSelectedGameObject(skillButtons[_skillIndex].gameObject);
                InterfaceInputManager.instance.EnableCursor(true);
                BG.SetSiblingIndex(2);
                break;
            case 2:
                InterfaceInputManager.instance.es.SetSelectedGameObject(skillButtons[_skillIndex].gameObject);
                InterfaceInputManager.instance.EnableCursor(true);
                break;
            default:
                break;
        }
    }

    public override void OnFadeIn()
    {

    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            PartyUI partyUI = referrer.GetComponent<PartyUI>();

            if (partyUI)
            {
                partyUI.inSummary = false;
            }

            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
        if (isNicknaming && InterfaceSystem.temporaryString.Length != 0)
        {
            if (inBag)
            {
                SaveSystem.playerRanch[curMonIndex].SetNickname(InterfaceSystem.temporaryString);
            }
            else
            {
                SaveSystem.playerParty[curMonIndex].SetNickname(InterfaceSystem.temporaryString);
            }

            InterfaceInputManager.instance.es.SetSelectedGameObject(infoSkillsButton[0].gameObject);
            InterfaceInputManager.instance.EnableCursor(true);

            UpdateInfo(curMonIndex);
            isNicknaming = false;
        }
    }

    public override void OnNotPriority()
    {

    }
}