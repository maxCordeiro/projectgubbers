﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDatabase;
using MonsterSystem;
using UnityEngine.EventSystems;

public class DaycareDenUpdater : MonoBehaviour, ISelectHandler
{
    public Image mom, dad, item, egg, status, pouch, momBG, dadBG, itemBG, eggBG, lockIcon;
    public Button button;
    
    public Sprite[] eggs, statusIcons, bgSprites;
    
    public DaycareUI dmv;
    public int index;
    
    public void ClearDisplay(bool locked = false)
    {
        dad.enabled = false;
        mom.enabled = false;
        egg.enabled = false;
        pouch.enabled = false;
        item.enabled = false;
        status.enabled = false;
        lockIcon.enabled = locked;
        
        momBG.sprite = bgSprites[1];
        dadBG.sprite = bgSprites[1];
        itemBG.sprite = bgSprites[1];
        eggBG.sprite = bgSprites[1];
        button.interactable = !locked;
    }
    
    public void OnSelect(BaseEventData baseEvent)
    {
        dmv.UpdatePanelInfo(index);
    }
    
    public void UpdateDisplay()
    {        
        int mons = 0;
        
        if (SaveSystem.dcMons[index].male == null)
        {
            dad.enabled = false;
        } else {
            dad.sprite = SaveSystem.dcMons[index].male.GetIcon();
            dad.enabled = true;
            mons++;
        }
        
        if (SaveSystem.dcMons[index].female == null)
        {
            mom.enabled = false;
        } else {
            mom.sprite = SaveSystem.dcMons[index].female.GetIcon();
            mom.enabled = true;
            mons++;
        }
        
        if (mons == 2)
        {
            status.sprite = statusIcons[GameUtility.CompatibilityCheck(index) >= 0.5f? 0 : 1];
            status.enabled = true;
        } else {
            status.enabled = false;
        }
        
        if (SaveSystem.dcMons[index].hasEgg)
        {
            egg.sprite = eggs[(int)SaveSystem.dcMons[index].egg.GetFamily()];
            egg.enabled = true;
        } else {
            egg.enabled = false;
        }
        
        if (SaveSystem.dcMons[index].breedingItem == null)
        {
            item.enabled = false;
            pouch.enabled = false;
        } else {
            item.sprite = ItemSystem.GetItemSprite(SaveSystem.dcMons[index].breedingItem);
            item.enabled = true;
            pouch.enabled = true;
        }
        
        momBG.sprite = bgSprites[0];
        dadBG.sprite = bgSprites[0];
        itemBG.sprite = bgSprites[0];
        eggBG.sprite = bgSprites[0];
        lockIcon.enabled = false;
    }
}
