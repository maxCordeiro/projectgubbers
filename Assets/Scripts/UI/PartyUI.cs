﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDatabase;
using MonsterSystem;
using System.Linq;

public class PartyUI : UISystem {

    public Button[] monButtons;

    public SuperTextMesh[] monName;
    public SuperTextMesh[] monLevel;
    public SuperTextMesh[] monGender;
    public SuperTextMesh[] monHealthAmount;
    public SuperTextMesh[] monItemName;

    public Image[] monIcon;
    public Image[] monIconBG;
    public Image[] monHPOutline;
    public Image[] monHPBG;
    public Image[] monHPBar;
    public Image[] monItemIcon;
    public Image[] monItemBG;
    public Image[] monStats;
    Material[] monMats;

    public Sprite[] normalSprites;
    public Sprite[] deadSprites;
    public Sprite[] statusEffects;

    public Material[] textMats;

    public Sprite noMon;

    public float xCursor;
    public float yCursor;

    public MonsterRequest request;
    public bool oneOff;

    public bool isSelected;
    public bool isSwitching;

    int lastSelected;
    public bool inSummary;

    float switchFlickerTimer;
    bool switchFlickerOn;

    SpriteState nSS = new SpriteState();
    SpriteState dSS = new SpriteState();

    public new void Start()
    {
        base.Start();

        nSS.highlightedSprite = normalSprites[1];
        nSS.pressedSprite = normalSprites[1];
        nSS.disabledSprite = normalSprites[2];

        dSS.highlightedSprite = deadSprites[1];
        dSS.pressedSprite = deadSprites[1];
        dSS.disabledSprite = deadSprites[2];

        monMats = new Material[4];

        InterfaceInputManager.instance.es.SetSelectedGameObject(monButtons[0].gameObject);
        lastSelected = 0;

        ReposCursor();
        InterfaceInputManager.instance.EnableCursor(true);

        UpdateButtons();

        for (int i = 0; i < monButtons.Length; i++)
        {
            int index = i;
            monButtons[i].onClick.AddListener(() => { StartCoroutine(SelectMonster(index)); });
        }

        GameUtility.EnableBackground();
        AddControlGraphic("sys_view", 0);
        AddControlGraphic("sys_cancel", 0);
        ShowControlBar();
        FadeIn();
    }

    public void UpdateButtons()
    {
        for (int i = 0; i < monButtons.Length; i++)
        {
            if (SaveSystem.playerParty.ElementAtOrDefault(i) == null)
            {
                monName[i].text = "";
                monLevel[i].text = "";
                monGender[i].text = "";
                monHealthAmount[i].text = "";
                monItemName[i].text = "";

                monIcon[i].sprite = null;
                monIcon[i].color = new Color(0, 0, 0, 0);
                monIconBG[i].color = new Color(0, 0, 0, 0);

                monHPOutline[i].color = new Color(0, 0, 0, 0);
                monHPBG[i].color = new Color(0, 0, 0, 0);
                monHPBar[i].color = new Color(0, 0, 0, 0);
                monHPBar[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 76);

                monItemBG[i].color = new Color(0, 0, 0, 0);
                monItemIcon[i].color = new Color(0, 0, 0, 0);

                monButtons[i].interactable = false;
                monButtons[i].image.color = new Color(1, 1, 1, 0.5f);
                monStats[i].enabled = false;
                //monButtons[i].image.sprite = noMon;

                continue;
            }

            Monster tempMon = SaveSystem.playerParty[i];

            monName[i].text = tempMon.GetName();
            monLevel[i].text = "Lv" + tempMon.level;
            monGender[i].text = tempMon.GetGender();
            monHealthAmount[i].text = tempMon.GetCurrentHP() + "/" + tempMon.GetHPStat().ToString();
            monItemName[i].text = tempMon.accessory == null? "--" : ItemSystem.GetItemName(tempMon.accessory);

            monIcon[i].sprite = tempMon.GetIcon();
            monIcon[i].color = Color.white;

            if (tempMon.GetCurrentHP() == 0)
            {
                monHPBar[i].enabled = false;
            } else
            {
                monHPBar[i].enabled = true;
                monHPBar[i].rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Floor(76 * (float)((float)tempMon.GetCurrentHP() / (float)tempMon.GetHPStat())));
            }

            monIconBG[i].color = Color.white;
            
            if (tempMon.GetStatusEffect() == StatusEffect.DEAD)
            {
                monButtons[i].spriteState = dSS;
                monButtons[i].image.sprite = deadSprites[0];
                monItemBG[i].sprite = deadSprites[3];

                monIconBG[i].sprite = deadSprites[4];

                monName[i].textMaterial = textMats[2];
                monLevel[i].textMaterial = textMats[2];
                monGender[i].textMaterial = textMats[2];
                monHealthAmount[i].textMaterial = textMats[2];

                monItemName[i].textMaterial = textMats[3];
                
                monStats[i].enabled = false;
            }
            else
            {   
                monStats[i].enabled = tempMon.GetStatusEffect() != StatusEffect.NONE;

                if (tempMon.GetStatusEffect() != StatusEffect.NONE)
                {
                    monStats[i].sprite = statusEffects[(int)tempMon.GetStatusEffect() - 1];
                }

                monButtons[i].spriteState = nSS;
                monButtons[i].image.sprite = normalSprites[0];
                monItemBG[i].sprite = normalSprites[3];

                monIconBG[i].sprite = normalSprites[4];

                monName[i].textMaterial = textMats[0];
                monLevel[i].textMaterial = textMats[0];
                monGender[i].textMaterial = textMats[0];
                monHealthAmount[i].textMaterial = textMats[0];

                monItemName[i].textMaterial = textMats[1];
            }

            monName[i].Rebuild(false);
            monLevel[i].Rebuild(false);
            monGender[i].Rebuild(false);
            monHealthAmount[i].Rebuild(false);
            monItemName[i].Rebuild(false);

            monHPOutline[i].color = Color.white;
            monHPBG[i].color = Color.white;
            monHPBar[i].color = Color.white;

            monItemBG[i].color = Color.white;
            monItemIcon[i].color = Color.white;

            monButtons[i].interactable = true;
            monButtons[i].image.color = new Color(1, 1, 1, 1);
        }

        if (InterfaceInputManager.instance.es.currentSelectedGameObject == null)
        {
            InterfaceInputManager.instance.es.SetSelectedGameObject(monButtons[0].gameObject);
            InterfaceInputManager.instance.EnableCursor(true);
        }
    }

    // Update is called once per frame
    void Update() {
        if (isFading || !IsPriority) return;

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) &&
            !InterfaceSystem.InDialogueChoices() &&
            !inSummary &&
            (!request.isRequesting || request.isRequesting && request.request != MonsterRequestType.STATUS && request.status != StatusEffect.NONE))
        {
            FadeOut(_destroyAfter: true);
        }

        if (isSwitching)
        {
            switchFlickerTimer += Time.deltaTime;

            if (switchFlickerTimer > 0.25f)
            {
                switchFlickerOn = !switchFlickerOn;
                switchFlickerTimer = 0;
                monButtons[lastSelected].image.color = new Color(1, 1, 1, switchFlickerOn ? 0.5f : 1);
            }
        }

        if (InterfaceInputManager.instance.es.currentSelectedGameObject != null && !InterfaceSystem.InDialogueChoices())
        {
            ReposCursor();
        }
    }

    public IEnumerator SelectMonster(int index)
    {
        if (isFading || isSelected) yield break;

        isSelected = true;

        if (lastSelected == index && isSwitching)
        {
            isSwitching = false;
            isSelected = false;
            InterfaceInputManager.instance.es.SetSelectedGameObject(null);
            yield break;
        } else if (isSwitching)
        {
            Monster aMonster = SaveSystem.playerParty[lastSelected];
            Monster bMonster = SaveSystem.playerParty[index];

            SaveSystem.playerParty[index] = aMonster;
            SaveSystem.playerParty[lastSelected] = bMonster;

            isSwitching = false;

            if (SaveSystem.settings_UIAnim) FadeOut(false);
            yield return new WaitForSeconds(0.1f);
            UpdateButtons();

            FadeIn();
            yield return new WaitForSeconds(0.1f);

            isSelected = false;

            InterfaceInputManager.instance.es.SetSelectedGameObject(null);


            yield break;
        }

        lastSelected = index;

        /*monButtons[0].enabled = false;
        monButtons[1].enabled = false;
        monButtons[2].enabled = false;
        monButtons[3].enabled = false;*/

        //cGroup.interactable = false;

        ChoiceHandler choices = ChoiceHandler.instance;
        choices.SetPos(new Vector2(440, 50));

        if (request.request == MonsterRequestType.USINGITEM)
        {
            switch (SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex].GetData().menuFunction)
            {
                case ItemEffects_Menu.NONE:
                    break;
                case ItemEffects_Menu.HEALING:
                    yield return StartCoroutine(HealMonster(index));
                    yield break;
                case ItemEffects_Menu.EQUIP:
                    break;
                case ItemEffects_Menu.WEARTOP:
                case ItemEffects_Menu.WEARBOTTOM:
                    break;
                default:
                    break;
            }
        }

        if (oneOff)
        {
            InterfaceSystem.OpenSummary(this, index);
            FadeOut(true, true);
            yield break;
        }

        if (request.MeetsRequirement(SaveSystem.playerParty[index]) && request.isRequesting) choices.AddChoice(0, SystemVariables.trans["sys_select"]);
        choices.AddChoice(3, "View");
        if (!InterfaceSystem.inBattle) choices.AddChoice(1, SystemVariables.trans["sys_switch"]);
        choices.AddChoice(2, SystemVariables.trans["sys_cancel"]);

        choices.Show();
        yield return new WaitForChoice();

        int choiceInd = InterfaceSystem.choiceIndex;

        switch (choiceInd)
        {
            case 0:
                InterfaceInputManager.instance.EnableCursor(false);
                InterfaceSystem.temporaryMonsterIndex = index;
                FadeOut(_destroyAfter: true);
                yield break;
                
            case 1:
                //cGroup.interactable = true; //ButtonsActive(true, transform, false);
                isSwitching = true;
                break;
            case 2:

                //cGroup.interactable = true;
                UpdateButtons();
                break;
            case 3:
                inSummary = true;
                cGroup.interactable = false; //ButtonsActive(false, transform);
                FadeOut(false, false);
                yield return new WaitForUIFade(this);
                InterfaceSystem.OpenSummary(this, index);
                break;
            default:
                break;
        }

        isSelected = false;
    }

    public IEnumerator HealMonster(int id)
    {
        Item itm = SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex)[InterfaceSystem.temporaryItemIndex];

        int healamt = (int)itm.GetData().param;
        bool worked = false;

        Debug.Log(itm.id);

        if (GameUtility.In(itm.id, Items.POTION, Items.POTIONHIGH, Items.POTIONULTRA))
        {
            worked = SaveSystem.playerParty[id].Heal(healamt);
        }
        else
        {
            if (SaveSystem.playerParty[id].GetStatusEffect() == (StatusEffect)itm.GetData().param)
            {
                worked = SaveSystem.playerParty[id].SetStatusEffect(StatusEffect.NONE);

                if (worked)
                {
                    switch (itm.id)
                    {
                        case Items.REVIVE:
                            SaveSystem.playerParty[id].Heal(SaveSystem.playerParty[id].stats.statArray[5]/4);
                            break;
                        case Items.HOTSNOT:
                            SaveSystem.playerParty[id].Heal(10);
                            break;
                        default:
                            break;
                    }
                }
            }
        }


        if (worked)
        {
            UpdateButtons();

            itm.currentQuantity--;
            yield return new WaitForSeconds(0.5f);

            if (itm.currentQuantity <= 0)
            {
                SaveSystem.GetInventoryList(InterfaceSystem.temporaryPocketIndex).RemoveAt(InterfaceSystem.temporaryItemIndex);
                FadeOut(_destroyAfter: true);
            }
        } else
        {
            DialogueHandler d = DialogueHandler.instance;
            d.AddMessage("It had no effect.");
            d.Show();

            yield return new WaitForDialogueClose();
        }

        //ButtonsActive(true, transform, false);
        cGroup.interactable = true; //ButtonsActive(true, monButtons[0].transform.parent, false);

        isSelected = false;
        
        yield break;
    }

    private void OnDestroy()
    {
        /*PauseUI temp = FindObjectOfType<PauseUI>();

        if (temp)
        {
            temp.ButtonsActive(true, temp.buttonsContainer);
            temp.FadeIn();
        }


        InterfaceInputManager.instance.EnableCursor(false);

        if (InterfaceSystem.inSubMenu)
        {
            InterfaceSystem.inSubMenu = false;
        } else
        {
            if (!InterfaceSystem.isPaused && !InterfaceSystem.inSubMenu) GameUtility.DisableBackground();
            InterfaceSystem.inMenu = false;
        }*/
    }


    public override void OnFadeIn()
    {

    }

    public override void OnFadeOut()
    {
        HideControlbar(true);

        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        } else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
        UpdateButtons();
        ButtonsActive(true);

        cGroup.interactable = true;
        InterfaceInputManager.instance.es.SetSelectedGameObject(monButtons[lastSelected].gameObject);
    }

    public override void OnNotPriority()
    {
        ButtonsActive(false);
    }

    public void SetRequest(MonsterRequest m)
    {
        request.isRequesting = true;
        request = m;
    }
}

public enum MonsterRequestType
{
    ANY,
    GENDER,
    SPECIES,
    SPECIESANDFORM,
    LEVELGREATER,
    LEVELEQUAL,
    LEVELLESS,
    COLOR,
    STATUS,

    USINGITEM
}

[System.Serializable]
public struct MonsterRequest
{
    public MonsterRequestType request;

    // Gender 0-1 F-M
    // Level
    // Color
    // Form
    public int intParam;

    public Monsters species;

    public StatusEffect status;

    public bool isRequesting;

    public bool MeetsRequirement(Monster m)
    {
        bool meets = false;

        switch (request)
        {
            case MonsterRequestType.ANY:
                meets = true;
                break;
            case MonsterRequestType.GENDER:
                meets = m.GetGenderInt() == intParam;
                break;
            case MonsterRequestType.SPECIES:
                meets = m.mon.id == species;
                break;
            case MonsterRequestType.SPECIESANDFORM:
                meets = m.mon.id == species && m.mon.formId == intParam;
                break;
            case MonsterRequestType.LEVELGREATER:
                meets = m.level > intParam;
                break;
            case MonsterRequestType.LEVELEQUAL:
                meets = m.level == intParam;
                break;
            case MonsterRequestType.LEVELLESS:
                meets = m.level < intParam;
                break;
            case MonsterRequestType.COLOR:
                meets = m.monColor == intParam;
                break;
            case MonsterRequestType.STATUS:
                meets = m.statEffect == status;
                break;
            case MonsterRequestType.USINGITEM:
                meets = true;
                break;
            default:
                break;
        }

        return meets;
    }
}