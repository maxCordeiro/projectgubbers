﻿using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NicknamePanel : UISystem
{
    public InputField inputField;
    Transform BG;
    string stringEdit;

    public override void OnBecomePriority()
    {
        inputField.ActivateInputField();
    }

    public override void OnFadeIn()
    {
        InterfaceInputManager.instance.EnableCursor(false);
        inputField.ActivateInputField();
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnNotPriority()
    {
        throw new System.NotImplementedException();
    }

    private new void Start()
    {
        base.Start();

        FadeIn(OnFadeIn);
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if (InterfaceSystem.rePlayer.GetButtonDown("Start"))
        {
            InterfaceSystem.temporaryString = inputField.text;

            Transform BG = transform.parent.Find("BG");
            BG.SetSiblingIndex(2);

            FadeOut(_destroyAfter: true);
        }
    }

    public void OnEditting()
    {
        if (!Input.GetKeyDown(KeyCode.Escape))
        {
            stringEdit = inputField.text;
        }
    }
    public void OnEndEdit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            inputField.text = stringEdit;
        }
    }

}
