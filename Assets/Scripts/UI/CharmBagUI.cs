﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameDatabase;
using MonsterSystem;
using System.Linq;
using System;

public class CharmBagUI : UISystem
{
    public CharmBagSlotUpdater[] bagMonSlots;
    public CharmBagSlotUpdater[] partyMonSlots;
    
    public SuperTextMesh monName, monLevelGender, monItem, monPerk, pageNumber;
    public Image monSprite, leftArrow, rightArrow;

    public MonsterRequest request;
    public bool inSummary, buttonPressed, isRequesting;

    int currentPage, totalPages, currCount, lastSelectedPageIndex, lastSelectedSlot;
    bool isMoving, lastSelectedIsParty;

    float pageTimer;
    bool pageTimerUp, pageTimerStarted, canMoveToNextPage;

    public Action onPageChanged;

    public void PageChanged()
    {
        onPageChanged?.Invoke();
    }

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        for (int i = 0; i < bagMonSlots.Length; i++)
        {
            int _i = i;
            bagMonSlots[i].cbm = this;
            bagMonSlots[i].slotIndex = _i;
        }

        for (int i = 0; i < partyMonSlots.Length; i++)
        {
            int _i = i;
            partyMonSlots[i].cbm = this;
            partyMonSlots[i].partySlot = true;
            partyMonSlots[i].pageIndex = _i;
            partyMonSlots[i].slotIndex = _i;
        }

        lastSelectedPageIndex = 0;

        UpdateBagDisplay();

        InterfaceInputManager.instance.es.SetSelectedGameObject(bagMonSlots[0].gameObject);
        InterfaceInputManager.instance.EnableCursor(true);

        GameUtility.EnableBackground();
        FadeIn();
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if ((InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B")) && !InterfaceSystem.InDialogueChoices())
        {
            FadeOut(true, true);
        }

        if (!InterfaceSystem.InDialogueChoices() && InterfaceInputManager.instance.es.currentSelectedGameObject != null)
        {
            ReposCursor();
        }

        if (!canMoveToNextPage)
        {
            pageTimer += Time.deltaTime;

            if (pageTimer > 0.15f)
            {
                canMoveToNextPage = true;
                pageTimer = 0;
            }
        }

        if (InterfaceSystem.rePlayer.GetButton("KBModifier") && canMoveToNextPage)
        {
            if (InterfaceSystem.rePlayer.GetAxisRaw("Horizontal") > 0)
            {
                InterfaceInputManager.instance.es.enabled = false;
                NextPage();
                canMoveToNextPage = false;
                InterfaceInputManager.instance.es.enabled = true;
            } else if (InterfaceSystem.rePlayer.GetAxisRaw("Horizontal") < 0)
            {
                InterfaceInputManager.instance.es.enabled = false;
                PrevPage();
                canMoveToNextPage = false;
                InterfaceInputManager.instance.es.enabled = true;
            }
        }

        if (InterfaceSystem.rePlayer.GetButtonDown("LShoulder"))
        {
            PrevPage();
        } else if (InterfaceSystem.rePlayer.GetButtonDown("RShoulder"))
        {
            NextPage();
        }
    }

    void PrevPage()
    {
        if (currentPage == 0)
        {
            currentPage = totalPages;
        }
        else
        {
            currentPage--;
        }

        UpdateBagDisplay(1);
    }

    void NextPage()
    {
        if (currentPage == totalPages)
        {
            currentPage = 0;
        }
        else
        {
            currentPage++;
        }

        UpdateBagDisplay(1);
    }

    // -1 = All
    // 0 = Party
    // 1 = Bag
    public void UpdateBagDisplay(int updateType = -1)
    {
        currCount = SaveSystem.playerRanch.Length;
        totalPages = (int)Mathf.Ceil(currCount / 24);

        pageNumber.text = (currentPage + 1) + "/" + (totalPages + 1);

        if (updateType == -1 || updateType == 1)
        {
            for (int i = 0; i < 24; i++)
            {
                int ind = i;
                int pageInd = i + (24 * currentPage);
                bool isMon = SaveSystem.playerRanch.ElementAtOrDefault(pageInd) != null;

                bagMonSlots[i].pageIndex = pageInd;
                bagMonSlots[i].UpdateDisplay();
           
            }
        }

        if (updateType == -1 || updateType == 0)
        {
            for (int i = 0; i < 4; i++)
            {
                partyMonSlots[i].UpdateDisplay();
            }
        }

        PageChanged();
    }

    public void UpdateSummaryDisplay(bool party, int slotIndex, int pageIndex)
    {
        if (party)
        {
            if (SaveSystem.playerParty.ElementAtOrDefault(pageIndex) != null)
            {
                monSprite.enabled = true;
                monSprite.sprite = SaveSystem.playerParty[pageIndex].GetSprite();
                monName.text = SaveSystem.playerParty[pageIndex].GetName();
                monLevelGender.enabled = true;
                monLevelGender.text = string.Format(monLevelGender.text, SaveSystem.playerParty[pageIndex].GetMonLevel(), SaveSystem.playerParty[pageIndex].GetGender());
                return;
            }
        } else
        {
            if (SaveSystem.playerRanch.ElementAtOrDefault(pageIndex) != null)
            {
                monSprite.enabled = true;
                monSprite.sprite = SaveSystem.playerRanch[pageIndex].GetSprite();

                monName.text = SaveSystem.playerRanch[pageIndex].GetName();
                monLevelGender.enabled = true;
                monLevelGender.text = string.Format(monLevelGender.text, SaveSystem.playerRanch[pageIndex].GetMonLevel(), SaveSystem.playerRanch[pageIndex].GetGender());

                if (SaveSystem.playerRanch[pageIndex].accessory != null)
                {
                    monItem.text = ItemSystem.GetItemName(SaveSystem.playerRanch[pageIndex].accessory);
                } else
                {
                    monItem.text = "--";
                }

                monPerk.text = SystemVariables.trans[SaveSystem.playerRanch[pageIndex].perk.ToString() + "_NAME"];

                return;
            }
        }

        monSprite.enabled = false;
        monName.text = "";
        monLevelGender.enabled = false;
        monItem.text = "";
        monPerk.text = "";
    }

    public void SelectMonster(bool _partySlot, int _slotIndex, int _pageIndex)
    {
        StartCoroutine(MonsterSelected(_partySlot, _pageIndex, _slotIndex));
    }

    public IEnumerator MonsterSelected(bool partySlot, int pageIndex, int slotIndex)
    {
        buttonPressed = true;

        Monster selectedMon = partySlot ? SaveSystem.playerParty[pageIndex] : SaveSystem.playerRanch[pageIndex];

        if (isMoving)
        {
            if (partySlot)
            {
                if (!lastSelectedIsParty)
                {
                    Monster tempBagPrevSel = SaveSystem.playerRanch[lastSelectedPageIndex];
                    Monster tempBagCurrentSel = null;

                    if (pageIndex >= SaveSystem.playerParty.Count)
                    {
                        SaveSystem.playerParty.Add(tempBagPrevSel);
                    }
                    else
                    {
                        SaveSystem.playerParty[pageIndex] = tempBagPrevSel;
                        tempBagCurrentSel = SaveSystem.playerRanch[pageIndex];
                    }

                    SaveSystem.playerRanch[lastSelectedPageIndex] = tempBagCurrentSel;

                } else
                {
                    Monster tempBagPrevSel = SaveSystem.playerParty[lastSelectedPageIndex];
                    Monster tempBagCurrentSel = null;


                    if (pageIndex >= SaveSystem.playerParty.Count)
                    {
                        SaveSystem.playerParty.Add(tempBagPrevSel);
                        SaveSystem.playerParty.RemoveAt(lastSelectedPageIndex);
                    } else
                    {
                        tempBagCurrentSel = SaveSystem.playerParty[pageIndex];
                        SaveSystem.playerParty[pageIndex] = tempBagPrevSel;
                        SaveSystem.playerParty[lastSelectedPageIndex] = tempBagCurrentSel;
                    }


                }
            } else
            {
                if (!lastSelectedIsParty)
                {
                    Monster tempBagPrevSel = SaveSystem.playerRanch[lastSelectedPageIndex];
                    Monster tempBagCurrentSel = SaveSystem.playerRanch[pageIndex];

                    SaveSystem.playerRanch[pageIndex] = tempBagPrevSel;
                    SaveSystem.playerRanch[lastSelectedPageIndex] = tempBagCurrentSel;
                } else
                {
                    Monster tempBagPrevSel = SaveSystem.playerParty[lastSelectedPageIndex];
                    Monster tempBagCurrentSel = SaveSystem.playerRanch[pageIndex];

                    SaveSystem.playerRanch[pageIndex] = tempBagPrevSel;
                    SaveSystem.playerParty[lastSelectedPageIndex] = tempBagCurrentSel;
                }
            }

            isMoving = false;
            //FadeOut(false, false);

            //yield return new WaitForSeconds(0.1f);

            UpdateBagDisplay();
            //FadeIn();

            yield return new WaitForSeconds(0.1f);


            lastSelectedSlot = slotIndex;
            lastSelectedPageIndex = pageIndex;
            lastSelectedIsParty = partySlot;
            InterfaceInputManager.instance.es.SetSelectedGameObject((lastSelectedIsParty ? partyMonSlots[lastSelectedSlot] : bagMonSlots[lastSelectedSlot]).gameObject);
            InterfaceInputManager.instance.EnableCursor(true);
            buttonPressed = false;

            yield break;
        } else
        {
            if ((partySlot && SaveSystem.playerParty.ElementAtOrDefault(pageIndex) == null) || (!partySlot && SaveSystem.playerRanch.ElementAtOrDefault(pageIndex) == null))
            {
                buttonPressed = false;
                yield break;
            }
        }

        ChoiceHandler ch = ChoiceHandler.instance;
        ch.SetPos(SystemVariables.choiceboxDialoguePos);
        DialogueHandler msg = DialogueHandler.instance;

        if (!isRequesting && ((partySlot && GameUtility.GetAliveMonsters() > 1) || !partySlot)) ch.AddChoice(0, "Move");
        if (isRequesting && request.MeetsRequirement(SaveSystem.playerRanch[pageIndex])) ch.AddChoice(5, "Select");
        ch.AddChoice(1, "View");
        if (!isRequesting) ch.AddChoice(2, "Check Accessory");
        if (!isRequesting) ch.AddChoice(3, "Release");
        ch.AddChoice(4, "Cancel");
        ch.Show();

        yield return new WaitForChoice();
        int choiceInd = InterfaceSystem.choiceIndex;

        switch (choiceInd)
        {
            case 0:
                isMoving = true;

                break;
            case 1:
                inSummary = true;
                cGroup.interactable = false;
                FadeOut(false, false);
                InterfaceSystem.OpenSummary(this, pageIndex, !partySlot);

                while (!IsPriority)
                {
                    yield return null;
                }

                ButtonsActive(true);

                break;
            case 3:
                InterfaceInputManager.instance.es.SetSelectedGameObject(null);
                cGroup.interactable = false;
                msg.AddMessage("Are you sure you want to release " + selectedMon.GetName() + "?");
                msg.KeepOpen();
                msg.Show();
                yield return new WaitForDialogueReading();

                ch.AddChoice(0, "No");
                ch.AddChoice(1, "Yes");
                ch.CloseMessage();
                ch.Show();
                yield return new WaitForChoice();

                int releaseChoice = InterfaceSystem.choiceIndex;

                if (releaseChoice == 1)
                {
                    if (partySlot)
                    {
                        SaveSystem.playerParty.RemoveAt(pageIndex);
                    } else
                    {
                        SaveSystem.playerRanch[pageIndex] = null;
                    }
                }

                UpdateBagDisplay();
                cGroup.interactable = true;

                break;
            case 5:
                InterfaceInputManager.instance.EnableCursor(false);
                InterfaceSystem.temporaryInBag = !partySlot;
                InterfaceSystem.temporaryMonsterIndex = pageIndex;
                FadeOut(_destroyAfter: false);
                yield break;

            default:
                break;
        }

        lastSelectedSlot = slotIndex;
        lastSelectedPageIndex = pageIndex;
        lastSelectedIsParty = partySlot;
        InterfaceInputManager.instance.es.SetSelectedGameObject((lastSelectedIsParty ? partyMonSlots[lastSelectedSlot] : bagMonSlots[lastSelectedSlot]).gameObject);
        InterfaceInputManager.instance.EnableCursor(true);
        buttonPressed = false;
        yield break;
    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnBecomePriority()
    {
    }

    public override void OnNotPriority()
    {
    }

    public void SetRequest(MonsterRequest r)
    {
        request = r;
        isRequesting = true;
    }
}
