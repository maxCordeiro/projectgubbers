﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MonsterSystem;

public class ItemMessageUI : UISystem
{
    public SuperTextMesh itemName, itemDesc, itemPocket;
    public Image itemIcon;

    public Item item;

    private new void Start()
    {
        base.Start();
        DisplayItem();
    }

    public void DisplayItem()
    {
        itemName.text = ItemSystem.GetItemName(item);
        itemDesc.text = ItemSystem.GetItemDescription(item);
        itemIcon.sprite = ItemSystem.GetItemSprite(item);
        itemPocket.text = SystemVariables.trans["inv_" + item.GetData().pocket.ToString().ToLower()];

        FadeIn();
    }

    public void Update()
    {
        if (isFading || !IsPriority) return;

        if (InterfaceSystem.rePlayer.GetButtonDown("Start") || InterfaceSystem.rePlayer.GetButtonDown("B"))
        {
            FadeOut(_destroyAfter: true);
        }
    }

    public override void OnBecomePriority()
    {
    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnNotPriority()
    {
    }
}
