﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;
using UnityEngine.UI;

public class FishingUI : UISystem
{
    public GameObject container;

    public Image ring, target, prize, arrow, ringSuccess, resultImg;
    public RectTransform arrowRT, targetRT, prizeRT;
    float arrowZ, targetZ, prizeZ;

    public Items lure;

    public float arrowSpeed;
    public float targetBuffer;

    public bool isRotating, isSuccess, isTweening;

    public int rounds;
    public int arrowRotationMultiplier;

    // Start is called before the first frame update
    new void Start()
    {
        base.Start();
        
        arrowRT = arrow.GetComponent<RectTransform>();
        lure = Items.LURE;

        targetRT = target.GetComponent<RectTransform>();
        prizeRT = prize.GetComponent<RectTransform>();

        targetBuffer = 0.04f;

        arrowRotationMultiplier = 1;

        NewRound();

        GameUtility.EnableBackground();
        InterfaceInputManager.instance.EnableCursor(false);
        FadeIn(hideFade: true);
    }

    // Update is called once per frame
    void Update()
    {
        if (isFading || !IsPriority) return;

        if (isRotating)
        {
            arrowRT.Rotate(0, 0, -(arrowSpeed * Time.deltaTime) * arrowRotationMultiplier);

            arrowZ = Mathf.Abs(arrowRT.rotation.z);
            targetZ = Mathf.Abs(targetRT.rotation.z);
            prizeZ = Mathf.Abs(prizeRT.rotation.z);
            
            if (InterfaceSystem.rePlayer.GetButtonDown("A"))
            {
                Debug.Log("[{0} ____ A {1} ____ {2}]".With(targetZ - targetBuffer, arrowZ, targetZ + targetBuffer));

                if (arrowZ >= targetZ - targetBuffer && arrowZ <= targetZ + targetBuffer)
                {
                    HitSuccess();
                } else
                {
                    CatchFail();
                }
            }
        } else if (!isTweening)
        {
            if (InterfaceSystem.rePlayer.GetButtonDown("A"))
            {
                Init();
            }
        }
    }

    void Init()
    {
        rounds = Random.Range(1, 5);

        isRotating = true;

        int tempSpeed = Random.Range(110, 160);
        LeanTween.value(0, tempSpeed, 0.4f).setOnUpdate((float val) => {
            arrowSpeed = val;
        });

    }

    void NewRound()
    {
        targetRT.Rotate(new Vector3(0, 0, Random.Range(30f, 300f)));
        prizeRT.Rotate(new Vector3(0, 0, Random.Range(30f, 300f)));
    }

    void HitSuccess()
    {
        StartCoroutine(HitSuccessCo());
    }

    IEnumerator HitSuccessCo()
    {
        isTweening = true;
        isRotating = false;

        rounds--;

        LeanTween.scale(ringSuccess.gameObject, new Vector3(1.2f, 1.2f, 1), 0.4f).setEaseOutSine();
        LeanTween.value(1f, 0f, 0.4f).setOnUpdate((float val) =>
        {
            ringSuccess.color = new Color(1, 1, 1, val);
        }).setOnComplete(() =>
        {
            ringSuccess.color = Color.white;
            ringSuccess.rectTransform.localScale = new Vector3(1, 1, 1);
        });

        yield return new WaitForSeconds(0.2f);

        if (rounds > 0)
        {
            arrowRotationMultiplier *= -1;

            float newTargetZ = Random.Range(30f, 300f);
            float newPrizeZ = Random.Range(30f, 300f);

            LeanTween.rotateZ(targetRT.gameObject, newTargetZ, 0.1f);
            LeanTween.rotateZ(prizeRT.gameObject, newPrizeZ, 0.1f);

            yield return new WaitForSeconds(0.1f);

            isRotating = true;
        }
        else
        {
            CatchSuccess();
        }
        isTweening = false;
    }

    void CatchSuccess()
    {
        StartCoroutine(CatchSucessCo());
    }

    IEnumerator CatchSucessCo()
    {
        isRotating = false;
        isSuccess = true;

        InterfaceSystem.caughtLastfish = true;

        LeanTween.alpha(resultImg.rectTransform, 1, 0.1f);
        LeanTween.moveY(resultImg.rectTransform, 0, 0.2f).setEaseOutQuart();

        yield return new WaitForSeconds(1.6f);

        LeanTween.alpha(resultImg.rectTransform, 0, 0.1f);

        FadeOut(true);
    }

    void CatchFail()
    {
        LeanTween.moveX(container, container.transform.position.x, 0.2f).setFrom(container.transform.position.x - 10).setEaseOutElastic();
        isRotating = false;
        arrowRT.SetPositionAndRotation(arrowRT.position, new Quaternion(0, 0, 0, 0));
    }

    public override void OnBecomePriority()
    {
    }

    public override void OnFadeIn()
    {
    }

    public override void OnFadeOut()
    {
        if (referrer == null)
        {
            InterfaceInputManager.instance.EnableCursor(false);
            GameUtility.DisableBackground();
        }
        else
        {
            referrer.SetPriority(true);
        }
    }

    public override void OnNotPriority()
    {
    }

    private void OnGUI()
    {
        /*GUI.Label(new Rect(10, 10, 300, 300),
            "Target Left: {0}\n".With(targetZ - targetBuffer) +
            "Arrow: {0}\n".With(arrowZ) +
            "Target Right: {0}\n".With(targetZ + targetBuffer) +
            "\nArrowMultiplier: {0}".With(arrowRotationMultiplier)
            );*/
    }
}
