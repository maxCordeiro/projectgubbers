﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;
using UnityEngine.SceneManagement;

public class Door : TransferSystem
{
    SpriteRenderer spr;
    public Sprite open, close;

    public override void Start()
    {
        base.Start();
        spr = GetComponent<SpriteRenderer>();

        spr.sprite = close;
    }

    public void DoorTransfer()
    {
        spr.sprite = open;
        Transfer();
    }
}
