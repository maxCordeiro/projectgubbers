﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using MonsterSystem;

public class ChoicesManager : MonoBehaviour {
/*
    public string[] choiceText;
    public UnityAction[] choiceActions;

    public int choiceAmount;

    InterfaceInputManager inpMan;
    bool initialized;

    Image choiceWindow;

    public void Initialize()
    {
        GameUtility.BringToFront(gameObject);

        choiceWindow = GetComponent<Image>();
        choiceWindow.color = new Color(1, 1, 1, 1);

        for (int i = 0; i < choiceAmount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(true);
        }

        inpMan = FindObjectOfType<InterfaceInputManager>();
        inpMan.controllerCursor.transform.SetAsLastSibling();
        inpMan.controllerCursor.EnableCursor(true);
        inpMan.es.SetSelectedGameObject(transform.GetChild(0).gameObject);

        DialogueSystem.inChoiceWindow = true;
        initialized = true;
    }

    public void Update()
    {
        if (InterfaceSystem.textFinished && !initialized) Initialize();

        if (!initialized) return;
        if (inpMan.es.currentSelectedGameObject == null) return;

        inpMan.controllerCursor.transform.localPosition = new Vector2(52, inpMan.es.currentSelectedGameObject.transform.localPosition.y - 80);
    }

    public void AfterChoiceSelect()
    {
        InterfaceSystem.choiceSelected = true;
        if (GameObject.FindGameObjectWithTag("GameplayCanvas").transform.Find("DialogueWindow"))
        {
            //if (DialogueSystem.closeAfterDone) GameObject.FindGameObjectWithTag("GameplayCanvas").transform.Find("DialogueWindow").GetComponent<DialogueHandler>().DisableBox();
            InterfaceSystem.isInteracting = false;
        }
        inpMan.controllerCursor.EnableCursor(false);

        DialogueSystem.inChoiceWindow = false;
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        InterfaceSystem.choiceSelected = false;
    }*/
}
