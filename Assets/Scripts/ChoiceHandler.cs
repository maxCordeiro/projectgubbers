﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using MonsterSystem;

public class ChoiceHandler : MonoBehaviour
{
    CanvasGroup cGroup;

    public GameObject choiceButton;
    public List<GameObject> choices = new List<GameObject>();
    
    RectTransform rectT;

    bool beginHide, doneHide;
    bool keepCursor;
    
    public float xx, yy;

    public bool isOpen, choiceSelected;

    private static ChoiceHandler _instance;
    public static ChoiceHandler instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(ChoiceHandler)) as ChoiceHandler;
                if (!_instance)
                    Debug.Log("There need to be at least one active ChoiceHandler on the scene");
            }

            return _instance;
        }
    }

    private void Start()
    {
        cGroup = GetComponent<CanvasGroup>();
        cGroup.alpha = 0;
        rectT = GetComponent<RectTransform>();
    }

    public ChoiceHandler AddChoice(int choiceIndex, string _text, bool hideAfter = true)
    {
        GameObject tempButton = Instantiate(choiceButton, transform);
        tempButton.name = "Button: " + _text;
        tempButton.transform.GetChild(0).GetComponent<SuperTextMesh>().text = _text;

        Button b = tempButton.GetComponent<Button>();

        b.onClick.AddListener(() => { StartCoroutine(ButtonClick(choiceIndex, hideAfter)); });

        choices.Add(tempButton);
        return this;
    }

    public void CloseMessage()
    {
        foreach (GameObject obj in choices)
        {
            obj.GetComponent<Button>().onClick.AddListener(() => {
                DialogueHandler.instance.Hide();
            });
        }
    }

    public void SetPos(Vector2 pos)
    {
        GetComponent<RectTransform>().anchoredPosition = pos;
    }

    public void RememberCursorPos()
    {
    }

    public void Show()
    {
        StartCoroutine(ShowCoroutine());
    }

    public ChoiceHandler KeepCursor()
    {
        keepCursor = true;
        return this;
    }

    public void Hide()
    {
        doneHide = false;
        beginHide = true;

        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }

        InterfaceInputManager.instance.EnableCursor(keepCursor);
        InterfaceInputManager.instance.es.SetSelectedGameObject(null);

        choices = new List<GameObject>();

        doneHide = true;
        beginHide = false;

        keepCursor = false;

        cGroup.alpha = 0;

        choiceSelected = true;
        isOpen = false;
    }

    public void Update()
    {
        if (!isOpen) return;

        if (InterfaceInputManager.instance.es.currentSelectedGameObject == null) return;

        RectTransform sRect = InterfaceInputManager.instance.es.currentSelectedGameObject.GetComponent<RectTransform>();
        Vector3[] c = new Vector3[4];
        sRect.GetWorldCorners(c);

        InterfaceInputManager.instance.PositionCursor(new Vector3(c[1].x, c[1].y));
    }

    IEnumerator ShowCoroutine()
    {
        isOpen = true;
        InterfaceSystem.ResetTempChoices();
        choiceSelected = false;

        while (!doneHide && beginHide)
        {
            yield return null;
        }

        yield return new WaitForFixedUpdate();

        rectT.anchorMin = new Vector2(0, 0);
        rectT.anchorMax = new Vector2(0, 0);
        rectT.pivot = new Vector2(0, 0);

        transform.SetAsLastSibling();

        for (int i = 0; i < choices.Count; i++)
        {
            Button b = choices[i].GetComponent<Button>();
            Navigation n = new Navigation
            {
                mode = Navigation.Mode.Explicit
            };

            if (choices.Count > 1)
            {
                if (i == 0)
                {
                    n.selectOnDown = choices[i + 1].GetComponent<Button>();
                } else if (i == choices.Count - 1)
                {
                    n.selectOnUp = choices[i - 1].GetComponent<Button>();
                } else
                {
                    n.selectOnUp = choices[i - 1].GetComponent<Button>();
                    n.selectOnDown = choices[i + 1].GetComponent<Button>();
                }
            }

            b.navigation = n;
        }

        InterfaceInputManager.instance.es.SetSelectedGameObject(transform.GetChild(0).gameObject);
        InterfaceInputManager.instance.controllerCursor.transform.localPosition = new Vector2(32, InterfaceInputManager.instance.es.currentSelectedGameObject.transform.localPosition.y - 82);
        InterfaceInputManager.instance.controllerCursor.transform.SetAsLastSibling();

        InterfaceInputManager.instance.EnableCursor(true);

        cGroup.alpha = 1;

        yield break;
    }

    public IEnumerator ButtonClick(int _index, bool hideAfter = true)
    {
        if (DialogueHandler.instance.isOpen) yield return new WaitForDialogueReading();

        InterfaceSystem.choiceIndex = _index;
        Hide();
    }
}