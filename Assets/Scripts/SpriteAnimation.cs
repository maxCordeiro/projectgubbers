﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;
using System.Linq;

public class SpriteAnimation : MonoBehaviour
{
    [System.Serializable]
    public class Anim
    {
        public string name;
        public AnimFrame[] frames;
        public float framesPerSec = 5;
        public bool loop = true;
        [Tooltip("Clear playing animation after loop")]
        public bool clearAfter;

        public float duration
        {
            get
            {
                return frames.Length * framesPerSec;
            }
            set
            {
                framesPerSec = value / frames.Length;
            }
        }

        public Anim(string animName, AnimFrame[] spr, bool lop)
        {
            name = animName;
            frames = spr;
            loop = lop;
        }
    }

    [System.Serializable]
    public class AnimFrame
    {
        public Sprite frameSprite;
        public float frameLength = -1;

        public AnimFrame(Sprite _sprite, float _frameLength = -1)
        {
            frameSprite = _sprite;
            frameLength = _frameLength;
        }
    }

    public float globalFPS;

    public List<Anim> animations = new List<Anim>();

    [HideInInspector]
    public int currentFrame;

    public bool autoStart;

    [HideInInspector]
    public bool done
    {
        get { return currentFrame >= current.frames.Length; }
    }

    [HideInInspector]
    public bool playing
    {
        get { return _playing; }
    }

    SpriteRenderer spriteRenderer;
    public Anim current;
    bool _playing;
    float secsPerFrame;
    float nextFrameTime;

    [HideInInspector]
    public int playCount = -1;
    [HideInInspector]
    public int currentPlay;


    [ContextMenu("Sort All Frames by Name")]
    void DoSort()
    {
        foreach (Anim anim in animations)
        {
            System.Array.Sort(anim.frames, (a, b) => a.frameSprite.name.CompareTo(b.frameSprite.name));
        }
        Debug.Log(gameObject.name + " animation frames have been sorted alphabetically.");
    }

    void Start()
    {
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        if (spriteRenderer == null)
        {
            Debug.Log(gameObject.name + ": Couldn't find SpriteRenderer");
        }

        if (animations.Count > 0 && autoStart) PlayByIndex(0);
    }

    void Update()
    {
        if (!_playing || Time.time < nextFrameTime || spriteRenderer == null) return;
        currentFrame++;
        if (currentFrame >= current.frames.Length)
        {
            if (!current.loop || (playCount != -1 && currentPlay >= playCount))
            {
                _playing = false;
                if (current.clearAfter) spriteRenderer.sprite = null;

                return;
            }
            currentFrame = 0;

            if (playCount != -1 && currentPlay < playCount)
            {
                currentPlay++;
            }
        }
        spriteRenderer.sprite = current.frames[currentFrame].frameSprite;
        nextFrameTime += (current.frames[currentFrame].frameLength == -1? secsPerFrame : (1f / current.frames[currentFrame].frameLength));
    }

    public void Play(string name)
    {
        int index = animations.FindIndex(a => a.name == name);
        if (index < 0)
        {
            Debug.LogError(gameObject + ": No such animation: " + name);
        }
        else
        {
            PlayByIndex(index);
        }
    }

    public void PlayByIndex(int index)
    {
        if (index < 0) return;
        Anim anim = animations[index];

        current = anim;

        secsPerFrame = 1f / (current.frames[currentFrame].frameLength == -1 ? globalFPS : current.frames[currentFrame].frameLength);//(globalFPS > 0 ? globalFPS : anim.framesPerSec);
        currentFrame = -1;
        _playing = true;
        nextFrameTime = Time.time;
    }

    public void Stop()
    {
        _playing = false;
    }

    public void Stop(bool reset)
    {
        currentFrame = 0;
        spriteRenderer.sprite = current.frames[0].frameSprite;
        _playing = false;
    }

    public void Resume()
    {
        _playing = true;
        nextFrameTime = Time.time + secsPerFrame;
    }
}