﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using Rewired;
using Rewired.Integration.UnityUI;
using UnityEngine.EventSystems;
using UnityEditorInternal;

public class InterfaceInputManager : MonoBehaviour {

	Rewired.Player rePlayer;
    RewiredStandaloneInputModule inputModule;

    [HideInInspector]
    public GameObject controllerCursor;
    public EventSystem es;

    private static InterfaceInputManager _instance;
    public static InterfaceInputManager instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType(typeof(InterfaceInputManager)) as InterfaceInputManager;
                if (!_instance)
                    Debug.Log("There need to be at least one active InterfaceInputManager on the scene");
            }

            return _instance;
        }
    }

    void Start ()
    {
		rePlayer = ReInput.players.GetPlayer(0);

        es = FindObjectOfType<EventSystem>();
        inputModule = es.GetComponent<RewiredStandaloneInputModule>();
        controllerCursor = GameObject.FindGameObjectWithTag("ControllerCursor");
        EnableCursor(!SaveSystem.settings_controllerEnabled);
        ControllerMethod(SaveSystem.settings_controllerEnabled);
        FlipCursor(SaveSystem.settings_controllerEnabled ? 1 : 0);

    }

    void Update () {

	}

    public void FlipCursor(int dir)
    {
        return;
    }

    public void PositionCursor(Vector3 pos, bool overrideZ = false)
    {
        //controllerCursor.transform.position = new Vector3(pos.x, pos.y, overrideZ? pos.z : controllerCursor.transform.position.z);

        LeanTween.cancel(controllerCursor);
        LeanTween.move(controllerCursor, new Vector3(pos.x, pos.y, overrideZ ? pos.z : controllerCursor.transform.position.z), 0.05f);
    }

    public void EnableCursor(bool enabled)
    {
        //if (!SaveSystem.settings_controllerEnabled) return;
        if (controllerCursor != null) controllerCursor.SetActive(enabled);
    }

    // Switch Controller method while in-game
    // True - Controller
    // False - Mouse
    public void ControllerMethod(bool controllerMouse)
    {
        //inputModule.allowMouseInput = !controllerMouse;

        //rePlayer.controllers.maps.SetMapsEnabled(!controllerMouse, ControllerType.Keyboard, 1);
        rePlayer.controllers.maps.SetMapsEnabled(controllerMouse, ControllerType.Joystick, 1);
    }
}
