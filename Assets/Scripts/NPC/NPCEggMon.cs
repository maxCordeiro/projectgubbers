﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;

public class NPCEggMon : NPC
{
    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        InterfaceSystem.OpenEggMonitor(null);

        yield return new WaitForMenu();
    }

}
