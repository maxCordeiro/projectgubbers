﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class NPCFenceGate : NPC
{
    public Sprite[] fenceSprites;

    [HideInInspector]
    public OverworldDir facingDir;

    Player p;
    BoxCollider2D box;
    SpriteRenderer spr;

    public override void Start()
    {
        p = FindObjectOfType<Player>();
        box = GetComponent<BoxCollider2D>();
    }

    public override IEnumerator InteractEvent()
    {
        InterfaceSystem.isInteracting = true;

        if (p.owDir == facingDir)
        {

            box.enabled = false;
            yield return StartCoroutine(p.MoveToPoint(transform.position + new Vector3(0, facingDir == OverworldDir.DOWN? 1 : -1)));

            yield return StartCoroutine(p.MoveDirection(facingDir, 2));

            box.enabled = true;
            InterfaceSystem.isInteracting = false;

            yield break;
        }

        DialogueHandler msg = DialogueHandler.instance;
        msg.AddMessage("This gate is locked from the other side.");
        msg.Show();

        yield return new WaitForDialogueReading();

        InterfaceSystem.isInteracting = false;

        yield break;
    }
}
