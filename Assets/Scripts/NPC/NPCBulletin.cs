﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCBulletin : NPC
{
    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        Debug.Log("Interact");
        choices.SetPos(new Vector2(350, 96));

        msg.AddMessage("Test message. 1");
        msg.AddMessage("Test Test Message Message Test Test Message Message Test Test Message Message Test Test Message Message. 2");
        msg.AddMessage("Test message. 3");
        msg.Show();

        yield return new WaitForDialogueReading();



        //yield return new WaitForChoice();

    }

}
