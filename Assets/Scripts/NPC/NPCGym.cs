﻿using MonsterSystem;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;
using GameDatabase;
using System.Linq;

public class NPCGym : NPC {

    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        choices.SetPos(new Vector2(350, 96));
        msg.Name(DialogueSystem.LoadName("npc_gym", "dialogue_npc"));

        if (SaveSystem.gymTraining.Count > 0)
        {
            msg.AddMessage(DialogueSystem.LoadMsg("npc_gym", "greeting", "dialogue_npc"));
            msg.KeepOpen();
            msg.Show();

            yield return new WaitForDialogueReading();

            for (int i = 0; i < SaveSystem.gymTraining.Count; i++)
            {
                int ii = i;
                //choices.AddChoice(string.Format("{0}{1}", SaveSystem.gymTraining[i].GetName(), GameUtility.HoursUntil(SaveSystem.gymWait[i]) > 0? "- " + GameUtility.HoursUntil(SaveSystem.gymWait[i]) + " hrs" : ""), () => {
                //    Debug.Log(ii);
                //    StartCoroutine(CheckingMonster(ii));
                //});
            }

            if (SaveSystem.gymTraining.Count < 5)
            {
                //choices.AddChoice("Add Monster", () => {
                //    StartCoroutine(SelectingMonster());
                //});
            }

            //choices.AddChoice("Cancel", () =>
            //{
            //    msg.Message(DialogueSystem.LoadMsg("npc_gym", "train_no", "dialogue_npc"));
            //    msg.Show();
            //});

            choices.Show();
        }
        else
        {
            msg.AddMessage(DialogueSystem.LoadMsg("npc_gym", "greeting_nomon", "dialogue_npc"));
            msg.KeepOpen();
            msg.Show();

            yield return new WaitForDialogueReading();

            msg.AddMessage(DialogueSystem.LoadMsg("npc_gym", "greeting_nomon_2", "dialogue_npc"));
            msg.KeepOpen();
            msg.Show();

            yield return new WaitForDialogueReading();
            
            //choices.AddChoice("Yes", () => {
            //    StartCoroutine(SelectingMonster());
            //});
            //choices.AddChoice("No", () => {
            //    msg.Message(DialogueSystem.LoadMsg("npc_gym", "train_no", "dialogue_npc"));
            //    msg.Show();
            //});
            choices.Show();
        }

        yield return new WaitForChoice();

    }

    public IEnumerator SelectingMonster()
    {
        msg.Name(DialogueSystem.LoadName("npc_gym", "dialogue_npc"));
        msg.AddMessage(DialogueSystem.LoadMsg("npc_gym", "train_yes", "dialogue_npc"));
        msg.Show();

        yield return new WaitForDialogueReading();

        //InterfaceSystem.OpenParty(0);
        
        yield return new WaitForMonsterSelect();

        if (SaveSystem.playerParty.Count == 1)
        {
            msg.Name(DialogueSystem.LoadName("npc_gym", "dialogue_npc"));

            msg.AddMessage(DialogueSystem.LoadMsg("npc_gym", "lastmon", "dialogue_npc"));
        } else
        {
            msg.Name(DialogueSystem.LoadName("npc_gym", "dialogue_npc"));

            SaveSystem.gymTraining.Add(SaveSystem.playerParty[InterfaceSystem.temporaryMonsterIndex]);

            //int total = 

            SaveSystem.gymWait.Add(new Date(_hour: Mathf.RoundToInt((SaveSystem.gymTraining.Last().gymUses + 1) * 1.6f)));
            GameUtility.RemoveMonsterFromParty(InterfaceSystem.temporaryMonsterIndex);

            InterfaceSystem.temporaryMonsterIndex = -1;

            msg.AddMessage(string.Format(DialogueSystem.LoadMsg("npc_gym", "confirm", "dialogue_npc"), SaveSystem.gymTraining.Last().GetName(), GameUtility.HoursUntil(SaveSystem.gymWait.Last())));
        }

        msg.Show();

        yield return new WaitForDialogueReading();

    }

    public IEnumerator CheckingMonster(int _index)
    {
        msg.Name(DialogueSystem.LoadName("npc_gym", "dialogue_npc"));

        if (GameUtility.HoursUntil(SaveSystem.gymWait[_index]) == 0)
        {
            SaveSystem.gymTraining[_index].UseGym();

            int points = GetStatPoints(SaveSystem.gymTraining[_index].gymUses);

            msg.AddMessage(string.Format(DialogueSystem.LoadMsg("npc_gym", "done", "dialogue_npc"), SaveSystem.gymTraining[_index].GetName(), points));
            msg.Show();

            SaveSystem.gymTraining[_index].AddStatPoints(points);
            GameUtility.ObtainMonster(SaveSystem.gymTraining[_index]);

            SaveSystem.gymTraining.RemoveAt(_index);
            SaveSystem.gymWait.RemoveAt(_index);
        }
        else
        {
            msg.AddMessage(string.Format(DialogueSystem.LoadMsg("npc_gym", "stilltraining", "dialogue_npc"), SaveSystem.gymTraining[_index].GetName(), GameUtility.HoursUntil(SaveSystem.gymWait[_index])));
            msg.Show();
        }

        yield return new WaitForDialogueReading();
    }

    public int GetStatPoints(int uses)
    {
        return Mathf.FloorToInt(30 / (1.33f * uses));
    }
}