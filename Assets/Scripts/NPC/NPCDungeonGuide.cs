﻿using MonsterSystem;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;
using GameDatabase;
using System.Linq;

public class NPCDungeonGuide : NPC
{

    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        choices.SetPos(new Vector2(350, 96));
        msg.Name("Shopkeeper A");

        msg.AddMessage("How's it going?");
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();

        choices.AddChoice(0, "Leave");
        choices.AddChoice(1, "Sell");

        choices.CloseMessage();
        choices.Show();

        yield return new WaitForChoice();
    }
}