﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class NPCSingleText : NPC
{
    public string npcName;
    public string npcMsg;

    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        choices.SetPos(new Vector2(350, 96));
        msg.Name(DialogueSystem.LoadName(npcName, "dialogue_npc"));

        msg.AddMessage(DialogueSystem.LoadMsg("npc_daycare", "greeting", "dialogue_npc"));
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();

        yield break;
    }
}