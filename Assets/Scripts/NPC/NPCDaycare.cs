﻿using MonsterSystem;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;
using GameDatabase;
using System.Linq;

public class NPCDaycare : NPC {

    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        InterfaceSystem.isInteracting = true;
        choices.SetPos(new Vector2(350, 96));
        msg.Name(SystemVariables.trans["npc_daycare_name"]);

        msg.AddMessage(SystemVariables.trans["npc_daycare_greeting_day"]);
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();
        
        choices.AddChoice(0, SystemVariables.trans["npc_daycare_viewdens"]); //, () => {StartCoroutine(ViewingDens());}
        choices.AddChoice(4, "Egg Monitor");
        choices.AddChoice(1, SystemVariables.trans["npc_daycare_upgrade"]);
        choices.AddChoice(2, SystemVariables.trans["npc_daycare_help"]);
        choices.AddChoice(3, SystemVariables.trans["sys_cancel"]);/*
    choices.AddChoice("Need an upgrade?", () => {

        });
        choices.AddChoice("What's this?", () => {

        });
        choices.AddChoice("Cancel", () => {
            msg.Name(DialogueSystem.LoadName("npc_daycare", "dialogue_npc"));
            msg.Message(DialogueSystem.LoadMsg("npc_daycare", "leave", "dialogue_npc"));
            msg.Show();
        });*/
        choices.CloseMessage();
        choices.Show();

        yield return new WaitForChoice();
        yield return new WaitForDialogueClose();

        int ch = InterfaceSystem.choiceIndex;

        Debug.Log(ch.ToString());

        switch (ch)
        {
            case 0:
                yield return StartCoroutine(ViewingDens());
                break;
            case 4:
                yield return StartCoroutine(ViewingEggMon());
                break;
            default:
                break;
        }

        InterfaceSystem.isInteracting = false;
                Debug.Log("endinteract");
    }

    public IEnumerator ViewingDens()
    {
        Debug.Log("viewdens");
        msg.Hide();
        yield return new WaitForDialogueClose();

        InterfaceSystem.OpenDaycareMonsterViewer(null);
        yield return new WaitForMenu();
    }

    public IEnumerator ViewingEggMon()
    {
        msg.Hide();
        yield return new WaitForDialogueClose();

        InterfaceSystem.OpenEggMonitor(null);
        yield return new WaitForMenu();
    }
}