﻿using MonsterSystem;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;
using GameDatabase;
using System.Linq;

public class NPCGatehouse : NPC {

    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;
    }

    public override IEnumerator InteractEvent()
    {
        choices.SetPos(new Vector2(350, 96));
        msg.Name(SystemVariables.trans["gatehouse_name"]);

        msg.AddMessage(SystemVariables.trans["gatehouse_greeting"]);
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();

        choices.AddChoice(0, "Let me out!");
        choices.AddChoice(1, "What's this?");
        choices.AddChoice(2, "Nevermind");
        choices.Show();

        yield return new WaitForChoice();

        int choiceIndex = InterfaceSystem.choiceIndex;

        switch (choiceIndex)
        {
            case 0:
                StartCoroutine(ViewingMap());
                break;
            case 1:
                break;
            case 2:
                msg.Name(DialogueSystem.LoadName("npc_gatehouse", "dialogue_npc"));
                msg.AddMessage(DialogueSystem.LoadMsg("npc_gatehouse", "leave", "dialogue_npc"));
                msg.Show();
                break;
            default:
                break;
        }
    }

    public IEnumerator ViewingMap()
    {
        msg.Hide();

        InterfaceSystem.OpenMapViewer(null);
        yield return new WaitForMenu();
    }
}