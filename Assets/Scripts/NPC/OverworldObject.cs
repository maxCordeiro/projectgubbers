﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using UnityEditor;

public class OverworldObject : MonoBehaviour
{
    public OverworldDir owDir = OverworldDir.DOWN;
    public OverworldDir prevDir = OverworldDir.DOWN;
    public bool beingMoved;

    SpriteAnimation spAnim;
    
    // Start is called before the first frame update
    public virtual void Start()
    {
        spAnim = transform.GetChild(0).GetComponent<SpriteAnimation>();
        //UpdateSpriteAnimation();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateAnimation(Vector2 direction)
    {
        if (direction != Vector2.zero && (beingMoved || !InterfaceSystem.IsNotFree()))
        {
            owDir = (Mathf.Abs(direction.x) == Mathf.Abs(direction.y))? prevDir : GameUtility.CalculateDirection(direction);
            if (owDir != prevDir || !spAnim.playing)
            {
                spAnim.Play(GameUtility.FirstCharToUpper(owDir.ToString().ToLower()));
            }
            prevDir = owDir;
        }
        else
        {
            if (spAnim.playing)
            {
                spAnim.Stop(true);
            }
        }
    }

    public void UpdateSpriteAnimation()
    {
        Sprite[] baseSprite = Resources.LoadAll<Sprite>("Apparel/humanbase");
        Sprite[] runSprite = Resources.LoadAll<Sprite>("Apparel/humanbase_running");

        float walkLength = 3f;
        float runLength = 9f;

        spAnim.animations.Add(new SpriteAnimation.Anim("Down", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( baseSprite[0]),
            new SpriteAnimation.AnimFrame( baseSprite[1], walkLength),
            new SpriteAnimation.AnimFrame( baseSprite[2]),
            new SpriteAnimation.AnimFrame( baseSprite[3], walkLength),
        }, true));
        spAnim.animations.Add(new SpriteAnimation.Anim("Right", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( baseSprite[4]),
            new SpriteAnimation.AnimFrame( baseSprite[5], walkLength),
            new SpriteAnimation.AnimFrame( baseSprite[6]),
            new SpriteAnimation.AnimFrame( baseSprite[7], walkLength),
        }, true));
        spAnim.animations.Add(new SpriteAnimation.Anim("Up", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( baseSprite[8]),
            new SpriteAnimation.AnimFrame( baseSprite[9], walkLength),
            new SpriteAnimation.AnimFrame( baseSprite[10]),
            new SpriteAnimation.AnimFrame( baseSprite[11], walkLength),
        }, true));
        spAnim.animations.Add(new SpriteAnimation.Anim("Left", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( baseSprite[12]),
            new SpriteAnimation.AnimFrame( baseSprite[13], walkLength),
            new SpriteAnimation.AnimFrame( baseSprite[14]),
            new SpriteAnimation.AnimFrame( baseSprite[15], walkLength),
        }, true));

        spAnim.animations.Add(new SpriteAnimation.Anim("Down_Run", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( runSprite[0], 10),
            new SpriteAnimation.AnimFrame( runSprite[1], runLength),
            new SpriteAnimation.AnimFrame( runSprite[2], 10),
            new SpriteAnimation.AnimFrame( runSprite[3], runLength),
        }, true));
        spAnim.animations.Add(new SpriteAnimation.Anim("Right_Run", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( runSprite[4]),
            new SpriteAnimation.AnimFrame( runSprite[5]),
            new SpriteAnimation.AnimFrame( runSprite[6]),
            new SpriteAnimation.AnimFrame( runSprite[7]),
        }, true));
        spAnim.animations.Add(new SpriteAnimation.Anim("Up_Run", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( runSprite[8]),
            new SpriteAnimation.AnimFrame( runSprite[9]),
            new SpriteAnimation.AnimFrame( runSprite[10]),
            new SpriteAnimation.AnimFrame( runSprite[11]),
        }, true));
        spAnim.animations.Add(new SpriteAnimation.Anim("Left_Run", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( runSprite[12]),
            new SpriteAnimation.AnimFrame( runSprite[13]),
            new SpriteAnimation.AnimFrame( runSprite[14]),
            new SpriteAnimation.AnimFrame( runSprite[15]),
        }, true));
    }

    public IEnumerator MoveDown(int tiles)
    {
        beingMoved = true;
        int tilesMoved = 0;

        UpdateAnimation(Vector2.down);

        while (tilesMoved < tiles)
        {
            float y = transform.position.y;
            LeanTween.moveY(gameObject, y - 1, 1f);
            tilesMoved++;
            yield return new WaitForSeconds(1f);
        }

        beingMoved = false;
        yield break;
    }

    public IEnumerator MoveDirection(OverworldDir direction, int tiles, bool angle = false, OverworldDir angleDir = OverworldDir.DOWN)
    {
        beingMoved = true;
        int tilesMoved = 0;
        int moveDir = 0;
        float moveTime = 0.3f;
        Vector2 vecDir = Vector2.zero;

        switch (direction)
        {
            case OverworldDir.DOWN:
                moveDir = -1;
                vecDir = Vector2.down;
                break;
            case OverworldDir.RIGHT:
                moveDir = 1;
                vecDir = Vector2.right;
                break;
            case OverworldDir.UP:
                moveDir = 1;
                vecDir = Vector2.up;
                break;
            case OverworldDir.LEFT:
                moveDir = -1;
                vecDir = Vector2.left;
                break;
        }
        
        if (angle)
        {
            switch (angleDir)
            {
                case OverworldDir.DOWN:
                    vecDir.y -= 1;
                    break;
                case OverworldDir.RIGHT:
                    vecDir.x += 1;
                    break;
                case OverworldDir.UP:
                    vecDir.y += 1;
                    break;
                case OverworldDir.LEFT:
                    vecDir.x -= 1;
                    break;
                default:
                    break;
            }
        }

        while (tilesMoved < tiles)
        {
            UpdateAnimation(vecDir);

            float x = transform.position.x;
            float y = transform.position.y;
            
            if (angle)
            {
                LeanTween.move(gameObject, new Vector2(x, y) + vecDir, moveTime);
            } else
            {
                if (direction == OverworldDir.DOWN || direction == OverworldDir.UP)
                {
                    LeanTween.moveY(gameObject, y + moveDir, moveTime);
                } else
                {
                    LeanTween.moveX(gameObject, x + moveDir, moveTime);
                }
            }

            tilesMoved++;
            yield return new WaitForSeconds(moveTime);
            UpdateAnimation(Vector2.zero);
        }

        beingMoved = false;
        yield break;
    }

    // Does not have pathfinding
    public IEnumerator MoveToPoint(Vector3 pos)
    {
        beingMoved = true;

        Vector3 heading = pos - transform.position;
        float distance = heading.magnitude;

        Vector3 dir = heading / distance;

        UpdateAnimation(dir);

        LeanTween.move(gameObject, pos, 0.5f);
        yield return new WaitForSeconds(0.6f);

        beingMoved = false;
        yield break;
    }

    public IEnumerator FaceDirection(OverworldDir _dir)
    {
        owDir = _dir;
        yield break;
    }
}
