﻿using MonsterSystem;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;

public class NPCDebug : NPC {

    bool choseYes;

    public override IEnumerator InteractEvent()
    {
        DialogueHandler msg = DialogueHandler.instance;
        ChoiceHandler choices = ChoiceHandler.instance;
        //choices.SetPos(new Vector2(350, 96));
        msg.Name(DialogueSystem.LoadName("npc_debug", "dialogue_npc"));

        msg.AddMessage(DialogueSystem.LoadMsg("npc_debug", "trade_0", "dialogue_npc"));
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();

        //choices.AddChoice("Yes", () => {
        //    StartCoroutine(FirstChoice()); });
        //choices.AddChoice("No", () => { Debug.Log("Yeet 2"); });
        //choices.CloseMessage();
        choices.Show();

        yield return new WaitForChoice();

        LeanTween.scale(gameObject, new Vector3(1.4f, 0.6f, transform.localScale.z), 0.1f);
        yield return new WaitForSeconds(0.1f);

        LeanTween.scale(gameObject, new Vector3(1, 1, transform.localScale.z), 0.1f);
        yield return new WaitForSeconds(0.1f);
    }

    public IEnumerator FirstChoice()
    {
        //InterfaceSystem.OpenParty(0);
        
        yield return new WaitForMonsterSelect();
        
        DialogueHandler msg = DialogueHandler.instance;

        if (InterfaceSystem.temporaryMonster.GetID() == 0)
        {
            msg.Name(DialogueSystem.LoadName("npc_debug", "dialogue_npc"));

            msg.AddMessage(DialogueSystem.LoadMsg("npc_debug", "trade_conf", "dialogue_npc"));
            msg.KeepOpen();
            msg.Show();

            yield return new WaitForDialogueReading();

            ChoiceHandler choices = ChoiceHandler.instance;
            choices.SetPos(new Vector2(350, 96));

            //choices.AddChoice("Yes", () => {
            //    StartCoroutine(YesTrade());
            //});
            //choices.AddChoice("No", () => {
            //    msg.Message(DialogueSystem.LoadMsg("npc_debug", "trade_no", "dialogue_npc"));
            //    msg.Show();
            //});
            choices.CloseMessage();
            choices.Show();

            yield return new WaitForChoice();
        }
    }

    public IEnumerator YesTrade()
    {
        Monster jilo = new Monster(new MonsterIdentifier(GameDatabase.Monsters.JILO, 0));

        jilo.level = InterfaceSystem.temporaryMonster.level;
        SaveSystem.playerParty.Insert(0, jilo);

        yield break;
    }
}