﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class NPC : OverworldObject
{
    public int windowSkin;
    public int nameSkin;

    public Items hair, top, bottom;
    
    [HideInInspector]
    public Material npcMat;

    public override void Start()
    {
        base.Start();
        npcMat = transform.GetChild(0).GetComponent<SpriteRenderer>().material;
        //UpdateNPCSprite();
    }

    public void UpdateNPCSprite()
    {
        Texture topTex = Resources.Load<Texture>("Apparel/" + "Tops/" + top.ToString());
        Texture bottomTex = Resources.Load<Texture>("Apparel/" + "Bottoms/" + bottom.ToString());

        Texture none = Resources.Load<Texture>("Apparel/none");

        npcMat.SetTexture("_Bottom", bottomTex == null? none : bottomTex);
        npcMat.SetTexture("_Top", topTex == null? none : topTex);
    }

    public virtual IEnumerator InteractEvent()
    {
        yield break;
    }
}