﻿using MonsterSystem;
using UnityEngine;
using System;
using UnityEngine.Events;
using System.Collections;
using GameDatabase;
using System.Linq;

public class NPCShopB : NPC
{
    DialogueHandler msg;
    ChoiceHandler choices;

    public override void Start()
    {
        base.Start();

        msg = DialogueHandler.instance;
        choices = ChoiceHandler.instance;

        top = Items.TOP_BROWNPONCHO;
        bottom = Items.BOT_REDPANTS;
        UpdateNPCSprite();
    }

    public override IEnumerator InteractEvent()
    {
        InterfaceSystem.isInteracting = true;

        choices.SetPos(new Vector2(350, 96));
        msg.Name("Shopkeeper A");

        msg.AddMessage("What do?");
        msg.KeepOpen();
        msg.Show();

        yield return new WaitForDialogueReading();
        /*
        QuantityHandler q = QuantityHandler.Instance();
        q.SetRange(0, 50);
        q.Show();

        yield return new WaitForQuantity();*/



        choices.AddChoice(0, "Buy");
        choices.AddChoice(1, "Sell");

        choices.CloseMessage();
        choices.Show();

        yield return new WaitForChoice();
        yield return new WaitForDialogueClose();

        int choiceInd = InterfaceSystem.choiceIndex;

        if (choiceInd == 0) //Buy
        {
            InterfaceSystem.OpenShop(null, SaveSystem.shopB, "Shopkeeper B");
        }
        else //Sell
        {
            InterfaceSystem.OpenInventory(null, false, true);
        }

        yield return new WaitForMenu();

        msg.AddMessage("Thanks for stopping by!");
        msg.Show();

        yield return new WaitForDialogueReading();

        InterfaceSystem.isInteracting = false;

    }
}