﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MonsterSystem;
using GameDatabase;

public class NPCBossMon : NPC
{
    public Monster spawnedMon;

    Player p;
    BoxCollider2D box;
    SpriteRenderer spr;
    SpriteAnimation sprAnim;

    public override void Start()
    {
        p = FindObjectOfType<Player>();
        box = GetComponent<BoxCollider2D>();

        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
        sprAnim = transform.GetChild(0).GetComponent<SpriteAnimation>();

        spawnedMon = GameUtility.GenerateMonsterSpawn(EncounterType.BOSS);
        UpdateSpriteAnimation();
    }

    public override IEnumerator InteractEvent()
    {       
        InterfaceSystem.isInteracting = true;

        //GameObject[] mapChunks = GameObject.FindGameObjectsWithTag("TiledMap");


        TransitionHandler tHand = TransitionHandler.instance;
        tHand.SetTexture("transTest3");
        tHand.SetMidTransAction(() =>
        {
            BattleNewHandler bHandle = BattleNewHandler.instance;
            bHandle.SetOpponent(spawnedMon);
            bHandle.isBoss = true;
            bHandle.BeginBattle();
            tHand.Fade();
            /*foreach (GameObject map in mapChunks)
            {
                map.SetActive(false);
            }*/

        });
        tHand.Transition();

        while (!InterfaceSystem.inBattle)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForBattle();

        /*foreach (GameObject map in mapChunks)
        {
            map.SetActive(true);
        }*/

        //GameObject chest = Instantiate(Resources.Load<GameObject>("ChestMonster"), transform.position + new Vector3(GameUtility.Choose(-5, -4, -3, 3, 4, 5), GameUtility.Choose(3, 4)), Quaternion.identity);
        //chest.GetComponent<ChestMonster>().OverrideItem(Items.OVERCLOCKMODULE);

        InterfaceSystem.isInteracting = false;

        gameObject.SetActive(false);

        yield break;
    }


    public void GenerateMonster()
    {
        MapData map = MapSystem.GetMapDatabaseData(SaveSystem.currentMap, SaveSystem.currentTier);
        int monIndex = Random.Range(0, map.bossMons.Count);

        float spawnChance = Random.Range(0f, 1f);

        List<MonsterIdentifier> availMons = new List<MonsterIdentifier>();

        foreach (MonsterSpawnInfo m in map.bossMons)
        {
            if (spawnChance <= m.spawnChance)
            {
                availMons.Add(m.mon);
            }
        }

        int spawnIndex = Random.Range(0, availMons.Count);

        //Debug.Log(string.Format("Spawn Chance: {2}, Spawn Index: {0}, List Count: {1}", spawnIndex, availMons.Count, spawnChance));

        Monster newMon = new Monster(availMons[spawnIndex]);
        int level = 0;

        switch (SaveSystem.currentTier)
        {
            case MapTier.A:
                level = 90;
                break;
            case MapTier.B:
                level = 75;
                break;
            case MapTier.C:
                level = 55;
                break;
            case MapTier.D:
                level = 35;
                break;
            case MapTier.E:
                level = 15;
                break;
            default:
                break;
        }

        newMon.SetLevel(level);
        spawnedMon = newMon;

        name = string.Format("Boss: {0}, {1} L{2}", spawnedMon.GetName(), spawnedMon.GetGender(), spawnedMon.GetMonLevel());

    }

    void UpdateSpriteAnimation()
    {
        /*string monName = spawnedMon.GetInternalName();
        Sprite[] monSprite = Resources.LoadAll<Sprite>("Monster_OW/ow_" + monName + "_" + spawnedMon.monColor);

        if (monSprite.Length == 0)
        {
            monSprite = Resources.LoadAll<Sprite>("Monster_OW/debug");
        }

        sprAnim.animations.Add(new SpriteAnimation.Anim("Down", new Sprite[] { monSprite[0], monSprite[1], monSprite[2], monSprite[3] }, true));
        sprAnim.animations.Add(new SpriteAnimation.Anim("Right", new Sprite[] { monSprite[4], monSprite[5], monSprite[6], monSprite[7] }, true));
        sprAnim.animations.Add(new SpriteAnimation.Anim("Up", new Sprite[] { monSprite[8], monSprite[9], monSprite[10], monSprite[11] }, true));
        sprAnim.animations.Add(new SpriteAnimation.Anim("Left", new Sprite[] { monSprite[12], monSprite[13], monSprite[14], monSprite[15] }, true));

        spr.sprite = monSprite[0];*/
    }
}
