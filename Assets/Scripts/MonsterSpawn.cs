﻿using MonsterSystem;
using GameDatabase;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Prime31;

public class MonsterSpawn : MonoBehaviour
{
    SpriteRenderer childSprite;
    SpriteRenderer shadowSprite;

    SpriteAnimation sprAnim;
    PlayerAnimState animState;
    PlayerAnimState prevState;

    public Monster spawnedMon;
    public SpawnRegion spawn;
    public Camera cam;

    float moveTime = 4, timer, despawnTimer;
    bool canMove;
    Vector2 dir;
    int moveSpeed = 4;

    bool inBattle;

    float pauseTime;

    float moveRandom() => Random.Range(0.2f, 1f);
    float pauseRandom() => Random.Range(8f, 14f);

    public CharacterController2D charCont;

    public Player p;

    // Start is called before the first frame update
    public void Start()
    {
        cam = Camera.main;

        p = FindObjectOfType<Player>();

        childSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        shadowSprite = childSprite.transform.GetChild(0).GetComponent<SpriteRenderer>();

        sprAnim = childSprite.GetComponent<SpriteAnimation>();

        charCont = GetComponent<CharacterController2D>();
        moveTime = moveRandom();

        dir = CalcDirection();
        spawnedMon = GameUtility.GenerateMonsterSpawn(spawn.encounterType);

        if (spawnedMon == null)
        {
            spawn.currentSpawnCount--;
            Destroy(gameObject);
        } else
        {
            name = string.Format("{0}, {1} L{2}", spawnedMon.GetName(), spawnedMon.GetGender(), spawnedMon.GetMonLevel());
            UpdateSpriteAnimation();
        }
    }

    public void FixedUpdate()
    {
        if (canMove && !InterfaceSystem.IsNotFree()) charCont.move(dir.normalized * moveSpeed * Time.deltaTime);
    }

    // Update is called once per frame
    void Update()
    {
        if (InterfaceSystem.IsNotFree()) return;

        if (Vector2.Distance(transform.position, p.transform.position) > 16)
        {
            childSprite.color = new Color(1, 1, 1, 0f);
        } else
        {
            childSprite.color = Color.white;
        }

        /*
        if (!GameUtility.InView(gameObject, cam))
        {
            despawnTimer += Time.deltaTime * 2;

            if (despawnTimer >= 8)
            {
                spawn.currentSpawnCount--;
                Destroy(gameObject);
            }
        } else
        {
            despawnTimer = 0;
        }*/

        timer += Time.deltaTime;

        if (!canMove)
        {
            if (timer >= pauseTime)
            {
                canMove = true;
                dir = CalcDirection();
                //UpdateAnimation();
                moveTime = moveRandom();
                timer = 0;
            }
        } else
        {
            if (timer >= moveTime)
            {
                canMove = false;
                dir = CalcDirection();
                pauseTime = pauseRandom();
                timer = 0;
            }
        }
    }

    public Vector2 CalcDirection()
    {
        Vector2 direc;

        if (!canMove) return Vector2.zero;

        if (charCont.collisionState.above)
        {
            direc.y = Random.Range(-1f, 0f);
        } else if (charCont.collisionState.below)
        {
            direc.y = Random.Range(0f, 1f);
        } else
        {
            direc.y = Random.Range(-1f, 1f);
        }

        if (charCont.collisionState.left)
        {
            direc.x = Random.Range(0f, 1f);
        } else if (charCont.collisionState.right)
        {
            direc.x = Random.Range(-1f, 0f);
        } else
        {
            direc.x = Random.Range(-1f, 1f);
        }

        return direc;
    }

    void UpdateAnimation()
    {
        /*if (canMove)
        {
            owDir = GameUtility.CalculateDirection(dir);
            if (!InterfaceSystem.isPaused && !InterfaceSystem.isInteracting)
            {
                sprAnim.Play(GameUtility.FirstCharToUpper(owDir.ToString().ToLower()));
            }
            prevDir = owDir;
        }
        else
        {
            if (sprAnim.playing)
            {
                sprAnim.Stop(true);
            }
        }*/
    }


    void UpdateSpriteAnimation()
    {
        string monName = spawnedMon.GetInternalName();
        Sprite[] monSprite = Resources.LoadAll<Sprite>("Monster_OW/ow_" + monName + "_" + spawnedMon.monColor);

        if (monSprite.Length == 0)
        {
            monSprite = Resources.LoadAll<Sprite>("Monster_OW/debug");
        }

        sprAnim.animations.Add(new SpriteAnimation.Anim("Down", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( monSprite[0]),
            new SpriteAnimation.AnimFrame( monSprite[1]),
            new SpriteAnimation.AnimFrame( monSprite[2]),
            new SpriteAnimation.AnimFrame( monSprite[3]),
        }, true));
        sprAnim.animations.Add(new SpriteAnimation.Anim("Right", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( monSprite[4]),
            new SpriteAnimation.AnimFrame( monSprite[5]),
            new SpriteAnimation.AnimFrame( monSprite[6]),
            new SpriteAnimation.AnimFrame( monSprite[7]),
        }, true));
        sprAnim.animations.Add(new SpriteAnimation.Anim("Up", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( monSprite[8]),
            new SpriteAnimation.AnimFrame( monSprite[9]),
            new SpriteAnimation.AnimFrame( monSprite[10]),
            new SpriteAnimation.AnimFrame( monSprite[11]),
        }, true));
        sprAnim.animations.Add(new SpriteAnimation.Anim("Left", new SpriteAnimation.AnimFrame[]{
            new SpriteAnimation.AnimFrame( monSprite[12]),
            new SpriteAnimation.AnimFrame( monSprite[13]),
            new SpriteAnimation.AnimFrame( monSprite[14]),
            new SpriteAnimation.AnimFrame( monSprite[15]),
        }, true));


        childSprite.color = new Color(1, 1, 1, 0);
        childSprite.sprite = monSprite[0];
        shadowSprite.enabled = true;

        LeanTween.alpha(childSprite.gameObject, 1, 0.2f);
        LeanTween.alpha(shadowSprite.gameObject, 0.3019608f, 0.2f);
    }

    public IEnumerator BattleEnum()
    {
        Debug.Log("BATTLE");

        if (inBattle || SaveSystem.playerParty.Count == 0 || GameUtility.GetAliveMonsters() == 0) yield break;

        inBattle = true;

        GameObject map = GameObject.FindGameObjectWithTag("TiledMap");

        TransitionHandler tHand = TransitionHandler.instance;
        tHand.SetTexture("transTest3");
        tHand.SetMidTransAction(() =>
        {
            BattleNewHandler bHandle = BattleNewHandler.instance;
            bHandle.SetOpponent(spawnedMon);
            bHandle.BeginBattle();
            tHand.Fade();

            //if (map != null) map.SetActive(false);
        });
        tHand.Transition();

        while (!InterfaceSystem.inBattle)
        {
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForBattle();

        //if (map != null) map.SetActive(true);

        /*yield return new WaitForFade();

        float tameChance = Random.Range(0f, 1f);

        if (tameChance <= 0.7f && InterfaceSystem.wonLastBattle)
        {
            DialogueHandler msg = DialogueHandler.Instance();
            ChoiceHandler ch = ChoiceHandler.Instance();

            msg.AddMessage(string.Format("{0} is impressed by your skills!", spawnedMon.GetName()));
            msg.AddMessage("Would you like to tame them?");
            msg.KeepOpen();
            msg.Show();

            yield return new WaitForDialogue();

            ch.AddChoice(0, "Yes");
            ch.AddChoice(1, "No");
            ch.SetPos(new Vector2(350, 96));
            ch.Show();

            yield return new WaitForChoice();

            int choiceIndex = InterfaceSystem.choiceIndex;

            if (choiceIndex == 0)
            {
                msg.AddMessage(string.Format("{0} tamed the {1}!", SaveSystem.playerName, spawnedMon.GetName()));
                msg.Show();

                GameUtility.ObtainMonster(spawnedMon);

            } else if (choiceIndex == 1)
            {
                msg.AddMessage(string.Format("{0} fled back into the wild...", spawnedMon.GetName()));
                msg.Show();
            }

            yield return new WaitForDialogue();
        }

        InterfaceSystem.wonLastBattle = false;

        LeanTween.alpha(childSprite.gameObject, 0, 0.2f);
        LeanTween.alpha(shadowSprite.gameObject, 0, 0.2f);

        yield return new WaitForSeconds(0.2f);*/

        spawn.fromBattle = true;
        spawn.currentSpawnCount--;
        Destroy(gameObject);
    }
}
