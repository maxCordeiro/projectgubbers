﻿using GameDatabase;
using MonsterSystem;
using Prime31;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestMonster : OverworldObject
{

    public Sprite[] attrChestSprs, attrChestOpenSprs;
    
    CharacterController2D charContr;
    MonsterAttribute lockType;
    SpriteRenderer spr;
    bool opened, inCoroutine, overrideItem;

    public float spawnChance;
    Items assignedItem;

    // Start is called before the first frame update
    public override void Start()
    {
        if (Random.Range(0f, 1f) > spawnChance && !overrideItem)
        {
            Destroy(gameObject);
        }

        spr = transform.GetChild(0).GetComponent<SpriteRenderer>();
        charContr = GetComponent<CharacterController2D>();
    }

    public void OverrideItem(Items item)
    {
        assignedItem = item;
        overrideItem = true;
    }

    public IEnumerator DropItem()
    {
        if (inCoroutine || opened) yield break;

        InterfaceSystem.isInteracting = true;

        inCoroutine = true;
        DialogueHandler msg = DialogueHandler.instance;
        ChoiceHandler choice = ChoiceHandler.instance;
        bool canOpen = false;
        choice.SetPos(SystemVariables.choiceboxDialoguePos);

        if (lockType == MonsterAttribute.NONE)
        {
            canOpen = true;
        } else
        {
            int hasMon = GameUtility.HasMonsterWithAttr(lockType);

            msg.AddMessage(string.Format("The Chomp Chest is sealed with {0} energy.", GameUtility.FirstCharToUpper(lockType.ToString().ToLower())));
                    
            if (hasMon != -1)
            {
                msg.AddMessage(string.Format("Would you like to use {0} to attack it?", SaveSystem.playerParty[hasMon].GetName()));
                msg.KeepOpen();
                msg.Show();
                choice.AddChoice(0, "Yes");
                choice.AddChoice(1, "No");
                choice.CloseMessage();
                yield return new WaitForDialogueReading();
            } else
            {
                msg.Show();
                yield return new WaitForDialogueClose();
            }
        }

        if (choice.choices.Count > 0)
        {
            choice.Show();

            yield return new WaitForChoiceDialogueClose();

            int choiceInd = InterfaceSystem.choiceIndex;

            if (choiceInd == 0)
            {
                canOpen = true;
            }
        }

        if (canOpen)
        {
            yield return new WaitForSeconds(0.25f);
            LeanTween.moveX(gameObject, transform.position.x, 0.6f).setFrom(transform.position.x - 0.5f).setEaseOutElastic();

            yield return new WaitForSeconds(0.8f);
            spr.sprite = attrChestOpenSprs[(int)lockType];
            yield return new WaitForSeconds(0.6f);
            
            ItemSystem.ObtainItem(assignedItem);

            yield return new WaitForMenu();

            opened = true;
        }

        InterfaceSystem.isInteracting = false;
        inCoroutine = false;

        yield break;
    }

    public void Generate()
    {
        if (SaveSystem.currentMap == Maps.NONE) return;

        if (overrideItem)
        {
            lockType = MonsterAttribute.NONE;
            spr.sprite = attrChestSprs[(int)lockType];
            return;
        }

        MapData map = MapSystem.GetMapDatabaseData(SaveSystem.currentMap, SaveSystem.currentTier);
        if (map.availableItems.Count <= 0) return;


        lockType = Random.Range(0f, 1f) <= 0.15f ? (MonsterAttribute)Random.Range(0,10) : MonsterAttribute.NONE;

        spr.sprite = attrChestSprs[(int)lockType];

        float itemSpawnChance = Random.Range(0f, lockType == MonsterAttribute.NONE? 1f : 0.5f);

        List<ItemSpawnInfo> availItems = new List<ItemSpawnInfo>();
        availItems.AddRange(map.availableItems.FindAll(x => x.tier == SaveSystem.currentTier || x.tier == MapTier.ALL));

        int spawnIndex = -1;
        float cumChance = 0f;

        for (int i = 0; i < availItems.Count; i++)
        {
            float nChance = availItems[i].spawnChance;
            availItems[i].spawnChance += cumChance;
            cumChance += nChance;
        }

        float spawnChance = Random.Range(0f, cumChance);

        for (int i = 0; i < availItems.Count; i++)
        {
            if (spawnChance <= availItems[i].spawnChance)
            {
                spawnIndex = i;
                break;
            }
        }

        //Debug.Log(string.Format("Spawn Chance: {2}, Spawn Index: {0}, List Count: {1}", spawnIndex, availItems.Count, itemSpawnChance));

        assignedItem = availItems[spawnIndex].item;

    }

}
