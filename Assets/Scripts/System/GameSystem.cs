﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Xml;
using System.Collections;
using GameDatabase;
using Random = UnityEngine.Random;
using UnityEditor;
using System.Text;
using UnityEngine.UI;
using System.Xml.Serialization;
using System.IO;
using Translation;

namespace MonsterSystem
{
    [Serializable]
    public enum MonsterFamily
    {
        AQUATIC,
        AERIAL,
        FAIRY,
        MYSTERIOUS,
        UNDERGROUND,
        NATURE,
        BEAST,
        MACHINE,
        DARK,
        FIELD,
        ORIGIN,
        NONE
    }
    [Serializable]
    public enum MonsterAttribute
    {
        WATER,
        PLANT,
        FIRE,
        COSMIC,
        ELECTRIC,
        WIND,
        SHADOW,
        TECH,
        EARTH,
        NORMAL,
        NONE
    }

    [Serializable]
    public enum OverworldDir
    {
        DOWN,
        RIGHT,
        UP,
        LEFT
    }

    [Serializable]
    public enum Rarity
    {
        NONE = 0,
        COMMON = 1,
        UNCOMMON = 2,
        RARE = 3,
        ULTRARARE = 4,
        SPECIAL = 5
    }
    
    [Serializable]
    public class TypeData
    {
        public MonsterAttribute selfType;
        public MonsterAttribute[] weak;
        public MonsterAttribute[] strong;
        public MonsterAttribute[] resist;
        public MonsterAttribute[] immune;

        public TypeData(MonsterAttribute _selfType)
        {
            selfType = _selfType;
        }

        public TypeData Weak(params MonsterAttribute[] _weak)
        {
            weak = _weak;
            return this;
        }

        public TypeData Strong(params MonsterAttribute[] _strong)
        {
            strong = _strong;
            return this;
        }

        public TypeData Resist(params MonsterAttribute[] _resist)
        {
            resist = _resist;
            return this;
        }

        public TypeData Immune(params MonsterAttribute[] _immune)
        {
            immune = _immune;
            return this;
        }
    }

    [Serializable]
    public struct Stats
    {
        public int[] statArray;
        public int[] statPoints;

        public int currentHP;
        
        public int affection;
        public int accuracy;

        public Stats(BaseStats _base)
        {
            statArray = new int[6] { _base.baseArray[0], _base.baseArray[1], _base.baseArray[2], _base.baseArray[3], _base.baseArray[4], _base.baseArray[5]};
            statPoints = new int[6];

            affection = 0;
            accuracy = 100;

            currentHP = statArray[5];
        }
    }
    
    [Serializable]
    public struct BaseStats
    {
        public int[] baseArray;

        public BaseStats(int atk, int def, int matk, int mdef, int spe, int mhp)
        {
            baseArray = new int[6] { atk, def, matk, mdef, spe, mhp };
        }
    }

    public enum PlayerAnimState
    {
        NONE,
        walkDown,
        walkRight,
        walkUp,
        walkLeft,
    };


    public enum StatusEffect
    {
        NONE,
        BURN,
        TRAP,
        SEAL,
        PARALYZE,
        CURSE,
        DROWSY,
        DEAD
    }
    
    public enum ItemTypes
    {
        HEALING,
        STATBOOST,
        APPAREL,
        ATTACK,
        OTHER
    }
    
    public enum MenuPockets
    {
        GENERAL,
        HEALING,
        BATTLE,
        APPAREL,
        CAPSULES,
        CHARMS
    }
    public enum Months
    {
        JANUARY,
        FEBRUARY,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULY,
        AUGUST,
        SEPTEMBER,
        OCTOBER,
        NOVEMBER,
        DECEMBER
    }
    public enum Days
    {
        SUNDAY,
        MONDAY,
        TUESDAY,
        WEDNESDAY,
        THURSDAY,
        FRIDAY,
        SATURDAY
    }
    public enum DayNight
    {
        MORNING,
        DAY,
        EVENING,
        NIGHT
    }

    public enum ClothingType
    {
        HAIR,
        TOP,
        BOTTOM
    }

    [Serializable]
    public enum ExpType
    {
        LIGHT,
        STANDARD,
        TOUGH
    }

    [Serializable]
    public class Date
    {
        public int dayInWeek, dayInMonth, month, year, hour, minute;
        public float time;

        public Date(int _dayInWeek = -1, int _dayInMonth = -1, int _month = -1, int _year = -1, int _hour = -1, int _minute = -1, bool addToCurrent = true)
        {
            dayInWeek = _dayInWeek == -1? SaveSystem.currentDayIndex : _dayInWeek + (addToCurrent? SaveSystem.currentDayIndex : 0);
            dayInMonth = _dayInMonth == -1? SaveSystem.currentDayInMonth : _dayInMonth + (addToCurrent ? SaveSystem.currentDayInMonth : 0);
            month = _month == -1? SaveSystem.currentMonth : _month + (addToCurrent ? SaveSystem.currentMonth : 0);
            year = _year == -1? SaveSystem.currentYear : _year + (addToCurrent ? SaveSystem.currentYear : 0);

            hour = _hour == -1? SaveSystem.currentHour : _hour + (addToCurrent ? SaveSystem.currentHour : 0);
            minute = _minute == -1? SaveSystem.currentMinute : _minute + (addToCurrent ? SaveSystem.currentMinute : 0);

            time = SaveSystem.timeOfDay;

            Sort();
        }

        Date()
        {

        }

        public Date AddHours(int _hours)
        {
            hour += _hours;
            return Sort();
        }

        public Date Sort()
        {
            while (minute > 60)
            {
                minute -= 60;
                hour++;
            }

            while (hour > 24)
            {
                hour -= 24;
                dayInMonth++;
            }

            while (dayInMonth > SystemVariables.monthLengths[month])
            {
                dayInMonth -= SystemVariables.monthLengths[month];
                month++;

                while (month > 11)
                {
                    month -= 12;
                    year++;
                }
            }

            return this;
        }

        public string DebugLog()
        {
            return string.Format("{0}:{1}, {2} {3} Y{4}", hour.ToString("00"), (minute * 10).ToString("00"), dayInMonth, (Months)month, year);
        }
        
        public override string ToString()
        {
            return string.Format("{0} {1}, Y{2}", dayInMonth, ((Months)month).ToString().Substring(0, 3), year);
        }
    }

    [Serializable]
    public class DayCarePair
    {
        public Monster male, female, egg;

        public Item breedingItem;

        public int eggsMade;

        public bool hasEgg;

        public Date startTime;
        
        public DayCarePair()
        {
            startTime = new Date();
        }
    }

    [Serializable]
    public class Incubator
    {
        public Monster egg, mom, dad;

        public Date hatchDate, createDate;
        public Item hatchingItem, bredItem;

        public bool readyToHatch;

        public Incubator(Monster _egg, Monster _mom, Monster _dad, Date _hatchDate, Date _createDate, Item _hatchingItem, Item _bredItem)
        {
            egg = _egg;
            mom = _mom;
            dad = _dad;

            hatchDate = _hatchDate;
            createDate = _createDate;

            hatchingItem = _hatchingItem;
            bredItem = _bredItem;
        }
    }

    public class FamilyBreedingData
    {
        public MonsterFamily[] plus;
        public MonsterFamily[] low;

        public FamilyBreedingData(MonsterFamily[] _plus, MonsterFamily[] _low)
        {
            plus = _plus;
            low = _low;
        }
    }

    public static class GameUtility
    {
        public static string ToXML(object obj)
        {
            using (var stringwriter = new StringWriter())
            {
                var serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(stringwriter, obj);
                return stringwriter.ToString();
            }
        }

        public static T Choose<T>(T a, T b, params T[] p)
        {
            int random = Random.Range(0, p.Length + 2);
            if (random == 0) return a;
            if (random == 1) return b;
            return p[random - 2];
        }

        public static void EnableBackground()
        {
            GameObject gameplayCanvas = GameObject.FindGameObjectWithTag("GameplayCanvas");
            if (gameplayCanvas == null) throw new Exception("Background can't be instantiated. GameplayCanvas can't be found.");

            RectTransform BG = gameplayCanvas.transform.GetChild(2).GetComponent<RectTransform>();

            if (SaveSystem.settings_blurEnabled)
            {
                Image BGB = gameplayCanvas.transform.GetChild(3).GetComponent<Image>();

                BGB.enabled = true;
                BGB.material.SetInt("Radius", SaveSystem.settings_blurAmount);
            }

            LeanTween.alpha(BG, 0.5f, SaveSystem.settings_UIAnim? 0.1f : 0);
        }

        public static void DisableBackground()
        {
            GameObject gameplayCanvas = GameObject.FindGameObjectWithTag("GameplayCanvas");
            if (gameplayCanvas == null) return;

            RectTransform BG = gameplayCanvas.transform.GetChild(2).GetComponent<RectTransform>();

            if (SaveSystem.settings_blurEnabled)
            {
                Image BGB = gameplayCanvas.transform.GetChild(3).GetComponent<Image>();

                BGB.enabled = false;
            }

            LeanTween.alpha(BG, 0, SaveSystem.settings_UIAnim? 0.1f : 0);
        }

        public static float CompatibilityCheck(int dcIndex)
        {
            float chance = 0;

            MonsterFamily femFam = SaveSystem.dcMons[dcIndex].female.GetFamily();
            MonsterFamily maleFam = SaveSystem.dcMons[dcIndex].male.GetFamily();

            if (GameUtility.In(femFam, FamilyDatabase.db[(int)maleFam].plus))
            {
                chance += 0.55f;
            } else if (GameUtility.In(femFam, FamilyDatabase.db[(int)maleFam].low))
            {
                chance += 0.1f;
            } else if (femFam == maleFam)
            {
                chance += 0.8f;
            }

            return chance;
        }

        /*
         * 0 = Down
         * 1 = Right
         * 2 = Up
         * 3 = Left
         * 
         */
        public static OverworldDir CalculateDirection(Vector2 inp)
        {
            if (Mathf.Abs(inp.x) > Mathf.Abs(inp.y))
            {
                return Mathf.Sign(inp.x) == 1 ? OverworldDir.RIGHT : OverworldDir.LEFT;
            }
            else //if (Mathf.Abs(inp.x) < Mathf.Abs(inp.y))
            {
                return Mathf.Sign(inp.y) == 1 ? OverworldDir.UP : OverworldDir.DOWN;
            }

            //return OverworldDir.DOWN;
        }

        [RuntimeInitializeOnLoadMethod]
        public static void InitializeRewiredPlayer()
        {
            Debug.Log("running");
            if (InterfaceSystem.rePlayer != null) return;
            Rewired.Player pl = Rewired.ReInput.players.GetPlayer(0);

            Debug.Log("running");
            if (pl == null) return;

            InterfaceSystem.rePlayer = Rewired.ReInput.players.GetPlayer(0);
            Debug.Log("running");
        }

        public static bool IsCursorInScreen()
        {
            if (Input.mousePosition.x < Screen.width && Input.mousePosition.x > 0 && Input.mousePosition.y < Screen.height && Input.mousePosition.y > 0)
            {
                return true;
            }
            return false;
        }

        public static Monster GenerateMonsterSpawn(EncounterType enc)
        {
            if (SaveSystem.currentMap == Maps.NONE) return null;

            MapData map = MapSystem.GetMapDatabaseData(SaveSystem.currentMap, SaveSystem.currentTier);

            if (map.availableMons.Count <= 0) return null;

            List<MonsterSpawnInfo> monRange = new List<MonsterSpawnInfo>();

            //Debug.Log(enc.ToString());

            if (enc == EncounterType.BOSS)
            {
                monRange.AddRange(map.bossMons.FindAll(x => (x.tier == SaveSystem.currentTier || x.tier == MapTier.ALL) && x.mon.id != Monsters.NONE));
            } else
            {
                monRange.AddRange(map.availableMons.FindAll(x => (x.tier == SaveSystem.currentTier || x.tier == MapTier.ALL) && x.type == enc && x.mon.id != Monsters.NONE));
            }
            

            int spawnIndex = -1;
            float cumChance = 0f;

            for (int i = 0; i < monRange.Count; i++)
            {
                float nChance = monRange[i].spawnChance;
                monRange[i].spawnChance += cumChance;                
                cumChance += nChance;
            }

            float spawnChance = Random.Range(0f, cumChance);

            for (int i = 0; i < monRange.Count; i++)
            {
                if (spawnChance <= monRange[i].spawnChance)
                {
                    spawnIndex = i;
                    break;
                }
            }

            //Debug.Log("Spawn Chance: {0}, Spawn Index: {1}".With(spawnChance, spawnIndex));

            Monster newMon = new Monster(monRange[spawnIndex].mon);
            int level = 0;

            

            if (enc == EncounterType.BOSS)
            {
                switch (SaveSystem.currentTier)
                {
                    case MapTier.A:
                        level = 90;
                        break;
                    case MapTier.B:
                        level = 75;
                        break;
                    case MapTier.C:
                        level = 55;
                        break;
                    case MapTier.D:
                        level = 30;
                        break;
                    case MapTier.E:
                        level = 15;
                        break;
                    default:
                        break;
                }
            } else
            {
                switch (SaveSystem.currentTier)
                {
                    case MapTier.A:
                        level = AverageLevels(76, 95);
                        break;
                    case MapTier.B:
                        level = AverageLevels(56, 80);
                        break;
                    case MapTier.C:
                        level = AverageLevels(36, 60);
                        break;
                    case MapTier.D:
                        level = AverageLevels(16, 40);
                        break;
                    case MapTier.E:
                        level = AverageLevels(3, 19);
                        break;
                    default:
                        break;
                }
            }

            newMon.SetLevel(level);

            Debug.Log("SPAWNING: " + newMon.GetName() + " [" + newMon.GetGender() + " Lv" + newMon.GetMonLevel() + "]");
            return newMon;
        }

        public static int AverageLevels(int _min, int _max)
        {
            int level = -1;

            int avg = 0;

            for (int i = 0; i < SaveSystem.playerParty.Count; i++)
            {
                avg += SaveSystem.playerParty[i].level;
            }

            int count = Mathf.Clamp(SaveSystem.playerParty.Count, 1, 4);

            level = Mathf.Clamp(avg / count, _min, _max);

            float r = Random.Range(0f, 1f);

            if (r <= 0.6f)
            {
                level -= Random.Range(2, 6);
            } else if (r > 0.6f && r <= 0.8f)
            {
                level -= Random.Range(0, 4);
            } else if (r > 0.8f && r <= 0.9f)
            {
                level += Random.Range(0, 2);
            } else if (r > 0.9f && r <= 1f)
            {
                level += Random.Range(1, 3);
            }

            level = Mathf.Clamp(level, _min, _max);

            return level;
        }

        public static int GetMonsterDatabaseIndex(MonsterIdentifier _mon)
        {
            for (int i = 0; i < MonsterDatabase.db.Count; i++)
            {
                if (MonsterDatabase.db[i].id == _mon.id && MonsterDatabase.db[i].form == _mon.formId)
                {
                    return i;
                }
            }

            return -1;
        }

        public static int GetAliveMonsters()
        {
            int m = SaveSystem.playerParty.Count;

            for (int i = 0; i < m; i++)
            {
                if (SaveSystem.playerParty[i].GetStatusEffect() == StatusEffect.DEAD)
                {
                    m -= 1;
                }
            }

            return m;
        }

        public static MonsterData GetMonsterDatabaseData(MonsterIdentifier _mon)
        {
            return MonsterDatabase.db[GetMonsterDatabaseIndex(_mon)];
        }

        public static Sprite GetSpeciesIcon(MonsterIdentifier _mon)
        {
            Sprite[] icon = Resources.LoadAll<Sprite>("MonsterIcons/" + _mon.id.ToString().ToLower() + "_" + _mon.formId);
            
            return icon.Length == 0? null : icon[0];
        }

        public static void ObtainMonster(MonsterIdentifier _mon, bool toBag = false)
        {
            ObtainMonster(new Monster(_mon), toBag);
        }

        public static void ObtainMonster(Monster _mon, bool toBag = false)
        {
            _mon.metLevel = _mon.GetMonLevel();
            _mon.dateObtained = new Date();
            _mon.mapObtainedID = SaveSystem.currentMap;

            EncyclopediaSystem.TameMonster(_mon.mon);

            if (SaveSystem.playerParty.Count == SystemVariables.maxParty || toBag)
            {
                SaveSystem.playerRanch[Array.IndexOf(SaveSystem.playerRanch, null)] = _mon;
            }
            else
            {
                SaveSystem.playerParty.Add(_mon);
            }
        }

        public static void RemoveMonsterFromParty(int index)
        {
            if (SaveSystem.playerParty.Count <= 1) return;

            SaveSystem.playerParty.RemoveAt(index);
        }


        public static void RemoveMonster(bool partySlot, int index)
        {
            if (partySlot && SaveSystem.playerParty.Count <= 1) return;

            if (partySlot)
            {
                SaveSystem.playerParty.RemoveAt(index);
            } else
            {
                SaveSystem.playerRanch[index] = null;
            }
        }

        public static string FirstCharToUpper(string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                throw new ArgumentException("ARGH!");

            }
            return input.First().ToString().ToUpper() + input.Substring(1);
        }

        public static IEnumerator WaitForInput(string inputName)
        {
            yield return null;
        }

        public static void BringToFront(GameObject obj)
        {
            obj.transform.SetAsLastSibling();
        }
        
        public static string ToFeetInches(float measurement)
        {
            string height = measurement.ToString();

            string ft = height.Substring(0, height.IndexOf("."));
            string inch = height.Substring(height.IndexOf(".") + 1).TrimStart(new char[] { '0' });

            return ft + "'" + inch + "\"";
        }

        public static string GetGold(int amt = -1) // -1 = Player
        {
            return "<m=default><q=gold></m>" + (amt == -1? SaveSystem.playerGold : amt).ToString("##,#");
        }

        public static bool In<T>(this T obj, params T[] args)
        {
            return args.Contains(obj);
        }

        public static GameObject FindCamera()
        {
            return GameObject.FindGameObjectWithTag("MainCamera");
        }

        public static bool InView(GameObject obj, Camera cam)
        {
            Transform objTrans = obj.transform;

            return InView(objTrans, cam);
        }

        public static bool InView(Transform pos, Camera cam)
        {
            return InView(pos.position, cam);
        }

        public static bool InView(Vector3 pos, Camera cam)
        {
            float camVert = cam.orthographicSize;
            float camHorz = camVert * Screen.width / Screen.height;

            return pos.x < cam.transform.position.x + camHorz && pos.x > cam.transform.position.x + -camHorz
                && pos.y < cam.transform.position.y + camVert && pos.y > cam.transform.position.y + -camVert;
        }


        public static int HoursUntil(Date _date)
        {
            Date curr = new Date();

            int hoursLeft = 0;

            if (_date.year > curr.year)
            {
                hoursLeft += 8760 * (_date.year - curr.year);
            }
            
            if (_date.month > curr.month)
            {
                for (int i = curr.month; i < _date.month; i++)
                {
                    hoursLeft += 24 * SystemVariables.monthLengths[i];
                }
            }

            if (_date.dayInMonth > curr.dayInMonth)
            {
                hoursLeft += 24 * (_date.dayInMonth - curr.dayInMonth);
            }
            
            if (_date.hour > curr.hour)
            {
                hoursLeft += _date.hour - curr.hour;
            }

            return hoursLeft;
        }

        public static string GetRandomString(System.Random rnd, int length)
        {
            string charPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvw xyz1234567890";
            StringBuilder rs = new StringBuilder();

            while (length > 0)
            {
                rs.Append(charPool[(int)(rnd.NextDouble() * charPool.Length)]);
                length--;
            }
            return rs.ToString();
        }

        public static bool FreeIncubatorsAvailable()
        {
            return SaveSystem.dcIncubators.Count < SystemVariables.defaultIncubators + SaveSystem.dcIncubatorUnlocks;
        }

        public static int HasMonsterWithAttr(MonsterAttribute _type)
        {
            for (int i = 0; i < SaveSystem.playerParty.Count; i++)
            {
                if (SaveSystem.playerParty[i].GetType() == _type || SaveSystem.playerParty[i].GetType(1) == _type)
                {
                    return i;
                }
            }

            return -1;
        }

        public static bool HasMonster(MonsterIdentifier _mon)
        {
            for (int i = 0; i < SaveSystem.playerParty.Count; i++)
            {
                if (SaveSystem.playerParty[i].mon == _mon)
                {
                    return true;
                }
            }

            return false;
        }

        public static int GetFirstAliveMon()
        {
            for (int i = 0; i < SaveSystem.playerParty.Count; i++)
            {
                if (SaveSystem.playerParty[i].GetStatusEffect() != StatusEffect.DEAD)
                {
                    return i;
                }
            }

            return -1;
        }

        public static string With(this string s, params object[] obj)
        {
            return string.Format(s, obj);
        }

       public static float Choose(params float[] probs)
        {
            float total = 0;

            foreach (float elem in probs)
            {
                total += elem;
            }

            float randomPoint = Random.value * total;

            for (int i = 0; i < probs.Length; i++)
            {
                if (randomPoint < probs[i])
                {
                    return i;
                }
                else
                {
                    randomPoint -= probs[i];
                }
            }

            return probs.Length - 1;
        }

        public static void ChangeSpriteAlpha(SpriteRenderer spr, float val)
        {
            Color col = spr.color;
            col.a = val;
            spr.color = col;
        }

        public static float Approach(float arg0, float arg1, float arg2)
        {
            if (arg0 < arg1)
            {
                return Mathf.Min(arg0 + arg2, arg1);
            }
            else
            {
                return Mathf.Max(arg0 - arg2, arg1);
            }
        }
    }

    public enum Language
    {
        EN_US = 0,
        PT_PT = 1
    }

    public static class LocalUtils
    {
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
        public static void Initialize()
        {
            LocalGameDatabase db = new LocalGameDatabase();

            TextAsset dbxml = Resources.Load<TextAsset>("db");
            db = LocalGameDatabase.LoadFromText(dbxml.text);//(Path.Combine(Application.streamingAssetsPath, "db.xml"));
            
            //db.Save(Path.Combine(Application.persistentDataPath, "db.xml"));

            MonsterDatabase.db = db.monDB;
            SkillDatabase.db = db.skillDB;
            ItemDatabase.db = db.itemDB;
            PerkDatabase.db = db.perkDB;
            MapDatabase.db = db.mapDB;
            MovesetDatabase.db = db.movesetsDB;

            EncyclopediaSystem.UnlockMap(Maps.OVERGROWNGRASSLANDS);
            
            //InitMonDatabase();

            /*InitSkills();
            InitMons();
            InitMenuText();
            InitItems();
            InitPerks();
            InitMaps();*/
            
            InitLocale();
        }

        public static void InitLocale()
        {
            try
            {
                SystemVariables.trans  = new Translator(ELangFormat.Csv, "Localization/languages", ",");
                SystemVariables.transSelection = SystemVariables.trans.GetAvailableLanguages();

                SystemVariables.trans.LoadDictionary (SystemVariables.transSelection[0]);
            }
            catch(Exception ex)
            {
                Debug.Log(ex.Message);
                SystemVariables.trans = null;
            }
        }

        public static void InitSkills()
        {
            Dictionary<string, string> skillNames = new Dictionary<string, string>();

            for (int i = 0; i < SkillDatabase.db.Count; i++)
            {
                string languageText = DialogueSystem.LoadText(((Skills)SkillDatabase.db[i].id).ToString(), 1, "skills");
                if (languageText != "") skillNames.Add(((Skills)SkillDatabase.db[i].id).ToString(), languageText);
            }

            SystemVariables.skillNames = skillNames;
        }

        public static void InitMons()
        {
            Dictionary<string, string> monNames = new Dictionary<string, string>();

            for (int i = 0; i < MonsterDatabase.db.Count; i++)
            {
                string languageText = DialogueSystem.LoadText(((Monsters)MonsterDatabase.db[i].id).ToString(), 1, "monsters");
                if (languageText != "") monNames.Add(((Monsters)MonsterDatabase.db[i].id).ToString(), languageText);
            }

            SystemVariables.monNames = monNames;

        }

        public static void InitMenuText()
        {
            Dictionary<string, string> menuText = new Dictionary<string, string>();
            string[] keys = new string[] { "pauseOptions", "stats", "pocketNames" };

            for (int i = 0; i < keys.Length; i++)
            {
                string tempText = DialogueSystem.LoadText(keys[i], 1, "system");
                if (tempText != "") menuText.Add(keys[i], tempText);
            }
            
            SystemVariables.menuStrings = menuText;

        }

        public static void InitItems()
        {
            Dictionary<string, DictTwoPair> items = new Dictionary<string, DictTwoPair>();

            for (int i = 0; i < ItemDatabase.db.Count; i++)
            {
                if (ItemDatabase.db[i].pocket == MenuPockets.CAPSULES)
                {
                    continue;
                }

                string iName = DialogueSystem.LoadTest(ItemDatabase.db[i].id.ToString(), "name", "items");
                string iDesc = DialogueSystem.LoadTest(ItemDatabase.db[i].id.ToString(), "desc", "items");

                DictTwoPair it = new DictTwoPair(iName, iDesc);
                
                items.Add(ItemDatabase.db[i].id.ToString(), it);
            }

            SystemVariables.itemText = items;
        }

        public static void InitPerks()
        {
            Dictionary<string, DictTwoPair> perks = new Dictionary<string, DictTwoPair>();

            for (int i = 0; i < PerkDatabase.db.Count; i++)
            {
                string pName = DialogueSystem.LoadText(PerkDatabase.db[i].id.ToString(), 1, "perks");
                string pDesc = DialogueSystem.LoadText((PerkDatabase.db[i].id).ToString(), 2, "perks");


                DictTwoPair dp = new DictTwoPair(pName, pDesc);

                perks.Add(PerkDatabase.db[i].id.ToString(), dp);
            }

            SystemVariables.perkText = perks;
        }

        public static void InitMaps()
        {
            Dictionary<string, string> mapNames = new Dictionary<string, string>();
            Dictionary<string, string> mapDesc = new Dictionary<string, string>();

            for (int i = 0; i < MapDatabase.db.Count; i++)
            {
                string langtext = DialogueSystem.LoadText(MapDatabase.db[i].id.ToString(), 1, "maps");
                if (langtext != "" && !mapNames.ContainsKey(MapDatabase.db[i].id.ToString())) mapNames.Add(MapDatabase.db[i].id.ToString(), langtext);

                langtext = "";

                langtext = DialogueSystem.LoadText(MapDatabase.db[i].id.ToString(), 2, "maps");
                if (langtext != "" && !mapDesc.ContainsKey(MapDatabase.db[i].id.ToString())) mapDesc.Add(MapDatabase.db[i].id.ToString(), langtext);
            }

            SystemVariables.mapNames = mapNames;
            SystemVariables.mapDesc = mapDesc;
        }

        public static void UpdateMonStats()
        {
            foreach (Monster mon in SaveSystem.playerParty)
            {
                mon.CalculateAllStats(false);
            }

            foreach (Monster mon in SaveSystem.playerRanch)
            {
                mon.CalculateAllStats(false);
            }
        }
    }

    [XmlRoot("Database")]
    public class LocalGameDatabase
    {
        [XmlElement("Version")]
        public string ver = new GameVariables().gameVersion;

        [XmlArray("Monsters")]
        [XmlArrayItem("Monster")]
        public List<MonsterData> monDB;

        [XmlArray("Items")]
        [XmlArrayItem("Item")]
        public List<ItemData> itemDB;// = new List<ItemData>() { new ItemData(Items.POTION, MenuPockets.HEALING, ItemEffects_Menu.HEALING, ItemEffects_Battle.HEALING, ItemEffects_Hold.NONE, 1, 1) };

        [XmlArray("Skills")]
        [XmlArrayItem("Skill")]
        public List<SkillData> skillDB;

        [XmlArray("Perks")]
        [XmlArrayItem("Perk")]
        public List<PerkData> perkDB;

        [XmlArray("Maps")]
        [XmlArrayItem("Map")]
        public List<MapData> mapDB;

        [XmlArray("Movesets")]
        [XmlArrayItem("Moveset")]
        public List<Moveset> movesetsDB;

        [XmlArray("Tamers")]
        [XmlArrayItem("Tamer")]
        public List<TamerData> tamersDB;

        public void Save(string path)
        {
            var serializer = new XmlSerializer(typeof(LocalGameDatabase));
            using (var stream = new FileStream(path, FileMode.Create))
            {
                serializer.Serialize(stream, this);
            }
        }

        public static LocalGameDatabase Load(string path)
        {
            var serializer = new XmlSerializer(typeof(LocalGameDatabase));
            using (var stream = new FileStream(path, FileMode.Open))
            {
                return serializer.Deserialize(stream) as LocalGameDatabase;
            }
        }

        //Loads the xml directly from the given string. Useful in combination with www.text.
        public static LocalGameDatabase LoadFromText(string text)
        {
            var serializer = new XmlSerializer(typeof(LocalGameDatabase));
            return serializer.Deserialize(new StringReader(text)) as LocalGameDatabase;
        }
    }
}
