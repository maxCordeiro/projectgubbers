﻿using GameDatabase;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace MonsterSystem
{
    [Serializable]
    public class TamerData
    {
        [XmlAttribute("class")]
        public TamerClass tamerClass;
        [XmlAttribute("id")]
        public Tamers id;

        [XmlElement("items")]
        public List<Items> items = new List<Items>();

        [XmlArray("Party")]
        [XmlArrayItem("monster")]
        public MonsterIdentifier[] party = new MonsterIdentifier[4];

        [XmlArray("level")]
        public int[] partyLevel = new int[4];

        [XmlArray("perks")]
        [XmlArrayItem("perk")]
        public Perks[] partyPerk = new Perks[4];

        [XmlArray("nicknames")]
        [XmlArrayItem("name")]
        public string[] partyName = new string[4];

        [XmlArray("monColors")]
        [XmlArrayItem("color")]
        public int[] partyColor = new int[4]; // -1 = random

        [XmlArray("monStatPoints")]
        public TamerStatPoints[] partyStatPoints = new TamerStatPoints[4];

        [XmlArray("upStatA")]
        [XmlArrayItem("index")]
        public int[] partyUpStatIndexA = new int[4]; // -1 = random

        [XmlArray("upStatB")]
        [XmlArrayItem("index")]
        public int[] partyUpStatIndexB = new int[4]; // -1 = random

        [XmlArray("downStat")]
        [XmlArrayItem("index")]
        public int[] partyDownStatIndex = new int[4]; // -1 = random
        
        [XmlArray("moveset")]
        public TamerMoveset[] partyMoveset = new TamerMoveset[4];
        
        [XmlArray("accessories")]
        [XmlArrayItem("item")]
        public Items[] partyAccessories = new Items[4];

        [XmlElement("tamerDifficulty")]
        public TamerDifficulty tamerDifficulty;

        TamerData()
        {

        }

        public TamerData(TamerClass _class, Tamers _id, TamerDifficulty _tamerDifficulty)
        {
            tamerClass = _class;
            id = _id;
            tamerDifficulty = _tamerDifficulty;

            party[0] = new MonsterIdentifier(Monsters.NONE, 0);
            party[1] = new MonsterIdentifier(Monsters.NONE, 0);
            party[2] = new MonsterIdentifier(Monsters.NONE, 0);
            party[3] = new MonsterIdentifier(Monsters.NONE, 0);

            partyAccessories[0] = Items.NONE;
            partyAccessories[1] = Items.NONE;
            partyAccessories[2] = Items.NONE;
            partyAccessories[3] = Items.NONE;

            partyUpStatIndexA[0] = -1;
            partyUpStatIndexA[1] = -1;
            partyUpStatIndexA[2] = -1;
            partyUpStatIndexA[3] = -1;

            partyUpStatIndexB[0] = -1;
            partyUpStatIndexB[1] = -1;
            partyUpStatIndexB[2] = -1;
            partyUpStatIndexB[3] = -1;

            partyDownStatIndex[0] = -1;
            partyDownStatIndex[1] = -1;
            partyDownStatIndex[2] = -1;
            partyDownStatIndex[3] = -1;


        }

        public TamerData SetItems(params Items[] _items)
        {
            items = _items.ToList();
            return this;
        }

        public TamerData AddMonster(int index, Monsters mon, int _form)
        {
            party[index] = new MonsterIdentifier(mon, _form);
            return this;
        }

    }

    [Serializable]
    public class TamerMoveset
    {
        public Skills[] moveset;
        public int[] movesetCooldowns;
        public Skills specialSkill;

        public TamerMoveset(Skills a, Skills b, Skills c, Skills d, Skills e, Skills _specialSkill)
        {
            moveset = new Skills[5] { a, b, c, d, e };
            movesetCooldowns = new int[5] { -1, -1, -1, -1, -1 };
            specialSkill = _specialSkill;
        }

        public TamerMoveset Cooldowns(int a, int b, int c, int d, int e)
        {
            movesetCooldowns = new int[5] { a, b, c, d, e };
            return this;
        }

        TamerMoveset()
        {

        }
    }

    [Serializable]
    public struct TamerStatPoints
    {
        public int[] statPoints;

        public TamerStatPoints(int str, int def, int mag, int res, int agi, int hp)
        {
            statPoints = new int[6] { str, def, mag, res, agi, hp };
        }
    }
    
    public class Tamer
    {
        public TamerClass tamerClass;
        public Tamers id;

        public List<Item> tamerItems = new List<Item>();
        public Monster[] party = new Monster[4];

        public Tamer(TamerClass _tamerClass, Tamers _id)
        {
            id = _id;

            TamerData tempData = TamerSystem.GetTamerData(_tamerClass, _id);

            for (int i = 0; i < tempData.party.Count(mon => mon.id != Monsters.NONE); i++)
            {
                party[i] = new Monster(tempData.party[i]);

                party[i].level = tempData.partyLevel[i];

                if (tempData.partyPerk[i] != Perks.NONE) party[i].perk = tempData.partyPerk[i];

                if (tempData.partyName[i].Length != 0) party[i].SetNickname(tempData.partyName[i]);

                if (tempData.partyColor[i] != -1) party[i].monColor = tempData.partyColor[i];

                party[i].stats.statPoints = tempData.partyStatPoints[i].statPoints;

                if (tempData.partyUpStatIndexA[i] != -1) party[i].upStatA = tempData.partyUpStatIndexA[i];
                if (tempData.partyUpStatIndexB[i] != -1) party[i].upStatB = tempData.partyUpStatIndexB[i];

                if (tempData.partyDownStatIndex[i] != -1) party[i].downStat = tempData.partyDownStatIndex[i];

                for (int s = 0; s < tempData.partyMoveset[i].moveset.Length; s++)
                {
                    if (tempData.partyMoveset[i].moveset[s] != Skills.NONE) party[i].monSkills[s] = new Skill(tempData.partyMoveset[i].moveset[s]);//.DecreaseCooldown(tempData.partyMoveset[i].movesetCooldowns[s] != -1? tempData.partyMoveset[i].movesetCooldowns[s] : 0);
                }

                if (tempData.partyMoveset[i].specialSkill != Skills.NONE) party[i].specialSkill = new Skill(tempData.partyMoveset[i].specialSkill);

                party[i].accessory = tempData.partyAccessories[i] != Items.NONE ? new Item(tempData.partyAccessories[i]) : null;

                party[i].CalculateAllStats();
            }
        }

        Tamer()
        {

        }

        public bool HasHealingItems()
        {
            return tamerItems.Any(item => item.GetData().battleFunction == ItemEffects_Battle.HEALING);
        }
    }

    public static class TamerSystem
    {
        public static int GetTamerDatabaseIndex(TamerClass _tamerClass, Tamers _id)
        {
            for (int i = 0; i < TamerDatabase.db.Count; i++)
            {
                if (TamerDatabase.db[i].id == _id && TamerDatabase.db[i].tamerClass == _tamerClass)
                {
                    return i;
                }
            }

            return -1;
        }

        public static TamerData GetTamerData(TamerClass _tamerClass, Tamers _id)
        {
            return TamerDatabase.db[GetTamerDatabaseIndex(_tamerClass, _id)];
        }
    }

    public enum TamerClass
    {
        RANCHER,
        HIKER,
        FISHER
    }

    public enum Tamers
    {
        TAMERA,
        TAMERB
    }

    public enum TamerDifficulty
    {
        EASY,
        NORMAL,
        HARD
    }
}