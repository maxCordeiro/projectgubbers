﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MonsterSystem;
using GameDatabase;

namespace MonsterSystem
{
    [Serializable]
    public class EncUnlockMonster
    {
        public MonsterIdentifier mon;
        public bool seen, tamed;
    }

    [Serializable]
    public class EncUnlockMap
    {
        public Maps map;
        public bool discovered;
    }

    public static class EncyclopediaSystem
    {
        public static bool HasSeen(MonsterIdentifier _mon)
        {
            foreach (EncUnlockMonster mon in SaveSystem.encMons)
            {
                if (mon.mon == _mon && mon.seen) return true;
            }

            return false;
        }

        public static bool HasTamed(MonsterIdentifier _mon)
        {
            foreach (EncUnlockMonster mon in SaveSystem.encMons)
            {
                if (mon.mon == _mon && mon.tamed) return true;
            }

            return false;
        }

        public static void SeeMonster(MonsterIdentifier _mon)
        {
            if (!HasSeen(_mon))
            {
                EncUnlockMonster mu = new EncUnlockMonster();
                mu.mon = _mon;
                mu.seen = true;

                SaveSystem.encMons.Add(mu);
            }
        }

        public static void TameMonster(MonsterIdentifier _mon)
        {
            SeeMonster(_mon);

            if (!HasTamed(_mon))
            {
                foreach (EncUnlockMonster unl in SaveSystem.encMons)
                {
                    if (unl.mon == _mon)
                    {
                        unl.tamed = true;
                        break;
                    }
                }
            }
        }

        public static bool HasDiscovered(Maps _map)
        {
            foreach (EncUnlockMap map in SaveSystem.encMaps)
            {
                if (map.map == _map && map.discovered) return true;
            }

            return false;
        }

        public static void UnlockMap(Maps _map)
        {
            if (!HasDiscovered(_map))
            {
                EncUnlockMap map = new EncUnlockMap();
                map.map = _map;
                map.discovered = true;

                SaveSystem.encMaps.Add(map);
            }
        }
    }
}
