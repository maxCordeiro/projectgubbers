﻿using UnityEngine;
using UnityEditor;
using MonsterSystem;
using GameDatabase;
using System.Linq;

namespace MonsterSystem
{
    public static class TypeSystem
    {
        public static bool IsStrongAgainst(MonsterAttribute attacker, MonsterAttribute receiver)
        {
            TypeData t = GetTypeDatabaseData(receiver);

            return t.weak == null ? false : t.weak.Contains(attacker);
        }

        public static bool IsResistedBy(MonsterAttribute attacker, MonsterAttribute receiver)
        {
            TypeData t = GetTypeDatabaseData(receiver);

            return t.resist == null? false : t.resist.Contains(attacker);
        }

        public static int GetTypeDatabaseIndex(MonsterAttribute _type)
        {
            for (int i = 0; i < TypeDatabase.types.Length; i++)
            {
                if (TypeDatabase.types[i].selfType == _type)
                {
                    return i;
                }
            }

            return -1;
        }

        public static TypeData GetTypeDatabaseData(MonsterAttribute _type)
        {
            return TypeDatabase.types[GetTypeDatabaseIndex(_type)];
        }
    }
}