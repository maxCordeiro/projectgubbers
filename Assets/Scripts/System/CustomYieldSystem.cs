﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using MonsterSystem;

public class WaitForDialogueReading : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get { return !DialogueHandler.instance.isFinishedReading; }
    }

    public WaitForDialogueReading()
    {
    }
}

public class WaitForDialogueClose : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get { return DialogueHandler.instance.isOpen; }
    }

    public WaitForDialogueClose()
    {
    }
}

public class WaitForQuantity : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return !InterfaceSystem.quantitySelected && InterfaceSystem.inQuantity;
        }
    }

    public WaitForQuantity()
    {
    }
}

public class WaitForChoice : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return !ChoiceHandler.instance.choiceSelected && ChoiceHandler.instance.isOpen;
        }
    }

    public WaitForChoice()
    {
    }
}

public class WaitForChoiceDialogueClose : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return !ChoiceHandler.instance.choiceSelected && ChoiceHandler.instance.isOpen && DialogueHandler.instance.isOpen;
        }
    }

    public WaitForChoiceDialogueClose()
    {
    }
}

public class WaitForMonsterSelect : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return InterfaceSystem.temporaryMonsterIndex == -1 && InterfaceSystem.inMenu;
        }
    }

    public WaitForMonsterSelect()
    {
    }
}

public class WaitForInMenu : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return !InterfaceSystem.inMenu;
        }
    }

    public WaitForInMenu()
    {

    }
}

public class WaitForItemSelect : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return InterfaceSystem.temporaryItemIndex == -1 && InterfaceSystem.inMenu;
        }
    }

    public WaitForItemSelect()
    {

    }
}

public class WaitForMenu : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return InterfaceSystem.inMenu;
        }
    }

    public WaitForMenu()
    {

    }
}

public class WaitForBattle : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return InterfaceSystem.inBattle;
        }
    }

    public WaitForBattle()
    {

    }
}

public class WaitForFade : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return InterfaceSystem.isFading;
        }
    }

    public WaitForFade()
    {

    }
}

public class WaitForBool : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return waitVal;
        }
    }

    bool waitVal;

    public WaitForBool(bool _waitVal)
    {
        waitVal = _waitVal;
    }
}

public class WaitForUIFade : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return uis.isFading;
        }
    }

    UISystem uis;

    public WaitForUIFade(UISystem _uis)
    {
        uis = _uis;
    }
}

public class WaitForUIPriority : CustomYieldInstruction
{
    public override bool keepWaiting
    {
        get
        {
            return !uis.IsPriority;
        }
    }

    UISystem uis;

    public WaitForUIPriority(UISystem _uis)
    {
        uis = _uis;
    }
}