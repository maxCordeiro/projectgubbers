﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;
using GameDatabase;
using System.Runtime.CompilerServices;

namespace MonsterSystem
{
    public static class InterfaceSystem
    {
        public static Monster temporaryMonster;

        public static int temporaryMonsterIndex = -1;
        public static bool temporaryInBag = false;
        public static int temporaryItemIndex = -1;
        public static int temporaryPocketIndex = -1;
        public static int temporarySkillIndex = -1;
        public static int temporaryQuantity = -1;
        public static string temporaryString = "";
        
        public static ItemEffects_Menu requestingMenuEffect = ItemEffects_Menu.NONE;
        public static ItemEffects_Battle requestingBattleEffect = ItemEffects_Battle.NONE;

        public static bool quantitySelected;
        public static int choiceIndex;

        public static bool inMenu;
        public static bool isInteracting;
        public static bool inQuantity;

        public static bool isFading;
        public static bool inPanel;

        public static bool inBattle;
        public static bool wonLastBattle;
        public static bool caughtLastfish;

        public static Rewired.Player rePlayer;

        public static bool isPaused;
        
        public static bool IsNotFree()
        {
            return inBattle || isInteracting || isFading || ChoiceHandler.instance.isOpen || DialogueHandler.instance.isOpen || inMenu || isPaused || inQuantity;
        }
        
        public static bool InDialogueChoices()
        {
            return ChoiceHandler.instance.isOpen || DialogueHandler.instance.isOpen;
        }

        public static void Pause()
        {
            if (isPaused)
            {
                Object.FindObjectOfType<PauseUI>().FadeOut(true);
            }
            else if (Object.FindObjectOfType<PauseUI>() == null)
            {
                GameObject canvas = GameObject.Find("GameplayCanvas");
            
                GameObject pause = GameObject.Instantiate<GameObject>(Resources.Load<GameObject>("UI/PauseUI"), canvas.transform);
                pause.transform.SetAsLastSibling();

                isPaused = true;
            }
        }

        #region Party
        public static PartyUI OpenParty(UISystem _referrer, bool _oneOff = false)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            ResetTempMonsterPartyIndex();
            ResetTempChoices();

            PartyUI pManager = GameObject.Instantiate(Resources.Load<GameObject>("UI/PartyUI"), canvas.transform).GetComponent<PartyUI>();
            pManager.transform.SetAsLastSibling();

            pManager.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            pManager.oneOff = _oneOff;

            return pManager;
        }

        #endregion

        #region Summary

        public static SummaryUI OpenSummary(UISystem _referrer, int monID, bool inBag = false)
        {
            if (isFading) return null;

            ResetTempChoices();

            GameObject canvas = GameObject.Find("GameplayCanvas");
            SummaryUI sManager = GameObject.Instantiate(Resources.Load<GameObject>("UI/SummaryUI"), canvas.transform).GetComponent<SummaryUI>();
            sManager.transform.SetAsLastSibling();
            
            ResetTempMonsterPartyIndex();

            sManager.startingMonID = monID;
            sManager.inBag = inBag;

            sManager.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return sManager;
        }
        #endregion

        #region Inventory

        public static InventoryUI OpenInventory(UISystem _referrer, bool requestingItem = false, bool selling = false)
        {
            if (isFading) return null;

            ResetTempChoices();

            GameObject canvas = GameObject.Find("GameplayCanvas");
            InventoryUI invManager = GameObject.Instantiate(Resources.Load<GameObject>("UI/InventoryUI"), canvas.transform).GetComponent<InventoryUI>();

            invManager.transform.SetAsLastSibling();
            invManager.requestingItem = requestingItem;
            if (requestingItem) ResetTempItemIndex();
            invManager.selling = selling;

            invManager.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return invManager;            
        }
        #endregion

        #region RanchPermit
        public static void OpenPermit()
        {

        }
        #endregion

        #region Settings
        public static void OpenSettings()
        {

        }
        #endregion

        #region Exit
        public static void ExitGame()
        {

        }
        #endregion

        public static NicknamePanel OpenNicknamePanel(UISystem _referrer, string placeholder = "")
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            Transform BG = canvas.transform.Find("BG");
            BG.SetAsLastSibling();

            temporaryString = "";

            NicknamePanel nickname = GameObject.Instantiate(Resources.Load<GameObject>("UI/NicknamePanel"), canvas.transform).GetComponent<NicknamePanel>();
            nickname.transform.SetAsLastSibling();

            nickname.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            nickname.inputField.placeholder.GetComponent<Text>().text = placeholder.Length == 0? "Name..." : "";

            return nickname;
        }

        public static DaycareUI OpenDaycareMonsterViewer(UISystem _referrer)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            DaycareUI daycare = GameObject.Instantiate(Resources.Load<GameObject>("UI/DaycareUI"), canvas.transform).GetComponent<DaycareUI>();//.transform.GetChild(0).GetComponent<DaycareMonsterViewer>();
            daycare.transform.SetAsLastSibling();

            daycare.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return daycare;
        }


        public static EggMonitorUI OpenEggMonitor(UISystem _referrer)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");
            
            EggMonitorUI egg = GameObject.Instantiate(Resources.Load<GameObject>("UI/EggMonitorUI"), canvas.transform).GetComponent<EggMonitorUI>();//.transform.GetChild(0).GetComponent<DaycareMonsterViewer>();
            egg.transform.SetAsLastSibling();

            egg.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return egg;
        }

        public static GatehouseUI OpenMapViewer(UISystem _referrer)
        {
            if (isFading) return null;
            
            GameObject canvas = GameObject.Find("GameplayCanvas");

            GatehouseUI map = GameObject.Instantiate(Resources.Load<GameObject>("UI/GatehouseUI"), canvas.transform).GetComponent<GatehouseUI>();//.transform.GetChild(0).GetComponent<DaycareMonsterViewer>();
            map.transform.SetAsLastSibling();

            map.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return map;
        }

        public static InventoryUI OpenShop(UISystem _referrer, List<Items> inv, string _shopName)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            InventoryUI invManage = GameObject.Instantiate(Resources.Load<GameObject>("UI/InventoryUI"), canvas.transform).GetComponent<InventoryUI>();
            invManage.transform.SetAsLastSibling();

            invManage.headingText.text = _shopName;
            invManage.isShop = true;
            invManage.shopInv = inv;

            invManage.referrer = _referrer;
            _referrer.SetPriority(false, true);

            return invManage;
        }
        
        public static SkillPanelUI DisplaySkill(UISystem _referrer, Skills _skill, Transform _parent)
        {
            if (isFading) return null;

            SkillPanelUI skill = GameObject.Instantiate(Resources.Load<GameObject>("UI/SkillsPanelUpdater"), _parent).GetComponent<SkillPanelUI>();

            skill.skill = _skill;

            skill.referrer = _referrer;
            _referrer.SetPriority(false, true);

            skill.DisplaySkill();

            return skill;
        }

        public static ItemMessageUI DisplayItemMessage(UISystem _referrer, Item _item)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            ItemMessageUI itm = GameObject.Instantiate(Resources.Load<GameObject>("UI/ItemMessageUI"), canvas.transform).GetComponent<ItemMessageUI>();
            itm.item = _item;

            itm.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return itm;
        }

        public static AttackaponUI OpenAttackapon(UISystem _referrer, int _qA, int _qB, int _qS, AttackaponSpecial _s = AttackaponSpecial.NONE, Rarity _sRar = Rarity.NONE, MonsterAttribute _sAttr = MonsterAttribute.NONE)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            AttackaponUI at = GameObject.Instantiate(Resources.Load<GameObject>("UI/AttackaponUI"), canvas.transform).GetComponent<AttackaponUI>();
            at.transform.SetAsLastSibling();

            int[] quantities = new int[]{_qA, _qB, _qS};
            at.quantity = quantities;
            at.special = _s;
            at.specRarity = _sRar;
            at.specAttr = _sAttr;

            at.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return at;
        }

        public static CharmBagUI OpenCharmBag(UISystem _referrer)
        {
            if (isFading) return null;

            GameObject canvas = GameObject.Find("GameplayCanvas");

            CharmBagUI c = GameObject.Instantiate(Resources.Load<GameObject>("UI/CharmBagUI"), canvas.transform).GetComponent<CharmBagUI>();
            c.transform.SetAsLastSibling();

            c.referrer = _referrer;
            _referrer?.SetPriority(false, true);

            return c;
        }

        #region Utilities
        public static void ButtonsActive(bool active, Transform obj)
        {
            foreach (Button butt in obj)
            {
                butt.interactable = active;
            }
        }

        public static string GetPlayerGold()
        {
            return "<m=default><q=gold></m>" + SaveSystem.playerGold.ToString("##,#");
        }


        public static void ResetTempMonsterPartyIndex()
        {
            temporaryMonsterIndex = -1;
        }

        public static void ResetTempItemIndex()
        {
            temporaryItemIndex = -1;
        }

        public static void ResetTempChoices()
        {
            choiceIndex = -1;
        }
        #endregion
    }
}