﻿using UnityEngine;
using UnityEditor;
using System;
using GameDatabase;
using Random = UnityEngine.Random;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using QFSW.QC;

namespace MonsterSystem
{

    [Serializable]
    public class Monster
    {
        public MonsterIdentifier mon;

        public Perks perk;

        public float multiPrimary; // Increase rate multipler
        public float multiSecondary; // Decrease rate multiplier

        //public float dropRate;
        
        public int upStatA = -1, upStatB = -1, downStat = -1;
        public bool upStatExtra, downStatExtra;

        public Stats stats = new Stats();
        public int statPointsLeft;
        public int totalPoints;
        public int gymUses;

        public int monColor;
        public bool charmed;

        public string nickname;
        public bool hasNickname;

        public int level;
        public float totalEXP;
        public float currentEXP;
        public float nextEXP;

        public int gender = -1; // 0 - Female, 1 - Male, -1 - Random

        public bool isEgg;
        public int daysToHatch;

        public Maps mapObtainedID;
        public int metLevel;

        public Date dateObtained;
        public Date dateRetired;

        public int weight;
        public float height; // To be defined as FEET.INCHES (ex: 5.11 = 5'11")
        public int multi; // 0 = Normal, 1 = XS, 2 = S, 3 = L, 4 = XL

        public string partner;
        public string partnerID;

        public Skill[] monSkills;
        public Skill specialSkill;

        public StatusEffect statEffect = StatusEffect.NONE;

        public Item accessory = null;
        public Item tamedCharm;
        public List<Skills> learnedExclusives;

        public Monster(MonsterIdentifier _mon)
        {
            mon = _mon;

            MonsterData tempMon = GameUtility.GetMonsterDatabaseData(mon);

            stats = new Stats(tempMon.baseStats);
            level = 1;

            multiPrimary = 1;
            multiSecondary = 1;

            hasNickname = false;

            gender = Random.Range(0, 2);

            isEgg = false;

            weight = tempMon.baseWeight + (int)(tempMon.baseWeight * Random.Range(-0.25f, 0.25f));
                        
            float baseInches = tempMon.baseHeight % 1;
            float baseFeet = Mathf.Floor(tempMon.baseHeight);

            float inches = baseInches * 100;
            
            // XS, S, Normal, L, XL
            float[] multipliers = new float[] { 1f, 0.2f, 0.6f, 1.4f, 1.8f };
            int multiIndex = Random.Range(0, multipliers.Length);
            float _multi = multipliers[multiIndex];

            multi = multiIndex;

            inches = Mathf.Round(inches * _multi);

            float feet = baseFeet;

            while (inches > 12)
            {
                inches -= 12;
                feet++;
            }

            height = feet + (inches/100);

            //Debug.Log(tempMon.baseHeight + ", " + height);

            partner = SaveSystem.playerName;
            partnerID = SaveSystem.playerID;

            Moveset tempSet = MovesetSystem.GetMonsterMoveset(mon);

            monSkills = new Skill[5];

            perk = Perks.NONE;
            if (tempMon.specialPerk != Perks.NONE) perk = tempMon.specialPerk;

            if (tempSet == null || tempSet.available.Count < 5) // Debug default moveset
            {
                //Debug.Log("Debug Moveset created for " + GetInternalName() + " Lv " + GetMonLevel() + " " + GetGender());
                monSkills[0] = new Skill(Skills.BOUNDINGZAP);
                monSkills[1] = new Skill(Skills.BOUNDINGZAP);
                monSkills[2] = new Skill(Skills.BOUNDINGZAP);
                monSkills[3] = new Skill(Skills.BOUNDINGZAP);
                monSkills[4] = new Skill(Skills.BOUNDINGZAP);
            } else
            {
                monSkills[0] = new Skill(tempSet.available[0]);
                monSkills[1] = new Skill(tempSet.available[1]);
                monSkills[2] = new Skill(tempSet.available[2]);
                monSkills[3] = new Skill(tempSet.available[3]);
                monSkills[4] = new Skill(tempSet.available[4]);
            }

            CalculateAllStats();

            //float col = Random.Range(0f, 1f);

            monColor = (int)GameUtility.Choose(SystemVariables.CommonChance, SystemVariables.UncommonChance, SystemVariables.RareChance, SystemVariables.UltraRareChance);

            upStatA = Random.Range(0, 5);
            downStat = Random.Range(0, 5);
        
            do
            {
                downStat = Random.Range(0, 5);
            } while (downStat == upStatA);

            upStatB = Random.Range(0f, 1f) <= 0.1f? Random.Range(0, 5) : -1;

            if (upStatB != -1)
            {
                do
                {
                    upStatB = Random.Range(0, 5);
                } while (upStatB == upStatA || upStatB == downStat);
            }

            downStatExtra = Random.Range(0f, 10f) <= 0.05f? true: false;
            learnedExclusives = new List<Skills>();

            tamedCharm = new Item(Items.NEUTRALCHARM);
        }

        private Monster()
        {

        }

        public Monster SetNickname(string name)
        {
            nickname = name;
            hasNickname = true;
            return this;
        }

        public Monster SetPerk(Perks _perk)
        {
            perk = _perk;
            return this;
        }

        public Monster SetEgg()
        {
            isEgg = true;
            // TODO: daysToHatch
            return this;
        }

        public Monster SetPartner(string _partner, string _id)
        {
            partner = _partner;
            partnerID = _id;

            return this;
        }

        public Monster SetGender(int _gender)
        {
            if (_gender < 0 || _gender > 1) throw new ArgumentOutOfRangeException();

            gender = _gender;
            return this;
        }

        public Monster SetLevel(int _level)
        {
            if (_level > 100) throw new ArgumentOutOfRangeException("Level can't be higher than 100.");

            level = _level;
            CalculateAllStats(true);
            return this;
        }

        // Used when increasing experience on a monsters stats
        public void GainEXP(float expAmount, bool levelUp = true)
        {
            currentEXP += expAmount;

            if (!levelUp) return;

            while (currentEXP >= nextEXP)
            {
                LevelUp();
            }
        }

        // Level up a stat
        public void LevelUp()
        {
            // Clear current EXP and increase level
            currentEXP -= nextEXP;
            level++;

            // Recalculate stat amount
            CalculateAllStats(false);

            // Calculate EXP
            CalculateEXP();
            
            // Restore current HP on level up
            //stats.currentHP = stats.statArray[5];
        }

        // Calculate current stat amount
        public void CalculateStat(int stat)
        {
            MonsterData tempMon = GameUtility.GetMonsterDatabaseData(mon);

            stats.statArray[stat] = Mathf.FloorToInt(((2.82f * (tempMon.baseStats.baseArray[stat] * level * (1 + (stats.statPoints[stat] * 0.0062f))) + 5.5f) / 100 + 2.5f) * 1);
        }

        // Calculate total EXP for current stat level
        public void CalculateEXP()
        {
            int nextLevel = (int)(Mathf.FloorToInt((level + 3) * Mathf.Pow(level + 1, 0.85f) * (Mathf.Sqrt(75) / 12) * 0.3f) * Mathf.Pow(level + 1, 1.35f));
            int curr = (int)(Mathf.FloorToInt((level + 2) * Mathf.Pow(level + 1, 0.85f) * (Mathf.Sqrt(75) / 12) * 0.3f) * Mathf.Pow(level, 1.35f));

            nextEXP = nextLevel - curr;
            totalEXP = curr;
        }

        public void CalculateAllStats(bool heal = true)
        {
            for (int i = 0; i < stats.statArray.Length; i++)
            {
                CalculateStat(i);
                CalculateEXP();
            }

            if (heal) HealFull();
        }
        
        public void AddStatPoints(int _amt)
        {
            statPointsLeft += _amt;
            totalPoints += _amt;
        }

        public void UseGym()
        {
            gymUses++;
        }

        public int GetHighestStat()
        {
            int maxStat = stats.statArray[0];

            for (int i = 0; i < stats.statArray.Length; i++)
            {
                if (stats.statArray[i] > maxStat)
                {
                    maxStat = stats.statArray[i];
                }
            }

            return maxStat;
        }

        public int GetStatTotal()
        {
            int total = 0;

            MonsterData tempMon = GameUtility.GetMonsterDatabaseData(mon);

            for (int i = 0; i < tempMon.baseStats.baseArray.Length; i++)
            {
                total += tempMon.baseStats.baseArray[i];
            }

            return total;
        }

        public void RecalculateBase()
        {
            for (int i = 0; i < stats.statArray.Length; i++)
            {
                CalculateStat(i);
                CalculateEXP();
            }
        }

        public bool Heal(float _healAmount)
        {
            if (stats.currentHP == stats.statArray[5] || GetStatusEffect() == StatusEffect.DEAD)
            {
                return false;
            }

            int healAmount = Mathf.RoundToInt(_healAmount);
            stats.currentHP = Mathf.Clamp(stats.currentHP + healAmount, 1, GetHPStat());

            return true;
        }

        public void HealFull()
        {
            stats.currentHP = GetHPStat();
        }

        public int GetStatValue(StatNames _stat)
        {
            int val = stats.statArray[(int)_stat];

            if (upStatA == (int)_stat)
            {
                float multi = upStatExtra? 1.25f : 1.15f;
                val = Mathf.RoundToInt((float)val * multi);
            }

            if (upStatB == (int)_stat)
            {
                val = Mathf.RoundToInt((float)val * 1.15f);
            }

            if (downStat == (int)_stat)
            {
                val = Mathf.RoundToInt((float)val * 0.85f);
            }

            return val;
        }

        public int GetMonLevel()
        {
            return level;
        }

        public float GetCurrentEXP()
        {
            return currentEXP;
        }

        public float GetTotalEXP()
        {
            return totalEXP;
        }

        public float GetNextEXP()
        {
            return nextEXP;
        }

        public int GetHPStat()
        {
            return stats.statArray[5];
        }

        public int GetCurrentHP()
        {
            return stats.currentHP;
        }

        public void Hurt(float _amount)
        {
            int amount = Mathf.RoundToInt(_amount);
            int modHP = stats.currentHP - amount;

            stats.currentHP = modHP;
            if (stats.currentHP < 0) stats.currentHP = 0;
        }

        public bool HasDrops()
        {
            MonsterData monD = GetData();

            return monD.commonDrops.Count > 0 || monD.uncommonDrops.Count > 0 || monD.rareDrops.Count > 0;
        }

        public bool HasCommonDrops()
        {
            return GetData().commonDrops.Count > 0;
        }

        public bool HasUncommonDrops()
        {
            return GetData().uncommonDrops.Count > 0;
        }

        public bool HasRareDrops()
        {
            return GetData().rareDrops.Count > 0;
        }

        public List<Items> GetCommonDrops()
        {
            return GetData().commonDrops;
        }

        public List<Items> GetUncommonDrops()
        {
            return GetData().uncommonDrops;
        }

        public List<Items> GetRareDrops()
        {
            return GetData().rareDrops;
        }

        public MonsterData GetData()
        {
            return GameUtility.GetMonsterDatabaseData(mon);
        }

        public string GetInternalName()
        {
            return mon.id.ToString();
        }

        public string GetSpeciesName()
        {
            return SystemVariables.trans[mon.id.ToString() + "_" + GetData().form + "_NAME"]; //SystemVariables.monNames[mon.ToString()];
        }

        public MonsterFamily GetFamily()
        {
            return GetData().family;
        }

        public int GetID()
        {
            return (int)mon.id;
        }

        public MonsterAttribute GetType(int _ind = 0)
        {
            return _ind == 0 ? GetData().firstType : GetData().secondType;
        }

        public Sprite GetIcon()
        {
            Sprite[] icon = Resources.LoadAll<Sprite>("MonsterIcons/" + GetInternalName().ToLower() + "_" + mon.formId);

            if (icon.Length == 0) Debug.Log("MonsterIcons/" + GetInternalName().ToLower() + "_" + mon.formId);

            return icon.Length == 0? null : icon[monColor];
        }

        public Sprite GetSprite()
        {
            Sprite[] spr = Resources.LoadAll<Sprite>("Monsters/" + GetInternalName().ToLower() + "_" + mon.formId);

            return spr.Length == 0 ? Resources.Load<Sprite>("Monsters/battleDummy") : spr[monColor];
        }

        public StatusEffect GetStatusEffect()
        {
            return statEffect;
        }

        public bool SetStatusEffect(StatusEffect effect)
        {
            if ((effect == StatusEffect.NONE) || (effect != StatusEffect.DEAD && statEffect == StatusEffect.NONE && GetCurrentHP() > 0) || (effect == StatusEffect.DEAD && GetCurrentHP() == 0)) // If curing
            {
                statEffect = effect;
                return true;
            }

            return false;
        }

        public void SetCharm(Item _charm)
        {
            tamedCharm = _charm;
        }

        public string GetName()
        {
            return hasNickname ? nickname : GameUtility.FirstCharToUpper(GetSpeciesName());
        }

        public string GetGender()
        {
            return gender == 0 ? "F" : "M";
        }

        public int GetGenderInt()
        {
            return gender;
        }

        public void EquipSkill (Skills _skill, int _index)
        {
            monSkills[_index] = new Skill(_skill);
        }

        public SkillData GetSkillData(int _index)
        {
            return _index == 5? GetSpecialSkillData() : SkillSystem.GetSkillDatabaseData(GetSkill(_index));
        }

        public SkillData GetSpecialSkillData()
        {
            return specialSkill.id == Skills.NONE? null : SkillSystem.GetSkillDatabaseData(specialSkill.id);
        }

        public Skills GetSkill(int _index)
        {
            return _index == 5? (specialSkill == null? Skills.NONE : specialSkill.id) : monSkills[_index].id;
        }

        public Skills GetSpecialSkill()
        {
            return specialSkill.id;
        }

        public bool CanLearn(Skills _skill)
        {
            bool canLearn = false;

            for (int i = 0; i < MovesetDatabase.db.Count; i++)
            {
                if (MovesetDatabase.db[i].mon.id == mon.id && MovesetDatabase.db[i].available.Contains(_skill))
                {
                    canLearn = true;
                    break;
                }
            }

            if (learnedExclusives.Contains(_skill)) canLearn = true;

            return canLearn;
        }

        public string ToDebugString()
        {
            string output;
            
            output = mon.id.ToString() + ", " + mon.formId.ToString() + "\n";
            output += level.ToString() + ", " + GetGender() + "\n";
            output += perk.ToString() + "\n";
            
            if (accessory == null)
            {
                output += "NO ACCESSORY\n";
            } else
            {
                output += accessory.id.ToString() + ", " + accessory.secID.ToString() + " [" + accessory.uniqueParam?.ToString() + "]" + "\n";
            }
            
            for (int i = 0; i < 6; i++)
            {
                output += SystemVariables.statNames[i] + ": " + stats.statArray[i] + (upStatA == i? upStatExtra? "++" : "+" : upStatB == i? "+" : downStat == i? downStatExtra? "--" : "-" : "") + ", " + stats.statPoints[i] + "\n";
            }

            for (int i = 0; i < 5; i++)
            {
                output += monSkills[i].id.ToString() + ", " + monSkills[i].cooldownMultiplier.ToString() + "\n";
            }

            if (specialSkill != null) output += specialSkill.id.ToString() + ", " + specialSkill.cooldownMultiplier.ToString() + "\n";

            return output;
        }

        public string GetHeight()
        {
            string heightString = height.ToString();

            string ft = heightString.Substring(0, heightString.IndexOf("."));
            string inch = heightString.Substring(heightString.IndexOf(".") + 1).TrimStart(new char[] { '0' });

            string[] multipliers = new string[] { "", "XS", "S", "L", "XL" };

            return ft + "'" + inch + "\"" + " " + multipliers[multi];
        }
    }

    public enum StatNames
    {
        STRENGTH = 0,
        DEFENSE = 1,
        MAGIC = 2,
        RESISTANCE = 3,
        SPEED = 4,
        HEALTH = 5
    }
}