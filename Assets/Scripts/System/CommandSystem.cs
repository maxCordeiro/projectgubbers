﻿using UnityEngine;
using System.Collections;
using QFSW;
using QFSW.QC;
using MonsterSystem;
using GameDatabase;

public static class CommandSystem
{
#if UNITY_EDITOR
    public static Monster[] monPresets = new Monster[]
    {
        new Monster(new MonsterIdentifier(Monsters.SOMBRUX, 0))
        {
            perk = Perks.CASHCOW,
            level = 10,
            monSkills = new Skill[]
            {
                new Skill(Skills.SPELLANCHOLY),
                new Skill(Skills.MULTIBLAST),
                new Skill(Skills.BOUNDINGZAP),
                new Skill(Skills.DEFLECT),
                new Skill(Skills.HEALINGHYMN)
            },
            monColor = 0
        }
    };

    [Command]
    public static void monAdd(Monsters mon, int level, int gender, int form = 0, bool inParty = true, int color = 0)
    {
        Monster m = new Monster(new MonsterIdentifier(mon, form));
        m.SetLevel(level);
        m.SetGender(gender);
        m.monColor = color;
        m.CalculateAllStats();

        GameUtility.ObtainMonster(m, !inParty);
    }

    [Command]
    public static void monAdd(int index, bool inParty = true)
    {
        Monster a = monPresets[index];
        a.CalculateAllStats();

        GameUtility.ObtainMonster(a, !inParty);
    }

    [Command]
    public static void monHeal(int index, int amt)
    {
        SaveSystem.playerParty[index].Heal(amt);
    }

    [Command]
    public static void monHurt(int index, int amt)
    {
        SaveSystem.playerParty[index].Hurt(amt);
    }

    [Command]
    public static void monStatus(int index, StatusEffect stat)
    {
        SaveSystem.playerParty[index].SetStatusEffect(stat);
    }

    [Command]
    public static void itemGet(Items item, int quantity)
    {
        ItemSystem.ObtainItem(item, quantity, 0, false);
    }

    [Command]
    public static void monColor(int index, int color, bool inParty = true)
    {
        if (inParty)
        {
            SaveSystem.playerParty[index].monColor = color;
        } else
        {
            SaveSystem.playerRanch[index].monColor = color;
        }
    }

    [Command]
    public static void monSkill(Skills skill, int index, int skillIndex, bool inParty = true)
    {
        if (inParty)
        {
            SaveSystem.playerParty[index].EquipSkill(skill, skillIndex);
        } else
        {
            SaveSystem.playerRanch[index].EquipSkill(skill, skillIndex);
        }
    }


    [Command]
    public static void reloadChests()
    {
        ChestMonster[] c = GameObject.FindObjectsOfType<ChestMonster>();

        foreach (ChestMonster chest in c)
        {
            chest.Generate();
        }
    }

    [Command]
    public static void reloadSpawns()
    {
        SpawnRegion[] m = GameObject.FindObjectsOfType<SpawnRegion>();
        NPCBossMon b = GameObject.FindObjectOfType<NPCBossMon>();

        foreach (SpawnRegion s in m)
        {
            s.GenerateAllMons();
        }

        if (b) b.spawnedMon = GameUtility.GenerateMonsterSpawn(EncounterType.BOSS);
    }

    [Command]
    public static void reloadMap()
    {
        reloadChests();
        reloadSpawns();
    }

    [Command]
    public static void getTOD()
    {
        Debug.Log(SaveSystem.dayNight.ToString());
    }
#endif
}