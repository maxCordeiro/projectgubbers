﻿using UnityEngine;
using UnityEditor;
using GameDatabase;
using System;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MonsterSystem
{
    [Serializable]
    public class PerkData
    {
        [XmlAttribute("id")]
        public Perks id;
        [XmlElement("type")]
        public int fieldOrBattle;
        [XmlElement("rarity")]
        public Rarity rarity;
        [XmlElement("family")]
        public MonsterFamily fam;

        public PerkData(Perks _id, MonsterFamily _fam, Rarity _rarity, int _fieldOrBattle)
        {
            id = _id;
            fieldOrBattle = _fieldOrBattle;
            rarity = _rarity;
            fam = _fam;
        }

        private PerkData()
        {
        }
    }

    public static class PerkSystem
    {
        public static string GetPerkName(Perks _perk) => SystemVariables.trans[_perk.ToString() + "_NAME"];
        public static string GetPerkDescription(Perks _perk) => SystemVariables.trans[_perk.ToString() + "_DESC"];

        public static int GetPerkDatabaseIndex(Perks _perk)
        {
            for (int i = 0; i < PerkDatabase.db.Count; i++)
            {
                if (PerkDatabase.db[i].id == _perk)
                {
                    return i;
                }
            }

            return -1;
        }
        
        public static PerkData GetPerkDatabaseData(Perks _perk)
        {
            return PerkDatabase.db[GetPerkDatabaseIndex(_perk)];
        }

        public static Perks GetRandomPerk(MonsterFamily _fam = MonsterFamily.NONE, Rarity _rarity = Rarity.NONE)
        {            
            List<Perks> perkPool = new List<Perks>();

            foreach (PerkData pData in PerkDatabase.db)
            {
                bool famCheck = (_fam != MonsterFamily.NONE && pData.fam == _fam) || _fam == MonsterFamily.NONE;

                if (_rarity != Rarity.NONE && pData.rarity == _rarity && famCheck)
                {
                    perkPool.Add(pData.id);
                    continue;
                }

                float probs = GameUtility.Choose(SystemVariables.CommonChance, SystemVariables.UncommonChance, SystemVariables.RareChance, SystemVariables.UltraRareChance);
                int perkRarityInt = ((int)pData.rarity) - 1;
                int _rarityInt = ((int)_rarity) - 1;

                if (probs == perkRarityInt && probs == _rarityInt && famCheck)
                {
                    perkPool.Add(pData.id);
                }
            }

            int ind = Random.Range(0, perkPool.Count);
            
            Debug.Log("Getting Random Perk: " + string.Format("Family: {0}, Rarity: {1}", _fam, _rarity) + ", Got: " + perkPool[ind]);

            return perkPool[ind];
        }
    }
}
 