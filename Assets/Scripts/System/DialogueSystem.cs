﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using System.Xml;
using UnityEngine.Events;
using UnityEngine.UI;
using System.IO;
using GameDatabase;

namespace MonsterSystem
{
    public static class DialogueSystem
    {
        public static Dictionary<string, string> replacements = new Dictionary<string, string>(){
            { "$PN", SaveSystem.playerName }
        };

        // Load text from XML files

        public static string LoadText(string _id, int _attr, string _fileName)
        {
            TextAsset textDoc = Resources.Load<TextAsset>("Localization/" + _fileName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(textDoc.text);

            foreach (XmlNode node in doc.DocumentElement)
            {
                if (node.NodeType != XmlNodeType.Comment && node.Attributes[0].Value == SaveSystem.settings_language.ToString())
                {
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.NodeType == XmlNodeType.Comment) continue;
                        if (child.Attributes[0].Value == _id)
                        {
                            if (child.Attributes[_attr].Value == null) throw new MissingReferenceException("XML attribute not found");
                            
                            return ReplacePlaceholders(child.Attributes[_attr].Value);
                        }
                    }
                }
            }

            return "*" + _id;
        }

        public static string LoadTest(string _id, string _value, string _filename)
        {
            TextAsset textDoc = Resources.Load<TextAsset>("Localization/" + _filename);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(textDoc.text);

            foreach (XmlNode node in doc.DocumentElement)
            {
                if (node.NodeType != XmlNodeType.Comment && node.Attributes[0].Value == SaveSystem.settings_language.ToString())
                {
                    //Debug.Log(string.Format("Node: {0} (Attr {1})", node.Name, node.Attributes[0].Value));
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.NodeType == XmlNodeType.Comment) continue;
                        if (child.Attributes[0].Value == _id)
                        {
                            //Debug.Log(string.Format("Child: {0} (Attr {1})", child.Name, child.Attributes[0].Value));

                            foreach(XmlElement e in child.ChildNodes)
                            {
                                //Debug.Log(string.Format("Element: {0} [{1}]", e.Name, e.InnerText));

                                if (e.Name == _value)
                                {
                                    //Debug.Log(e.Value);
                                    return e.InnerText;
                                }
                            }
                        }
                    }
                }
            }

            return "*" + _id;
        }

        public static string LoadMsg(string _id, string _msg, string _fileName)
        {
            TextAsset textDoc = Resources.Load<TextAsset>("Localization/" + _fileName);
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(textDoc.text);

            foreach (XmlNode node in doc.DocumentElement)
            {
                if (node.NodeType != XmlNodeType.Comment && node.Attributes[0].Value == SaveSystem.settings_language.ToString())
                {
                    foreach (XmlNode child in node.ChildNodes)
                    {
                        if (child.Attributes[0].Value == _id && child.Attributes[1].Value == _msg)
                        {
                            return ReplacePlaceholders(child.Attributes[2].Value);
                        }
                    }
                }
            }

            return "";
        }

        public static string LoadName(string _id, string _fileName)
        {
            return LoadText(_id, 1, _fileName);
        }

        public static string ReplacePlaceholders(string textFromInspector)
        {
            string s = textFromInspector;
            foreach (KeyValuePair<string, string> r in replacements)
            {
                s = s.Replace(r.Key, r.Value);
            }
            return s;
        }

        public static bool IsVowel(string text)
        {
            string[] vowels = new string[] { "a", "e", "i", "o", "u" };

            foreach (string letter in vowels)
            {
                if (text[0].ToString() == letter)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public struct LocaleItem
    {
        public Items itemID;
        public string locID, locDesc;

        public LocaleItem(Items _id, string _locID, string _locDesc)
        {
            itemID = _id;
            locID = _locID;
            locDesc = _locDesc;
        }
    }

    public struct LocalePerk
    {
        public Perks perkID;
        public string locID, locDesc;

        public LocalePerk(Perks _id, string _locID, string _locDesc)
        {
            perkID = _id;
            locID = _locID;
            locDesc = _locDesc;
        }
    }

    public struct LocaleMonster
    {
        public MonsterIdentifier monId;
        public string locID, locDesc;

        public LocaleMonster(MonsterIdentifier _monId, string _locID, string _locDesc)
        {
            monId = _monId;
            locID = _locID;
            locDesc = _locDesc;
        }
    }

    public struct DictTwoPair
    {
        public string pairID, pairDesc;

        public DictTwoPair(string _pairID, string _pairDesc)
        {
            pairID = _pairID;
            pairDesc = _pairDesc;
        }
    }
}