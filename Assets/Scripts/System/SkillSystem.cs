﻿using UnityEngine;
using UnityEditor;
using GameDatabase;
using System;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using UnityEngine.Events;
using System.Xml.Serialization;

namespace MonsterSystem
{
    [Serializable]
    public class SkillData
    {
        [XmlAttribute("id")]
        public Skills id;
        [XmlElement("attack")]
        public int attack;
        [XmlElement("cooldown")]
        public int cooldown;

        [XmlElement("ranged")]
        public bool ranged;
        [XmlElement("skilltype")]
        public SkillType skillType;
        [XmlElement("exclusive")]
        public MonsterIdentifier exclusiveMon;
        [XmlElement("attr")]
        public MonsterAttribute attribute;

        [XmlElement("effectid")]
        public string effectID;

        /*[XmlElement("haseffect")]
        public bool hasEffect;

        [XmlElement("hasexclusive")]
        public bool hasExclusive;*/

        [XmlElement("priority")]
        public int priorityLevel;
        [XmlElement("skillRarity")]
        public Rarity skillRarity;

        [XmlElement("sellprice")]
        public int sellPrice;

        [XmlElement("specialSkill")]
        public bool isSpecialSkill;

        public SkillData(Skills _id, int _attack, int _cooldown, bool _ranged, SkillType _skillType, MonsterAttribute _monType, int _sellPrice)
        {
            id = _id;

            attack = _attack;
            cooldown = _cooldown;
            ranged = _ranged;
            skillType = _skillType;
            attribute = _monType;

            priorityLevel = -1;

            sellPrice = _sellPrice;
        }

        private SkillData()
        {

        }

        public SkillData SetEffect(string _effectID)
        {
            effectID = _effectID;
            //hasEffect = true;
            return this;
        }

        public SkillData SetPriority(int _level)
        {
            priorityLevel = _level;
            return this;
        }

        public SkillData SetExclusive(Monsters _mon, int _form)
        {
            exclusiveMon = new MonsterIdentifier(_mon, _form);
            //hasExclusive = true;
            return this;
        }
    }

    [Serializable]
    public class Skill
    {
        public int cooldownMultiplier;
        public Skills id;

        public Skill(Skills _skill)
        {
            id = _skill;
        }

        Skill()
        {

        }

        public Skill DecreaseCooldown(int amt = 1)
        {
            if (SkillSystem.GetSkillDatabaseData(id).isSpecialSkill) return this;

            cooldownMultiplier = Mathf.Clamp(cooldownMultiplier - amt, 1, 10);
            return this;
        }
    }

    public enum SkillType
    {
        SPECIAL,
        STRENGTH,
        MAGIC
    }

    public enum SkillPriority
    {
        NONE,
        BEFORE,
        REGULAR
    }

    public static class SkillSystem
    {
        public static string GetSkillName(Skills _skill) => SystemVariables.trans[_skill + "_NAME"];
        public static string GetSkillDescription(Skills _skill) => SystemVariables.trans[_skill + "_DESC"];

        public static int GetSkillDatabaseIndex(Skills _skill)
        {
            for (int i = 0; i < SkillDatabase.db.Count; i++)
            {
                if (SkillDatabase.db[i].id == _skill)
                {
                    return i;
                }
            }

            return -1;
        }

        public static SkillData GetSkillDatabaseData(Skills _skill)
        {
            return SkillDatabase.db[GetSkillDatabaseIndex(_skill)];
        }

        public static Skills GetRandomSkill(Rarity _rarity = Rarity.NONE, MonsterAttribute _type = MonsterAttribute.NONE)
        {
            List<Skills> randPool = new List<Skills>();

            foreach (SkillData sData in SkillDatabase.db)
            {
                bool attr = (_type != MonsterAttribute.NONE && sData.attribute == _type) || _type == MonsterAttribute.NONE;

                if (_rarity != Rarity.NONE && sData.skillRarity == _rarity && attr)
                {
                    //Debug.Log("RARE CHECK ====== " + sData.id + ": " + sData.skillRarity + ", " + _rarity);
                    randPool.Add(sData.id);
                    continue;
                }

                float probs = GameUtility.Choose(SystemVariables.CommonChance, SystemVariables.UncommonChance, SystemVariables.RareChance, SystemVariables.UltraRareChance);

                int skillRareInt = ((int)sData.skillRarity) - 1;
                int _rarityInt = ((int)_rarity) - 1;

                if (probs ==  _rarityInt && probs == skillRareInt && attr)
                {
                    //Debug.Log("PROBS CHECK ====== " + sData.id + ": Skill Rarity [" + rareInt.ToString() + "], Probs [" + probs.ToString() + "]" + ", _rarity [" + _rarityInt + "]");
                    randPool.Add(sData.id);
                }
            }

            int ind = Random.Range(0, randPool.Count);

            Debug.Log("Getting Random Skill: " + string.Format("Rarity: {0}, Attribute: {1}", _rarity, _type) + ", Got: " + randPool[ind]);

            return randPool[ind];
        }
    }
}