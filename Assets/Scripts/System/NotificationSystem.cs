﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonsterSystem
{
    public static class NotificationSystem
    {
        public static void AddNotification(Notification _n)
        {
            //SaveSystem.playerNotifs.Add(_n);
        }
    }

    public enum NotificationTypes
    {
        EGGHATCH = 0,
        EGGFOUND = 1,
        TRAININGDONE = 2,
        MISSIONCOMPLETE = 3
    }

    public class Notification
    {
        public NotificationTypes notifType;
        public Date timeAdded;
        public bool seen;

        public Notification(NotificationTypes _type, Date _time)
        {
            notifType = _type;
            timeAdded = _time;
        }
    }
}
