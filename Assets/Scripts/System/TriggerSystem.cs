﻿using UnityEngine;
using UnityEditor;
using System;
using Prime31;

public class TriggerSystem : MonoBehaviour
{
    public Action<Collider2D> onPlayerTriggerEnterEvent;
    public Action<Collider2D> onPlayerTriggerStayEvent;
    public Action<Collider2D> onPlayerTriggerExitEvent;

    public void OnTriggerEnter2D(Collider2D collision) => onPlayerTriggerEnterEvent?.Invoke(collision);
    public void OnTriggerStay2D(Collider2D collision) => onPlayerTriggerStayEvent?.Invoke(collision);
    public void OnTriggerExit2D(Collider2D collision) => onPlayerTriggerExitEvent?.Invoke(collision);
}