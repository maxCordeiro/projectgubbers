﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

namespace MonsterSystem
{
    public static class TransitionSystem
    {
        public static float speed = 0.075f; // Amount used when decreasing the material value
        public static float delay = 1f; // Time to wait when the transition is in its middle state

        public static string sceneToLoad;
        public static bool sameMap;
        public static string tex;

        public static UnityAction midTrans;
        public static UnityAction endTrans;

        public static bool fade;
        public static bool midSceneTrans;

    }
}