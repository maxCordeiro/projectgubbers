﻿using GameDatabase;
using MonsterSystem;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using UnityEngine;

public class SaveFile
{
    public PlayerVariables localPVar;
    public GameVariables localGVar;
    public SettingsVariables localSVar;
}

public class SaveSystem
{
    static PlayerVariables pVar = new PlayerVariables();
    static GameVariables gVar = new GameVariables();
    static SettingsVariables sVar = new SettingsVariables();

    #region singleton

    private static SaveSystem instance;
    public SaveSystem()
    {

    }

    //To get this object you would call LevelInfo.Instance from anywhere in your code
    public static SaveSystem Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new SaveSystem();
            }
            return instance;
        }
    }

    #endregion singleton

    public static void SaveGame(int saveIndex)
    {
        var serializer = new XmlSerializer(typeof(PlayerVariables));
        using (var stream = new FileStream(Application.persistentDataPath + "/" + saveIndex + "_tamer.sav", FileMode.Create))
        {
            serializer.Serialize(stream, pVar);
        }
        /*
        BinaryFormatter saveBF = new BinaryFormatter();

        FileStream fStreamPlayer = new FileStream(Application.persistentDataPath + "/g_p - " + saveIndex + ".sav", FileMode.Create);
        FileStream fStreamGame = new FileStream(Application.persistentDataPath + "/g_g - " + saveIndex + ".sav", FileMode.Create);
        FileStream fStreamSystem = new FileStream(Application.persistentDataPath + "/g_s - " + saveIndex + ".sav", FileMode.Create);

        saveBF.Serialize(fStreamPlayer, pVar);
        saveBF.Serialize(fStreamGame, gVar);
        saveBF.Serialize(fStreamSystem, sVar);

        Debug.Log("Saved! Save: " + saveIndex.ToString());

        fStreamPlayer.Close();
        fStreamGame.Close();
        fStreamSystem.Close();*/
    }

    public static void LoadGame(int saveIndex)
    {
        if (File.Exists(Application.persistentDataPath + "/" + saveIndex + "_tamer.sav") ||
            File.Exists(Application.persistentDataPath + "/g_g - " + saveIndex + ".sav") || 
            File.Exists(Application.persistentDataPath + "/g_s - " + saveIndex + ".sav"))
        {
            var serializer = new XmlSerializer(typeof(PlayerVariables));
            using (var stream = new FileStream(Application.persistentDataPath + "/" + saveIndex + "_tamer.sav", FileMode.Open))
            {
                pVar = serializer.Deserialize(stream) as PlayerVariables;
            }

            //BinaryFormatter loadBF = new BinaryFormatter();
            //FileStream fStreamPlayer = new FileStream(Application.persistentDataPath + "/" + saveIndex + "_tamer.sav", FileMode.Open);
            //FileStream fStreamGame = new FileStream(Application.persistentDataPath + "/g_g - " + saveIndex + ".sav", FileMode.Open);
            //FileStream fStreamSystem = new FileStream(Application.persistentDataPath + "/g_s - " + saveIndex + ".sav", FileMode.Open);

            //pVar = loadBF.Deserialize(fStreamPlayer) as PlayerVariables;
            //gVar = loadBF.Deserialize(fStreamGame) as GameVariables;
            //sVar = loadBF.Deserialize(fStreamSystem) as SettingsVariables;

            Debug.Log("Loaded! Save: " + saveIndex.ToString());

            //fStreamPlayer.Close();
            //fStreamGame.Close();
            //fStreamSystem.Close();

            ReloadMonsterData();
        }
        else
        {
            Debug.LogWarning("Save file <b>" + saveIndex + "</b> doesn't exist! Clearing loaded data.");
        }
    }
     
    #region Player Variables

    public static List<Monster> playerParty { get { return pVar.partyMonsters; } set { pVar.partyMonsters = value; } }
    public static Monster[] playerRanch { get { return pVar.inactiveMonsters; } set { pVar.inactiveMonsters = value; } }

    public static List<Item> invGeneral { get { return pVar.inventoryGeneral; } set { pVar.inventoryGeneral = value; } }
    public static List<Item> invCare { get { return pVar.inventoryCare; } set { pVar.inventoryCare = value; } }
    public static List<Item> invBattle { get { return pVar.inventoryBattle; } set { pVar.inventoryBattle = value; } }
    public static List<Item> invApparel { get { return pVar.inventoryApparel; } set { pVar.inventoryApparel = value; } }
    public static List<Item> invSkills { get { return pVar.inventorySkills; } set { pVar.inventorySkills = value; } }
    public static List<Item> invCharms { get { return pVar.inventoryCharms; } set { pVar.inventoryCharms = value; } }

    public static List<Item> GetInventoryList(int pocket)
    {
        switch (pocket)
        {
            case 0:
                return invGeneral;
            case 1:
                return invCare;
            case 2:
                return invBattle;
            case 3:
                return invApparel;
            case 4:
                return invSkills;
            case 5:
                return invCharms;
            default:
                return null;
        }
    }

    public static List<TierInfo> playerTiers { get { return pVar.currentTiers; } set { pVar.currentTiers = value; } }

    //public static List<Notification> playerNotifs { get { return pVar.notifications; } set { pVar.notifications = value; } }
    //public static bool playerUnreadNotifs { get { return pVar.hasUnreadNotifs; } set { pVar.hasUnreadNotifs = value; } }

    public static string playerName { get { return pVar.playerName; } set { pVar.playerName = value; } }
    public static string playerID { get { return pVar.playerID; } set { pVar.playerID = value; } }

    public static Items playerHair { get { return pVar.playerHair; } set { pVar.playerHair = value; } }
    public static Items playerTop { get { return pVar.playerShirt; } set { pVar.playerShirt = value; } }
    public static Items playerBottom { get { return pVar.playerPant; } set { pVar.playerPant = value; } }

    public static bool playerEars { get { return pVar.coverEars; } set { pVar.coverEars = value; } }

    public static Maps currentMap { get { return pVar.activeMap; } set { pVar.activeMap = value; } }
    public static MapTier currentTier { get { return pVar.activeMapTier; } set { pVar.activeMapTier = value; } }

    public static int playerGold { get { return pVar.gold; } set { pVar.gold = value; } }

    #endregion

    #region Game Variables

    public static string gameVersion { get { return gVar.gameVersion; } set { gVar.gameVersion = value; } }

    #region Time

    public static int currentHour { get { return gVar._currentHour; } set { gVar._currentHour = value; } }
    public static int currentMinute { get { return gVar._currentMinute; } set { gVar._currentMinute = value; } }
    public static int currentMonth { get { return gVar._currentMonth; } set { gVar._currentMonth = value; } }
    public static Days currentDay { get { return gVar._currentDay; } set { gVar._currentDay = value; } }
    public static int currentDayIndex { get { return gVar._currentDayIndex; } set { gVar._currentDayIndex = value; } }
    public static int currentDayInMonth { get { return gVar._currentDayInMonth; } set { gVar._currentDayInMonth = value; } }
    public static int currentYear { get { return gVar._currentYear; } set { gVar._currentYear = value; } }
    public static DayNight dayNight { get { return gVar._timeOfDay; } set { gVar._timeOfDay = value; } }

    public static bool tick { get { return gVar._tick; } set { gVar._tick = value; } }
    public static TimeSpan currTimeSpan { get { return gVar._curr; } set { gVar._curr = value; } }
    public static TimeSpan prevTimeSpan { get { return gVar._prev; } set { gVar._prev = value; } }

    public static float timeOfDay { get { return gVar._currentTimeOfDay; } set { gVar._currentTimeOfDay = value; } }

    public static List<Items> shopA { get { return gVar.shopAInv; } set { gVar.shopAInv = value; } }
    public static List<Items> shopB { get { return gVar.shopBInv; } set { gVar.shopBInv = value; } }

    #endregion

    #region Gym
    public static List<Monster> gymTraining { get { return gVar.trainingMons; } set { gVar.trainingMons = value; } }
    public static List<Date> gymWait { get { return gVar.trainingWait; } set { gVar.trainingWait = value; } }
    #endregion

    #region Daycare
    
    public static List<DayCarePair> dcMons { get { return gVar.daycareMons; } set { gVar.daycareMons = value; } }
    public static List<Incubator> dcIncubators { get { return gVar.daycareIncubators; } set { gVar.daycareIncubators = value; } }

    public static int dcDenUnlocks { get { return gVar.denUnlocks; } set { gVar.denUnlocks = value; } }
    public static int dcIncubatorUnlocks { get { return gVar.incubatorUnlocks; } set { gVar.incubatorUnlocks = value; } }

    #endregion

    #region Encyclopedia

    public static List<EncUnlockMonster> encMons { get { return gVar.unlockMonsters; } set { gVar.unlockMonsters = value; } }
    public static List<EncUnlockMap> encMaps { get { return gVar.unlockMaps; } set { gVar.unlockMaps = value; } }

    #endregion

    #endregion

    #region Settings Variables

    public static int settings_textSpeed { get { return sVar.textSpeed; } set { sVar.textSpeed = value; } }
    public static int settings_textDrawEffect { get { return sVar.textDrawEffect; } set { sVar.textDrawEffect = value; } }
    public static bool settings_twelvehrclock { get { return sVar.twelvehr; } set { sVar.twelvehr = value; } }

    public static float settings_UIScale { get { return sVar.uiScale; } set { sVar.uiScale = value; } }
    public static bool settings_UIAnim { get { return sVar.uiAnim; } set { sVar.uiAnim = value; } }
    public static int settings_resolution { get { return sVar.resolution; } set { sVar.resolution = value; } }
    public static int settings_windowType { get { return sVar.windowType; } set { sVar.windowType = value; } }

    public static float settings_bgmLevel { get { return sVar.bgm; } set { sVar.bgm = value; } }
    
    public static float settings_sfxLevel { get { return sVar.sfx; } set { sVar.sfx = value; } }
    public static bool settings_textSound { get { return sVar.textSound; } set { sVar.textSound = value; } }

    public static bool settings_blurEnabled { get { return sVar.blurEnabled; } set { sVar.blurEnabled = value; } }
    public static int settings_blurAmount { get { return sVar.blurAmount; } set { sVar.blurAmount = value; } }

    public static bool settings_controllerEnabled { get { return sVar.controllerEnabled; } set { sVar.controllerEnabled = value; } }
    public static int settings_cursorType { get { return sVar.cursorType; } set { sVar.cursorType = value; } }

    public static Language settings_language { get { return sVar.lang; } set { sVar.lang = value; } }

    #endregion

    public static void ReloadMonsterData()
    {
        for (int i = 0; i < playerParty.Count; i++)
        {
            playerParty[i].CalculateAllStats(false);
        }
    }
}
