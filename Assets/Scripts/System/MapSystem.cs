﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GameDatabase;
using System.Xml;
using System.Xml.Serialization;

namespace MonsterSystem
{
    [Serializable]
    public class MapData
    {
        [XmlAttribute("id")]
        public Maps id;
        [XmlElement("defaulttier")]
        public MapTier defaultTier;
        [XmlElement("hidelighting")]
        public bool hideLighting;
        [XmlElement("isdungeon")]
        public bool isDungeon;
        [XmlElement("background")]
        public string backgroundID;

        [XmlArray("availablemons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> availableMons;

        /*[XmlArray("morningmons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> morningMons;

        [XmlArray("nightmons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> nightMons;*/

        [XmlArray("bossmons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> bossMons;

        /*[XmlArray("cavemons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> caveMons;

        [XmlArray("fishinglightmons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> fishingLightMons;

        [XmlArray("fishingheavymons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> fishingHeavyMons;

        [XmlArray("fishingmons")]
        [XmlArrayItem("mon")]
        public List<MonsterSpawnInfo> fishingMons;*/


        [XmlArray("availableitems")]
        [XmlArrayItem("item")]
        public List<ItemSpawnInfo> availableItems;

        [XmlArray("craftinga")]
        [XmlArrayItem("item")]
        public List<TierCraftingItems> craftingA;

        [XmlArray("craftingb")]
        [XmlArrayItem("item")]
        public List<TierCraftingItems> craftingB;

        [XmlArray("craftingc")]
        [XmlArrayItem("item")]
        public List<TierCraftingItems> craftingC;

        [XmlArray("craftingd")]
        [XmlArrayItem("item")]
        public List<TierCraftingItems> craftingD;

        [XmlArray("craftinge")]
        [XmlArrayItem("item")]
        public List<TierCraftingItems> craftingE;


        public MapData(Maps _id, bool _hideLighting = false, bool _isDungeon = false, MapTier _defaultTier = 0, MapTier _tierVariation = 0)
        {
            id = _id;

            hideLighting = _hideLighting;

            availableMons = new List<MonsterSpawnInfo>();
            //morningMons = new List<MonsterSpawnInfo>();
            //nightMons = new List<MonsterSpawnInfo>();
            bossMons = new List<MonsterSpawnInfo>();

            defaultTier = _defaultTier;
            //tierVariation = _tierVariation;
            isDungeon = _isDungeon;
        }

        private MapData()
        {

        }

        public MapData AvailableMonsters(params MonsterSpawnInfo[] _mons)
        {
            availableMons = _mons.ToList();
            return this;
        }

        /*public MapData MorningMons(params MonsterSpawnInfo[] _mons)
        {
            morningMons = _mons.ToList();
            return this;
        }

        public MapData NightMonsters(params MonsterSpawnInfo[] _mons)
        {
            nightMons = _mons.ToList();
            return this;
        }*/

        public MapData BossMonsters(params MonsterSpawnInfo[] _mons)
        {
            bossMons = _mons.ToList();
            return this;
        }

        public MapData AvailableItems(params ItemSpawnInfo[] _items)
        {
            availableItems = _items.ToList();
            return this;
        }

        public List<TierCraftingItems> GetCraftingList(MapTier _tier)
        {
            switch (_tier)
            {
                case MapTier.A:
                    return craftingA;
                case MapTier.B:
                    return craftingB;
                case MapTier.C:
                    return craftingC;
                case MapTier.D:
                    return craftingD;
                case MapTier.E:
                    return craftingE;
                default:
                    return null;
            }
        }
    }

    [System.Serializable]
    public class TierInfo
    {
        public Maps map;
        public int uses;
        public MapTier tier;

        public TierInfo(Maps _map, int _uses, MapTier _tier)
        {
            map = _map;
            uses = _uses;
            tier = _tier;
        }

        TierInfo() { }
    }

    [System.Serializable]
    public class TierCraftingItems
    {
        [XmlAttribute("id")]
        public Items item;
        public int quantity;

        public TierCraftingItems(Items _item, int _quantity)
        {
            item = _item;
            quantity = _quantity;
        }

        TierCraftingItems() { }
    }

    public class MonsterSpawnInfo
    {
        [XmlElement("mon")]
        public MonsterIdentifier mon;
        public float spawnChance;
        public MapTier tier;
        public EncounterType type;

        public MonsterSpawnInfo(Monsters _mon, float _spawnChance, MapTier _tier, EncounterType _type, int _form = 0)
        {
            mon = new MonsterIdentifier(_mon, _form);
            spawnChance = _spawnChance;
            tier = _tier;
            type = _type;
        }

        MonsterSpawnInfo() { }
    }

    public class ItemSpawnInfo
    {
        [XmlAttribute("id")]
        public Items item;
        public float spawnChance;
        public MapTier tier;

        public ItemSpawnInfo(Items _item, float _spawnChance, MapTier _tier)
        {
            item = _item;
            spawnChance = _spawnChance;
            tier = _tier;
        }

        ItemSpawnInfo() { }
    }

    public static class MapSystem
    {
        public static int GetMapDatabaseIndex(Maps _map, MapTier tier = MapTier.A)
        {
            for (int i = 0; i < MapDatabase.db.Count; i++)
            {
                if (MapDatabase.db[i].id == _map)// && MapDatabase.db[i].tierVariation == tier)
                {
                    return i;
                }
            }

            return -1;
        }

        public static string GetMapName(Maps _map) => SystemVariables.trans[_map.ToString() + "_NAME"];
        public static string GetMapDesc(Maps _map) => SystemVariables.trans[_map.ToString() + "_DESC"];

        public static MapData GetMapDatabaseData(Maps _map, MapTier tier = MapTier.A)
        {
            return MapDatabase.db[GetMapDatabaseIndex(_map, tier)];
        }

        public static int GetTierInfoIndex(Maps _map, MapTier _tier)
        {
            for (int i = 0; i < SaveSystem.playerTiers.Count; i++)
            {
                if (SaveSystem.playerTiers[i].tier == _tier && SaveSystem.playerTiers[i].map == _map)
                {
                    return i;
                }
            }

            return -1;
        }
    }

    public enum MapTier
    {
        A = 0,
        B = 1,
        C = 2,
        D = 3,
        E = 4,
        ALL = 5
    }

    public enum EncounterType
    {
        NONE = 0,
        DAY = 1,
        MORNING = 2,
        NIGHT = 3,
        CAVE = 4,
        FISHING = 5,
        FISHINGH = 6,
        FISHINGL = 7,
        BOSS = 8
    }
}
