﻿using UnityEngine;
using UnityEditor;
using GameDatabase;
using System;
using Random = UnityEngine.Random;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace MonsterSystem
{
    [Serializable]
    public class ItemData
    {
        [XmlAttribute("id")]
        public Items id;
        [XmlElement("secid")]
        public int secId;

        [XmlElement("pocket")]
        public MenuPockets pocket;

        // Max by default        
        [XmlElement("stackable")]
        public bool stackable = true;
        [XmlElement("stackamount")]
        public int stackAmount = SystemVariables.maxItemStack;

        [XmlElement("price")]
        public int price;

        [XmlElement("holdfunc")]
        public ItemEffects_Hold holdFunction;
        [XmlElement("menufunc")]
        public ItemEffects_Menu menuFunction;
        [XmlElement("battlefunc")]
        public ItemEffects_Battle battleFunction;

        [XmlElement("intparam")]
        public int intParam;
        [XmlElement("floatparam")]
        public float floatParam;
        [XmlElement("uses")]
        public int uses;

        [XmlElement("param")]
        public object param;

        public ItemData(Items _id, MenuPockets _pocket,
            ItemEffects_Menu _menuFunction, ItemEffects_Battle _battleFunction, ItemEffects_Hold _holdFunction,
            int _price, int _secId = 0, int _intParam = 0, float _floatParam = 0, object _param = null)
        {
            id = _id;
            secId = _secId;

            pocket = _pocket;

            menuFunction = _menuFunction;
            battleFunction = _battleFunction;
            holdFunction = _holdFunction;

            price = _price;

            intParam = _intParam;
            floatParam = _floatParam;
            param = _param;
            uses = -1;
        }

        private ItemData()
        {

        }

        public ItemData StackAmount(int _stackAmount)
        {
            stackAmount = _stackAmount;
            return this;
        }

        public ItemData NotStackable()
        {
            stackable = false;
            return this;
        }

        public ItemData SecondaryID(int _id)
        {
            secId = _id;
            return this;
        }

        public ItemData Uses(int _uses)
        {
            uses = _uses;
            return this;
        }
    }

    [Serializable]
    public class Item
    {
        public Items id;
        public int secID;
        public int currentQuantity = 1;
        public int currentUses = -1;

        public object uniqueParam;

        public Item(Items _id, int _secID = 0)
        {
            id = _id;
            secID = _secID;
            ItemData tempData = ItemSystem.GetItemDatabaseData(id, secID);

            if (tempData.uses != -1)
            {
                currentUses = tempData.uses;
            }
        }

        public Item (Items _id, object _uniqueParam, int _secID = 0)
        {
            id = _id;
            secID = _secID;
            ItemData tempData = ItemSystem.GetItemDatabaseData(id, secID);

            if (tempData.uses != -1)
            {
                currentUses = tempData.uses;
            }

            uniqueParam = _uniqueParam;
        }

        Item()
        {

        }

        public ItemData GetData()
        {
            return id == Items.NONE? null : ItemSystem.GetItemDatabaseData(id, secID);
        }
    }

    public static class ItemSystem
    {
        public static int GetItemDatabaseIndex(Items _item, int _secID = 0)
        {
            for (int i = 0; i < ItemDatabase.db.Count; i++)
            {
                if (ItemDatabase.db[i].id == _item && ItemDatabase.db[i].secId == _secID)
                {
                    return i;
                }
            }

            return -1;
        }

        public static ItemData GetItemDatabaseData(Items _item, int _secID = 0)
        {
            return ItemDatabase.db[GetItemDatabaseIndex(_item, _secID)];
        }

        public static void ObtainItem(Items _item, int quantity = 1, int _secID = 0, bool displayMessage = true, int msgType = 0)
        {

            Item addItem = new Item(_item, _secID);
            
            ObtainItem(addItem, quantity, _secID, displayMessage, msgType);
        }

        public static void ObtainItem(Item _item, int quantity = 1, int _secID = 0, bool displayMessage = true, int msgType = 0)
        {
            if (displayMessage && (InterfaceSystem.inMenu || InterfaceSystem.isPaused)) return;

            _item.currentQuantity = quantity;

            ItemData tempData = GetItemDatabaseData(_item.id, _secID);

            List<Item> splitItems = new List<Item>();

            bool added = false;
            int index = (int)tempData.pocket;
            int uses = _item.currentUses;
            
            if (tempData.stackable)
            {
                for (int i = 0; i < SaveSystem.GetInventoryList(index).Count; i++) // Iterate through items in Pocket
                {
                    Item currentItem = SaveSystem.GetInventoryList(index)[i]; // Current item in iteration
                    ItemData currentData = GetItemDatabaseData(currentItem.id, currentItem.secID);

                    if (currentItem.id == _item.id && currentItem.secID == _item.secID && currentItem.currentUses == tempData.uses && Equals(currentItem.uniqueParam, _item.uniqueParam)) // If current item is equal to item we're trying to add
                    {

                        if (currentItem.currentQuantity < currentData.stackAmount) // If current item quantity is less than it's max stack amount
                        {
                            if (currentItem.currentQuantity + quantity > currentData.stackAmount) // If adding to the current item's quantity will push it over the max stack amount
                            {
                                _item.currentQuantity = _item.currentQuantity + currentItem.currentQuantity - currentData.stackAmount; // Item we're trying to add gets its quantity set to the excess quantity count from adding it to the current item
                                currentItem.currentQuantity += currentData.stackAmount - currentItem.currentQuantity; // Current item's quantity gets max'd out
                                break;
                            }
                            else
                            {
                                currentItem.currentQuantity += quantity; // Current item's quantity increases
                                added = true;
                                break;
                            }
                        }
                    }
                }

                if (!added)
                {
                    // Add item to list X amount of times
                    for (int i = 0; i < Mathf.FloorToInt(_item.currentQuantity / tempData.stackAmount); i++)
                    {
                        Item tempItem = _item.uniqueParam == null ? new Item(_item.id, _item.secID) : new Item(_item.id, _item.uniqueParam, _item.secID);
                        tempItem.currentQuantity = tempData.stackAmount;
                        tempItem.currentUses = uses;
                        splitItems.Add(tempItem);
                    }

                    if (_item.currentQuantity % tempData.stackAmount != 0)
                    {
                        Item tempItem = _item.uniqueParam == null? new Item(_item.id, _item.secID) : new Item(_item.id, _item.uniqueParam, _item.secID);
                        tempItem.currentQuantity = _item.currentQuantity % tempData.stackAmount;
                        tempItem.currentUses = uses;
                        splitItems.Add(tempItem);
                    }
                }
            } else
            {
                splitItems.Add(_item);
            }

            for (int i = 0; i < splitItems.Count; i++)
            {
                SaveSystem.GetInventoryList(index).Add(splitItems[i]);
            }

            if (displayMessage)
            {
                InterfaceSystem.DisplayItemMessage(null, _item);

                /*DialogueHandler itemMSG = DialogueHandler.instance;
                string itemName = GetItemName(_item);
                if (quantity > 1)
                {
                    if (itemName.Substring(itemName.Length - 1) == "y")
                    {
                        itemName = itemName.Remove(itemName.Length - 1);
                        itemName += "ies";
                    } else
                    {
                        itemName += "s";
                    }
                }

                string getType = "";

                switch (msgType)
                {
                    case 0:
                    default:
                        getType = "obtain";
                        break;
                    case 1:
                        getType = "pickup";
                        break;
                    case 2:
                        getType = "found";
                        break;
                    case 3:
                        getType = "received";
                        break;
                }

                itemMSG.AddMessage(string.Format(DialogueSystem.LoadMsg("item", "get", "system"), DialogueSystem.LoadMsg("item", getType, "system"), quantity.ToString() + " ", itemName, _item.GetData().menuFunction == ItemEffects_Menu.EQUIP? " Capsule" : ""));
                itemMSG.Show();*/
            }
        }

        public static string GetItemName(Item _item)
        {
            string str = "";

            if (_item.GetData().pocket == MenuPockets.CAPSULES)
            {
                str = SystemVariables.trans[((Skills)_item.uniqueParam).ToString() + "_NAME"];
            } else
            {
                str = SystemVariables.trans[_item.id.ToString() + "_NAME"];

                if (_item.currentUses != -1)
                {
                    str += string.Format(" <m=default><c=76a05a>[{0}/{1}]</c></m>", _item.currentUses, _item.GetData().uses);
                }
            }
            
            return str;
        }

        public static string GetItemDescription(Item _item) => SystemVariables.trans[_item.id.ToString() + "_DESC"];
        public static string GetItemDescription(Items _item) => GetItemDescription(new Item(_item));

        public static string GetItemName(Items _item)
        {
            return GetItemName(new Item(_item));
        }

        public static Sprite GetItemSprite(Item _item)
        {
            return Resources.Load<Sprite>("Items/" + _item.id.ToString());
        }

        public static Sprite GetItemSprite(Items _item)
        {
            return GetItemSprite(new Item(_item));
        }

        public static bool IsClothing(Item _item)
        {
            return GameUtility.In(_item.GetData().menuFunction, ItemEffects_Menu.WEARBOTTOM, ItemEffects_Menu.WEARTOP);
        }

        public static bool IsClothing(Items _item)
        {
            return IsClothing(new Item(_item));
        }

        public static bool IsCapsule(Item _item)
        {
            return _item.GetData().pocket == MenuPockets.CAPSULES;
        }

        public static bool IsCapsule(Items _item)
        {
            return IsCapsule(new Item(_item));
        }

        public static Sprite GetClothingSprite(Item _item)
        {
            string dir = "";
            if (_item.GetData().menuFunction == ItemEffects_Menu.WEARTOP)
            {
                dir = "Tops";
            } else
            {
                dir = "Bottoms";
            }

            return Resources.Load<Sprite>("Apparel/" + dir + "/" + _item.id);
        }

        public static string GetInternalName(Item _item)
        {
            return (_item.id).ToString();
        }

        public static bool HasItem(Items _item)
        {
            for (int i = 0; i < 5; i++)
            {
                foreach (Item item in SaveSystem.GetInventoryList(i))
                {
                    if (item.id == _item)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool HasItems(List<Items> _items)
        {
            int count = 0;

            foreach (Items item in _items)
            {
                count += HasItem(item)? 1 : 0;
            }

            return count == _items.Count;
        }

        public static bool HasItems(List<TierCraftingItems> _items)
        {
            int count = 0;

            foreach (TierCraftingItems _tcitems in _items)
            {
                count += HasItem(_tcitems.item)? 1 : 0;
            }

            return count > 0;
        }

        public static bool HasItems(params Items[] _items)
        {
            List<Items> itemsList = new List<Items>(_items);

            return HasItems(itemsList);
        }

        public static int QuantityCheck(Items _item)
        {
            int quantity = 0;

            for (int i = 0; i < 5; i++)
            {
                foreach (Item item in SaveSystem.GetInventoryList(i))
                {
                    if (item.id == _item)
                    {
                        quantity += item.currentQuantity;
                    }
                }
            }

            return quantity;
        }

        public static void TossItem(Items _item, int amt)
        {
            if (!HasItem(_item)) Debug.LogWarning("Player does not have " + _item);

            for (int i = 0; i < 5; i++)
            {
                for (int t = 0; t < SaveSystem.GetInventoryList(i).Count; t++)
                {
                    Item item = SaveSystem.GetInventoryList(i)[t];
                    if (item.id == _item && item.currentQuantity >= amt)
                    {
                        item.currentQuantity -= amt;
                        SaveSystem.GetInventoryList(i)[t] = item;

                        if (item.currentQuantity == 0)
                        {
                            SaveSystem.GetInventoryList(i).RemoveAt(t);
                        }
                    }
                }
            }
        }
    }

    public enum ItemEffects_Menu
    {
        NONE = 0,
        HEALING,
        EQUIP,
        WEARTOP,
        WEARBOTTOM,
        VOUCHER
    }

    public enum ItemEffects_Battle
    {
        NONE = 0,
        HEALING = 1,
        STATBOOST = 2,
        TAME = 3,
    }

    public enum ItemEffects_Hold
    {
        NONE = 0,
        HEALING = 1,
        DAMAGECALC = 2,
    }
}