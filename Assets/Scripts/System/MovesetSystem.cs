﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using GameDatabase;

namespace MonsterSystem
{
    [Serializable]
    public class Moveset
    {
        [XmlElement("id")]
        public MonsterIdentifier mon;
        [XmlArray("available")]
        [XmlArrayItem("skill")]
        public List<Skills> available;

        public Moveset(Monsters _mon, int _form, params Skills[] _avail)
        {
            mon.id = _mon;
            mon.formId = _form;
            available = _avail.ToList();
        }

        private Moveset()
        {

        }
    }

    public static class MovesetSystem
    {
        public static Moveset GetMonsterMoveset(MonsterIdentifier _mon)
        {
            foreach (Moveset ms in MovesetDatabase.db)
            {
                if (ms.mon == _mon)
                {
                    return ms;
                }
            }

            return null;
        }
    }
}