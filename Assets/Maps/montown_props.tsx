<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="montown_props" tilewidth="144" tileheight="128" tilecount="12" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="96" height="112" source="Town/Props/postoffice.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="96" height="96"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <image width="144" height="128" source="Town/Props/townhall.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="144" height="112"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="16" height="48" source="Town/Props/lamp.png"/>
  <objectgroup draworder="index">
   <object id="3" x="0" y="32" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="32" height="32" source="Town/Props/bench.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <image width="48" height="32" source="Town/Props/bush.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="47" height="16"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <image width="112" height="96" source="Town/Props/daycare.png"/>
  <objectgroup draworder="index">
   <object id="2" x="0" y="96">
    <polygon points="0,0 0,-74 112,-74 112,0 48,0 48,-16 32,-16 32,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="6">
  <image width="144" height="128" source="Town/Props/gatehouse.png"/>
  <objectgroup draworder="index">
   <object id="2" x="0" y="128">
    <polygon points="0,0 0,-96 144,-96 144,0 80,0 80,-32 64,-32 64,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <image width="48" height="64" source="Town/Props/tree.png"/>
 </tile>
 <tile id="8">
  <image width="32" height="32" source="Town/Props/bigpot.png"/>
 </tile>
 <tile id="9">
  <image width="32" height="32" source="Town/Props/box.png"/>
 </tile>
 <tile id="10">
  <image width="16" height="16" source="Town/Props/smallpot.png"/>
 </tile>
 <tile id="11">
  <image width="80" height="112" source="Town/Props/home.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="112">
    <polygon points="0,0 0,-80 80,-80 80,0 48,0 48,-32 32,-32 32,0"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
