<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="tiles_playerhome_props" tilewidth="48" tileheight="48" tilecount="1" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="48" height="48" source="Props/couch.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="48" height="32"/>
  </objectgroup>
 </tile>
</tileset>
