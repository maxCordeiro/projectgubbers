<?xml version="1.0" encoding="UTF-8"?>
<tileset name="ow_tiles_anim" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <image source="ow_tiles_anim.png" width="256" height="256"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="300"/>
   <frame tileid="1" duration="300"/>
   <frame tileid="2" duration="300"/>
   <frame tileid="3" duration="300"/>
   <frame tileid="4" duration="300"/>
   <frame tileid="5" duration="300"/>
  </animation>
 </tile>
 <tile id="6">
  <animation>
   <frame tileid="6" duration="300"/>
   <frame tileid="7" duration="300"/>
   <frame tileid="8" duration="300"/>
   <frame tileid="9" duration="300"/>
   <frame tileid="10" duration="300"/>
   <frame tileid="11" duration="300"/>
  </animation>
 </tile>
 <tile id="16">
  <animation>
   <frame tileid="19" duration="300"/>
   <frame tileid="20" duration="300"/>
   <frame tileid="21" duration="300"/>
   <frame tileid="16" duration="300"/>
   <frame tileid="17" duration="300"/>
   <frame tileid="18" duration="300"/>
  </animation>
 </tile>
 <tile id="22">
  <animation>
   <frame tileid="25" duration="300"/>
   <frame tileid="26" duration="300"/>
   <frame tileid="27" duration="300"/>
   <frame tileid="22" duration="300"/>
   <frame tileid="23" duration="300"/>
   <frame tileid="24" duration="300"/>
  </animation>
 </tile>
 <tile id="32">
  <animation>
   <frame tileid="32" duration="500"/>
   <frame tileid="33" duration="500"/>
   <frame tileid="34" duration="500"/>
  </animation>
 </tile>
 <tile id="35">
  <animation>
   <frame tileid="35" duration="500"/>
   <frame tileid="36" duration="500"/>
   <frame tileid="37" duration="500"/>
  </animation>
 </tile>
 <tile id="48">
  <animation>
   <frame tileid="48" duration="500"/>
   <frame tileid="49" duration="500"/>
   <frame tileid="50" duration="500"/>
  </animation>
 </tile>
 <tile id="51">
  <animation>
   <frame tileid="51" duration="500"/>
   <frame tileid="52" duration="500"/>
   <frame tileid="53" duration="500"/>
  </animation>
 </tile>
 <tile id="64">
  <animation>
   <frame tileid="64" duration="300"/>
   <frame tileid="65" duration="300"/>
   <frame tileid="66" duration="300"/>
   <frame tileid="67" duration="300"/>
   <frame tileid="68" duration="300"/>
   <frame tileid="69" duration="300"/>
   <frame tileid="70" duration="300"/>
   <frame tileid="71" duration="300"/>
   <frame tileid="72" duration="300"/>
  </animation>
 </tile>
 <tile id="80">
  <animation>
   <frame tileid="80" duration="300"/>
   <frame tileid="81" duration="300"/>
   <frame tileid="82" duration="300"/>
   <frame tileid="83" duration="300"/>
   <frame tileid="84" duration="300"/>
   <frame tileid="85" duration="300"/>
   <frame tileid="86" duration="300"/>
   <frame tileid="87" duration="300"/>
   <frame tileid="88" duration="300"/>
  </animation>
 </tile>
 <tile id="96">
  <animation>
   <frame tileid="96" duration="300"/>
   <frame tileid="97" duration="300"/>
   <frame tileid="98" duration="300"/>
   <frame tileid="99" duration="300"/>
   <frame tileid="100" duration="300"/>
   <frame tileid="101" duration="300"/>
   <frame tileid="102" duration="300"/>
   <frame tileid="103" duration="300"/>
   <frame tileid="104" duration="300"/>
  </animation>
 </tile>
 <tile id="112">
  <animation>
   <frame tileid="112" duration="300"/>
   <frame tileid="113" duration="300"/>
   <frame tileid="114" duration="300"/>
   <frame tileid="115" duration="300"/>
   <frame tileid="116" duration="300"/>
   <frame tileid="117" duration="300"/>
   <frame tileid="118" duration="300"/>
   <frame tileid="119" duration="300"/>
   <frame tileid="120" duration="300"/>
  </animation>
 </tile>
 <tile id="128">
  <animation>
   <frame tileid="128" duration="300"/>
   <frame tileid="129" duration="300"/>
   <frame tileid="130" duration="300"/>
   <frame tileid="131" duration="300"/>
   <frame tileid="132" duration="300"/>
   <frame tileid="133" duration="300"/>
   <frame tileid="134" duration="300"/>
   <frame tileid="135" duration="300"/>
   <frame tileid="136" duration="300"/>
  </animation>
 </tile>
 <tile id="144">
  <animation>
   <frame tileid="144" duration="150"/>
   <frame tileid="145" duration="150"/>
   <frame tileid="146" duration="150"/>
   <frame tileid="147" duration="150"/>
   <frame tileid="148" duration="150"/>
   <frame tileid="149" duration="150"/>
   <frame tileid="150" duration="150"/>
   <frame tileid="151" duration="150"/>
   <frame tileid="152" duration="150"/>
   <frame tileid="160" duration="150"/>
   <frame tileid="161" duration="150"/>
   <frame tileid="162" duration="150"/>
   <frame tileid="163" duration="150"/>
   <frame tileid="164" duration="150"/>
   <frame tileid="165" duration="150"/>
   <frame tileid="166" duration="150"/>
  </animation>
 </tile>
 <tile id="167">
  <animation>
   <frame tileid="167" duration="420"/>
   <frame tileid="168" duration="420"/>
   <frame tileid="176" duration="420"/>
   <frame tileid="177" duration="420"/>
   <frame tileid="178" duration="420"/>
   <frame tileid="179" duration="420"/>
   <frame tileid="180" duration="420"/>
   <frame tileid="181" duration="420"/>
   <frame tileid="182" duration="420"/>
   <frame tileid="183" duration="420"/>
   <frame tileid="184" duration="420"/>
   <frame tileid="192" duration="420"/>
   <frame tileid="193" duration="420"/>
   <frame tileid="194" duration="420"/>
   <frame tileid="195" duration="420"/>
   <frame tileid="196" duration="420"/>
  </animation>
 </tile>
 <tile id="208">
  <animation>
   <frame tileid="208" duration="300"/>
   <frame tileid="209" duration="300"/>
   <frame tileid="210" duration="300"/>
   <frame tileid="211" duration="300"/>
   <frame tileid="212" duration="300"/>
   <frame tileid="213" duration="300"/>
   <frame tileid="214" duration="300"/>
   <frame tileid="215" duration="300"/>
   <frame tileid="216" duration="300"/>
  </animation>
 </tile>
 <tile id="224">
  <animation>
   <frame tileid="224" duration="300"/>
   <frame tileid="225" duration="300"/>
   <frame tileid="226" duration="300"/>
   <frame tileid="227" duration="300"/>
   <frame tileid="228" duration="300"/>
   <frame tileid="229" duration="300"/>
   <frame tileid="230" duration="300"/>
   <frame tileid="231" duration="300"/>
   <frame tileid="232" duration="300"/>
  </animation>
 </tile>
 <tile id="240">
  <animation>
   <frame tileid="240" duration="300"/>
   <frame tileid="241" duration="300"/>
   <frame tileid="242" duration="300"/>
   <frame tileid="243" duration="300"/>
   <frame tileid="244" duration="300"/>
   <frame tileid="245" duration="300"/>
   <frame tileid="246" duration="300"/>
   <frame tileid="247" duration="300"/>
   <frame tileid="248" duration="300"/>
  </animation>
 </tile>
</tileset>
