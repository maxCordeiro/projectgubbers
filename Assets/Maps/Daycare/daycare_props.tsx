<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="daycare_props" tilewidth="32" tileheight="64" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="16" height="32" source="Props/bush.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <image width="32" height="64" source="Props/eggmachine.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="32" width="32" height="32"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="16" height="32" source="Props/counter.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="3">
  <image width="16" height="32" source="Props/countermat.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
