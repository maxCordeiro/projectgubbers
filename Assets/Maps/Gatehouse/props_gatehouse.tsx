<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.3" name="props_gatehouse" tilewidth="96" tileheight="48" tilecount="4" columns="0">
 <grid orientation="orthogonal" width="1" height="1"/>
 <tile id="0">
  <image width="64" height="48" source="Props/machine.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="63" height="32"/>
  </objectgroup>
 </tile>
 <tile id="1">
  <properties>
   <property name="unity:SortingLayer" value="OnGround"/>
   <property name="unity:layer" value="Collision"/>
  </properties>
  <image width="16" height="32" source="Props/plant.png"/>
  <objectgroup draworder="index">
   <object id="3" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <image width="16" height="16" source="Props/deskend.png"/>
 </tile>
 <tile id="3">
  <image width="96" height="32" source="Props/helpdesk.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="96" height="16"/>
  </objectgroup>
 </tile>
</tileset>
