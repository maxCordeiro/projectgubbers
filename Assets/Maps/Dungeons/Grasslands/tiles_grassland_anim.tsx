<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.4" name="tiles_grassland_anim" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <image source="tiles_grassland_anim.png" width="256" height="256"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="300"/>
   <frame tileid="1" duration="300"/>
   <frame tileid="2" duration="300"/>
   <frame tileid="3" duration="300"/>
   <frame tileid="4" duration="300"/>
   <frame tileid="5" duration="300"/>
  </animation>
 </tile>
 <tile id="6">
  <animation>
   <frame tileid="6" duration="800"/>
   <frame tileid="7" duration="400"/>
   <frame tileid="8" duration="400"/>
   <frame tileid="9" duration="400"/>
  </animation>
 </tile>
 <tile id="16">
  <animation>
   <frame tileid="16" duration="300"/>
   <frame tileid="17" duration="300"/>
   <frame tileid="18" duration="300"/>
   <frame tileid="19" duration="300"/>
   <frame tileid="20" duration="300"/>
   <frame tileid="21" duration="300"/>
  </animation>
 </tile>
 <tile id="32">
  <animation>
   <frame tileid="32" duration="300"/>
   <frame tileid="33" duration="300"/>
   <frame tileid="34" duration="300"/>
  </animation>
 </tile>
 <tile id="35">
  <animation>
   <frame tileid="35" duration="300"/>
   <frame tileid="36" duration="300"/>
   <frame tileid="37" duration="300"/>
  </animation>
 </tile>
 <tile id="48">
  <animation>
   <frame tileid="48" duration="300"/>
   <frame tileid="49" duration="300"/>
   <frame tileid="50" duration="300"/>
  </animation>
 </tile>
 <tile id="51">
  <animation>
   <frame tileid="51" duration="300"/>
   <frame tileid="52" duration="300"/>
   <frame tileid="53" duration="300"/>
  </animation>
 </tile>
 <tile id="64">
  <animation>
   <frame tileid="64" duration="300"/>
   <frame tileid="65" duration="300"/>
   <frame tileid="66" duration="300"/>
   <frame tileid="67" duration="300"/>
   <frame tileid="68" duration="300"/>
   <frame tileid="69" duration="300"/>
   <frame tileid="70" duration="300"/>
   <frame tileid="71" duration="300"/>
   <frame tileid="72" duration="300"/>
  </animation>
 </tile>
 <tile id="80">
  <animation>
   <frame tileid="80" duration="300"/>
   <frame tileid="81" duration="300"/>
   <frame tileid="82" duration="300"/>
   <frame tileid="83" duration="300"/>
   <frame tileid="84" duration="300"/>
   <frame tileid="85" duration="300"/>
   <frame tileid="86" duration="300"/>
   <frame tileid="87" duration="300"/>
   <frame tileid="88" duration="300"/>
  </animation>
 </tile>
 <tile id="96">
  <animation>
   <frame tileid="96" duration="300"/>
   <frame tileid="97" duration="300"/>
   <frame tileid="98" duration="300"/>
   <frame tileid="99" duration="300"/>
   <frame tileid="100" duration="300"/>
   <frame tileid="101" duration="300"/>
   <frame tileid="102" duration="300"/>
   <frame tileid="103" duration="300"/>
   <frame tileid="104" duration="300"/>
  </animation>
 </tile>
 <tile id="112">
  <animation>
   <frame tileid="112" duration="300"/>
   <frame tileid="113" duration="300"/>
   <frame tileid="114" duration="300"/>
   <frame tileid="115" duration="300"/>
   <frame tileid="116" duration="300"/>
   <frame tileid="117" duration="300"/>
   <frame tileid="118" duration="300"/>
   <frame tileid="119" duration="300"/>
   <frame tileid="120" duration="300"/>
  </animation>
 </tile>
 <tile id="128">
  <animation>
   <frame tileid="128" duration="300"/>
   <frame tileid="129" duration="300"/>
   <frame tileid="130" duration="300"/>
   <frame tileid="131" duration="300"/>
   <frame tileid="132" duration="300"/>
   <frame tileid="133" duration="300"/>
   <frame tileid="134" duration="300"/>
   <frame tileid="135" duration="300"/>
   <frame tileid="136" duration="300"/>
  </animation>
 </tile>
</tileset>
