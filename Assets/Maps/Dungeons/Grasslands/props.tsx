<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="props" tilewidth="96" tileheight="128" tilecount="22" columns="6">
 <grid orientation="orthogonal" width="16" height="16"/>
 <tile id="0">
  <image width="16" height="32" source="Props/char.png"/>
 </tile>
 <tile id="1">
  <image width="16" height="16" source="Props/grass-stub.png"/>
 </tile>
 <tile id="2">
  <image width="16" height="16" source="Props/grass-stub2.png"/>
 </tile>
 <tile id="3">
  <image width="16" height="16" source="Props/minibush.png"/>
 </tile>
 <tile id="4">
  <image width="32" height="32" source="Props/rock1.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <image width="32" height="32" source="Props/bigbush.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="6">
  <image width="48" height="64" source="Props/treeprop.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="48" height="48"/>
  </objectgroup>
 </tile>
 <tile id="7">
  <image width="96" height="128" source="Props/bigtreeprop.png"/>
  <objectgroup draworder="index">
   <object id="3" x="0" y="32" width="96" height="96"/>
  </objectgroup>
 </tile>
 <tile id="8">
  <image width="96" height="128" source="Props/bigtreedark.png"/>
  <objectgroup draworder="index">
   <object id="2" x="0" y="32" width="96" height="96"/>
  </objectgroup>
 </tile>
 <tile id="9">
  <image width="48" height="64" source="Props/treepropdark.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="48" height="48"/>
  </objectgroup>
 </tile>
 <tile id="10">
  <image width="16" height="32" source="Props/sign.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="12">
  <image width="48" height="48" source="Props/grassbush.png"/>
 </tile>
 <tile id="13">
  <image width="32" height="48" source="Props/grassbush_offset.png"/>
 </tile>
 <tile id="14">
  <image width="32" height="32" source="Props/fence_l.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="15">
  <image width="32" height="32" source="Props/fence_r.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="32" height="16"/>
   <object id="2" x="0" y="16" width="32" height="16"/>
  </objectgroup>
 </tile>
 <tile id="16">
  <image width="16" height="32" source="Props/fence_l_s.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <image width="96" height="128" source="Props/bigtreeprop_gate.png"/>
  <objectgroup draworder="index">
   <object id="2" x="0" y="32" width="96" height="96"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <image width="16" height="32" source="Props/post.png"/>
  <objectgroup draworder="index" id="3">
   <object id="2" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="19">
  <image width="16" height="32" source="Props/post_h_1.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="20">
  <image width="16" height="32" source="Props/post_v_1.png"/>
  <objectgroup draworder="index">
   <object id="1" x="0" y="16" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="21">
  <image width="96" height="128" source="Props/bigtreeprop_1.png"/>
 </tile>
 <tile id="22">
  <image width="48" height="64" source="Props/treeprop_1.png"/>
 </tile>
</tileset>
