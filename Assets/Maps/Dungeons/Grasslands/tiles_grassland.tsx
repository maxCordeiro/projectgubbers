<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.2" name="tiles_grassland" tilewidth="16" tileheight="16" tilecount="380" columns="20">
 <image source="tiles_grassland.png" width="320" height="304"/>
 <terraintypes>
  <terrain name="Tall Grass" tile="-1"/>
  <terrain name="Dirt" tile="75"/>
  <terrain name="Mountain" tile="0"/>
 </terraintypes>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="15" height="16"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="6" terrain=",,,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="7" terrain=",,2,2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="16" height="14"/>
  </objectgroup>
 </tile>
 <tile id="8" terrain=",,2,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="11" terrain=",,,0"/>
 <tile id="12" terrain=",,0,0"/>
 <tile id="13" terrain=",,0,"/>
 <tile id="14" terrain="0,0,0,"/>
 <tile id="15" terrain="0,0,,0"/>
 <tile id="16">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="20">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="21">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="22">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="25" terrain="2,2,2,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="26" terrain=",2,,2">
  <objectgroup draworder="index" id="2">
   <object id="1" x="0" y="0" width="14" height="16"/>
  </objectgroup>
 </tile>
 <tile id="27" terrain="2,2,2,2"/>
 <tile id="28" terrain="2,,2,">
  <objectgroup draworder="index" id="2">
   <object id="1" x="2" y="0" width="14" height="16"/>
  </objectgroup>
 </tile>
 <tile id="31" terrain=",0,,0"/>
 <tile id="32" terrain="0,0,0,0"/>
 <tile id="33" terrain="0,,0,"/>
 <tile id="34" terrain="0,,0,0"/>
 <tile id="35" terrain=",0,0,0"/>
 <tile id="36">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="37">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="40">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="41">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="42">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="45" terrain="2,,2,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="46" terrain=",2,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="47" terrain="2,2,,">
  <objectgroup draworder="index" id="3">
   <object id="1" x="0" y="2" width="16" height="14"/>
  </objectgroup>
 </tile>
 <tile id="48" terrain="2,,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="49" terrain="2,,2,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="50" terrain=",2,2,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="51" terrain=",0,,"/>
 <tile id="52" terrain="0,0,,"/>
 <tile id="53" terrain="0,,,"/>
 <tile id="60">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="61">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="62">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="63" terrain="2,2,,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="64" terrain=",2,,2">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="66">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="67">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="68">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="91" terrain=",,,1"/>
 <tile id="92" terrain=",,1,1"/>
 <tile id="93" terrain=",,1,"/>
 <tile id="94" terrain="1,1,1,"/>
 <tile id="95" terrain="1,1,,1"/>
 <tile id="107">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="108">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="109">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="111" terrain=",1,,1"/>
 <tile id="112" terrain="1,1,1,1"/>
 <tile id="113" terrain="1,,1,"/>
 <tile id="114" terrain="1,,1,1"/>
 <tile id="115" terrain=",1,1,1"/>
 <tile id="116">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="118">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="131" terrain=",1,,"/>
 <tile id="132" terrain="1,1,,"/>
 <tile id="133" terrain="1,,,"/>
 <tile id="136">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="138">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="156">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="158">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="176">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="178">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="196">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="198">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="216">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="218">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="281">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="282">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="283">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="284">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="286">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="301">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="302">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="355">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="357">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="368">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="375">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
 <tile id="377">
  <objectgroup draworder="index">
   <object id="1" x="8" y="0" width="8" height="16"/>
  </objectgroup>
 </tile>
</tileset>
