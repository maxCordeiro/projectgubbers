<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.2" name="tiles_dungeon_woods" tilewidth="16" tileheight="16" tilecount="256" columns="16">
 <image source="tiles_dungeon_woods.png" width="256" height="256"/>
 <terraintypes>
  <terrain name="Sand" tile="0"/>
 </terraintypes>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="6" terrain=",,,0"/>
 <tile id="7" terrain=",,0,0"/>
 <tile id="8" terrain=",,0,"/>
 <tile id="9" terrain="0,0,0,"/>
 <tile id="10" terrain="0,0,,0"/>
 <tile id="16">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="22" terrain=",0,,0"/>
 <tile id="23" terrain="0,0,0,0"/>
 <tile id="24" terrain="0,,0,"/>
 <tile id="25" terrain="0,,0,0"/>
 <tile id="26" terrain=",0,0,0"/>
 <tile id="32">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="33">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="34">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="38" terrain=",0,,"/>
 <tile id="39" terrain="0,0,,"/>
 <tile id="40" terrain="0,,,"/>
 <tile id="48">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="49">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="50">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
