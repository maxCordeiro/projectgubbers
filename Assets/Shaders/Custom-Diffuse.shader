// Unity built-in shader source. Copyright (c) 2016 Unity Technologies. MIT license (see license.txt)

Shader "Sprites/Custom Diffuse"
{
    Properties
    {
        _HairTex ("Hair Texture", 2D) = "white" {}
        _TopTex ("Top Texture", 2D) = "white" {}
        _BotTex ("Bottom Texture", 2D) = "white" {}
        _MainTex ("Base Texture", 2D) = "white" {}
        _Color ("Tint", Color) = (1,1,1,1)
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        [HideInInspector] _RendererColor ("RendererColor", Color) = (1,1,1,1)
        [HideInInspector] _Flip ("Flip", Vector) = (1,1,1,1)
        [PerRendererData] _AlphaTex ("External Alpha", 2D) = "white" {}
        [PerRendererData] _EnableExternalAlpha ("Enable External Alpha", Float) = 0
    }

    SubShader
    {
        Tags
        {
            "Queue"="Transparent"
            "IgnoreProjector"="True"
            "RenderType"="Transparent"
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

        Cull Off
        Lighting Off
        ZWrite Off
        Blend One OneMinusSrcAlpha

        CGPROGRAM
        #pragma surface surf Lambert vertex:vert nofog nolightmap nodynlightmap keepalpha noinstancing
        #pragma multi_compile _ PIXELSNAP_ON
        #pragma multi_compile _ ETC1_EXTERNAL_ALPHA
		#pragma target 3.0

        #include "UnitySprites.cginc"

        struct Input
        {
			float2 uv_HairTex;
			float2 uv_TopTex;
			float2 uv_BotTex;
			float2 uv_MainTex;
            fixed4 color;
        };

        void vert (inout appdata_full v, out Input o)
        {
            v.vertex = UnityFlipSprite(v.vertex, _Flip);

            #if defined(PIXELSNAP_ON)
            v.vertex = UnityPixelSnap (v.vertex);
            #endif

            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.color = v.color * _Color * _RendererColor;
        }

        void surf (Input IN, inout SurfaceOutput o)
        {
            fixed4 hc = SampleSpriteTexture (IN.uv_HairTex);
            fixed4 tc = SampleSpriteTexture (IN.uv_TopTex);
            fixed4 bc = SampleSpriteTexture (IN.uv_BotTex);
            fixed4 c = SampleSpriteTexture (IN.uv_MainTex);
			o.Albedo = hc.rgb + tc.rgb + bc.rgb + c.rgb;
			o.Alpha = hc.a * tc.a * bc.a * c.a;
        }
        ENDCG
    }

Fallback "Transparent/VertexLit"
}
