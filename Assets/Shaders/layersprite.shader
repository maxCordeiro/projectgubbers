// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:1,cusa:True,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:True,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:True,atwp:True,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:1873,x:33229,y:32719,varname:node_1873,prsc:2|emission-1749-OUT,alpha-603-OUT;n:type:ShaderForge.SFN_Tex2d,id:4805,x:31624,y:32158,ptovrint:False,ptlb:layer1,ptin:_layer1,varname:_MainTex_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:True,tagnsco:False,tagnrm:False,tex:6b260fc833cbd4d448986b81d1f645bc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:1086,x:32753,y:32853,cmnt:RGB,varname:node_1086,prsc:2|A-3875-OUT,B-5983-RGB,C-5376-RGB;n:type:ShaderForge.SFN_Color,id:5983,x:32266,y:32826,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_VertexColor,id:5376,x:32357,y:33088,varname:node_5376,prsc:2;n:type:ShaderForge.SFN_Multiply,id:1749,x:32923,y:32831,cmnt:Premultiply Alpha,varname:node_1749,prsc:2|A-1086-OUT,B-603-OUT;n:type:ShaderForge.SFN_Multiply,id:603,x:32753,y:33014,cmnt:A,varname:node_603,prsc:2|A-4805-A,B-5983-A,C-5376-A;n:type:ShaderForge.SFN_Tex2d,id:5554,x:31383,y:32354,ptovrint:False,ptlb:layer2,ptin:_layer2,varname:node_5554,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:14af429d325e45a4e97d537304a95ecf,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:519,x:32034,y:32862,varname:node_519,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Multiply,id:1367,x:31910,y:32151,varname:node_1367,prsc:2|A-4805-RGB,B-5554-RGB;n:type:ShaderForge.SFN_Multiply,id:8980,x:32022,y:32316,varname:node_8980,prsc:2|A-1367-OUT,B-3751-RGB;n:type:ShaderForge.SFN_Multiply,id:3668,x:32205,y:32488,varname:node_3668,prsc:2|A-8980-OUT,B-7694-RGB;n:type:ShaderForge.SFN_Multiply,id:3875,x:32444,y:32675,varname:node_3875,prsc:2|A-3668-OUT,B-9141-RGB;n:type:ShaderForge.SFN_Tex2d,id:3751,x:31401,y:32595,ptovrint:False,ptlb:layer3,ptin:_layer3,varname:node_3751,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:b39e6e4238470e84faf8bdd152ae93fc,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:7694,x:31570,y:32712,ptovrint:False,ptlb:layer4,ptin:_layer4,varname:node_7694,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:d440902fad11e807d00044888d76c639,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:9141,x:31742,y:32794,ptovrint:False,ptlb:layer5,ptin:_layer5,varname:node_9141,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:6d91bc20e38b5c443bc240676aa68c2c,ntxv:0,isnm:False;proporder:4805-5983-5554-3751-7694-9141;pass:END;sub:END;*/

Shader "Shader Forge/layersprite" {
    Properties {
        [PerRendererData]_layer1 ("layer1", 2D) = "white" {}
        _Color ("Color", Color) = (1,1,1,1)
        _layer2 ("layer2", 2D) = "white" {}
        _layer3 ("layer3", 2D) = "white" {}
        _layer4 ("layer4", 2D) = "white" {}
        _layer5 ("layer5", 2D) = "white" {}
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
        [MaterialToggle] PixelSnap ("Pixel snap", Float) = 0
        _Stencil ("Stencil ID", Float) = 0
        _StencilReadMask ("Stencil Read Mask", Float) = 255
        _StencilWriteMask ("Stencil Write Mask", Float) = 255
        _StencilComp ("Stencil Comparison", Float) = 8
        _StencilOp ("Stencil Operation", Float) = 0
        _StencilOpFail ("Stencil Fail Operation", Float) = 0
        _StencilOpZFail ("Stencil Z-Fail Operation", Float) = 0
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
            "CanUseSpriteAtlas"="True"
            "PreviewType"="Plane"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            Stencil {
                Ref [_Stencil]
                ReadMask [_StencilReadMask]
                WriteMask [_StencilWriteMask]
                Comp [_StencilComp]
                Pass [_StencilOp]
                Fail [_StencilOpFail]
                ZFail [_StencilOpZFail]
            }
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform sampler2D _layer1; uniform float4 _layer1_ST;
            uniform float4 _Color;
            uniform sampler2D _layer2; uniform float4 _layer2_ST;
            uniform sampler2D _layer3; uniform float4 _layer3_ST;
            uniform sampler2D _layer4; uniform float4 _layer4_ST;
            uniform sampler2D _layer5; uniform float4 _layer5_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 vertexColor : COLOR;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.vertexColor = v.vertexColor;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
////// Lighting:
////// Emissive:
                float4 _layer1_var = tex2D(_layer1,TRANSFORM_TEX(i.uv0, _layer1));
                float4 _layer2_var = tex2D(_layer2,TRANSFORM_TEX(i.uv0, _layer2));
                float4 _layer3_var = tex2D(_layer3,TRANSFORM_TEX(i.uv0, _layer3));
                float4 _layer4_var = tex2D(_layer4,TRANSFORM_TEX(i.uv0, _layer4));
                float4 _layer5_var = tex2D(_layer5,TRANSFORM_TEX(i.uv0, _layer5));
                float node_603 = (_layer1_var.a*_Color.a*i.vertexColor.a); // A
                float3 emissive = ((((((_layer1_var.rgb*_layer2_var.rgb)*_layer3_var.rgb)*_layer4_var.rgb)*_layer5_var.rgb)*_Color.rgb*i.vertexColor.rgb)*node_603);
                float3 finalColor = emissive;
                return fixed4(finalColor,node_603);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #pragma multi_compile _ PIXELSNAP_ON
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                #ifdef PIXELSNAP_ON
                    o.pos = UnityPixelSnap(o.pos);
                #endif
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
