// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Waving Grass"
{
	Properties
	{
		_MainTex("MainTex", 2D) = "white" {}
		_WorldPositionSpeed("World Position Speed", Range( 0 , 5)) = 2.30463
		_Frequency("Frequency", Range( 0 , 1)) = 0.6027488
		_Y_Wave("Y_Wave", Range( 0 , 3)) = 0.691388
		_X_Wave("X_Wave", Range( 0 , 2)) = 0.9927627
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Custom"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.5
		#pragma surface surf StandardSpecular keepalpha noshadow vertex:vertexDataFunc 
		struct Input
		{
			float3 worldPos;
			float2 uv_texcoord;
		};

		uniform float _X_Wave;
		uniform float _WorldPositionSpeed;
		uniform float _Frequency;
		uniform float _Y_Wave;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;

		void vertexDataFunc( inout appdata_full v, out Input o )
		{
			UNITY_INITIALIZE_OUTPUT( Input, o );
			float3 ase_worldPos = mul( unity_ObjectToWorld, v.vertex );
			float temp_output_38_0 = sin( ( ( ase_worldPos.x + _Time.y ) * _WorldPositionSpeed ) );
			float lerpResult37 = lerp( cos( ( ( v.texcoord.xy.y + _Time.y ) / _Frequency ) ) , 0.0 , ( 1.0 - v.texcoord.xy.y ));
			float temp_output_47_0 = ( _X_Wave * ( temp_output_38_0 * lerpResult37 ) );
			float clampResult43 = clamp( ( 1.0 - v.texcoord.xy.y ) , 0.0 , 1.0 );
			float lerpResult44 = lerp( temp_output_38_0 , 0.0 , clampResult43);
			float4 appendResult49 = (float4(temp_output_47_0 , ( _Y_Wave * lerpResult44 ) , temp_output_47_0 , 0.0));
			float4 waving52 = ( v.color.r * appendResult49 * v.color.b );
			v.vertex.xyz += waving52.xyz;
		}

		void surf( Input i , inout SurfaceOutputStandardSpecular o )
		{
			float2 uv_MainTex = i.uv_texcoord * _MainTex_ST.xy + _MainTex_ST.zw;
			float4 tex2DNode19 = tex2D( _MainTex, uv_MainTex );
			o.Albedo = tex2DNode19.rgb;
			o.Alpha = tex2DNode19.a;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18400
506;419;1494;779;-527.5131;495.7665;1;True;True
Node;AmplifyShaderEditor.TimeNode;26;-1295.702,115.9486;Inherit;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;27;-1324.012,376.049;Inherit;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;28;-1009.059,308.812;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-1054.68,448.5767;Inherit;False;Property;_Frequency;Frequency;3;0;Create;True;0;0;False;0;False;0.6027488;0.318;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;30;-1109.789,-86.16061;Inherit;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;33;-895.3018,196.8479;Inherit;False;Property;_WorldPositionSpeed;World Position Speed;2;0;Create;True;0;0;False;0;False;2.30463;1.07;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;41;-1091.923,-263.4119;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-743.3694,16.61604;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;34;-768.6935,310.0514;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CosOpNode;36;-531.8595,333.8835;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;42;-849.1328,-328.9507;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;29;-726.9838,534.9673;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;32;-519.9435,114.9244;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;37;-314.3912,384.5274;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ClampOpNode;43;-661.4528,-261.9224;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SinOpNode;38;-300.9879,89.60362;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-89.48399,95.56026;Inherit;False;Property;_X_Wave;X_Wave;5;0;Create;True;0;0;False;0;False;0.9927627;0.063;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-122.2523,-343.846;Inherit;False;Property;_Y_Wave;Y_Wave;4;0;Create;True;0;0;False;0;False;0.691388;0;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;39;-53.72998,243.0238;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;44;-150.5499,-194.8944;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;244.1689,-8.705019;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;235.2335,-270.8597;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;51;489.9382,-470.4551;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;49;436.3152,-202.3431;Inherit;True;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;50;823.589,-190.4264;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;2;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;52;977.0095,-136.8037;Inherit;True;waving;-1;True;1;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SamplerNode;19;998.6913,-393.2222;Inherit;True;Property;_MainTex;MainTex;1;0;Create;True;0;0;False;0;False;19;None;8d3f389c6ff866943ad4a433e6d6d1e0;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;18;1525.424,-171.65;Float;False;True;-1;3;ASEMaterialInspector;0;0;StandardSpecular;Waving Grass;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Custom;;Transparent;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;28;0;27;2
WireConnection;28;1;26;2
WireConnection;31;0;30;1
WireConnection;31;1;26;2
WireConnection;34;0;28;0
WireConnection;34;1;35;0
WireConnection;36;0;34;0
WireConnection;42;0;41;2
WireConnection;29;0;27;2
WireConnection;32;0;31;0
WireConnection;32;1;33;0
WireConnection;37;0;36;0
WireConnection;37;2;29;0
WireConnection;43;0;42;0
WireConnection;38;0;32;0
WireConnection;39;0;38;0
WireConnection;39;1;37;0
WireConnection;44;0;38;0
WireConnection;44;2;43;0
WireConnection;47;0;48;0
WireConnection;47;1;39;0
WireConnection;45;0;46;0
WireConnection;45;1;44;0
WireConnection;49;0;47;0
WireConnection;49;1;45;0
WireConnection;49;2;47;0
WireConnection;50;0;51;1
WireConnection;50;1;49;0
WireConnection;50;2;51;3
WireConnection;52;0;50;0
WireConnection;18;0;19;0
WireConnection;18;9;19;4
WireConnection;18;11;52;0
ASEEND*/
//CHKSM=91A6FCF919EA584310FFE8880161A78732B7FF2E