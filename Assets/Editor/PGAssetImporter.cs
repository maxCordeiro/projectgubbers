﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Xml;
using System.Xml.Linq;
using SuperTiled2Unity.Editor;
using System;
using System.Linq;

public class TiledMapSplitter
{
    [MenuItem("Assets/Slice Map", priority = 4)]
    public static void SliceMap()
    {
        string filepath = AssetDatabase.GetAssetPath(Selection.activeObject);
        List<string> ground = new List<string>();

        XDocument doc = XDocument.Load(filepath);

        XElement map = doc.Element("map");

        List<StringReader> layerReaders = new List<StringReader>();
        List<List<string>> layerData = new List<List<string>>();

        int chunkSize = -1, width = -1, height = -1;

        width = map.GetAttributeAs<int>("width");
        height = map.GetAttributeAs<int>("height");

        foreach (XElement xNode in map.Elements())
        {
            if (!xNode.GetAttributeAs<bool>("visible", true))
            {
                continue;
            }

            if (xNode.Name == "properties")
            {
                chunkSize = xNode.Element("property").GetAttributeAs<int>("value", -1);
            }

            if (chunkSize == -1)
            {
                Debug.LogError("Chunk size cannot be -1");
                return;
            }

            if (xNode.Name == "layer")
            {
                StringReader tempReader = new StringReader(xNode.Element("data").Value);
                string inputLine = "";
                
                while ((inputLine = tempReader.ReadLine()) != null)
                {
                    if (inputLine.Trim().Length > 0)
                    {
                        string[] inputArray = inputLine.Split(new char[] { ',' });// inputLine.Split(new string[] { "\\n" }, StringSplitOptions.None);
                        layerData.Add(inputArray.ToList());
                    }
                }
            }
        }

        Debug.Log(string.Format("width: {0}, height: {1}", width, height));

        int chunkCount = height / chunkSize;

        List<List<string>> chunkLists = new List<List<string>>(chunkCount);

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {

            }
        }

        Debug.Log(layerData[1][1]);
    }

    public static Stream GenerateStreamFromString(string s)
    {
        var stream = new MemoryStream();
        var writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return stream;
    }
}

public class SpriteSlicer
{
    [MenuItem("Assets/Slice Spritesheet/Battle", priority = 5)]
    public static void BattleSprites()
    {
        if (Selection.activeObject is Texture2D)
        {
            var textures = Selection.GetFiltered<Texture2D>(SelectionMode.Assets);

            foreach (var texture in textures)
            {
                ProcessTexture(texture, 1, 4, 96, 0.5f);
            }
        }
    }

    [MenuItem("Assets/Slice Spritesheet/Overworld", priority = 5)]
    public static void OverworldSprites()
    {
        if (Selection.activeObject is Texture2D)
        {
            var textures = Selection.GetFiltered<Texture2D>(SelectionMode.Assets);

            foreach (var texture in textures)
            {
                ProcessTexture(texture, 4, 4, 48, 0.333333343267441f);
            }
        }
    }

    static void ProcessTexture(Texture2D texture, int rowCount, int colCount, int spriteSize, float pivot)
    {
        List<SpriteMetaData> monsterSprites = new List<SpriteMetaData>();

        string path = AssetDatabase.GetAssetPath(texture);
        var importer = AssetImporter.GetAtPath(path) as TextureImporter;
        
        importer.spriteImportMode = SpriteImportMode.Multiple;

        Vector2 piv = Vector2.zero;
        piv.x = pivot;

        string tName = Selection.activeObject.name;

        for (int r = 0; r < rowCount; ++r)
        {
            for (int c = 0; c < colCount; ++c)
            {
                SpriteMetaData spr = new SpriteMetaData();
                spr.rect = new Rect(c * spriteSize, r * spriteSize, spriteSize, spriteSize);
                spr.name = tName + "_" + ((r * rowCount) + c);
                spr.alignment = 9;
                spr.pivot = piv;
                monsterSprites.Add(spr);
            }
        }

        importer.spritesheet = monsterSprites.ToArray();

        AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
    }
}

public class PGAssetImporter : AssetPostprocessor
{
    void OnPreprocessTexture()
    {
        /*TextureImporter tImporter = (TextureImporter)assetImporter;
        tImporter.spritePixelsPerUnit = 16;
        tImporter.filterMode = FilterMode.Point;
        tImporter.textureCompression = TextureImporterCompression.Uncompressed;*/
    }
}
