﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MonsterSystem;
using GameDatabase;
using System;

[CustomEditor(typeof(NPC), true)]
public class NPCEditor : Editor
{
    /*
    NPC myNPC;

    bool isSpecial;

    List<string> topList = new List<string>();
    List<string> bottomList = new List<string>();
    List<string> hairList = new List<string>();

    string specialName;

    bool isOpen;
    
    void OnEnable()
    {
        myNPC = (NPC)target;

        for (int i = 10000; i < 13000; i++)
        {
            if (Enum.IsDefined(typeof(Items), i))
            {
                if (((Items)i).ToString().Contains("TOP_"))
                {
                    topList.Add(((Items)i).ToString());
                }

                if (((Items)i).ToString().Contains("PANT_"))
                {
                    bottomList.Add(((Items)i).ToString());
                }

                if (((Items)i).ToString().Contains("HAIR_"))
                {
                    hairList.Add(((Items)i).ToString());
                }
            }
        }
    }

    public override void OnInspectorGUI()
    {
        myNPC.editorOpen = EditorGUILayout.Toggle("Display Editor", myNPC.editorOpen);
        EditorGUILayout.Space();

        if (myNPC.editorOpen)
        {
            isSpecial = EditorGUILayout.Toggle("Special NPC", isSpecial);
            EditorGUILayout.Space();

            if (isSpecial)
            {
                specialName = EditorGUILayout.TextField(specialName);
            }

            EditorGUILayout.Space();

            EditorGUI.BeginDisabledGroup(isSpecial);

            myNPC.hairChoice = EditorGUILayout.Popup("Hair", myNPC.hairChoice, hairList.ToArray());
            myNPC.topChoice = EditorGUILayout.Popup("Top", myNPC.topChoice, topList.ToArray());
            myNPC.bottomChoice = EditorGUILayout.Popup("Bottom", myNPC.bottomChoice, bottomList.ToArray());

            EditorGUI.EndDisabledGroup();
            
            EditorGUILayout.Space();
        }
        EditorGUILayout.Space();

        DrawDefaultInspector();

        EditorUtility.SetDirty(target);
    }*/
}
