﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MonsterSystem;

public class PlayerViewer : EditorWindow
{
    [MenuItem("Project Gubbers/Player Viewer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(PlayerViewer));
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not active");
            return;
        }

        EditorGUILayout.LabelField(string.Format("Player Name: {0}", SaveSystem.playerName));

        EditorGUILayout.Space();

        if (SaveSystem.playerParty.Count == 0)
        {
            return;
        }

        EditorGUILayout.LabelField(string.Format("{0}, Level {1}", SaveSystem.playerParty[0].GetInternalName(), SaveSystem.playerParty[0].level));

        string stats = "";

        for (int i = 0; i < 6; i++)
        {
            stats += string.Format("{0} {1}{2}", ((StatNames)i).ToString().Substring(0, 3), SaveSystem.playerParty[0].stats.statArray[i], i < 5? ", " : "");
        }

        EditorGUILayout.LabelField(stats);

        //EditorGUILayout.LabelField(string.Format("BASE\nATK {0}, DEF {1}, MATK {2}, MDEF {3}, SPE {4}, HP {5}", SaveSystem.playerParty[0].GetData().baseStats.baseArray[0], SaveSystem.playerParty[0].GetData().baseStats.baseArray[1], SaveSystem.playerParty[0].GetData().baseStats.baseArray[2], PlayerVariables.partyMonsters[0].GetData().baseStats.baseArray[3], PlayerVariables.partyMonsters[0].GetData().baseStats.baseArray[4], PlayerVariables.partyMonsters[0].GetData().baseStats.baseArray[5]), GUILayout.ExpandHeight(true));
    }


    public void OnInspectorUpdate()
    {
        // This will only get called 10 times per second.
        Repaint();
    }

}
