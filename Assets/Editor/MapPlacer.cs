﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using SuperTiled2Unity;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;
using Rewired;
using UnityEditor.VersionControl;
using UnityEngine.SceneManagement;
using UnityEditor.SceneManagement;

public class MapPlacer : EditorWindow
{
    public Object mapParent;

    public Object map;

    public int chunkWidth, chunkHeight;

    public Vector2 startPosition;

    [MenuItem("Project Gubbers/Map Placer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(MapPlacer));
    }

    private void OnGUI()
    {
        mapParent = EditorGUILayout.ObjectField("Map Parent", mapParent, typeof(GameObject), true);

        map = EditorGUILayout.ObjectField("Map Prefab (0_0)", map, typeof(GameObject), true);

        chunkWidth = EditorGUILayout.IntField("Chunk Width", chunkWidth);
        chunkHeight = EditorGUILayout.IntField("Chunk Height", chunkHeight);

        startPosition = EditorGUILayout.Vector2Field("Start Position", startPosition);

        if (GUILayout.Button("Place Map", GUILayout.Height(40f)))
        {
            string path = Path.GetDirectoryName(AssetDatabase.GetAssetPath(map));
            string mapName = Regex.Match(Path.GetFileName(AssetDatabase.GetAssetPath(map)), @"[^_0-9]+").Value;

            Debug.Log(path);
            DirectoryInfo dir = new DirectoryInfo(path);
            FileInfo[] info = dir.GetFiles(mapName + "_*.tmx");

            Debug.Log(info.Length);

            if ((mapParent as GameObject).transform.childCount > 0 && EditorUtility.DisplayDialog("Map Parent already has chunk objects.", "Are you sure you want to instantiate new chunks?", "Place chunks"))
            {
                for (int i = 0; i < (mapParent as GameObject).transform.childCount; i ++)
                {
                    Object.DestroyImmediate((mapParent as GameObject).transform.GetChild(i).gameObject);
                }
            }

            foreach (FileInfo i in info)
            {
                Debug.Log(i.Name);

                int x = int.Parse(Regex.Match(i.Name, @"[\d]+").Value);
                int y = int.Parse(Regex.Match(i.Name, @"[\d]+").NextMatch().Value);

                Debug.Log(path + "\\" + i.Name);

                GameObject tempMap = PrefabUtility.InstantiatePrefab(AssetDatabase.LoadAssetAtPath(path + "\\" + i.Name, typeof(GameObject))) as GameObject;

                tempMap.transform.parent = (mapParent as GameObject).transform;
                tempMap.transform.position = new Vector3(startPosition.x + (chunkWidth * x), startPosition.y - (chunkHeight * y));

                EditorSceneManager.MarkSceneDirty(SceneManager.GetActiveScene());
            }
        }
    }
}
