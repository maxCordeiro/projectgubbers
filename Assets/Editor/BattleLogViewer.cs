﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MonsterSystem;
using System.IO;
using System;

public class BattleLogViewer : EditorWindow
{
    public List<string> eLog = new List<string>();
    bool writing;

    [MenuItem("Project Gubbers/Battle Log Viewer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(BattleLogViewer));
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not active");
            return;
        }

        if (!InterfaceSystem.inBattle) return;

        BattleNewHandler bnh = FindObjectOfType<BattleNewHandler>();

        if (bnh.log.Count <= 0) return;

        EditorGUILayout.BeginScrollView(Vector2.zero, true, true);

        for (int i = 0; i < bnh.log.Count; i++)
        {
            EditorGUILayout.LabelField(bnh.log[i]);
        }

        EditorGUILayout.EndScrollView();

        if (GUILayout.Button("Log to txt"))
        {
            StreamWriter txt;
            string path = Application.persistentDataPath + "/BATTLE LOG " + string.Format("[{0}-{1}-{2}] [{3}-{4}-{5}]", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Year, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) + ".txt";
            Debug.Log(path);
            txt = File.CreateText(path);
            

            for (int i = 0; i < bnh.log.Count; i++)
            {
                txt.WriteLine(bnh.log[i]);
            }

            txt.Close();
        }

        eLog = bnh.log;

        //EditorGUILayout.LabelField(string.Format("BASE\nATK {0}, DEF {1}, MATK {2}, MDEF {3}, SPE {4}, HP {5}", SaveSystem.playerParty[0].GetData().baseStats.baseArray[0], SaveSystem.playerParty[0].GetData().baseStats.baseArray[1], SaveSystem.playerParty[0].GetData().baseStats.baseArray[2], PlayerVariables.partyMonsters[0].GetData().baseStats.baseArray[3], PlayerVariables.partyMonsters[0].GetData().baseStats.baseArray[4], PlayerVariables.partyMonsters[0].GetData().baseStats.baseArray[5]), GUILayout.ExpandHeight(true));
    }


    public void OnInspectorUpdate()
    {
        // This will only get called 10 times per second.
        Repaint();
    }

}
