﻿using MonsterSystem;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System.Linq;
using GameDatabase;
using System.Text.RegularExpressions;

public class InventoryViewer : EditorWindow {

    bool addingItem;
    string quantity;

    int pocket;
    Items dropDown;

    [MenuItem("Project Gubbers/Inventory Viewer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(InventoryViewer));
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not active");
            return;
        }


        GUILayout.BeginHorizontal();
        for (int i = 0; i < System.Enum.GetValues(typeof(MenuPockets)).Length; i++)
        {
            if (GUILayout.Button(((MenuPockets)i).ToString(), GUILayout.MinWidth(5)))
            {
                pocket = i;
            }
        }

        GUILayout.EndHorizontal();

        dropDown = (Items)EditorGUILayout.EnumPopup(dropDown);
        if (!ItemSystem.IsClothing(new Item(dropDown)))
        {
            quantity = GUILayout.TextField(quantity, SystemVariables.maxItemStack.ToString().Length);
            quantity = Regex.Replace(quantity, @"[^0-9 ]", "");
        }

        if (GUILayout.Button("Add Item", GUILayout.ExpandWidth(true)))
        {
            if (!ItemSystem.IsClothing(new Item(dropDown)))
            {
                ItemSystem.ObtainItem(dropDown, int.Parse(quantity), displayMessage: false);
            } else
            {
                ItemSystem.ObtainItem(dropDown, 1, displayMessage: false);
            }

            quantity = "";
            dropDown = 0;
        }

        if (SaveSystem.GetInventoryList(pocket).ElementAtOrDefault(0) != null)
        {
            for (int t = 0; t < SaveSystem.GetInventoryList(pocket).Count; t++)
            {
                GUILayout.Label(string.Format("[{0}] {1}", SaveSystem.GetInventoryList(pocket)[t].currentQuantity, SaveSystem.GetInventoryList(pocket)[t].id.ToString()));
            }
        }

    }

    public void OnInspectorUpdate()
    {
        // This will only get called 10 times per second.
        Repaint();
    }
}