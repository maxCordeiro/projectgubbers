﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using GameDatabase;
using MonsterSystem;
using System.Linq;
using NUnit.Framework;

public class GameUtilityEditor : EditorWindow
{
    public int quantity = 1;
    Items _items = Items.AMNESIAELIXIR;
    public bool showMessage;
    public int secondID;
    int goldAmt;

    int category;
    string[] names = { "Charmlings", "Items", "Damage Calculator", "Menus"};

    Monsters AMon = Monsters.GUBBERS, BMon = Monsters.GUBBERS;
    int ALevel, BLevel;
    int power, dmg;
    bool attacker = false; //AMon = false, BMon = true
    string dmgStr;

    Monsters addMon = Monsters.GUBBERS;
    int formId = 0;
    int addLevel, addColor, addStatPoints, addGender;
    Perks addPerk = Perks.CASHCOW;
    Items addItem = Items.AMNESIAELIXIR;
    int[] statOverrides = { 0, 0, 0, 0, 0, 0 };
    Skills[] addSkills = { Skills.ABYSS, Skills.ABYSS, Skills.ABYSS, Skills.ABYSS, Skills.ABYSS};
    Skills specialSkill;
    bool hasItem, overrideStats, randomColor, randomGender, toBag;
    bool defaultMoveset = true;
    bool isBoss;

    string[] menus = {"AttackaponHandler", "DaycareViewer", "EggMonitor", "GatehouseViewer", "CharmBag"};
    int menuIndex = 0;

    Object referrer;

    [MenuItem("Project Gubbers/Game Utility Editor")]
    public static void ShowWindow()
    {
        GetWindow(typeof(GameUtilityEditor));
    }
    
    [MenuItem("Project Gubbers/Reload")]
    public static void Paint()
    {
        EditorWindow view = GetWindow<SceneView>();
        view.Repaint();
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not active");
            return;
        }

        GUILayout.BeginHorizontal();
        for (int i = 0; i < names.Length; i++)
        {
            if (GUILayout.Button(names[i]))
            {
                Repaint();
                
                category = i;
            }
        }
        GUILayout.EndHorizontal();

        switch (category)
        {
            case 1:
                _items = (Items)EditorGUILayout.EnumPopup("Item", _items);
                quantity = EditorGUILayout.IntField("Quantity", quantity);
                showMessage = EditorGUILayout.Toggle("Display Message", showMessage);
                secondID = EditorGUILayout.IntField("Secondary ID", secondID);

                if (GUILayout.Button("Add Item"))
                {
                    ItemSystem.ObtainItem(_items, quantity, secondID, displayMessage: showMessage);
                }

                EditorGUILayout.Space();

                goldAmt = EditorGUILayout.IntField("Gold", goldAmt);

                if (GUILayout.Button("Add Gold"))
                {
                    SaveSystem.playerGold += goldAmt;
                }
                break;
            case 2:
                GUILayout.BeginHorizontal();
                AMon = (Monsters)EditorGUILayout.EnumPopup(AMon);
                BMon = (Monsters)EditorGUILayout.EnumPopup(BMon);
                GUILayout.EndHorizontal();
                
                GUILayout.BeginHorizontal();
                ALevel = EditorGUILayout.IntSlider("A Level", ALevel, 1, 100);
                BLevel = EditorGUILayout.IntSlider("B Level", BLevel, 1, 100);
                GUILayout.EndHorizontal();

                power = EditorGUILayout.IntSlider("Attack Power", power, 10, 150);
                //int dmg = Mathf.RoundToInt(((lvl / 5 * (str / def) * pwr) / 50) * multi);

                if (GUILayout.Button("Swap Attackers: " + (!attacker ? "Left" : "Right")))
                {
                    attacker = !attacker;
                }

                if (GUILayout.Button("Calculate"))
                {
                    dmgStr = "";
                    Monster aMon = new Monster(new MonsterIdentifier(AMon, 0));
                    aMon.SetLevel(ALevel);

                    Monster bMon = new Monster(new MonsterIdentifier(BMon, 0));
                    bMon.SetLevel(BLevel);
                    
                    aMon.CalculateAllStats();
                    bMon.CalculateAllStats();

                    int lvl, str, def, cHP, HP;

                    lvl = !attacker ? ALevel : BLevel;
                    str = (!attacker ? aMon : bMon).GetStatValue(StatNames.STRENGTH);
                    def = (attacker ? aMon : bMon).GetStatValue(StatNames.DEFENSE);

                    dmg = (int)Mathf.Ceil(((((2 * lvl)/5 * (str / def) * power)/ 50) + 2) * 1);

                    (attacker ? aMon : bMon).Hurt(dmg);

                    cHP = (attacker ? aMon : bMon).GetCurrentHP();
                    HP = (attacker ? aMon : bMon).GetHPStat();
                    
                    dmgStr = string.Format(
                        "Attacker: {0}\n" +
                        "DMG: {1}\n" +
                        "Target HP: {2}/{3}\n" +
                        "(((2 * L[{4}]) / 5 * (S[{5}]/D[{6}]) * P[{7}]) / 50) + 2", attacker, dmg, cHP, HP, lvl, str, def, power);
                }
                
                EditorGUILayout.LabelField(dmgStr, GUILayout.ExpandHeight(true));

                break;
            case 0:
                addMon = (Monsters)EditorGUILayout.EnumPopup(addMon);
                formId = EditorGUILayout.IntField("Form Id", formId);

                addLevel = EditorGUILayout.IntSlider("Level", addLevel, 1, 100);

                randomGender = EditorGUILayout.Toggle("Random Gender", randomGender);
                if (!randomGender)
                {
                    addGender = EditorGUILayout.IntSlider("Gender [" + (addGender == 0? "F": "M") + "]", addGender, 0, 1);
                }
                
                addPerk = (Perks)EditorGUILayout.EnumPopup("Perk", addPerk);

                hasItem = EditorGUILayout.Toggle("Has Accessory", hasItem);
                if (hasItem)
                {
                    addItem = (Items)EditorGUILayout.EnumPopup("Item", addItem);
                }

                randomColor = EditorGUILayout.Toggle("Random Color", randomColor);
                if (!randomColor)
                {
                    addColor = EditorGUILayout.IntSlider("Color", addColor, 0, 3);
                }

                addStatPoints = EditorGUILayout.IntSlider("Stat Points", addStatPoints, 0, 100);

                defaultMoveset = EditorGUILayout.Toggle("Default Moveset", defaultMoveset);

                if (!defaultMoveset)
                {
                    EditorGUILayout.LabelField("Skills");
                    for (int i = 0; i < addSkills.Length; i++)
                    {
                        addSkills[i] = (Skills)EditorGUILayout.EnumPopup(addSkills[i]);
                    }

                    specialSkill = (Skills)EditorGUILayout.EnumPopup(specialSkill);
                }

                overrideStats = EditorGUILayout.Toggle("Override Stat Values", overrideStats);

                if (overrideStats)
                {
                    EditorGUILayout.LabelField("Stat Values");
                    for (int i = 0; i < statOverrides.Length; i++)
                    {
                        statOverrides[i] = EditorGUILayout.IntField(((StatNames)i).ToString(), statOverrides[i]);
                    }
                }

                toBag = EditorGUILayout.Toggle("To Bag", toBag);

                isBoss = EditorGUILayout.Toggle("Boss Battle", isBoss);

                if (GUILayout.Button("Add Charmling"))
                {
                    Monster a = new Monster(new MonsterIdentifier(addMon, formId));
                    a.SetLevel(addLevel);
                    if (!randomGender) a.SetGender(addGender);
                    a.SetPerk(addPerk);
                    if (hasItem)
                    {
                        a.accessory = new Item(addItem);
                    } else
                    {
                        a.accessory = null;
                    }
                    if (!randomColor) a.monColor = addColor;
                    a.AddStatPoints(addStatPoints);
                    if (!defaultMoveset)
                    {
                        a.monSkills = new Skill[5] { new Skill(addSkills[0]), new Skill(addSkills[1]), new Skill(addSkills[2]), new Skill(addSkills[3]), new Skill(addSkills[4]) };
                        a.specialSkill = new Skill(specialSkill);
                    }

                    GameUtility.ObtainMonster(a, toBag);
                }

                if (GUILayout.Button("Start Battle") && !InterfaceSystem.IsNotFree())
                {
                    Monster m = new Monster(new MonsterIdentifier(addMon, formId));
                    m.SetLevel(addLevel);
                    if (!randomGender) m.SetGender(addGender);
                    m.SetPerk(addPerk);
                    if (hasItem)
                    {
                        m.accessory = new Item(addItem);
                    } else
                    {
                        m.accessory = null;
                    }
                    if (!randomColor) m.monColor = addColor;
                    m.AddStatPoints(addStatPoints);
                    if (!defaultMoveset)
                    {
                        m.monSkills = new Skill[5] { new Skill(addSkills[0]), new Skill(addSkills[1]), new Skill(addSkills[2]), new Skill(addSkills[3]), new Skill(addSkills[4]) };
                        m.specialSkill = new Skill(specialSkill);
                    }
                    if (overrideStats)
                    {
                        Stats mStats = new Stats();
                        mStats.statArray = new int[6] { statOverrides[0], statOverrides[1], statOverrides[2], statOverrides[3], statOverrides[4], statOverrides[5] };
                        m.stats = mStats;
                    }

                    TransitionHandler tHand = TransitionHandler.instance;
                    tHand.SetTexture("transTest");
                    tHand.SetMidTransAction(() =>
                    {
                        BattleNewHandler bHandle = BattleNewHandler.instance;
                        bHandle.isBoss = isBoss;
                        bHandle.SetOpponent(m);
                        bHandle.BeginBattle();
                        tHand.Fade();
                    });
                    tHand.Transition();
                }

                break;
            case 3:
                referrer = EditorGUILayout.ObjectField(referrer, typeof(UISystem), true);

                menuIndex = EditorGUILayout.Popup(menuIndex, menus);


                if (GUILayout.Button("Open"))
                {
                    switch (menuIndex)
                    {
                        case 0:
                            InterfaceSystem.OpenAttackapon(referrer as UISystem, 1, 10, 5);
                            break;
                        case 1:
                            InterfaceSystem.OpenDaycareMonsterViewer(referrer as UISystem);
                            break;
                        case 2:
                            InterfaceSystem.OpenEggMonitor(referrer as UISystem);
                            break;
                        case 3:
                            InterfaceSystem.OpenMapViewer(referrer as UISystem);
                            break;
                        case 4:
                            InterfaceSystem.OpenCharmBag(referrer as UISystem);
                            break;
                        default:
                            break;
                    }
                }
                break;
            default:
                break;
        }


        
    }
}