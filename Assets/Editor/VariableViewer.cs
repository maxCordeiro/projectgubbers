﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using MonsterSystem;
using GameDatabase;

public class VariableViewer : EditorWindow
{

    Maps maps = Maps.NONE;
    MapTier tier = MapTier.ALL;

    float tScale;
    int fps;

    [MenuItem("Project Gubbers/Variable Viewer")]
    public static void ShowWindow()
    {
        GetWindow(typeof(VariableViewer));
    }

    void OnGUI()
    {
        if (!EditorApplication.isPlaying)
        {
            GUILayout.Label("Editor not active");
            return;
        }

        EditorGUILayout.LabelField(string.Format("" +
            "IsInteracting: {0}\n" +
            "IsPaused: {1}\n" +
            "InMenu: {2}\n" +
            "\n" +
            "Dialogue, isOpen: {3}\n" +
            "Dialogue, isFinishedReading: {4}\n" +
            "Choice, isOpen: {5}\n" + 
            "Choice, choiceSelected: {6}\n" +
            "\n" +
            "Temporary Monster Index: {7}" +
            "\n" +
            "Current Time of Day: {8}\n" +
            "Current Map: {9}\n" +
            "Current Tier: {10}\n",

            InterfaceSystem.isInteracting,
            InterfaceSystem.isPaused,
            InterfaceSystem.inMenu,

            DialogueHandler.instance.isOpen,
            DialogueHandler.instance.isFinishedReading,
            ChoiceHandler.instance.isOpen,
            ChoiceHandler.instance.choiceSelected,

            InterfaceSystem.temporaryMonsterIndex,

            SaveSystem.timeOfDay,

            SaveSystem.currentMap,
            SaveSystem.currentTier
            ), GUILayout.ExpandHeight(true));

        tScale = EditorGUILayout.Slider(tScale, 0, 1);

        if (GUILayout.Button("Set Time Scale"))
        {
            Time.timeScale = tScale;
        }

        fps = EditorGUILayout.IntSlider(fps, 5, 60);

        if (GUILayout.Button("Set FPS"))
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = fps;
        }

        if (GUILayout.Button("Reset FPS"))
        {
            QualitySettings.vSyncCount = 1;
            Application.targetFrameRate = 60;
        }

        maps = (Maps)EditorGUILayout.EnumPopup("Current Map", maps);
        tier = (MapTier)EditorGUILayout.EnumPopup("Current Tier", tier);

        GUILayout.BeginHorizontal();

        if (GUILayout.Button("Set Map"))
        {
            SaveSystem.currentMap = maps;
        }

        if (GUILayout.Button("Set Tier"))
        {
            SaveSystem.currentTier = tier;
        }

        GUILayout.EndHorizontal();

    }

    public void OnInspectorUpdate()
    {
        // This will only get called 10 times per second.
        Repaint();
    }
}