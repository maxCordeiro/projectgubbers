﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;

namespace LS.Editor
{
    public class LSMenu
    {
        [MenuItem("Window/Localization Editor (XML) v2")]
        public static void GenerateMenu()
        {
            var editor = EditorWindow.GetWindow<XMLEditorWindow>(true);
            editor.Show();
        }
    }
}
