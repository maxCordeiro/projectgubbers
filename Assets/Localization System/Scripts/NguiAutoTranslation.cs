﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Auto translate the Game Object's instance and its children (if recursive is enabled).
/// </summary>
public class NguiAutoTranslation : MonoBehaviour {

	public bool recursive = true;
	public bool includeInactive = true;

	void Start () {
		TranslationEngine.Instance.AutoTranslateNgui(gameObject, recursive, includeInactive);
	}

	public void Reload() {
		TranslationEngine.Instance.AutoTranslateNgui(gameObject, recursive, includeInactive);
	}
}
