using UnityEngine;
using System.Collections;

public class ExampleClass2 : MonoBehaviour {

	void Start () {
		Play();
	}
	
	void OnGUI () {

		if(TranslationEngine.Instance.Trans == null)
			return;

		if(GUI.Button(new Rect(10, 10, 80, 30), TranslationEngine.Instance.Trans["change"]))
		{
			// Change the language
			var translator = TranslationEngine.Instance;
			translator.SelectNextLanguage();
			
			// Play audio clip
			Play();
		}
	}
	
	void Play() {
		// Get the AudioTranslation component from "AudioContainer".
		AudioTranslation audio = GameObject.Find ("AudioContainer").GetComponent<AudioTranslation>(); 
		// And play
		audio.Play();
	}
}
