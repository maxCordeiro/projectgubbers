﻿using UnityEngine;
using System.Collections;
using Translation;

public class ExampleClass4 : MonoBehaviour
{

    Translator t;    // Instance of the translation main class
    string[] allLanguages;

    public Vector2 buttonSize = new Vector2(100, 40);
    public GameObject myButtonPrefab;

    void Start()
    {
        t = TranslationEngine.Instance.Trans;
        allLanguages = t.GetCodeAvailableLanguages();

        Vector2 position = new Vector2(0, 50);
        foreach (string langKey in allLanguages)
        {
            //  Creates a new button
            GameObject button = (GameObject)Instantiate(myButtonPrefab, new Vector3(position.x, position.y, 0), Quaternion.identity);

            //  Sets the button's language
            button.GetComponent<ButtonScript>().SetLanguage(langKey);

            // Change the position of the next button
            position = new Vector2(position.x + buttonSize.x, position.y);
            if (position.x + buttonSize.x >= Screen.width)
            {
                position.x = 0;
                position.y += buttonSize.y;
            }
        }
    }

    public void OnGUI()
    {
        GUI.Box(new Rect(0, 0, 200, 40), "Current language: " + t.SelectedLanguage);
    }

}
