﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawnTrigger : TriggerSystem
{
    MonsterSpawn parent;

    private void Start()
    {
        parent = transform.GetComponentInParent<MonsterSpawn>();

        onPlayerTriggerEnterEvent += (Collider2D c) => { StartCoroutine(parent.BattleEnum()); };
    }
}
